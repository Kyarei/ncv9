use v6;

constant @PUNCT = ".;?!«»".comb;

sub MAIN(*@fnames, Bool :$remove-punct) {
  for @fnames -> $fname {
    for slurp($fname).match(/ '◊glfree{◊nc{' (.*?) '}}'/):g {
      my $sentence = ~$_[0];
      if $remove-punct {
        say $sentence.subst(/ @PUNCT /, ""):g;
      } else {
        say $sentence;
      }
    }
  }
}
