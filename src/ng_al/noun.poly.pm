#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Nouns and pronouns}

Nouns in Ŋarâþ Âlþor are not inflected for case.

Most nouns do not obligatorily mark number; however, a few nouns have suppletive plurals, such as ◊ngal{þun} ◊trans{person} and ◊ngal{t·êŋ} ◊trans{people}. For other nouns, there is an optional plural suffix: ◊ngal{-cyân} for people and ◊ngal{-þe} for non-people. Alternatively, nouns can take an optional generic suffix ◊ngal{-reh}, which causes a noun to refer to something in general or as a concept, similarly to Ŋarâþ Crîþ’s generic number.

◊section{Personal pronouns}

Personal pronouns take a nominative–accusative alignment and also have weak and strong possessive forms. The two possessive forms are distinct in the singular number, but not in the dual and the plural.

◊table/x[
  #:options (table-options
    #:caption "Personal pronouns in Ŋarâþ Âlþor."
  )
  #:id "personal-pronouns"
]{
  Number & ◊colspan[4]{Singular} & ◊colspan[3]{Dual} & ◊colspan[3]{Plural}
  Person \ Case & Nom & Acc & Poss (W) & Poss (S) & Nom & Acc & Poss & Nom & Acc & Poss
  ◊HEADER
  1st & ◊ngal{xa} & ◊ngal{xam} & ◊ngal{xo} & ◊ngal{xom} & ◊ngal{ħafa} & ◊ngal{ħafa} & ◊ngal{ħafo} & ◊ngal{tye} & ◊ngal{tyeî} & ◊ngal{tyo}
  2nd & ◊ngal{con} & ◊ngal{cou} & ◊ngal{co} & ◊ngal{com} & ◊ngal{coo} & ◊ngal{coô} & ◊ngal{côo} & ◊ngal{cjeu} & ◊ngal{cjem} & ◊ngal{cjôo}
  3rd animate & ◊ngal{feŋ} & ◊ngal{faŋ} & ◊ngal{fjo} & ◊ngal{fjom} & ◊ngal{feħja} & ◊ngal{feħja} & ◊ngal{fjom} & ◊ngal{fya} & ◊ngal{fyaî} & ◊ngal{fyo}
  3rd inanimate & ◊ngal{fz} & ◊ngal{fzi} & ◊ngal{þo} & ◊ngal{þom} & ◊ngal{fzħja} & ◊ngal{fzħja} & ◊ngal{goo} & ◊ngal{gaa} & ◊ngal{gaai} & ◊ngal{goo}
}

For non-first-person pronouns, it is possible to use an honorific suffix:

◊table/x[
  #:options (table-options
    #:caption "Honorific suffixes in Ŋarâþ Âlþor."
  )
  #:id "honsuffix"
]{
  Level & Suffix
  Plain & ◊ngal{-∅}
  Polite & ◊ngal{-he}
  Royal & ◊ngal{-tyap}
}

This suffix is always applied on the nominative form. As a result, pronouns with honorific suffixes are no longer marked for case.

The plain form is used when speaking to an equal or an inferior, while the polite form is used when speaking to a superior (e.g. parents, teachers, government officials). The royal suffix is no longer in widespread use. Under the influence of Ŋarâþ Crîþ, the honorific suffixes are used less often in Sadun than in Âlþor.

◊section{Reflexive and reciprocal pronouns}

There is a single reflexive pronoun, ◊ngal{szh}. Reduplicating it gives the reciprocal pronoun ◊ngal{&szh}.

The possessive form of ◊ngal{szh} is ◊ngal{szi}. The possessive form of ◊ngal{&szh} is ◊ngal{szhszi}.

◊section[#:id "indef"]{Demonstrative and indefinite determiners and pro-forms}

◊table/x[
  #:options (table-options
    #:caption "Demonstrative and indefinite determiners and pro-forms in Ŋarâþ Âlþor."
  )
  #:id "indef-table"
]{
  & ◊colspan[3]{Demonstative}
  PoS \ Type & this & that & yon & all & some/any/what & particular
  ◊HEADER
  Determiner & ◊ngal{nzh} & ◊ngal{mzh} & ◊ngal{pzh} & ◊ngal{ce} & ◊ngal{ne} & ◊ngal{me}
  Pronoun (inanimate) & ◊ngal{nam} & ◊ngal{mam} & ◊ngal{pam} & ◊ngal{cam} & ◊ngal{njam} & ◊ngal{mem}
  Pronoun (animate) & ◊ngal{nagâ} & ◊ngal{magâ} & ◊ngal{pagâ} & ◊ngal{câre} & ◊ngal{nâre} & ◊ngal{mêre}
  Pronoun (place) & ◊ngal{nooî} & ◊ngal{mooî} & ◊ngal{peeî} & ◊ngal{câaî} & ◊ngal{nâaî} & ◊ngal{mêeî}
}
