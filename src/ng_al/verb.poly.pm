#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Verbs}

Verbs are inflected only for tense. The lemma form is the present; to form the past, the final tone of the form is inverted: ◊ngal{cyaî} ◊trans{bite} to ◊ngal{cyai}, ◊ngal{tjâa} ◊trans{fight} to ◊ngal{tjââ}, and ◊ngal{fjau} ◊trans{yawn} to ◊ngal{fjâu}. This modification only occurs for the past tense exactly, not for the past-in-past and future-in-past tenses.
