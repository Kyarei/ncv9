#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Ŋarâþ Âlþor}

This is the official grammar of Ŋarâþ Âlþor.

◊strong{This language has politeness distinctions in personal pronouns. The Ŋarâþ Crîþ project neither endorses nor condones this language feature and considers grammaticalized politeness distinctions morally repulsive.}

Note to mobile users: The grammar contains a few extra-wide tables that might overflow the right edge of the paper. If you’re bothered by that, then you might want to turn your phone sideways for those.

◊; An ◊link["grammar.pdf"]{experimental PDF version of the grammar} is available. This is made on a best-effort basis, and there’s no guarantee that it won’t look terrible.

◊(embed-pagetree "index.ptree")

◊show-version[]
