#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Syntax}

Ŋarâþ Âlþor has ergative alignment. The basic word order is ◊b{AVS}, where ◊var{S} denotes the subject of an intransitive verb or the object of a transitive verb, and ◊var{A} denotes the subject of a transitive verb:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{vâu cyai þun.}}
  ◊gla{vâu cyai þun.}
  ◊glb{wolf bite\%pst person}
  ◊glfree{The wolf bit the person.}
}

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{fjau t·êŋ.}}
  ◊gla{fjau t·êŋ.}
  ◊glb{yawn person.%pl}
  ◊glfree{The people yawn.}
}

◊var{A} can be omitted even for transitive verbs if it can be inferred from context or it is not relevant:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{pôŋ vâu.}}
  ◊gla{pôŋ vâu.}
  ◊glb{hit\%pst wolf}
  ◊glfree{The wolf was hit.}
}

◊var{S} can be topicalized by putting it in the beginning of a clause, followed by the particle ◊ngal{ħe}:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{won ħe raħya tjâanâ guh.}}
  ◊gla{won ħe raħya tjâanâ guh.}
  ◊glb{castle %top destroy\%pst battle during}
  ◊glfree{The castle was destroyed during the battle.}
}

◊section{Noun phrases}

The basic structure of a noun phrase is ◊b{(determiner) (small modifier) noun (modifiers)}.

A determiner is a ◊xref["noun.html" "indef" "Section"]{demonstrative or indefinite determiner} or a numeral determiner.

A small modifier is a non-reduplicated adjective or a weak possessive pronoun.

A modifier is an adjective, a strong possessive pronoun, a numeral adjective, or a postpositional phrase.

◊section{Postpositional phrases}

A postpositional phrase is a noun phrase followed by a postposition. It can modify a noun phrase by following it or a clause by following the rest of the clause. A postpositional phrase modifying a clause can also be topicalized by moving it to the beginning of the clause, followed by the particle ◊ngal{ħe}:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{tjâanâ guh ħe raħya won.}}
  ◊gla{tjâanâ guh ħe raħya won.}
  ◊glb{battle during %top destroy\%pst castle}
  ◊glfree{During the battle, the castle was destroyed.}
}

◊section{Tense}

The tense marker, if present, can occur at the end of the clause (after postpositional phrases modifying the clause).

◊table/x[
  #:options (table-options
    #:caption "Tense markers in Ŋarâþ Âlþor."
  )
  #:id "tense-table"
]{
  Tense & Marker
  Past of past & ◊ngal{wyêi}
  Past & (marked by verbal inflection)
  Future of past & ◊ngal{ŋê}
  Present & ∅
  Future & ◊ngal{hom}
}

The ‘past of past’ tense and ‘future of past’ tense refer to times before or after events put in the past tense.

◊section{Copular clauses}

◊term{Copular clauses} are clauses that link a subject to a complement. This complement can be another noun phrase or a modifier. In either case, the clause consists of the complement followed by the subject:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{co taaî feŋ.}}
  ◊gla{co taaî feŋ.}
  ◊glb{%pr.2%sg.%poss.%weak father %pr.3%anim.%sg}
  ◊glfree{He is your father.}
}

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{tup xo mjâu.}}
  ◊gla{tup xo mjâu.}
  ◊glb{white %pr.1%sg.%poss.%weak cat}
  ◊glfree{My cat is white.}
}

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{tet rm mjâu.}}
  ◊gla{tet rm mjâu.}
  ◊glb{house inside cat}
  ◊glfree{The cat is inside the house.}
}

The subject can be topicalized as in a regular clause using ◊ngal{ħe} and often is so:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{feŋ ħe co taaî.}}
  ◊gla{feŋ ħe co taaî.}
  ◊glb{%pr.3%anim.%sg %top %pr.2%sg.%poss.%weak father}
  ◊glfree{He is your father.}
}

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{xo mjâu ħe tup.}}
  ◊gla{xo mjâu ħe tup.}
  ◊glb{%pr.1%sg.%poss.%weak cat %top white}
  ◊glfree{My cat is white.}
}

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{mjâu ħe tet rm.}}
  ◊gla{mjâu ħe tet rm.}
  ◊glb{cat %top house inside}
  ◊glfree{The cat is inside the house.}
}

There is a tense marker for the past tense, ◊ngal{doî}, used in copular clauses:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{mjâu ħe tet rm doî.}}
  ◊gla{mjâu ħe tet rm doî.}
  ◊glb{cat %top house inside %pst}
  ◊glfree{The cat was inside the house.}
}

A copular clause with a nominal complement implies identity only. To denote membership, the postposition ◊ngal{sui} ◊trans{among} must be used:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{mam e mat sui.}}
  ◊gla{mam e mat sui.}
  ◊glb{that %top flower among}
  ◊glfree{That is a flower.}
}

◊section{Negation}

A verb phrase is negated using the particle ◊ngal{fn} after the verb:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{ħyogureh nip fn mat vo!}}
  ◊gla{ħyogu-reh nip fn mat vo!}
  ◊glb{owl-%gc eat %neg flower of_course}
  ◊glfree{Owls don’t eat flowers, silly!}
}

Any other constituent is negated using the particle ◊ngal{cn} after the noun:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{vâu cn nîp lavâ.}}
  ◊gla{vâu cn nîp lavâ.}
  ◊glb{wolf %neg.%nom eat\%pst bread}
  ◊glfree{It wasn’t the wolf who ate the bread.}
}

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{raħya won nzh tjâanâ guh cn.}}
  ◊gla{raħya won nzh tjâanâ guh cn.}
  ◊glb{destroy\%pst castle this battle during %neg.%nom}
  ◊glfree{It wasn’t during this battle that the castle was destroyed.}
}

Both ◊ngal{fn} and ◊ngal{cn} can be followed by another noun phrase and ◊ngal{dâ} to provide an affirmative alternative:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{vâu cn dâi dâ nîp lavâ.}}
  ◊gla{vâu cn dâi dâ nîp lavâ.}
  ◊glb{wolf %neg.%nom man but_rather eat\%pst bread}
  ◊glfree{It wasn’t the wolf who ate the bread, but rather the man.}
}

◊section{Questions}

All questions are formed by introducing the particle ◊ngal{ħôô} before the relevant clause. For polar questions, this is all that is needed:

◊gloss/x[#:lang "art-x-ngal"]{
  ◊glfree{◊ngal{ħôô ŋâcreh ħe con nip?}}
  ◊gla{ħôô ŋâc-reh ħe con nip?}
  ◊glb{%int meat-%gc %top %pr.2%sg eat}
  ◊glfree{Do you eat meat?}
}

For content questions, …

For choice questions, …

◊section{Coordination}

Coordinating two consituents involves placing a coordinator between them. The coordinator used is dependent on the type of the coordinands.

◊table/x[
  #:options (table-options
    #:caption "Coordinators in Ŋarâþ Âlþor."
  )
  #:id "coordinators"
]{
  Coordination \ Coordinands & Noun phrase & Modifier & Verb phrase & Clause
  Intersection & ◊ngal{pi} & ◊ngal{pe} & ◊ngal{haa} & ◊ngal{haai}
  Conjunction & ◊ngal{ci} & ◊ngal{cat} & ◊ngal{cat} & ◊ngal{cau}
  Inclusive disjunction & ◊ngal{soi} & ◊ngal{soi} & ◊ngal{soiþi} & ◊ngal{soiþi}
  Exclusive disjunction & ◊ngal{dyâa} & ◊ngal{dyâa} & ◊ngal{dyâħo} & ◊ngal{dyâħo}
}

◊section{Attitudinals}

◊term{Attitudinals} are clause-final particles (occuring after tense particles) that show the speaker’s emotions toward a statement.
