#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Overview}

◊term{Ŋarâþ Âlþor} (endonym TBA) is a language spoken in the country of Âlþor, as well as in parts of Sadun.

In terms of phonology, it has a two-level tone distinction that becomes complex as long vowels and falling diphthongs can introduce multiple tone-bearing vowels within a syllable. Additionally, two vowels are not capable of bearing tone, as are ‘checked vowels’. Aside from its tone system, the language has a three-way VOT contrast in its plosives.

Ŋarâþ Âlþor is an analytic language, with few inflections on both nouns and verbs. It has a dedicated class of adjectives and a method of negation. The language also has productive reduplication on verbs and adjectives.
