let data = window.localStorage;

function toggleSidebar() {
  let cl = document.getElementById('sidebar').classList;
  cl.toggle('collapsed');
  let collapsed = cl.contains('collapsed');
  data.setItem('sidebar-collapsed', collapsed ? 'yes' : 'no');
}

function isCollapsed() {
  let value = data.getItem('sidebar-collapsed');
  if (value === undefined) {
    let collapsed = window.screen.width < 1024;
    data.setItem('sidebar-collapsed', collapsed ? 'yes' : 'no');
    return collapsed;
  }
  return value == 'yes';
}

function init() {
  if (isCollapsed()) {
    document.getElementById('sidebar').classList.add('collapsed');
  }
}
