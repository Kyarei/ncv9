#lang pollen

◊define-meta[title]{Into paradigms}

We define a ◊term{paradigm} ◊${\Pi} over ◊${(\Sigma, L)} as a triple ◊${(C, S, \psi)} where:

◊items{
    ◊item{◊${C} is a set of categories that ◊${\Pi} inflects for,}
    ◊item{◊${S = (S_0, S_1, \ldots, S_{p - 1})} is a sequence of subsets of ◊${\Sigma^*}, such that ◊${S_i} defines the set of values that the ◊${i}th variable part can take, and}
    ◊item{◊${\psi : S_0 \times S_1 \times \cdots \times S_{p - 1} \times C \rightarrow L} determines the form of an inflected word.}
}

In most cases, we constrain ◊${\psi} such that ◊${\psi(s_0, \ldots, s_{p - 1}, c)} is the result of concatenating elements of ◊${\{s_0, \ldots s_{p - 1}\}} in a manner that depends only on the value of ◊${c}. (The particular semantics of concatenation depends on the language being modeled, but we require truncation to be available.)
