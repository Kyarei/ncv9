#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type book]
◊define-meta[book-title]{On invertible inflection theory}
◊define-meta[book-author]{+merlan #flirora}

◊show-version[]

◊(include-pages-from-pagetree "index.ptree")
