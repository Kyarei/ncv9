#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{On invertible inflection theory}

A text on ◊term{invertible inflection theory}.

An ◊link["iit.pdf"]{experimental PDF version of this text} is available. This is made on a best-effort basis, and there’s no guarantee that it won’t look terrible.

◊(embed-pagetree "index.ptree")

◊show-version[]
