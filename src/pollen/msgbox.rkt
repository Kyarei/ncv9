#lang racket

(require "figure.rkt")

(provide (all-defined-out))

(define (msgbox style . contents)
  `(div ((class ,(format "msgbox ~a" style)))
        (div ((class "msgbox-icon"))
             ,(image (format "/images/icons/~a.svg" style) ""))
        (div ((class "msgbox-text"))
             ,@contents)))

(define (msgbox-outdated version)
  (msgbox "msgbox-outdated"
          `(b "This content pertains to version "
              ,version
              " of Ŋarâþ Crîþ.")
          '(br)
          "It is now outdated as the current version differs from this version."))
