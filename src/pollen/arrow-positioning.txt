Summary: haha multivariable calculus go brr

We use the following model for positioning arrows:

The initial lower-y value (yᵢ) of each arrow i is determined.
Then the final lower-y value of arrow i is the initial value plus a non-negative offset (Δyᵢ).
We try to minimize the total penalty P, which has the equation

P =

    Σ     pc([yᵢ + Δyᵢ − yⱼ − Δyⱼ]²)
i, j ∈ C

 +  Σ     pce([yᵢ + Δyᵢ − src_yⱼ]²)
i, j ∈ Cₛ

 +  Σ     pce([yᵢ + Δyᵢ − dest_yⱼ]²)
i, j ∈ Cₑ

 +  Σ     pd([Δyᵢ]²)
    i

where:

* C = the set of pairs of arrows whose x-positions overlap
* Cₛ = the set of pairs of arrows (i, j) where the x-coordinate of the starting point of j is within the x-range of i
* Cₑ = the set of pairs of arrows (i, j) where the x-coordinate of the ending point of j is within the x-range of i
* src_yᵢ = y-coordinate of the starting point of arrow i
* dest_yᵢ = y-coordinate of the ending point of arrow i
* pc, pce, and pd are penalty functions

C, Cₛ, and Cₑ all exclude (i, i) pairs.

We can take the partial derivative of P with respect to Δyₖ:

∂P/∂Δyₖ =

    Σ     2 pc′([yₖ + Δyₖ − yⱼ − Δyⱼ]²) (yₖ + Δyₖ − yⱼ − Δyⱼ)
k, j ∈ C

 -  Σ     2 pc′([yᵢ + Δyᵢ − yₖ − Δyₖ]²) (yᵢ + Δyᵢ − yₖ − Δyₖ)
i, k ∈ C

 +  Σ     2 pce′([yₖ + Δyₖ − src_yⱼ]²) (yₖ + Δyₖ − src_yⱼ)
k, j ∈ Cₛ

 +  Σ     2 pc′([yₖ + Δyₖ − dest_yⱼ]²) (yₖ + Δyₖ − dest_yⱼ)
k, j ∈ Cₑ

+ 2 Δyₖ pd′([Δyₖ]²)

REVISED APPROACH:

quadratically constrained quadratic programming problem

minimize

T = Σ Δyₖ²
    k

subject to

Δyₖ ≥ 0                                          (1)

|yᵢ + Δyᵢ − yⱼ − Δyⱼ| ≥ d for i, j ∈ C           (2)
yᵢ + Δyᵢ − src_yⱼ ≥ d for i, j ∈ Cₛ              (3)
yᵢ + Δyᵢ − dest_yⱼ ≥ d for i, j ∈ Cₑ             (4)

note:

* Δyₖ are the variables we are optimizing for
* everything else is a constant
* we don't need the absolute value on (3) and (4) since if the LHS is < 0, then Δyᵢ < 0 (since yᵢ − src_yⱼ and yᵢ − dest_yⱼ are always positive)

(2) can be turned into

Δyᵢ − Δyⱼ ≥ d − yᵢ + yⱼ     or ← NOTE: not a plain quadratic programming problem
Δyᵢ − Δyⱼ ≤ −d − yᵢ + yⱼ    for i, j ∈ C

or

(yᵢ + Δyᵢ − yⱼ − Δyⱼ)² ≥ d  for i, j ∈ C

(3) can be turned into

Δyᵢ ≥ d - yᵢ + src_yⱼ for i, j ∈ Cₛ

(4) can be turned into

Δyᵢ ≥ d - yᵢ + dest_yⱼ for i, j ∈ Cₑ

Also note that if Q = 2I, then T = Δyᵀ Q Δy and Q is positive definite. In fact, this is a least-squares cost function.
