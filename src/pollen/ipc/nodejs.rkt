#lang racket

(require racket/runtime-path "../log.rkt")
(provide send-message/node stop-process)

(define-values (*process* *output* *input* *err*) (values #f #f #f #f))

(define (running?)
  (and *process*
       (equal? (subprocess-status *process*) 'running)))

(define-runtime-path index.js "../../ncv9-js-helper/index.js")

(define (start-process)
  (log-ncv9-info "Starting ncv9-helper process...")
  (set!-values
   (*process* *output* *input* *err*)
   (parameterize ([current-subprocess-custodian-mode 'kill])
     (subprocess #f #f (current-error-port)
                 (find-executable-path "npm") "exec" "node" index.js)))
  (file-stream-buffer-mode *output* 'none)
  (file-stream-buffer-mode *input* 'none))

(define (stop-process)
  (log-ncv9-info "Stopping ncv9-helper process...")
  ; (printf "*process* = ~a\n" *process*)
  ; (when *process*
  ;   (printf "(subprocess-status *process*) = ~a\n" (subprocess-status *process*)))
  (when (running?)
    (begin0 (subprocess-kill *process* #t)
            (when *output* (close-input-port *output*))
            (when *input* (close-output-port *input*))
            (when *err* (close-input-port *err*))))
  (void))

(define (ensure-process-running)
  (cond
    [(running?) (void)]
    [*process*
     (log-ncv9-warning "Process exited with exit code ~a; restarting"
                       (subprocess-status *process*))
     (stop-process)
     (start-process)]
    [else (start-process)]))

(exit-handler
 (let ([old-exit-handler (exit-handler)])
   (lambda (v)
     (stop-process)
     (old-exit-handler v))))

(define (u32->bytes/le n)
  (unless (and (exact-nonnegative-integer? n) (< n (expt 2 32)))
    (raise-contract-error 'u32->bytes "expected u32; got ~a" n))
  (bytes (bitwise-bit-field n 0 8)
         (bitwise-bit-field n 8 16)
         (bitwise-bit-field n 16 24)
         (bitwise-bit-field n 24 32)))

(define (read-len+buf port)
  (define bytes (read-bytes 4 port))
  (define len (+ (arithmetic-shift (bytes-ref bytes 0) 0)
                 (arithmetic-shift (bytes-ref bytes 1) 8)
                 (arithmetic-shift (bytes-ref bytes 2) 16)
                 (arithmetic-shift (bytes-ref bytes 3) 24)))
  (read-bytes len port))

(define (send-message/node msg)
  (define msg-buf
    (cond [(bytes? msg) msg]
          [(string? msg) (string->bytes/utf-8 msg)]))
  (define msg-len (bytes-length msg-buf))
  (define msg-len+buf (bytes-append (u32->bytes/le msg-len) msg-buf))
  (ensure-process-running)
  (display msg-len+buf *input*)
  (read-len+buf *output*))
