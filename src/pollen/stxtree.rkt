#lang racket

(require pollen/setup pollen-count data/interval-map racket/flonum "common.rkt" "numerical-optimization.rkt" "log.rkt")
; (require "osqp.rkt" ffi/unsafe)

(provide (all-defined-out))

; Racket version

; heuristic for text width, since calculating it precisely would require a font library
(define EMS-PER-CHAR 18/32)
(define PADDING-CHARS 2)
(define FONT-SIZE-PX 18)
(define SPACING-BETWEEN-LEVELS (* 2 FONT-SIZE-PX))
(define ARROW-SEP 15)
(define ARROW-END-SEP 5)
(define BOX-PADDING-INNER 3)
(define BOX-PADDING-OUTER 4)

(struct leaf (contents) #:transparent)
(struct labeled-node (label content) #:transparent)
(struct boxed-node (content) #:transparent)
(struct triangle-node (parent child) #:transparent)

(struct stxtree-text (x y text width height) #:transparent)
(struct stxtree-line (x1 y1 x2 y2) #:transparent)
(struct stxtree-triangle (x1 y1 x2 y2 x3 y3) #:transparent)
(struct stxtree-arrow (x1 y1 x2 y2 lower-y) #:transparent)
(struct stxtree-rectangle (x1 y1 width height) #:transparent)

(struct layout-box (width height content) #:transparent)
(struct stxtree-box (width height cmds) #:transparent)

(struct stxadd-arrow (source dest) #:transparent)

(struct stxtree-additions (arrows))
(define (stxtree-add #:arrows [arrows '()])
  (stxtree-additions arrows))
(define NO-ADDITIONS (stxtree-add))

(define (->string x)
  (cond [(number? x) (number->string x)]
        [(string? x) x]))
(define (trace n)
  (case (current-poly-target)
    [(html) (leaf `(var "t" (sub ,(->string n))))]
    [(pdf) (leaf `($ ,(format "t_{~a}" (->string n))))]))

(define (html-text->svg str)
  (match str
    [(? string? s) s]
    [`(var . ,contents)
     `(tspan ((style "font-style: italic;"))
             . ,(map html-text->svg contents))]
    [`(sub . ,contents)
     `(tspan ((dy "6")
              (style "font-style: normal;")
              (font-size "70%"))
             . ,(map html-text->svg contents))]))

(define (total-width boxes)
  (for/sum ([b boxes]) (layout-box-width b)))
(define (max-height boxes)
  (for/fold ([m 0]) ([b boxes]) (max m (layout-box-height b))))

(define (stxtree-cmd->xexpr cmd)
  (match cmd
    [(stxtree-text x y text width height)
     `(text ((x ,(number->string (round x)))
             (y ,(number->string (round y)))
             (aria-hidden "true")
             (text-anchor "middle")) ,(html-text->svg text))]
    [(stxtree-line x1 y1 x2 y2)
     `(line ((x1 ,(number->string (round x1)))
             (y1 ,(number->string (round y1)))
             (x2 ,(number->string (round x2)))
             (y2 ,(number->string (round y2)))
             (stroke "black")
             (stroke-width "1")))]
    [(stxtree-triangle x1 y1 x2 y2 x3 y3)
     `(polygon ((points ,(format "~a, ~a ~a, ~a ~a, ~a"
                                 (round x1) (round y1)
                                 (round x2) (round y2)
                                 (round x3) (round y3)))
                (fill "none")
                (stroke "black")
                (stroke-width "1")))]
    [(stxtree-arrow x1 y1 x2 y2 lower-y)
     `(polyline ((points ,(format "~a, ~a ~a, ~a ~a, ~a ~a, ~a"
                                  (round x1) (round y1)
                                  (round x1) (round lower-y)
                                  (round x2) (round lower-y)
                                  (round x2) (round y2)))
                 (fill "none")
                 (stroke "black")
                 (stroke-width "1")
                 (marker-end "url(#arrow)")))]
    [(stxtree-rectangle x1 y1 width height)
     `(rect ((x ,(number->string (round x1)))
             (y ,(number->string (round y1)))
             (width ,(number->string (round width)))
             (height ,(number->string (round height)))
             (fill "none")
             (stroke "black")
             (stroke-width "0.5")))]))

; stxtree-cmd -> xmin xmax ymax
(define (stxtree-cmd-block cmd)
  (match cmd
    [(stxtree-text x y text width height)
     (values (- x (/ width 2))
             (+ x (/ width 2))
             y)]
    [(stxtree-line x1 y1 x2 y2)
     (values (min x1 x2)
             (max x1 x2)
             (max y1 y2))]
    [(stxtree-triangle x1 y1 x2 y2 x3 y3)
     (values (min x1 x2 x3)
             (max x1 x2 x3)
             (max y1 y2 y3))]
    [(stxtree-arrow x1 y1 x2 y2 lower-y)
     (values (min x1 x2)
             (max x1 x2)
             lower-y)]
    [(stxtree-rectangle x1 y1 width height)
     (values x1
             (+ x1 width)
             (+ y1 height))]))

(define (stxtree-cmds->interval-map cmds width)
  (define imap (make-interval-map `(((0 . ,(ceiling width)) . 0))))
  (for ([cmd cmds])
    (define-values (xmin xmax ymax) (stxtree-cmd-block cmd))
    (define xmin-round (round xmin))
    (define xmax-round (round xmax))
    (interval-map-update*!
     imap
     xmin-round
     (if (= xmin-round xmax-round) (add1 xmax-round) xmax-round)
     (lambda (e) (max e ymax))))
  imap)

(define (imap-greatest-in-range imap lower upper)
  (define greatest 0)
  (interval-map-update*!
   imap lower upper
   (lambda (val)
     (set! greatest (max greatest val))
     val))
  greatest)

(define (string-length-in-xexpr expr)
  (cond [(string? expr) (string-length expr)]
        [(cons? expr) (apply + (map string-length-in-xexpr (cdr expr)))]))

(define (layout-stxtree tree)
  (match tree
    [(? symbol? x) (layout-stxtree (symbol->string x))]
    [(? string? x) (layout-stxtree (leaf x))]
    [(leaf contents)
     (define width
       (* FONT-SIZE-PX EMS-PER-CHAR
          (+ (string-length-in-xexpr contents) PADDING-CHARS)))
     (define height (* 3/2 FONT-SIZE-PX))
     (layout-box width height tree)]
    [(triangle-node parent child)
     (define parent-box (layout-stxtree parent))
     (define child-box (layout-stxtree child))
     (layout-box (max (layout-box-width parent-box)
                      (layout-box-width child-box))
                 (+ (layout-box-height parent-box)
                    SPACING-BETWEEN-LEVELS
                    (layout-box-height child-box))
                 (triangle-node parent-box child-box))]
    [(labeled-node label content)
     (define content-box (layout-stxtree content))
     (layout-box (layout-box-width content-box)
                 (layout-box-height content-box)
                 (labeled-node label content-box))]
    [(boxed-node content)
     (define content-box (layout-stxtree content))
     (define EXPANSION (* 2 (+ BOX-PADDING-INNER BOX-PADDING-OUTER)))
     (layout-box (+ (layout-box-width content-box) EXPANSION)
                 (+ (layout-box-height content-box) EXPANSION)
                 (boxed-node content-box))]
    [(list* parent children)
     (define parent+child-boxes (map layout-stxtree tree))
     (define parent-box (car parent+child-boxes))
     (define child-boxes (cdr parent+child-boxes))
     (define twidth (total-width child-boxes))
     (define mheight (max-height child-boxes))
     (layout-box (max (layout-box-width parent-box) twidth)
                 (+ (layout-box-height parent-box) SPACING-BETWEEN-LEVELS mheight)
                 parent+child-boxes)
     ]))

(define (realize-layout-box the-box x y labels-out)
  (match-define (layout-box width height tree) the-box)
  (match tree
    [(leaf s)
     (define text
       (stxtree-text (+ x (/ width 2))
                     (+ y (* height 3/4))
                     s
                     width
                     height))
     (stxtree-box width
                  height
                  (list text))
     ]
    [(triangle-node parent-box child-box)
     (define parent-x-offset
       (/ (- width (layout-box-width parent-box)) 2))
     (define realized-parent-box
       (realize-layout-box parent-box (+ x parent-x-offset) y labels-out))
     (define child-x-offset
       (/ (- width (layout-box-width child-box)) 2))
     (define child-y-offset
       (+ (layout-box-height parent-box) SPACING-BETWEEN-LEVELS))
     (define realized-child-box
       (realize-layout-box child-box (+ x child-x-offset) (+ y child-y-offset) labels-out))
     (stxtree-box width height
                  `(,(stxtree-triangle
                      (+ x (/ width 2))
                      (+ y (layout-box-height parent-box))
                      x
                      (+ y child-y-offset)
                      (+ x width)
                      (+ y child-y-offset))
                    ,@(stxtree-box-cmds realized-parent-box)
                    ,@(stxtree-box-cmds realized-child-box)))]
    [(labeled-node label contents)
     (define content-box (realize-layout-box contents x y labels-out))
     (define label-x (+ x (/ (stxtree-box-width content-box) 2)))
     (define label-y (+ y (stxtree-box-height content-box)))
     (hash-set! labels-out label (cons label-x label-y))
     content-box]
    [(boxed-node contents)
     (define x2 (+ x BOX-PADDING-OUTER))
     (define y2 (+ y BOX-PADDING-OUTER))
     (define x3 (+ x BOX-PADDING-OUTER BOX-PADDING-INNER))
     (define y3 (+ y BOX-PADDING-OUTER BOX-PADDING-INNER))
     (define EXPANSION (* 2 (+ BOX-PADDING-INNER BOX-PADDING-OUTER)))
     (match-define (stxtree-box width height cmds) (realize-layout-box contents x3 y3 labels-out))
     (stxtree-box (+ width EXPANSION)
                  (+ height EXPANSION)
                  (cons (stxtree-rectangle x2 y2 width height)
                        cmds))]
    [(list* parent-box child-boxes)
     (define parent-x-offset
       (/ (- width (layout-box-width parent-box)) 2))
     (define child-y-offset
       (+ (layout-box-height parent-box) SPACING-BETWEEN-LEVELS))
     (define realized-parent-box
       (realize-layout-box parent-box (+ x parent-x-offset) y labels-out))
     (define line-x1 (+ x (/ width 2)))
     (define line-y1 (+ y (layout-box-height parent-box)))
     (define-values (_ commands)
       (for/fold ([child-x-offset 0]
                  [commands (stxtree-box-cmds realized-parent-box)])
                 ([child child-boxes])
         (define realized-child
           (realize-layout-box child
                               (+ x child-x-offset)
                               (+ y child-y-offset)
                               labels-out))
         (define line
           (stxtree-line line-x1 line-y1
                         (+ x child-x-offset (/ (layout-box-width child) 2))
                         (+ y child-y-offset)))
         (values (+ child-x-offset (layout-box-width child))
                 (cons line
                       (append (stxtree-box-cmds realized-child)
                               commands)))))
     (stxtree-box width height commands)
     ]))

; thx https://theworld.com/~swmcd/steven/tech/interval.html
(define (intervals-intersect? x1l x1u x2l x2u)
  (and (< x1l x2u) (< x2l x1u)))

(define (cons-when cond x lst)
  (if cond (cons x lst) lst))

(define (group-by-i n f lst)
  (define v (make-vector n '()))
  (for ([e lst])
    (define i (f e))
    (vector-set! v i (cons e (vector-ref v i))))
  v)

; Looking for a better method of optimizing arrow placement.
; The current approach (gradient descent) can only find local minima.

(define (alias-sets arrows)
  (for/fold ([C '()]
             [Cs '()]
             [Ce '()])
            ([i (in-naturals)]
             [arrow-i (in-vector arrows)]
             #:when #t
             [j (in-naturals)]
             [arrow-j (in-vector arrows)]
             #:when (not (= i j)))
    (match-define (stxtree-arrow xi1 yi1 xi2 yi2 yi) arrow-i)
    (match-define (stxtree-arrow xj1 yj1 xj2 yj2 yj) arrow-j)
    (define xil (min xi1 xi2))
    (define xiu (max xi1 xi2))
    (define xjl (min xj1 xj2))
    (define xju (max xj1 xj2))
    (define in-C? (intervals-intersect? xil xiu xjl xju))
    (define in-Cs? (<= xil xj1 xiu))
    (define in-Ce? (<= xil xj2 xiu))
    (define i+j (cons i j))
    (values (cons-when (and (< i j) in-C?) i+j C)
            (cons-when in-Cs? i+j Cs)
            (cons-when in-Ce? i+j Ce))))

; ; Tried to solve it with quadratic programming, but because of some of the
; ; constraints (|yᵢ + Δyᵢ − yⱼ − Δyⱼ| ≥ d), this doesn't quite fit into
; ; the problem description.
; (define (optimize-arrow-placements arrows)
;   (log-ncv9-debug "Optimizing for ~a" arrows)
;   (define D 30)
;   (define n (vector-length arrows))
;   (define (y i)
;     (exact->inexact (stxtree-arrow-lower-y (vector-ref arrows i))))
;   (define (src-y i)
;     (exact->inexact (stxtree-arrow-y1 (vector-ref arrows i))))
;   (define (dest-y i)
;     (exact->inexact (stxtree-arrow-y2 (vector-ref arrows i))))
;   (define-values (C Cs Ce) (alias-sets arrows))
;   (define k (length C))
;   (define m (+ n k))
;   (define lower-bounds (make-flvector m 0.0))
;   (define upper-bounds (make-flvector m +inf.0))
;   (for ([p Cs])
;     (match-define (cons i j) p)
;     (flvector-set! lower-bounds i
;                    (max (flvector-ref lower-bounds i)
;                         (+ D (- (y i)) (src-y j)))))
;   (for ([p Ce])
;     (match-define (cons i j) p)
;     (flvector-set! lower-bounds i
;                    (max (flvector-ref lower-bounds i)
;                         (+ D (- (y i)) (dest-y j)))))
;   (for ([p C]
;         [q (in-naturals)])
;     (match-define (cons i j) p)
;     (flvector-set! lower-bounds (+ n q)
;                    (+ D (- (y i)) (y j)))
;     (flvector-set! upper-bounds (+ n q)
;                    (+ D (- (y j)) (y i))))
;   (log-ncv9-debug "lower-bounds = ~a; upper-bounds = ~a" lower-bounds upper-bounds)
;   (define P (csc-eye n))
;   (define q (make-zero-block _c_float n))
;   (define l (flvector->cpointer lower-bounds))
;   (define u (flvector->cpointer upper-bounds))
;   ; For A, we need to populate the first n rows with the n×n identity
;   ; matrix.
;   ; The last k rows are filled with two values each.
;   ; Thus, for each column number, we have to find the indices of the pairs in C in which they occur.
;   (define A
;     (let* ([total-set (+ n (* 2 k))]
;            [cp (malloc (add1 n) _c_int 'atomic)]
;            [ci (malloc total-set _c_int 'atomic)]
;            [cx (malloc total-set _c_float 'atomic)]
;            [w
;             (make-vector (add1 n) 1)])
;       (vector-set! w n 0)
;       (for ([pr C])
;         (match-define (cons ii jj) pr)
;         (vector-set! w ii (add1 (vector-ref w ii)))
;         (vector-set! w ii (add1 (vector-ref w jj))))
;       (for/fold ([partial-sum 0])
;                 ([i (in-naturals)]
;                  [e w])
;         (vector-set! w i partial-sum)
;         (+ partial-sum e))
;       (ncv9-log-debug "~a" w)
;       (for ([j (in-range n)])
;         (ptr-set! cp _c_int j (vector-ref w j)))
;       (ptr-set! cp _c_int n total-set)
;       (for ([j (in-range n)])
;         (define p (vector-ref w j))
;         (vector-set! w j (add1 p))
;         (ptr-set! ci _c_int p j)
;         (ptr-set! cx _c_float j 1.0))
;       (for ([pr C]
;             [q (in-naturals)])
;         (match-define (cons ii jj) pr)
;         (define j (+ n q))
;         (define p1 (vector-ref w ii))
;         (vector-set! w ii (add1 p1))
;         (ptr-set! ci _c_int p1 j)
;         (ptr-set! cx _c_float j 1.0)
;         (define p2 (vector-ref w jj))
;         (vector-set! w jj (add1 p2))
;         (ptr-set! ci _c_int p2 (add1 j))
;         (ptr-set! cx _c_float (add1 j) -1.0))
;       (make-csc total-set m n cp ci cx -1)))
;   (define osqp-data (make-OSQPData n m P A q l u))
;   (define osqp-settings (osqp_set_default_settings))
;   (define setup (osqp_setup osqp-data osqp-settings))
;   (osqp_solve setup)
;   (get-solution setup))

(define (arrow-placement-penalty+gradient arrows)
  (define (sqr x) (fl* x x))
  (define n (vector-length arrows))
  (define (y i)
    (exact->inexact (stxtree-arrow-lower-y (vector-ref arrows i))))
  (define (src-y i)
    (exact->inexact (stxtree-arrow-y1 (vector-ref arrows i))))
  (define (dest-y i)
    (exact->inexact (stxtree-arrow-y2 (vector-ref arrows i))))
  (define PC-BASE 5000.0)
  (define PC-DECAY -1/35)
  (define (pc dist2)
    (* PC-BASE (exp (* PC-DECAY dist2))))
  (define (pc-deriv dist2)
    (* PC-DECAY (pc dist2)))
  (define PCE-BASE 5000.0)
  (define PCE-DECAY -1/35)
  (define (pce dist2)
    (* PCE-BASE (exp (* PCE-DECAY dist2))))
  (define (pce-deriv dist2)
    (* PCE-DECAY (pce dist2)))
  (define (pd dist2) (* 1.2 dist2))
  (define (pd-deriv dist2) 1.2)
  (define Q 1000.0)
  (define-values (C Cs Ce) (alias-sets arrows))
  (log-ncv9-debug "C = ~s" C)
  (log-ncv9-debug "Cs = ~s" Cs)
  (log-ncv9-debug "Ce = ~s" Ce)
  (define (penalty delta-ys)
    (define (delta-y i) (flvector-ref delta-ys i))
    (define term-C
      (for/sum ([pair C])
        (match-define (cons i j) pair)
        (pc (sqr (fl+ (y i)
                      (fl- (y j))
                      (delta-y i)
                      (fl- (delta-y j)))))))
    (define term-Cs
      (for/sum ([pair Cs])
        (match-define (cons i j) pair)
        (pce (sqr (fl+ (y i)
                       (delta-y i)
                       (fl- (src-y j)))))))
    (define term-Ce
      (for/sum ([pair Ce])
        (match-define (cons i j) pair)
        (pce (sqr (fl+ (y i)
                       (delta-y i)
                       (fl- (dest-y j)))))))
    (define term-D
      (for/sum ([delta-yi delta-ys]) (pd (sqr delta-yi))))
    ; Right now, we add an additional "Q" term to encourage the results
    ; to be positive. However, I don't think this is the best way to
    ; ensure we don't get negative offsets.
    (define term-Q
      (for/sum ([delta-yi delta-ys]) (max 0.0 (- (* Q delta-yi)))))
    (exact->inexact (+ term-C term-Cs term-Ce term-D term-Q))
    )
  (define VCl (group-by-i n car C))
  (define VCr (group-by-i n cdr C))
  (define VCs (group-by-i n car Cs))
  (define VCe (group-by-i n car Ce))
  (define (penalty-gradient delta-ys)
    (define (delta-y i) (flvector-ref delta-ys i))
    (for/flvector ([k (in-naturals)]
                   [delta-yk (in-flvector delta-ys)]
                   [Cl (in-vector VCl)]
                   [Cr (in-vector VCr)]
                   [Cs (in-vector VCs)]
                   [Ce (in-vector VCe)])
      (define term-Cl
        (for/sum ([pair Cl])
          (match-define (cons _ j) pair)
          (define gap (fl+ (y k)
                           (fl- (y j))
                           delta-yk
                           (fl- (delta-y j))))
          (fl* 2.0 gap (pc-deriv (sqr gap)))))
      (define term-Cr
        (for/sum ([pair Cr])
          (match-define (cons i _) pair)
          (define gap (fl+ (y i)
                           (fl- (y k))
                           (delta-y i)
                           (fl- delta-yk)))
          (fl* -2.0 gap (pc-deriv (sqr gap)))))
      (define term-Cs
        (for/sum ([pair Cs])
          (match-define (cons _ j) pair)
          (define gap (fl+ (y k)
                           delta-yk
                           (fl- (src-y j))))
          (fl* 2.0 gap (pce-deriv (sqr gap)))))
      (define term-Ce
        (for/sum ([pair Ce])
          (match-define (cons _ j) pair)
          (define gap (fl+ (y k)
                           delta-yk
                           (fl- (dest-y j))))
          (fl* 2.0 gap (pce-deriv (sqr gap)))))
      (define term-D (fl* 2.0 delta-yk (pd-deriv (sqr delta-yk))))
      (define term-Q (if (< delta-yk 0) (fl- Q) 0.0))
      ; (log-ncv9-debug "k = ~a" k)
      ; (log-ncv9-debug "collisions: cl ~a cr ~a cs ~a ce ~a" Cl Cr Cs Ce)
      ; (log-ncv9-debug "terms: ~a ~a ~a ~a ~a" term-Cl term-Cr term-Cs term-Ce term-D)
      (exact->inexact (+ term-Cl term-Cr term-Cs term-Ce term-D term-Q))
      )
    )
  (values penalty penalty-gradient))

(define (accept-arrows stxtree arrows labels imap)
  (match-define (stxtree-box old-width old-height old-cmds) stxtree)
  (define arrow-commands
    (for/vector ([arrow arrows])
      (match-define (stxadd-arrow source dest) arrow)
      (match-define (cons source-x source-y) (hash-ref labels source))
      (match-define (cons dest-x dest-y) (hash-ref labels dest))
      (define lower-y
        (+ ARROW-SEP
           (max source-y
                dest-y
                ; Make sure the arrow doesn't collide with other elements
                (imap-greatest-in-range imap
                                        (round (min source-x dest-x))
                                        (round (max source-x dest-x))))))
      (stxtree-arrow source-x (+ source-y ARROW-END-SEP)
                     dest-x (+ dest-y ARROW-END-SEP)
                     lower-y)
      ))
  (define-values (penalty penalty-gradient)
    (arrow-placement-penalty+gradient arrow-commands))
  ; Avoid having every arrow at the same offset, but make the initial
  ; values deterministic
  (define initial-offsets
    (for/flvector ([i (in-range (vector-length arrow-commands))])
      (+ 150.0 i)))
  (define opt-offsets (optimize penalty
                                penalty-gradient
                                initial-offsets
                                -0.01
                                1e-4))
  ; (define opt-offsets (optimize-arrow-placements arrow-commands))
  (log-ncv9-debug "opt-offsets = ~a" opt-offsets)
  (define adjusted-arrows
    (for/list ([arrow arrow-commands]
               [offset opt-offsets])
      (struct-copy stxtree-arrow arrow
                   [lower-y (+ (stxtree-arrow-lower-y arrow) offset)])))
  ; Adjust the box's height to fit the arrows
  (define new-height
    (for/fold ([max-y old-height])
              ([arrow (in-list adjusted-arrows)])
      (max max-y (+ (stxtree-arrow-lower-y arrow) ARROW-END-SEP))))
  (stxtree-box old-width
               new-height
               (append adjusted-arrows old-cmds)))

(define (accept-additions stxtree additions labels)
  (let* ([imap (stxtree-cmds->interval-map (stxtree-box-cmds stxtree)
                                           (stxtree-box-width stxtree))]
         [stxtree (accept-arrows
                   stxtree
                   (stxtree-additions-arrows additions)
                   labels
                   imap)])
    stxtree))

(define (stxtree->sentence tree)
  (match tree
    [(? symbol? x) (stxtree->sentence (symbol->string x))]
    [(? string? x) (list x)]
    [(leaf contents) (list contents)]
    [(triangle-node parent child)
     (stxtree->sentence child)]
    [(labeled-node label content) (stxtree->sentence content)]
    [(boxed-node content) (stxtree->sentence content)]
    [(list* _ children) (append* (intersperse
                                  (map stxtree->sentence children)
                                  '(" ")))]))
(define (describe-stxtree tree)
  (match tree
    [(? symbol? x) (describe-stxtree (symbol->string x))]
    [(? string? x) (list x)]
    [(leaf contents) (list contents)]
    [(labeled-node label content) (describe-stxtree content)]
    [(boxed-node content) (describe-stxtree content)]
    [(triangle-node parent child)
     `("[" ,@(describe-stxtree parent) " " ,@(describe-stxtree child) "]")]
    [(? list? x)
     `("["
       ,@(append* (intersperse (map describe-stxtree x) '(" ")))
       "]")]))

(define (syntax-tree/html tree #:caption caption #:id id #:additions [additions '()])
  (define labels (make-hash))
  (define box-wo-additions
    (realize-layout-box (layout-stxtree tree) 0 0 labels))
  (match-define (stxtree-box width height commands)
    (accept-additions box-wo-additions additions labels))
  `(figure ((class "syntax-tree") (id ,id))
           (div ((class "syntax-tree"))
                (svg ((version "1.1")
                      (baseProfile "full")
                      (width ,(number->string (round width)))
                      (height ,(number->string (round height)))
                      (style "font-size: 18px;")
                      (xmlns "http://www.w3.org/2000/svg"))
                     (title "The syntax tree for " ,@(stxtree->sentence tree))
                     (desc ,@(describe-stxtree tree))
                     (defs
                       (marker ((id "arrow")
                                (viewbox "0 0 10 10")
                                (refX "5")
                                (refY "5")
                                (markerWidth "6")
                                (markerHeight "6")
                                (orient "auto-start-reverse"))
                               (path ((d "M 0 0 L 10 5 L 0 10 z")))))
                     ,@(map stxtree-cmd->xexpr commands)
                     ))
           (figcaption ,caption)
           ))

(define (stxtree->latex tree [postfix '()])
  (define (rec tree extra-postfix)
    (stxtree->latex+brackets tree `(,@postfix ,extra-postfix)))
  (match tree
    [(? symbol? x) (stxtree->latex (symbol->string x))]
    [(? string? x) `(@ (group ,x) ,@postfix)]
    [(leaf contents) `(@ (group ,contents) ,@postfix)]
    [(triangle-node parent child)
     `(@ ,(stxtree->latex parent)
         " "
         ,(rec child ",roof"))]
    [(labeled-node label content)
     `(@ ,(stxtree->latex
           content
           `(,@postfix ",name=" ,(symbol->string label))))]
    [(boxed-node content)
     `(@ ,(stxtree->latex
           content
           `(,@postfix ",tikz=" (group (cmd/bare node) "[draw] " (group) ";"))))]
    [(? list? x)
     `(@ ,(stxtree->latex (car x))
         ,@(append*
            (map (lambda (c) `(,(stxtree->latex+brackets c) " "))
                 (cdr x))))]))
(define (stxtree->latex+brackets tree [postfix ""])
  `(@ "[" ,(stxtree->latex tree postfix) "]"))

(define (stxtree-addition-latex/arrow arrow)
  (match arrow
    [(stxadd-arrow source dest)
     `(@ (cmd/bare draw) "[->]"
         "(" ,(symbol->string source) ")
         to[in=south,out=south] (",(symbol->string dest) ");")]))

(define (syntax-tree/pdf tree #:caption caption #:id id #:additions [additions '()])
  `(env ((cmd "figure"))
        (cmd/bare centering)
        (env ((cmd "forest"))
             ,(stxtree->latex+brackets tree)
             ,@(map stxtree-addition-latex/arrow
                    (stxtree-additions-arrows additions)))
        (cmd caption ,caption)
        (label ,(global-id id))))

(define-countable-tag
  (syntax-tree* tree #:caption caption #:id [id #f] #:additions [additions NO-ADDITIONS])
  (0 number->string #f ".")
  (count)
  (let ([id1 (or id (string-append "figure-" count))])
    (case (current-poly-target)
      [(html) (syntax-tree/html
               tree
               #:caption `(@ "Figure " ,count ": " ,caption)
               #:id id1
               #:additions additions)]
      [(pdf) (syntax-tree/pdf
              tree
              #:caption caption
              #:id id1
              #:additions additions)])))

(define (syntax-tree tree #:caption caption #:id [id #f] #:additions [additions NO-ADDITIONS])
  (syntax-tree* tree #:caption caption #:id id #:label id #:additions additions))
