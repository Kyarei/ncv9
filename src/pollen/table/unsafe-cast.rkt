#lang typed/racket/base

(require typed/racket/unsafe)

(provide unsafe-cast)

(unsafe-require/typed racket/base
                      [[values unsafe-values] (All (X) (-> Any X))])

(define-syntax unsafe-cast
  (syntax-rules ()
    [(_ val type)
     ((inst unsafe-values type) val)]))
