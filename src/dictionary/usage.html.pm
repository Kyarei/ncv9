#lang pollen

◊; ◊(require "../pollen/lexicon.rkt" "ncv9.kaltoklel" racket/serialize)

◊define-meta[title]{Usage notes for the Ŋarâþ Crîþ v9 dictionary}

◊section{The anatomy of an entry}

◊make-figure["usage.svg" #:alt "The anatomy of a dictionary entry in Ŋarâþ Crîþ v9."]{The anatomy of a dictionary in Ŋarâþ Crîþ v9.}

An entry consists of the following:

◊items{
  ◊item{The ◊b{headword}, used to identify the entry. This usually starts with a lemma form, followed by forms showing other principal parts of the entry. Headwords for irregular lexical items end with ellipses.}
  ◊item{The ◊b{raw f9t text} for the entry. This is the information about how the word is inflected as it appears in the dictionary source. This text is passed into f9i.}
  ◊item{The ◊b{part of speech}. This encompasses the lexical category of the word as well as morphosyntactic properties.}
  ◊item{The ◊b{etymology} of the entry, occurring right after the part of speech. This shows how the entry was derived, either from other Ŋarâþ Crîþ v9 words or from words in earlier versions of Ŋarâþ Crîþ (such as Ŋarâþ Crîþ v7 or Necarasso Cryssesa).}
  ◊item{The ◊b{definitions}. Each definition can have either a detailed description of what the word means, one or more translations of the word into the metalanguage, or both. Each definition can also have one or more examples of how the word is used with that definition.}
  ◊item{Optionally, any ◊b{notes} about the word’s usage.}
  ◊item{Optionally, any ◊b{relationships} that an entry has to other entries.}
  ◊item{If the word can be inflected, then the ◊b{inflection tables} containing its forms are listed at this point. These tables might not include every possible form of a word.}
}

◊section{Parts of speech}

◊subsection{Nouns}

The part-of-speech tag for nouns has the format ◊i{n<paradigm><gender>.<clareþ>}. The clareþ is implied to be singular by default:

◊items{
  ◊item{◊i{n0c} describes a celestial singular paradigm-0 noun.}
  ◊item{◊i{n11t.c} describes a terrestrial collective paradigm-11 noun.}
  ◊item{◊i{n3h} describes a human singular paradigm-3 noun.}
  ◊item{◊i{n!c.m} describes a celestial mass noun with irregular declensions.}
}

For compound nouns where both components are inflected, the part-of-speech tag looks like ◊i{n<gender> (n<paradigm 1>, n<paradigm 2>)}. For instance, ◊i{nh (n5, n0)} describes a compound human singular noun, in which the first word is in paradigm 5 and the second word is in paradigm 0.

Some definitions might contain placeholders such as ◊|gen#|, which refers to a genitive-case noun phrase modifying the noun.

◊subsection{Verbs}

The part-of-speech tag for verbs has the format ◊i{v<genus><species><valency>}:

◊items{
  ◊item{◊i{v2ci} describes an intransitive verb of species 2c.}
  ◊item{◊i{v3ct} describes a transitive verb of species 3c.}
  ◊item{◊i{v1es} describes a semitransitive verb of species 1e.}
  ◊item{◊i{v0nd} describes a ditransitive verb of species 0n.}
  ◊item{◊i{v0ea} describes an auxiliary verb of species 0e.}
}

An exclamation mark at the end of the part-of-speech tag indicates an irregular verb.

Vowel affections are enclosed in parentheses: ◊i{v0nd(n:o)} indicates that the final vowel of the stem becomes ◊l0{o} in present-tense forms, and ◊i{v0cd(p:e)} indicates that it becomes ◊l0{e} in past-tense forms.

Most verbs will have only one form in their headwords. If a verb has three forms in its headword, then it has impersonator stems. In this case, the second form is the nominative / nominative celestial singular participle, and the third form is the accusative / nominative celestial singular participle.

The following placeholders are used in verb definitions:

◊items{
  ◊item{◊|S|: the nominative argument}
  ◊item{◊|O|: the accusative argument}
  ◊item{◊|I|: the dative argument}
  ◊item{◊|T|: the target of an auxiliary verb}
  ◊item{◊|Š|: the shifted subject of an auxiliary verb}
}

A few definitions mark other arguments such as ◊|loc#|.

