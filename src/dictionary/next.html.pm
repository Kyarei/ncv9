#lang pollen

◊(require "../pollen/lexicon.rkt" "ncv9.kaltoklel" racket/serialize)

◊define-meta[title]{The Ŋarâþ Crîþ v9 dictionary (next version)}

◊noscript{JavaScript is required to use this dictionary.}

◊script[#:src "/js/sql/sql-wasm.js"]◊;
◊script[#:src "/js/dict-next.js"]◊;

◊form[#:id "dict-next-entry" #:action "javascript:search()"]{
  ◊div{
    ◊label[#:for "search-term"]{Search: }◊;
    ◊input[#:type "text" #:name "search-term" #:id "search-term"]
  }
  ◊div{
    ◊input[#:type "radio" #:name "search-method" #:value "sm-forward" #:id "sm-forward" #:checked "checked"]◊;
    ◊label[#:for "sm-forward"]{Search forms}
    ◊input[#:type "radio" #:name "search-method" #:value "sm-forward-hw" #:id "sm-forward-hw"]◊;
    ◊label[#:for "sm-forward-hw"]{Search headwords}
    ◊input[#:type "radio" #:name "search-method" #:value "sm-backward" #:id "sm-backward"]◊;
    ◊label[#:for "sm-backward"]{Search translations}
    ◊input[#:type "radio" #:name "search-method" #:value "sm-backward-full" #:id "sm-backward-full"]◊;
    ◊label[#:for "sm-backward-full"]{Search the entire entry}
  }
  ◊div{
    ◊input[#:type "checkbox" #:name "search-fuzzy" #:id "search-fuzzy"]◊;
    ◊label[#:for "search-fuzzy"]{Enable fuzzy searching (NYI)}
  }
  ◊div{
    ◊button[#:type "submit"]{Search}
  }
}

◊div[#:id "search-results"]{
  Dolphins
}

◊div[#:id "entry-count"]{}
