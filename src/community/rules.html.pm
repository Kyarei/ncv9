#lang pollen

◊define-meta[title]{Ŋarâþ Crîþ community rules}

◊b{These rules are a draft and not yet effective. We welcome your feedback in one of these communities.}

Current version: ◊b{1.0} (??? –) ◊; ◊render-date{XXXX-XX-XX – ?}

◊section[#:id "scope"]{Scope of these rules}

These rules apply to official Ŋarâþ Crîþ communities, which include the following:

◊items{
    ◊item{The ◊link["https://discord.gg/jkkaMNCeyu"]{Ŋarâþ Crîþ Discord server}}
    ◊item{The ◊link["https://www.reddit.com/r/NecarassoCryssesa/"]{/r/NecarassoCryssesa} subreddit}
    ◊item{Any issue or merge request trackers for repositories under the ◊link["https://gitlab.com/ncv9"]{ncv9 GitLab organization}}
}

These rules are enforced not only on posts or messages but also on usernames, statuses, flairs, and other similar text that other people might see when viewing the community.

If you are currently excluded in one or more official communities, then we reserve the right to take actions against you in any other official communities.

We will not ban you solely on the basis of your behavior outside official communities, but we may consult such behavior for context about your actions within official communities.

◊section[#:id "inclusion"]{Our commitment to inclusion}

We strive to welcome people of all beliefs and characteristics when possible, and we expect all members of the Ŋarâþ Crîþ v9 community to do the same. This means that you should refrain from actions such as the following:

◊enum{
    ◊item{Insulting or otherwise personally attacking another member of the community}
    ◊item{Revealing personal information or images about an individual without their consent or encouraging others to do so}
    ◊item{Encouraging violence against, threatening, harassing, or dehumanizing an individual person or group of people}
    ◊item{Encouraging someone or telling someone how to harm or kill themselves}
}

◊section[#:id "discussion-controversial"]{Discussion of controversial topics}

We recognize that artistic expression is sometimes connected with religion or politics and that restricting discussion of these controversial issues may sometimes hamper the development of Ŋarâþ Crîþ. ◊strong{Nevertheless, discussion of these topics should be kept at a minimum.} In particular, avoid bringing them up unless they are relevant to the discussion at hand, and avoid being more inflammatory than necessary.

Posting about religion or politics in a worldbuilding context is allowed, as long as they are treated separately from real-world religion and politics.

◊section[#:id "advertising"]{Advertising}

Advertising your content is allowed only to the extent that it is relevant to the discussion at hand. In addition, directly posting donation links is not allowed.

◊b{Advertising other communities is not allowed in general.} However, the Ŋarâþ Crîþ Discord server has a server partnership program.

◊section[#:id "prohibited-conduct"]{Other prohibited conduct}

Please refrain from doing the following:

◊enum{
    ◊item{Violating the terms of service of the platform on which the community in question is hosted}
    ◊item{Engaging in or encouraging sexual exploitation of children}
    ◊item{Posting or linking to explicit or strongly disturbing content, including but not limited to pornography or gore}
    ◊item{Engaging in or encouraging infringement of intellectual property rights}
    ◊item{Engaging in spam:◊;
    ◊enum{
        ◊item{Posting excessive messages within the community}
        ◊item{Excessively mentioning other users in a way that gives them notifications}
        ◊item{Sending unsolicited private messages to other members}
        ◊item{Posting messages that are off-topic for the channels they are posted in}
    }}
    ◊item{Speaking in a language that is not appropriate for the situation}
}

Remember that jokingly pretending to engage in one of the above behaviors might be confused with a genuine attempt to do so.

The Ŋarâþ Crîþ Discord server formerly forbade any mentions of ◊cite{Minecraft}. Mentioning this game is no longer disallowed, but see ◊xref["rules.html" "discussion-controversial" "Section" #:type 'inline]{Discussion of controversial topics} for the addition of the chat reporting feature.

◊section[#:id "punishments"]{Punishments for breaking these rules}

If you break one or more of these rules, then we may take one or more of the following actions against you, depending on the frequency and severity of the violation and any prior history of violations.

◊enum{
    ◊item{◊b{Giving you a warning.}}
    ◊item{◊b{Restricting your access to certain parts of the community,} if the violation was materially related to those parts.}
    ◊item{◊b{Revoking any special privileges} such as moderator status.}
    ◊item{◊b{Temporarily or indefinitely excluding you from participating in the community.} Depending on the platform in question, this might also prevent you from reading messages posted in the community.}
}

If you have been punished, we will notify you with the type of punishment you received, the violation or violations that warranted the punishments, and the duration of the punishment if applicable.

If you believe that your punishment has been unjustified or inappropriate, then you may appeal using one of the following methods:

◊items{
    ◊item{Messaging ◊kbd{flirora#8409} on Discord.}
    ◊item{Using ◊link["https://www.reddit.com/message/compose/?to=/r/NecarassoCryssesa"]{the /r/NecarassoCryssesa modmail.}}
    ◊item{Opening an issue on ◊link["https://gitlab.com/ncv9/ncv9/-/issues"]{this website’s issue tracker.}}
}

◊section[#:id "changes"]{Changes to the rules}

◊strong{This policy will come into effect when these rules are formally adopted.}

These rules may be revised or changed. When we make material changes to the rules (that is, excluding spelling or minor grammar fixes), we will notify you of these changes by or before the time they become effective. We will also keep old versions of the rules if they are materially different from the current version.

As a stopgap measure, we might introduce temporary rules in certain communities. These will be posted in a prominent place and will be effective for at most 14 days before they either expire or are added to this document.
