const katex = require('katex');
const temml = require('temml');
const process = require('process');
const BufferList = require('bl');

console.error("starting...");

let buffer = new BufferList();
process.stdin.encoding = null;
process.stdin.on('readable', () => {
  while ((data = process.stdin.read())) {
    buffer.append(data);
  }
  while (true) {
    if (buffer.length < 4) break;
    let len = buffer.readUInt32LE(0);
    if (buffer.length < 4 + len) break;
    let msg = buffer.slice(4, 4 + len);
    process.stdout.write(wrapString(handleMessage(msg.toString())));
    buffer.consume(4 + len);
  }
});

const MACROS = {
  '\\epsilon': '\\varepsilon',
  '\\Dom': '\\mathop{\\mathrm{Dom}}',
  '\\cat': '\\mathbin{:}',
  '\\os': '&\\text{ if #1}',
  '\\osamgen': '&\\text{ otherwise}',
  '\\cv': '\\textsf',
  // Workaround for https://github.com/KaTeX/KaTeX/issues/3641
  '·': '${\\cdot}$'
};

function handleMath(body) {
  let displayMode = false, formula;
  if (body.startsWith('$$')) {
    displayMode = true;
    formula = body.substr(2);
  } else {
    formula = body.substr(1);
  }
  return temml.renderToString(formula, {
    displayMode: displayMode,
    macros: MACROS,
    // I want to set this, but it would cause Temml to reject slashes
    // in text mode, such as in
    //   \eta(\nu) = \nu^{\cv{aeio/âêîô}}
    // strict: true,
    throwOnError: true,
  });
}

function handleArrowOpt(body) {
  let entries = body.split(" ").map((x) => Number.parseInt(x));
  // Get # arrows
  let numArrows = entries[0];
  let arrows = [];
  for (let i = 0; i < numArrows; ++i) {
    arrows.push(entries.slice(5 * i + 1, 5 * i + 6));
  }
  console.log(arrows);
}

const DISPATCH = {
  "katex": handleMath,
  "saeth": handleArrowOpt,
};

function handleMessage(msg) {
  let spacePos = msg.indexOf(' ');
  let cmd = msg.substr(0, spacePos);
  let body = msg.substr(spacePos + 1);
  return DISPATCH[cmd](body);
}

function wrapString(s) {
  let len = Buffer.byteLength(s);
  let buf = Buffer.allocUnsafe(4);
  buf.writeUInt32LE(len);
  return Buffer.concat([buf, Buffer.from(s)], 4 + len);
}
