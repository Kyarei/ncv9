#lang pollen

◊define-meta[title]{桜、舞い散るあの丘へ}
◊define-meta[title-language]{ja}
◊define-meta[date]{2022-01-10 – 2022-01-14}

◊msgbox-outdated{9c}

A translation of ◊link["https://w.atwiki.jp/hmiku/pages/5400.html"]{◊ja{桜、舞い散るあの丘へ}} by ◊ja{かぐらP} featuring Hatsune Miku.

Thanks to ◊link["https://sites.google.com/site/tsapsaanja/%E4%BE%8B%E6%96%87/%E6%A1%9C%E8%88%9E%E3%81%84%E6%95%A3%E3%82%8B%E3%81%82%E3%81%AE%E4%B8%98%E3%81%B8"]{Caligula Armjanskaja’s translation into their conlang} for giving me ideas on what to do with my own.

◊section{Title}

◊gloss/x[#:id "song-title"]{
  ◊glfree{◊nc{«coþoren tovra toraiþ sicjašeþasos om dercos ar»}}
  ◊gla{«coþor-en tovr-a tora-iþ si-cjaš-eþasos om derc-os ar»}
  ◊glb{cherry_tree-%gen.%co flower-%nom.%co dance-%ser %appl.%loc-fall-%rel.%dat,%dat.%ter that.%ter hill-%dat.%sg %all}
  ◊glfree{‘To that hill where cherry blossoms dance and fall’}
}

◊section{Verse 1}

◊gloss/x[#:id "toozagaru"]{
  ◊glfree{◊nc{arcen cþiro gemitracliris; miðas coþora tevrit šoni;}}
  ◊gla{arc-en cþir-o g\e<mitra>cl-iris; mið-as coþor-a tevr-it šon-i;}
  ◊glb{winter-%gen.%di sound-%nom.%co %pfv\far<%ddt>-3%pl.%inv night-%loc.%sg cherry_tree-%nom.%co blossom-%inf do_completely-3%pl}
  ◊glfree{The winter’s sound grows more distant; the cherry trees blossom fully at night;}
}

◊gloss/x[#:id "watashi-hitori"]{
  ◊glfree{◊nc{minae elven samos cenþ’pe minen cjaširþ enlas anvit engres ar dtorael.}}
  ◊gla{mina-e elv-en sam-os cenþ=’pe minen cjaš-irþ enl-as anv-it engres ar d\tora-el.}
  ◊glb{alone-%rel.%nom,%nom.%sg half_hour-%gen.%sg dream-%dat.%sg self.%nom.%sg=%poss.1 one.%gen.%cel fall-%ser farther-%rel.%nom,%dat.%sg wind-%sembl.%di there.%dat.%sg %all %pfv\dance-1%sg.%inv}
  ◊glfree{Alone, I fall into a half-hour dream and dance toward a distant place like the wind.}
}

The first line of the Japanese lyrics consists of two noun phrases. In the Ŋarâþ Crîþ translation ◊xref/p{toozagaru}, its structure has been inverted to place the verb at the head.

The second line ◊xref/p{watashi-hitori} needed some massaging to fill the extra syllables: using ◊l1{falþel} ◊trans{short-lived} caused the first half to be two syllables short, so I opted for ◊l1{elven minen} ◊trans{of one ◊i{elva}}. Note that the original Japanese lyrics use ◊ja{◊i{浅き}} ◊trans{shallow} to describe the dream, but to my knowledge, this word can be used to mean ◊trans{short-lived} as well.

◊l1{ecljat} would be the usual word for ◊trans{far away}, but it had already been used on the first line. As a result, we use a different word, ◊l1{enlat}, for the second line.

◊section{Pre-chorus 1}

◊gloss/x[#:id "oborodzuki"]{
  ◊glfree{◊nc{lêcþen łandis nîs šileaþ feŋa recen enôrimeþos ar gesnefrel.}}
  ◊gla{lêcþ-en łand-is nîs šile-aþ feŋ-a rec-en enôrimeþ-os ar g\es-nefr-el.}
  ◊glb{gray-%gen.%di cloud-%dat.%co through shine-%ser faded-%rel.%nom,%nom.%cel crescent_moon-%nom.%sg landscape-%dat.%sg %all %pfv\%inch-sink-3%sg.%inv}
  ◊glfree{The hazy crescent moon, shining through the gray clouds, starts to sink into the landscape.}
}

◊gloss/x[#:id "harushigure"]{
  ◊glfree{◊nc{lerþil cþerpecþa tełavo aste setaðis cjerandit relcro.}}
  ◊gla{lerþ-il cþerp-ecþa tełav-o ast-e seta-ðis cjerand-it relcr-o.}
  ◊glb{spring-%gen.%sg drizzle-%inst.%di sweet_smelling-%rel.%nom,%nom.%pl petal-%nom.%co 16⁸-%cls butterfly-%sembl.%co fly-3%pl}
  ◊glfree{With the spring rain fly like butterflies countless sweet-smelling petals.}
}

Again, I had to take (relatively extreme) liberties with the meaning, not only because I’d struggle to fill up the syllables otherwise, but also because the original lyrics are difficult to understand. (Bad at Japanese gang rise up.)

In particular, ◊xref/p{harushigure} is phrased weirdly to fit in with the 5+5+8 phrasing. Most forms of ◊l1{cþarpas} have two syllables and thus prevent the lyrics from fitting that structure.

◊section{Chorus 1}

◊gloss/x[#:id "awaku-yureru"]{
  ◊glfree{◊nc{feŋime łerla ondis lirnas serend·rênil}}
  ◊gla{feŋ-ime łerl-a ond-is lirn-as serend·rên-il}
  ◊glb{faded-and shake-%rel.%nom,%nom.%cel now-%loc.%di bright-%rel.%nom,%dat.%sg half_month_0-%gen.%sg}
  ◊glfree{Faded and swaying, now, over the bright *March}
}

◊gloss/x[#:id "rasen-egaki"]{
  ◊glfree{◊nc{anasor dêrmiþ mitriþ cretan grenče tovra}}
  ◊gla{an-asor dêrm-iþ mitr-iþ cret-an grenč-e tovr-a}
  ◊glb{sky-%dat.%di scatter-%ser fast-%ser spiral-%acc.%sg draw-%rel.%nom,%nom.%cel flower-%nom.%co}
  ◊glfree{sky scatter flowers, swiftly drawing a spiral,}
}

◊gloss/x[#:id "yurari-hirari"]{
  ◊glfree{◊nc{setame taŋame vescþime ceŋa’pe’moc}}
  ◊gla{set-ame taŋ-ame vescþ-ime ceŋa=’pe=’moc}
  ◊glb{gentle-and agile-and beautiful-and self.%inst.%sg=%poss.1=also}
  ◊glfree{gently, nimbly, beautifully, along with me as well,}
}

◊gloss/x[#:id "sakura-maichiru"]{
  ◊glfree{◊nc{coþoren ieletor om dercos nelso.}}
  ◊gla{coþor-en i-el-etor om derc-os nels-o.}
  ◊glb{cherry_tree-%gen.%co %adn-1%sg-in_front_of that.%ter hill-%dat.%sg go-3%pl}
  ◊glfree{travel to that hill of cherry trees in front of me.}
}

Unlike the original lyrics, in which a postpositional phrase is moved after the verb, the Ŋarâþ Crîþ translation goes the straightforward way of using a long verb-final sentence.

The original lyrics mention ◊ja{◊i{弥生}}, the third month of the traditional Japanese calendar. The closest equivalent in Ŋarâþ Crîþ is ◊l1{serend·rênerþ}, the name of the first half-month of the Crîþol calendar, which occurs at a similar time of year.

The word order in ◊xref/p{sakura-maichiru} is somewhat questionable, but it is written this way in order to fit the meter. For the same reason, this line does not match the translation of the song title.

Also note that we cannot insert an interjection mid-clause unless we wrap it in the numquote ◊l1{B{}}. I couldn’t get that approach to work anyway.

◊section{Verse 2}

◊gloss/x[#:id "chikadzuita"]{
  ◊glfree{◊nc{lerþil cþiro cþîmitraširis; astil avonas es toraħa;}}
  ◊gla{lerþ-il cþir-o cþî<mitra>š-iris; ast-il avon-as es tora-ħa;}
  ◊glb{spring-%gen.%di sound-%nom.%co near<%ddt>-3%pl.%inv petal-%nom.%sv wind-%dat.%di inside dance-3%sg}
  ◊glfree{The sound of spring approaches; a petal dances in the wind;}
}

◊gloss/x[#:id "kazumi-no-naka"]{
  ◊glfree{◊nc{elven samos reltens þon cenþ’pe minen cjaširþ enlas anvit engres ar dtorael.}}
  ◊gla{elv-en sam-os relt-ens þon cenþ=’pe minen cjaš-irþ enl-as anv-it engres ar d\tora-el.}
  ◊glb{half_hour-%gen.%sg dream-%dat.%sg mist-%dat.%co among self.%nom.%sg=%poss.1 one.%gen.%cel fall-%ser farther-%rel.%nom,%dat.%sg wind-%sembl.%di there.%dat.%sg %all %pfv\dance-1%sg.%inv}
  ◊glfree{I, inside the mist, fall into a half-hour dream and dance toward a distant place like the wind.}
}

In ◊xref/p{chikadzuita}, ◊l1{es} is used instead of ◊l1{þon} because the latter has an adnominal bias and requires adding an extra syllable to use adverbially, and also to avoid repeating ◊l1{þon} in ◊xref/p{kazumi-no-naka}.

◊section{Pre-chorus 2}

◊gloss/x[#:id "uraraka"]{
  ◊glfree{◊nc{aŋarirþ łorcþa gelšidit lerþil enva en samirit peanłit cþonłas}}
  ◊gla{aŋar-irþ łorcþ-a gelšid-it lerþ-il env-a en samir-it pe-anł-it cþonł-as}
  ◊glb{warm-%ser clear-%rel.%nom,%nom.%cel shadow-%sembl.%coll spring-%gen.%di day-%nom.%sg %inf.%gen smile-%inf 1%sg-in_return-%inf instant-%loc.%sg}
  ◊glfree{The moment the warm, clear spring day smiles back at me like shadows,}
}

◊gloss/x[#:id "harukaze"]{
  ◊glfree{◊nc{lerþil avona tełavon asten setaðis całot dorelcra.}}
  ◊gla{lerþ-il avon-a tełav-on ast-en seta-ðis cał-ot do-relcr-a.}
  ◊glb{spring-%gen.%sg wind-%nom.%di sweet_smelling-%rel.%nom,%acc.%pl petal-%acc.%co 16⁸-%cls dragonfly-%sembl.%co %caus-fly-3%sg}
  ◊glfree{The spring wind causes countless sweet-smelling petals to fly like dragonflies.}
}

◊xref/p{harukaze} loses some of its parallelism with ◊xref/p{harushigure} because ◊l1{cþarpas} and ◊l1{avona} have different numbers of syllables, both in the nominative direct and in the instrumental direct forms.

◊section{Chorus 2}

◊gloss/x[#:id "sakura-sakura"]{
  ◊glfree{◊nc{coþoras; coþoras; elgres’moc jas menavjo.}}
  ◊gla{coþor-as; coþor-as; elgres=’moc jas men-a-vjo.}
  ◊glb{cherry_tree-%dat.%co cherry_tree-%dat.%co here.%dat.%sg=also %abl see-1%sg-2%pl}
  ◊glfree{O, cherry trees, I can see you even from here.}
}

◊gloss/x[#:id "keshiki-mitsume"]{
  ◊glfree{◊nc{enlas a ganasor varmenat tovran dêrmo.}}
  ◊gla{enl-as a g\an-asor varmen-at tovr-an dêrm-o.}
  ◊glb{farther-%rel.%nom.%dat.%sg %inf.%loc (%inf\)sky-%dat.%di look_at-%inf flower-%acc.%co scatter-3%pl}
  ◊glfree{When I look at the distant sky, they scatter flowers.}
}

◊gloss/x[#:id "yurari-hirari-2"]{
  ◊glfree{◊nc{setame taŋame vescþo se. ša cenþ’ve’moc}}
  ◊gla{set-ame taŋ-ame vescþ-o se. ša cenþ=’ve=’moc}
  ◊glb{gentle-and agile-and beautiful-3%pl %mirative %int self.%nom.%sg=%poss.2=also}
  ◊glfree{Gently, nimbly, beautifully. You, too, will you}
}

◊gloss/x[#:id "sakura-maichiru-2"]{
  ◊glfree{◊nc{coþoren om dercos pelca nelsit geves?}}
  ◊gla{coþor-en om derc-os pelca nels-it gev-es?}
  ◊glb{cherry_tree-%gen.%co that.%ter hill-%dat.%sg %pr.1%sg.%inst go-%inf %request-2%sg}
  ◊glfree{travel to that hill of cherry trees with me?}
}

Note that ◊xref/p{yurari-hirari-2} and ◊xref/p{sakura-maichiru-2} lose parallelism with ◊xref/p{yurari-hirari} and ◊xref/p{sakura-maichiru} in the translation.

◊section{Chorus 3}

Repeat of Chorus 1.

◊section{Chorus 4}

◊gloss/x[#:id "sakura-sakuran"]{
  ◊glfree{◊nc{telmaðos coþoras. el serend·rênil onos}}
  ◊gla{telmað-os coþor-as. el serend·rên-il on-os}
  ◊glb{fierce-%rel.%nom,%dat.%pl cherry_tree-%dat.%co this.%ter month_0-%gen.%sg sky-%loc.%di}
  ◊glfree{O, fierce cherry trees! In this *March sky,}
}

◊gloss/x[#:id "sakura-sakura-3"]{
  ◊glfree{◊nc{coþoren tovra’cjo eristat ħarecisrin.}}
  ◊gla{coþor-en tovr-a=’cjo; erist-at ħar-ecis-rin.}
  ◊glb{cherry_tree-%gen.%co flower-%nom.%co=and.1 encounter-%inf do_again-1%pl.%inv-%recip}
  ◊glfree{I have met with the cherry blossoms again.}
}

◊gloss/x[#:id "yurari-hirari-3"]{
  ◊glfree{◊nc{setame taŋame vescþin asten vrigame}}
  ◊gla{set-ame taŋ-ame vescþ-in ast-en vrig-ame}
  ◊glb{gentle-and agile-and beautiful-%rel.%nom,%acc.%cel petal-%acc.%co follow-and}
  ◊glfree{I’ll follow the petals, gentle, agile, and beautiful,}
}

◊gloss/x[#:id "sakura-maichiru-3"]{
  ◊glfree{◊nc{coþoren om darnos cemi’ve nelsa þal.}}
  ◊gla{coþor-en om darn-os cemi=’ve nels-a þal.}
  ◊glb{cherry_tree-%gen.%co that.%ter hill-%loc.%sg self.%dat.%sg=%poss.2 go-1%sg %assert}
  ◊glfree{And go to where you are on the hill of cherry trees.}
}
