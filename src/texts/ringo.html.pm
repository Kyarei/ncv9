#lang pollen

◊define-meta[title]{Apple sentences (Zaslon)}
◊define-meta[date]{2021-08-30 – 2021-09-05}

◊msgbox-outdated{9c}

A translation of ◊link["https://zaslon.info/langcontents/1450/"]{◊ja{「りんごを食べたい58文」}} (◊cite{“58 ‘I want to eat an apple’ sentences”}) by Zaslon, used to help develop grammar.

◊section/n[1]{◊ja{私はりんごを食べる．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênča.}}
  ◊gla{nem-iren mênč-a.}
  ◊glb{apple-%acc.%sg eat-1%sg}
  ◊glfree{I eat an apple.}
}

Yes, the original sentences use U+FF0E (◊sc{fullwidth full stop}) as a period instead of U+3002 (◊sc{ideographic full stop}).

If we want to make it clear that the action is habitual (as this sentence was interpreted by ◊link["https://ziphil.com/conlang/theory/36.html"]{Ziphil’s translation into Shaleian}), then we can use the following:

◊gloss/x{
  ◊glfree{◊nc{nemins mênčat nespa.}}
  ◊gla{nem-iren mênč-at nesp-a.}
  ◊glb{apple-%acc.%gc eat-%inf %habitual-1%sg}
  ◊glfree{I eat apples.}
}

◊section/n[2]{◊ja{私はりんごを食べた．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčaþ.}}
  ◊gla{nem-iren mênč-a-þ.}
  ◊glb{apple-%acc.%sg eat-1%sg-%past}
  ◊glfree{I ate an apple.}
}

Now we use past tense. With the direct subject suffixes, this implies the perfective aspect as well.

◊section/n[3]{◊ja{彼はりんごを食べている．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênče.}}
  ◊gla{nem-iren mênč-e.}
  ◊glb{apple-%acc.%sg eat-3%sg}
  ◊glfree{He is eating an apple.}
}

If we wanted to distinguish the progressive action from other interpretations arising from the imperfective aspect, then we can say the following:

◊gloss/x{
  ◊glfree{◊nc{os nemiren m·ênčat elveła.}}
  ◊gla{os nem-iren m·ênč-at el-veła.}
  ◊glb{%inf.%dat apple-%acc.%sg eat-%inf inside-exist.3%sg}
  ◊glfree{He is eating an apple.}
}

Note that the original sentences distinguish between ◊trans{he} (◊ja{「彼」}) and ◊trans{she} (◊ja{「彼女」}), but Ŋarâþ Crîþ makes no such distinction.

◊section/n[4]{◊ja{彼女はりんごを食べ終わっている．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren erm·ênče.}}
  ◊gla{nem-iren er-m·ênč-e.}
  ◊glb{apple-%acc.%sg %term-eat-3%sg}
  ◊glfree{She is finishing eating the apple.}
}

We use the terminative prefix ◊l0{er-}. Note that this is different from the cessative prefix ◊l0{car-}.

I wanted the terminative prefix to be ◊l0{ar-} by analogy with NCS6 ◊v6{ar} ◊trans{to, toward}, but that would have made it homophonous with ◊l0{car-} in perfective forms.

◊section/n[5]{◊ja{彼女はりんごを食べ終わっていた．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren erm·ênčelta.}}
  ◊gla{nem-iren er-m·ênč-el-ta.}
  ◊glb{apple-%acc.%sg %term-eat-3%sg.%inv-%past}
  ◊glfree{She was finishing eating the apple.}
}

The default aspect is perfective in the past tense, so in order to express the past imperfective, we need to use the inverse affixes.

◊section/n[6]{◊ja{私の妻はりんごを食べたことがある．}}

◊gloss/x{
  ◊glfree{◊nc{veliša’pe os nemiren m·ênčat ŋače.}}
  ◊gla{veliš-a=’pe os nem-iren m·ênč-at ŋač-e.}
  ◊glb{spouse-%nom.%sg=%poss.1 %inf.%dat apple-%acc.%sg eat-%inf cross-3%sg}
  ◊glfree{My wife has eaten an apple before.}
}

We use the verb ◊l1{ŋačat} ◊trans{to cross} to indicate the experiential aspect.

◊section/n[7]{◊ja{私の妻はりんごを毎日食べる．}}

◊gloss/x{
  ◊glfree{◊nc{veliša’pe šinen neðaf nemins mênče.}}
  ◊gla{veliš-a=’pe šin-en neð-af nem-ins mênč-e.}
  ◊glb{spouse-%nom.%sg=%poss.1 all-%gen.%sg day-%loc.%gc apple-%acc.%gc eat-3%sg}
  ◊glfree{My wife eats an apple every day.}
}

The speaker is talking about one particular person, so the subject is singular, but everything else is generic as the sentence describes a general fact about that person.

◊section/n[8]{◊ja{私と私の妻は昨日りんごを食べた．}}

◊gloss/x{
  ◊glfree{◊nc{enven dores veliša’pe’cjo nemiren mênčanta.}}
  ◊gla{env-en dor-es veliš-a=’pe=’cjo nem-iren mênč-an-ta.}
  ◊glb{day-%gen.%sg previous-%loc.%sg spouse-%nom.%sg=%poss.1=and.1 apple-%acc.%sg eat-1%du.%excl-%past}
  ◊glfree{My wife and I ate an apple yesterday.}
}

It’s coordination time! Ŋarâþ Crîþ has special versions of coordinating clitics that are fused with pronouns. In this case, ◊l1{=’cjo} means ◊trans{and I}.

◊section/n[9]{◊ja{私と私の妻は6日前にりんごを食べた．}}

◊gloss/x{
  ◊glfree{◊nc{enver cfersîn’po fêtecto veliša’pe’cjo nemiren mênčanta.}}
  ◊gla{enver cfersîn’po fê-tecto veliš-a=’pe=’cjo nem-iren mênč-an-ta.}
  ◊glb{day-%acc.%pl six.%acc.%cel=%sep 3%gc-before spouse-%nom.%sg=%poss.1=and.1 apple-%acc.%sg eat-1%du.%excl-%past}
  ◊glfree{My wife and I ate an apple six days ago.}
}

We use ◊l1{=’po} to indicate the duration between the event in question and ‘now’ (as implied by the third-person generic argument).

◊section/n[10]{◊ja{彼らは明日りんごを食べる．}}

◊gloss/x{
  ◊glfree{◊nc{enven sodos nemiren mênčiris.}}
  ◊gla{env-en sod-os nem-iren mênč-iris.}
  ◊glb{day-%gen.%sg next-%loc.%sg apple-%acc.%pl eat-3%pl.%inv}
  ◊glfree{They will eat apples tomorrow.}
}

The verb is now in the present perfective.

◊section/n[11]{◊ja{彼らは6日後にりんごを食べる．}}

◊gloss/x{
  ◊glfree{◊nc{enver cfersîn’po fêmir nemiren mênčiris.}}
  ◊gla{env-er cfersîn=’po fê-mir nem-iren mênč-iris.}
  ◊glb{day-%acc.%pl six.%acc.%cel=%sep 3%gc-after apple-%acc.%pl eat-3%pl.%inv}
  ◊glfree{They will eat apples six days from now.}
}

Same business as Sentence #9.

◊section/n[12]{◊ja{彼女らは3日間りんごを食べている．}}

◊gloss/x{
  ◊glfree{◊nc{envai prênos nîs nemins mênčirista.}}
  ◊gla{env-ai prênos nîs nem-ins mênč-iris-ta.}
  ◊glb{day-%dat.%pl three-%dat.%cel during apple-%acc.%gc eat-3%pl.%inv-%past}
  ◊glfree{They have been eating apples for three days.}
}

The original source has the note ◊ja{◊q{（食事としてりんごだけを食べている）}}, translating to ◊q{(they ate only apples for their meals)}.

Note that this is a valid way to translate the sentence:

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčirþ envai prênos ŋačirista.}}
  ◊gla{nem-iren mênč-irþ env-ai prênos ŋač-iris-ta.}
  ◊glb{apple-%acc.%pl eat-%ser day-%dat.%pl three-%dat.%cel take-3%pl.%inv-%past}
  ◊glfree{They have been eating apples for three days.}
}

but implies that the process of eating apples for their meals has a goal.

◊section/n[13]{◊ja{彼女らはりんごを5分間食べ続けている．}}

◊gloss/x{
  ◊glfree{◊nc{cenðoþ ŋatis pâ nîs nemins mênčirista.}}
  ◊gla{cenð-oþ ŋatis pâ nîs nem-ins mênč-iris-ta.}
  ◊glb{cenðos-%dat.%pl eight.%dat directly during apple-%acc.%gc eat-3%pl.%inv-%past}
  ◊glfree{They have been eating apples for five minutes straight.}
}

The original source has the note ◊ja{◊q{（りんごを食べる所要時間に5分かかっている）}}, translating to ◊q{(the time taken to eat the apples is five minutes)}. I interpret this remark to mean that ‘they’ in Example #12 have been eating apples for five minutes continuously, while ‘they’ in Example #11 have not been eating apples for three days without breaks (rather, when they ate something, what they ate was apples).

5 minutes is approximately 7.89 ◊i{cenðor}. Round that up to 8.

◊section/n[14]{◊ja{彼は常にりんごを食べている．}}

◊gloss/x{
  ◊glfree{◊nc{šesos nemins mênče.}}
  ◊gla{šes-os nem-ins mênč-e.}
  ◊glb{all_times-%loc.%sg apple-%acc.%gc eat-3%sg}
  ◊glfree{He is always eating apples.}
}

The original source has the note ◊ja{◊q{（四六時中ずっとりんごを食べている）}}, translating to ◊q{(he eats apples all day and all night)}.

◊l1{šinen ðês} means ◊trans{at all occurrences of some event}, or more simply, ◊trans{every time}, as in ◊trans{he is eating apples every time he returns from school}. Therefore, it is not the appropriate term to use here.

Interestingly, Ziphil’s translations into Shaleian skip #12 – 14.

◊section/n[15]{◊ja{りんごが3つある．}}

◊gloss/x{
  ◊glfree{◊nc{nemir prêno von.}}
  ◊gla{nem-ir prêno von.}
  ◊glb{apple-%nom.%pl three.%nom.%cel exist.3%pl}
  ◊glfree{There are three apples.}
}

It is entirely possible to omit ◊l1{von} in this case.

◊section/n[16]{◊ja{りんご達がテーブルの上にある．}}

◊gloss/x{
  ◊glfree{◊nc{nemir nelas ilvon.}}
  ◊gla{nem-ir nel-as il-von.}
  ◊glb{apple-%nom.%pl table.%dat.%sg on_top_of-exist.3%pl}
  ◊glfree{The apples are on the table.}
}

In this sentence, the relational ◊l1{il} is used predicatively.

◊section/n[17]{◊ja{誰かがあのりんごを食べてしまった．}}

◊gloss/x{
  ◊glfree{◊nc{nema tê nemiren mênčeþ.}}
  ◊gla{nem-a tê nem-iren mênč-e-þ.}
  ◊glb{some-%nom.%sg that.%cel apple-%acc.%sg eat-3%sg-%past}
  ◊glfree{Someone ate that apple.}
}

The original source has the note ◊ja{◊q{（その結果，あのりんごは今はない）}}, translating to ◊q{(as a result, that apple is no longer here)}.

For comparison, Ziphil’s Shaleian translation uses the present tense and continuous aspect, focusing on the fact that the apple is no longer present.

◊section/n[18]{◊ja{誰かがこのりんごを食べそうだ．}}

◊gloss/x{
  ◊glfree{◊nc{nema tê nemiren mênčat cenmireþ.}}
  ◊gla{nem-a tê nem-iren mênč-at cenmir-e-þ.}
  ◊glb{some-%nom.%sg that.%cel apple-%acc.%sg eat-%inf seem_to-3%sg-%past}
  ◊glfree{It seems that someone ate that apple.}
  ◊glfree[#:prefix ◊@{Or: }]{Someone must have eaten that apple.}
}

The original source has the note ◊ja{◊q{（推量）}}, translating to ◊q{(inference)}. In Ŋarâþ Crîþ, this concept is expressed with the auxiliary verb ◊l1{cenmirat}.

◊section/n[19]{◊ja{誰かがこのりんご，そのりんご，あのりんごを食べたそうだ．}}

◊gloss/x{
  ◊glfree{◊nc{«nema lê nemiren tê nemiren'ce tê ecljan nemiren'ce mênčeþ» reþ gcrešaraþ.}}
  ◊gla{«nem-a lê nem-iren tê nem-iren='ce tê eclj-an nem-iren='ce mênč-e-þ» reþ g\crešar-a-þ.}
  ◊glb{some-%nom.%sg this.%cel apple-%acc.%sg that.%cel apple-%acc.%sg=and that.%cel far-%rel.%nom,%acc.%cel apple-%acc.%sg=and eat-3%sg-%past %quot.%ind.%acc %pfv\overhear-1%sg-%past}
  ◊glfree{I heard that someone ate this apple, that apple, and that apple over there.}
}

The original source has the note ◊ja{◊q{（伝聞）}}, translating to ◊q{(hearsay)}.

This is awkward to translate into Ŋarâþ Crîþ because Japanese has a three-way demonstrative distinction while Ŋarâþ Crîþ has only two.

Ziphil’s Shaleian translation omits sentences #18 and 19.

◊section/n[20]{◊ja{りんごがひとつもない．}}

◊gloss/x{
  ◊glfree{◊nc{nemir mina’moc ceła.}}
  ◊gla{nem-ir mina=’moc ceła.}
  ◊glb{apple-%nom.%sg one.%nom.%cel=also not_exist.3%sg}
  ◊glfree{There are no apples.}
}

A straightforward translation from the Japanese sentence.

◊section/n[21]{◊ja{私はりんごを食べない．}}

◊gloss/x{
  ◊glfree{◊nc{nemiþ *maþe.}}
  ◊gla{nem-iþ *maþ-e.}
  ◊glb{apple-%dat.%gc abstain-1%sg}
  ◊glfree{I abstain from eating apples.}
}

The original sentence translates to ◊trans{I do not eat apples.} Ŋarâþ Crîþ has no single negative marker. Instead, we use a suppletive negative here.

◊section/n[22]{◊ja{私はりんごを食べられない．}}

◊gloss/x{
  ◊glfree{◊nc{mênčat pepentaþe nemir ceła.}}
  ◊gla{mênč-at pe-pent-aþe nem-ir ceła.}
  ◊glb{eat-%inf 1%sg-able_to-%rel.%acc.%nom,%cel apple-%nom not_exist.3%sg}
  ◊glfree{There is no apple that I can eat.}
}

The original sentence translates to ◊trans{I cannot eat an apple.} It has the note ◊ja{◊q{（りんごがないので状況的に食べられない）}}, translating to ◊q{(can’t eat an apple in this situation because there are none to eat)}.

◊section/n[23]{◊ja{私はりんごを床に落とした．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren cajoþ ndocjašeþ.}}
  ◊gla{nem-iren caj-oþ n\do-cjaš-e-þ.}
  ◊glb{apple-%acc.%sg ground-%dat.%sg %pfv\%caus-fall-1%sg-%past}
  ◊glfree{I dropped an apple onto the floor.}
}

We use the causative prefix on the verb meaning ◊trans{to fall} to derive ◊trans{to drop}. Since ◊l1{cjašit} is semitransitive, its dative argument stays as is in ◊l1{docjašit}, while the nominative argument becomes an accusative argument to make room for the cause.

◊section/n[24]{◊ja{りんごが床に落ちた．}}

◊gloss/x{
  ◊glfree{◊nc{nemir cajoþ gcjašaþ.}}
  ◊gla{nem-ir caj-oþ g\cjaš-a-þ.}
  ◊glb{apple-%nom.%sg ground-%dat.%sg %pfv\fall-3%sg-%past}
  ◊glfree{The apple fell to the floor.}
}

This time without the causative.

◊section/n[25]{◊ja{りんごは食べられない．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčat rjotu.}}
  ◊gla{nem-iren mênč-at rjot-u.}
  ◊glb{apple-%acc.%sg eat-%inf cannot-3%gc}
  ◊glfree{The apple cannot be eaten.}
}

The original source has the note ◊ja{◊q{（りんごの性質として，可食でなくなった）}}, translating to ◊q{(in terms of the apple’s characteristics, it has become inedible)}.

◊section/n[26]{◊ja{私はりんごを食べたい．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčat renda.}}
  ◊gla{nem-iren mênč-at rend-a.}
  ◊glb{apple-%acc.%sg eat-%inf want_to-1%sg}
  ◊glfree{I want to eat an apple.}
}

TODO: should the object be singular or generic in this case?

◊section/n[27]{◊ja{私はりんごを買いたい．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren vaðit renda.}}
  ◊gla{nem-iren vað-it rend-a.}
  ◊glb{apple-%acc.%sg buy-%inf want_to-1%sg}
  ◊glfree{I want to buy an apple.}
}

The same matter as with Example #26.

◊section/n[28]{◊ja{このりんごは汚い．}}

◊gloss/x{
  ◊glfree{◊nc{lê nemir gene.}}
  ◊gla{lê nem-ir gen-e.}
  ◊glb{this.%cel apple-%nom.%sg dirty-3%sg}
  ◊glfree{This apple is dirty.}
}

◊l1{genat} is used specifically for food or water.

◊section/n[29]{◊ja{このりんごは綺麗ではない．}}

◊gloss/x{
  ◊glfree{◊nc{lê nemir genat išira.}}
  ◊gla{lê nem-ir gen-at išir-a.}
  ◊glb{this.%cel apple-%nom.%sg dirty-%inf at_least_slightly-3%sg}
  ◊glfree{This apple is not clean.}
}

◊l1{iširit} can be translated as ◊trans{not un-}.

◊section/n[30]{◊ja{あのりんごは食べられそうだ．}}

◊gloss/x{
  ◊glfree{◊nc{tê nemiren mênčat pentat cenmiru.}}
  ◊gla{tê nem-iren mênč-at pent-at cenmir-u.}
  ◊glb{that.%cel apple-%acc.%sg eat-%inf able_to-%inf seem_to-3%gc}
  ◊glfree{That apple seems edible.}
}

Nothing truly new here.

◊section/n[31]{◊ja{あれはりんごではない．}}

◊gloss/x{
  ◊glfree{◊nc{enta nemils corveła.}}
  ◊gla{ent-a nem-ils cor-veła.}
  ◊glb{that_thing.%cel-%nom.%sg apple-%dat.%sg not_one_of-exist.3%sg}
  ◊glfree{That is not an apple.}
}

Unlike its antonym ◊l1{varit}, ◊l1{cor} is a relational. A similar situation occurs with the verb ◊l1{censit} and the relational ◊l1{ema}.

◊section/n[32]{◊ja{これはりんごではなくみかんだ．}}

◊gloss/x{
  ◊glfree{◊nc{enta nemils corveła; *failena vara.}}
  ◊gla{ent-a nem-ils cor-veła; *fail-ena var-a.}
  ◊glb{that_thing.%cel-%nom.%sg apple-%dat.%sg not_one_of-exist.3%sg tangerine-%acc.%sg one_of-3%sg}
  ◊glfree{That is a tangerine, not an apple.}
}

We use two independent clauses chained together.

◊section/n[33]{◊ja{あなたはみかんを食べるか？}}

◊gloss/x{
  ◊glfree{◊nc{ša *faila mênčes?}}
  ◊gla{ša *fail-a mênč-es?}
  ◊glb{%int tangerine-%acc.%gc eat-2%sg}
  ◊glfree{Do you eat tangerines?}
}

The object is in the generic number because this is asking about a general fact about a listener.

◊section/n[34]{◊ja{はい，私はみかんを食べます．}}

◊gloss/x{
  ◊glfree{◊nc{vil; *faila mênča.}}
  ◊gla{vil; *fail-a mênč-a.}
  ◊glb{yes tangerine-%acc.%gc eat-1%sg}
  ◊glfree{Yes, I eat tangerines.}
}

◊l1{vil} is the short numeral for ◊trans{one}, but it also functions as an interjection.

◊section/n[35]{◊ja{あなたはみかんを食べないのですか？}}

◊gloss/x{
  ◊glfree{◊nc{ša *faila mênčat’ve criþe?}}
  ◊gla{ša *fail-a mênč-at=’ve criþ-e?}
  ◊glb{%intj tangerine-%acc.%sg eat-%inf=%poss.2 false-3%sg}
  ◊glfree{Is it false that you eat tangerines?}
  ◊glfree[#:prefix "Or: "]{Don’t you eat tangerines?}
}

As usual, a roundabout way to get around the lack of negation in Ŋarâþ Crîþ.

◊section/n[36]{◊ja{ええ，私はみかんを食べません．}}

◊gloss/x{
  ◊glfree{◊nc{vil; *failas *maþa.}}
  ◊gla{vil; *fail-as *maþ-a.}
  ◊glb{yes tangerine-%dat.%gc abstain_from-1%sg}
  ◊glfree{No, I don’t eat tangerines.}
}

◊l1{vil} asserts that what was questioned is true, working more like Japanese ◊trans{◊ja{はい}} than English ◊trans{yes}.

◊section/n[37]{◊ja{あなたはみかんを食べたかった．ですよね？}}

◊gloss/x{
  ◊glfree{◊nc{*failena mênčat renderesta. refe šan?}}
  ◊gla{*fail-ena mênč-at rend-eres-ta. ref-e šan?}
  ◊glb{tangerine-%acc.%sg eat-%inf want_to-2%sg.%inv-%past true-3%sg %tag}
  ◊glfree{You wanted to eat a tangerine. Is that right?}
}

To create a tag question, the tailp ◊l1{šan} is used. Note that a tag question ends with a question mark but does not begin with the headp ◊l1{ša}.

◊section/n[38]{◊ja{いいえ，私はみかんを食べたくはありません．}}

◊gloss/x{
  ◊glfree{◊nc{ces; *failena mênčat fonalta.}}
  ◊gla{ces; *fail-ena mênč-at fon-al-ta.}
  ◊glb{no tangerine-%acc.%sg eat-%inf not_want-1%sg.%inf-%past}
  ◊glfree{No, it’s not that I wanted to eat a tangerine.}
}

This answer indicates merely the lack of desire to eat a tangerine, not a desire to avoid eating it. The latter would be expressed as:

◊gloss/x{
  ◊glfree{◊nc{ces; *failena mênčat pečit rendalta.}}
  ◊gla{ces; *fail-ena mênč-at pečit rend-al-ta.}
  ◊glb{no tangerine-%acc.%sg eat-%inf avoid-%inf want-1%sg.%inf-%past}
  ◊glfree{No, I wanted to avoid eating a tangerine.}
}

◊section/n[39]{◊ja{あなたはりんごもみかんも食べないのですか？}}

◊gloss/x{
  ◊glfree{◊nc{ša nemins *faila’te mênčat garas?}}
  ◊gla{ša nem-ins *fail-a=’te mênč-at gar-as?}
  ◊glb{%int apple-%acc.%gc tangerine-%acc.%gc=or eat-%inf refrain_from-2%sg}
  ◊glfree{Is it true that you eat neither apples nor tangerines?}
}

To refrain from eating apples or tangerines is to not eat either fruit. To refrain from eating apples ◊em{and} tangerines is to eat at most one of them.

◊section/n[40]{◊ja{いや，私はりんごを食べます．}}

◊gloss/x{
  ◊glfree{◊nc{ces; nemins mênče.}}
  ◊gla{ces; nem-ins mênč-e.}
  ◊glb{no apple-%acc.%gc eat-1%sg}
  ◊glfree{No, I eat apples.}
}

Which matters here.

◊section/n[41]{◊ja{私はみかんを食べない．だから，みかんは食べられない．}}

◊gloss/x{
  ◊glfree{◊nc{*failas *maþe. ea *failena mênčat vandru.}}
  ◊gla{*fail-as *maþ-e. ea *fail-ena mênč-at vandr-u.}
  ◊glb{tangerine-%dat.%gc abstain_from-1%sg therefore tangerine-%acc.%sg eat-%inf leave_undone-3%gc}
  ◊glfree{I don’t eat tangerines. Therefore, the tangerine is not eaten.}
}

The original source has the note ◊ja{◊q{（受け身の否定）}}, translating to ◊q{(negation of a passive)}.

◊section/n[42]{◊ja{りんごは食べることができない．なぜなら，りんごは綺麗ではないから．}}

◊gloss/x{
  ◊glfree{◊nc{nemiren mênčat rjote; gene fose.}}
  ◊gla{nem-iren mênč-at rjot-e; gen-e fose.}
  ◊glb{apple-%acc.%sg eat-%inf cannot-3%sg dirty-3%sg because}
  ◊glfree{It is not possible to eat the apple because it is not clean.}
}

The second clause in the sentence is a ◊i{so}-clause that has been insubordinated.

◊section/n[43]{◊ja{りんごは汚い．一方で，みかんは綺麗だ．}}

◊gloss/x{
  ◊glfree{◊nc{nemir genameca *failen fenre.}}
  ◊gla{nem-ir gen-ameca *fail-en fenr-e.}
  ◊glb{apple-%nom.%sg dirty-but tangerine-%nom.%sg clean-3%sg}
  ◊glfree{The apple is dirty; on the other hand, the tangerine is clean.}
}

◊l1{fenrat} means ◊trans{clean} in general, such that it can act as the antonym of both ◊l1{velcit} and ◊l1{genat}.

◊section/n[44]{◊ja{みかんを食べましょうよ？}}

◊gloss/x{
  ◊glfree{◊nc{fel rendes so on *faila mênčat triłepjo.}}
  ◊gla{fel rend-es so on *fail-a mênč-at trił-e-pjo.}
  ◊glb{%tgellp want_to-2%sg if %inf.%acc tangerine-%acc.%gc eat-%inf suggest-1%sg-2%du}
  ◊glfree{If you want, then I suggest that we eat some tangerines.}
  ◊glfree[#:prefix "Or: "]{Why don’t we eat some tangerines?}
}

The original source has the note ◊ja{◊q{（勧誘）}}, translating to ◊q{(invitation)}.

◊section/n[45]{◊ja{いいえ，お断りします．}}

◊gloss/x{
  ◊glfree{◊nc{neftrel.}}
  ◊gla{neftr-el.}
  ◊glb{refuse-1%sg.%inv}
  ◊glfree{No, thank you.}
}

This is the canonical way to turn down an offer.

◊section/n[46]{◊ja{あそこでみかんを食べろ．りんごは食べるな．}}

◊gloss/x{
  ◊glfree{◊nc{le eči *failena mênčas. le nemiren mênčat garas.}}
  ◊gla{le eči *fail-ena mênč-as. le nem-iren mênč-at gar-as.}
  ◊glb{%imp there.%loc.%sg tangerine-%acc.%sg eat-2%sg %imp apple-%acc.%sg eat-%inf refrain_from-2%sg}
  ◊glfree{Eat the tangerine over there. Don’t eat the apple.}
}

Or more concisely:

◊gloss/x{
  ◊glfree{◊nc{le nemils dêt eči *failena mênčas.}}
  ◊gla{le nem-ils dêt eči *fail-ena mênč-as.}
  ◊glb{%imp apple-%dat.%sg instead_of there.%loc.%sg tangerine-%acc.%sg eat-2%sg}
  ◊glfree{Eat the tangerine over there. Don’t eat the apple.}
}

◊section/n[47]{◊ja{りんごを頂いても宜しいですか？みかんは差し上げます．}}

◊gloss/x{
  ◊glfree{◊nc{ša nemiren mirat sarae? *failena orłal.}}
  ◊gla{ša nem-iren mir-at sara-e? *fail-ena orł-al.}
  ◊glb{%int apple-%acc take-%inf may-1%sg tangerine-%acc.%sg sell-1%sg.%inv}
  ◊glfree{May I take an apple? I will give you a tangerine.}
}

I might have misinterpreted this sentence. I’m not that good with Japanese honorifics.

◊section/n[48]{◊ja{私は「りんごは食べるな，みかんを食べろ」と言った．}}

◊gloss/x{
  ◊glfree{◊nc{«le nemiren mênčat garas; le *failena mênčas» ne mareþ.}}
  ◊gla{«le nem-iren mênč-at gar-as; le *fail-ena mênč-as» ne mar-e-þ.}
  ◊glb{%imp apple-%acc.%sg eat-%inf refrain_from-2%sg %imp tangerine-%acc.%sg eat-2%sg %quot.%acc say-1%sg-%past}
  ◊glfree{I said “Don’t eat the apple; eat the tangerine.”}
}

We use a direct quotative particle because we are quoting direct speech.

◊section/n[49]{◊ja{あなたはりんごを食べろと言うのですか？}}

◊gloss/x{
  ◊glfree{◊nc{ša «le nemiren mênčas» reþ marasta?}}
  ◊gla{ša «le nem-iren mênč-as» reþ mar-as-ta?}
  ◊glb{%int %imp apple-%acc.%sg eat-2%sg %quot.%ind.%acc say-2%sg-%past}
  ◊glfree{Did you say to eat the apple?}
}

We now use an indirect quotation. The quotation marks are still necessary.

◊section/n[50]{◊ja{貴様はりんごを食べたいと言った．}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênčat rendes» reþ marasta.}}
  ◊gla{«nem-iren mênč-at rend-es» reþ mar-as-ta.}
  ◊glb{apple-%acc.%sg eat-%inf want_to-2%sg %quot.%ind.%acc say-2%sg-%past}
  ◊glfree{You said you wanted to eat an apple.}
}

The personal deixis inside an indirect quotation is the same as that outside of the quotation.

◊section/n[51]{◊ja{私はりんごを食べたいと言った覚えがない．}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênčat renda» reþ on marit varacrit rjota.}}
  ◊gla{«nem-iren mênč-at rend-a» reþ on mar-it varacr-it rjot-a.}
  ◊glb{apple-%acc.%sg eat-%inf want_to-1%sg %quot.%ind.%acc %inf.%acc say-%inf remember-%inf fail_to-1%sg}
  ◊glfree{I don’t remember saying that I wanted to eat an apple.}
}

Here, ◊l1{rjotat} is used to mean ◊trans{fail to}.

◊section/n[52]{◊ja{あなたは確か「私はりんごを食べたい」と言ったはずだ．}}

◊gloss/x{
  ◊glfree{◊nc{colþas «nemiren mênčat renda» ne marit vrasasta.}}
  ◊gla{colþ-as «nem-iren mênč-at rend-a» ne mar-it vras-as-ta.}
  ◊glb{certainty-%loc.%sg apple-%acc.%sg eat-%inf want_to-1%sg %quot.%acc say-%inf %epsnec-2%sg-%past}
  ◊glfree{Surely you must have said “I want to eat an apple.”}
}

We return to direct quotation here.

◊section/n[53]{◊ja{私はもしかしたらそう言ったかもしれない．}}

◊gloss/x{
  ◊glfree{◊nc{fetjan marit ðþaraþ.}}
  ◊gla{fetj-an mar-it ð\þar-a-þ.}
  ◊glb{that_idea-%acc.%sg say-%inf %pfv\%epspos-1%sg-%past}
  ◊glfree{Perhaps I might have said that.}
}

For the demonstrative pronoun, we use ◊l1{fetja}, which refers to an idea or speech, rather than ◊l1{ela} or such.

◊section/n[54]{◊ja{あなたは絶対に私に対して「りんごを食べたい」と言った．}}

◊gloss/x{
  ◊glfree{◊nc{«nemiren mênčat renda» ne colþas maraspes tal.}}
  ◊gla{«nem-iren mênč-at rend-a» ne colþ-as mar-as-pe-þ þal.}
  ◊glb{apple-%acc.%sg eat-%inf want_to-1%sg %quot.%acc certainty-%loc.%sg say-2%sg-1%sg-%past %assert}
  ◊glfree{You absolutely said “I want to eat an apple” to me.}
}

The past ending ◊l0{-þ} and the assertive tailp ◊l0{þal} dissimilate into ◊l1{-s tal}.

◊section/n[55]{◊ja{彼は私にりんごを食べさせる．}}

◊gloss/x{
  ◊glfree{◊nc{nemiþ domênčepe.}}
  ◊gla{nem-iþ do-mênč-e-pe.}
  ◊glb{apple-%dat.%sg %caus-eat-3%sg-1%sg}
  ◊glfree{He makes me eat apples.}
}

When the causative of ◊l1{mênčat} is taken:

◊items{
  ◊item{The causer of the action takes the nominative case.}
  ◊item{The nominative argument of ◊l1{mênčat} (i.e. the one eating) now takes the accusative case.}
  ◊item{The accusative argument of ◊l1{mênčat} (i.e. the thing being eaten) now takes the dative case.}
}

◊section/n[56]{◊ja{みかんは私に食べられていない．}}

◊gloss/x{
  ◊glfree{◊nc{*failena aliþ’pe mênčeþ.}}
  ◊gla{*fail-ena al-iþ=’pe mênč-e-þ.}
  ◊glb{tangerine-%acc.%sg other_than-%nom.%sg=%poss.1 eat-3%sg-%past}
  ◊glfree{The tangerine was not eaten by me.}
}

We use ◊l1{aliþ} ◊trans{something other than} for this purpose. This noun is used as the possessee in a possessive construction. In our case, the possessor is a first-person pronoun, so we can simply use the possessive clitic ◊l1{=’pe} without specifying the possessor explicitly.

◊section/n[57]{◊ja{りんごは彼によって私に食べさせられた．}}

◊gloss/x{
  ◊glfree{◊nc{nemils ndomênčepeþ.}}
  ◊gla{nem-ils n\do-mênč-e-pe-þ.}
  ◊glb{apple-%dat.%sg %pfv\%caus-eat-3%sg-1%sg-%past}
  ◊glfree{I was made to eat the apple by him.}
}

This turns out to be surprisingly straightforward since Ŋarâþ Crîþ does not have a passive voice.

◊section/n[58]{◊ja{ここには何もない．}}

◊gloss/x{
  ◊glfree{◊nc{eši nema’moc ceła.}}
  ◊gla{eši nem-a=’moc ceła.}
  ◊glb{here.%loc.%sg anything-%nom.%sg=also not_exist.3%sg}
  ◊glfree{There is nothing here.}
}

◊l1{eši šino ceła} would assume that other things exist but are somewhere else. This is probably true, but we avoid assuming that anyway.
