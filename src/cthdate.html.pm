#lang pollen

◊define-meta[title]{The cþ-date}

◊script{
const DATE = new Date("2013-04-13");

function dateToCthNum(date) {
  let signed = Math.round((date - DATE) / (86400 * 1000));
  if (signed < 0) signed += 0x100000000;
  return signed;
}

function cthNumToDate(num) {
  if (num >= 0x80000000) num -= 0x100000000;
  return new Date(DATE.getTime() + num * 86400 * 1000);
}

function updateFromDate() {
  let date = document.getElementById("western").valueAsDate;
  document.getElementById("cth").value = dateToCthNum(date).toString(16).toUpperCase();
}

function updateFromCthNum() {
  let num = Number.parseInt(document.getElementById("cth").value, 16);
  document.getElementById("western").valueAsDate = cthNumToDate(num);
}

function setToToday() {
  let date = new Date();
  date.setUTCHours(0);
  date.setUTCMinutes(0);
  date.setUTCSeconds(0);
  document.getElementById("western").valueAsDate = date;
  document.getElementById("cth").value = dateToCthNum(date).toString(16).toUpperCase();
}

document.addEventListener("onload", setToToday);
}

This date format shows the number of days since 2013/04/13, the date when the original Necarasso Cryssesa grammar was created, according to its metadata.

The format is as follows: ◊i{cþ} plus the number of days in hexadecimal (with capital letters for higher digits). For dates before cþ0, add 2◊sup{32} to the date number.

◊label[#:for "western"]{Date:} ◊input[#:id "western" #:type "date" #:onchange "updateFromDate()"]{} ◊button[#:id "today" #:onclick "setToToday()"]{Set to today}

◊label[#:for "cth"]{As cþ-date: cþ} ◊input[#:id "cth" #:type "text" #:maxlength "8" #:onchange "updateFromCthNum()"]

◊small{Established cþB4F}
