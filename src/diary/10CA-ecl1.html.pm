#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{The ECL1 language}
◊define-meta[date]{2024-01-18}

◊b{E}xperimental ◊b{C}on◊b{l}ang ◊b{1} (tentative name) is a conlang that strives to do away with verbs by following a ‘chemical reaction’ paradigm. It also seeks to be a testbed for a simplified form of ◊i{oginiþe cfarðerþ} deduplication.

◊section{Orthography and phonology}

◊table/x[
  #:options (table-options
    #:caption "Alphabet of ECL1 with IPA values."
  )
]{
  Letter & IPA
  ◊ecl1{’} & ɨ~ə
  ◊ecl1{a} & a
  ◊ecl1{c} & θ
  ◊ecl1{e} & e
  ◊ecl1{h} & h ([x] before a back vowel)
  ◊ecl1{i} & i
  ◊ecl1{k} & k
  ◊ecl1{l} & l
  ◊ecl1{m} & m
  ◊ecl1{n} & n
  ◊ecl1{o} & o ([ɔ] before /w/)
  ◊ecl1{p} & p ([f] before a back vowel)
  ◊ecl1{r} & ɹ
  ◊ecl1{ŗ} & ɹ̩
  ◊ecl1{s} & s
  ◊ecl1{t} & t
  ◊ecl1{u} & ɯ ([u] before /t/, /s/, /n/, or /ɬ/)
  ◊ecl1{w} & w
  ◊ecl1{x} & ɬ
  ◊ecl1{y} & j
}

◊i{◊ecl1{’aeioŗu}} are vowels; everything else is a consonant. ◊i{◊ecl1{w}} and ◊i{◊ecl1{y}} are considered glides.

Syllable structure is ◊sub{◊var{s}}(C)(C)◊sub{◊var{o}}V◊sub{◊var{n}}(C)◊sub{◊var{s}|◊var{ω}}, where:

◊items{
  ◊item{The onset clusters allowed are any of ◊i{◊ecl1{kpst}} followed by ◊i{◊ecl1{l}} or ◊i{◊ecl1{r}}, any of ◊i{◊ecl1{kst}} followed by ◊i{◊ecl1{w}}, or any of ◊i{◊ecl1{hklmnpst}} followed by ◊i{◊ecl1{y}}.}
  ◊item{◊i{◊ecl1{y}} is deleted before ◊i{◊ecl1{i}}, ◊i{◊ecl1{w}} is deleted before ◊i{◊ecl1{u}}, and ◊i{◊ecl1{r}} is deleted before ◊i{◊ecl1{ŗ}}.}
  ◊item{Both ◊i{◊ecl1{’}} and ◊i{◊ecl1{ŗ}} are forbidden after ◊i{◊ecl1{w}} and ◊i{◊ecl1{y}} and do not occur in suffixes originating in the ◊var{o} state.}
  ◊item{The codas allowed are ◊i{◊ecl1{l}}, ◊i{◊ecl1{m}}, ◊i{◊ecl1{n}}, ◊i{◊ecl1{r}}, and additionally ◊i{◊ecl1{k}} and ◊i{◊ecl1{s}} word-finally. The nasal codas assimilate to ◊i{◊ecl1{m}} before ◊i{◊ecl1{m}} or ◊i{◊ecl1{p}} and to ◊i{◊ec1{n}} before any other consonant.}
}

Note that ◊i{◊ecl1{ŗ}} is distinct from ◊i{◊ecl1{r’}}.

ECL1 resolves two types of ◊i{oginiþe cfarðerþ} when they occur across morpheme boundaries:

◊items{
  ◊item{◊term{Type I ◊i{oginiþe cfarðerþ}} consists of two identical consonants separated by an optional glide followed by a vowel other than ◊i{◊ecl1{ŗ}}.}
  ◊item{◊term{Type II ◊i{oginiþe cfarðerþ}} consists of two consecutive syllables with the same coda. (Note: ◊i{◊ecl1{m}} and ◊i{◊ecl1{n}} are considered different codas for this purpose.)}
}

Resolution of type II ◊i{oginiþe cfarðerþ} occurs from end to start and always changes the earlier coda.

◊table/x[#:options (table-options
    #:caption ◊@{Resolution of codas in type II ◊i{oginiþe cfarðerþ}.}
    #:num-header-cols 0
  )
  #:id "ocresolve2"]{
  Repeated coda & First coda changed to
  ◊ecl1{m} or ◊ecl1{n} & ◊ecl1{l}
  ◊ecl1{l} & ◊ecl1{r}
  ◊ecl1{r} & ◊ecl1{l}
}

Resolution of type I ◊i{oginiþe cfarðerþ} for consonants other than ◊i{◊ecl1{w}} or ◊i{◊ecl1{y}} occurs from end to start. If the first occurrence of the repeated consonant occurs word-initially, then the second consonant is changed; otherwise, the first consonant is changed.

◊table/x[#:options (table-options
    #:caption ◊@{Resolution of type I ◊i{oginiþe cfarðerþ} in non-initial positions.}
    #:num-header-cols 0
    #:colgroup '(c d)
  )
  #:id "ocresolve1ni"]{
  Repeated consonant & First occurrence changed to
  ◊ecl1{c} & ◊ecl1{k} if before ◊ecl1{i}; ◊ecl1{t} otherwise
  ◊ecl1{h} & ◊ec11{k}
  ◊ecl1{k} & ◊ecl1{h} if before ◊ecl1{i} or ◊ecl1{y}; ◊ecl1{t} otherwise
  ◊ecl1{l} & ◊ecl1{n} if before ◊ecl1{y}; ◊ecl1{r} otherwise
  ◊ecl1{m} & ◊ecl1{n} if before ◊ecl1{’} or ◊ecl1{y}; ◊ecl1{y} if before ◊ecl1{u}; ◊ecl1{w} otherwise
  ◊ecl1{n} & ◊ecl1{l} if before ◊ecl1{i} or ◊ecl1{y}; ◊ecl1{t} otherwise
  ◊ecl1{p} & ◊ecl1{t}
  ◊ecl1{r} & ◊ecl1{s} if in a single-consonant onset before ◊ecl1{’} or ◊ecl1{u}; ◊ecl1{∅} if in a multi-consonant onset before ◊ecl1{’}; ◊ecl1{y} if in a multi-consonant onset before ◊ecl1{u} or in the onset ◊ecl1{pr}; ◊ecl1{w} otherwise
  ◊ecl1{s} & ◊ecl1{n} if before ◊ecl1{’}; ◊ecl1{t} otherwise
  ◊ecl1{t} & ◊ecl1{s}
  ◊ecl1{x} & ◊ecl1{s} if preceded by a coda; ◊ecl1{l} otherwise
}

◊table/x[#:options (table-options
    #:caption ◊@{Resolution of type I ◊i{oginiþe cfarðerþ} in initial positions. The first option that does not cause the creation of type I or II ◊i{oginiþe cfarðerþ} is chosen.}
    #:num-header-cols 0
    #:colgroup '(c d)
  )
  #:id "ocresolve1i"]{
  Repeated consonant & Second occurrence changed to
  ◊ecl1{c} & ◊ecl1{s} or ◊ecl1{x}
  ◊ecl1{h} & ◊ecl1{k} or ◊ecl1{x}
  ◊ecl1{k} & ◊ecl1{t} or ◊ecl1{h}
  ◊ecl1{l} & ◊ecl1{r} if coda; otherwise ◊ecl1{r} or ◊ecl1{t}
  ◊ecl1{m} & ◊ecl1{l} if coda; otherwise ◊ecl1{r} or ◊ecl1{w}
  ◊ecl1{n} & ◊ecl1{l} if coda; otherwise ◊ecl1{r} or ◊ecl1{y}
  ◊ecl1{p} & ◊ecl1{t} or ◊ecl1{m}
  ◊ecl1{r} & ◊ecl1{n}/◊ecl1{m} if coda; otherwise ◊ecl1{w} or ◊ecl1{y}
  ◊ecl1{s} & ◊ecl1{x} or ◊ecl1{h}
  ◊ecl1{t} & ◊ecl1{r} or ◊ecl1{s}
  ◊ecl1{x} & ◊ecl1{y} or ◊ecl1{k}
}

Finally, for repeated consonants of ◊i{◊ecl1{w}} or ◊i{◊ecl1{y}}, then the second occurrence is always changed to ◊i{◊ecl1{∅}}, or to the other consonant if doing so would result in a sequence of two identical vowels. This occurs from start to end.

◊section{Syntax}

ECL1 has ◊var{n} types of clauses: ◊term{reactive clauses}, ◊term{stative clauses}, ◊term{interrogative clauses}, ◊term{topical clauses}, …

The words in a clause that mark its existence are considered ◊term{operators}.

◊subsection{Reactive clauses}

◊term{Reactive clauses} specify three (possibly empty) groups of noun phrases: ◊term{reactants}, ◊term{catalysts}, and ◊term{products} and have the meaning of “(catalyst) causes (reactant) to become (product)”.

◊gloss/x[#:lang "art-x-ecl1"]{
  ◊glfree{◊ecl1{samora mukato tleka.}}
  ◊gla{samor-a mukat-o tlek-a.}
  ◊glb{window-%dir hammer-%cat smithereens-%dir}
  ◊glfree{The hammer breaks the window.}
}

◊gloss/x[#:lang "art-x-ecl1"]{
  ◊glfree{◊ecl1{kahin maxa.}}
  ◊gla{kah-in maxa.}
  ◊glb{sun-%cat light-%dir}
  ◊glfree{The sun shines.}
}

The reactants and products are separated by the catalysts. Therefore, if no catalyst is specified, then the particle ◊i{◊ecl1{te}} takes its place.

More generally, there might be multiple groups of catalysts, in which case the clause indicates multiple ‘reactions’ chained together: ◊i{a b! c d! e} is short for ◊i{(a b! c) and (c d! e)}.

◊subsection{Stative clauses}

◊term{Stative clauses} specify two groups of noun phrases separated by a relational particle.

◊table/x[#:options (table-options
    #:caption ◊@{Relational particles in ECL1.}
  )
  #:id "relpart"]{
  Form & Meaning
  ◊ecl1{ma} & ◊var{A} equals ◊var{B} (if ◊var{B} is unspecified, then ◊var{A} exists)
  ◊ecl1{swe} & ◊var{A} does not equal ◊var{B}
  ◊ecl1{le} & ◊var{A} is a ◊var{B}
  ◊ecl1{twi} & ◊var{A} is not a ◊var{B}
  ◊ecl1{ce} & ◊var{A} is in the same place as ◊var{B}
  ◊ecl1{ya} & ◊var{A} is not in the same place as ◊var{B}
}

A special type of stative clause indicates that an action is ongoing. This consists of the particle ◊i{◊ecl1{syok}} followed by another clause.

◊subsection{Interrogative clauses}

◊term{Interrogative clauses} denote a question and consist of the particle ◊i{◊ecl1{kes}} followed by another clause.

◊gloss/x[#:lang "art-x-ecl1"]{
  ◊glfree{◊ecl1{kes kahin maxa?}}
  ◊gla{kes kah-in maxa?}
  ◊glb{%int sun-%cat light-%dir}
  ◊glfree{Does the sun shine?}
}

◊gloss/x[#:lang "art-x-ecl1"]{
  ◊glfree{◊ecl1{kes pene h’ yulo wara?}}
  ◊gla{kes pen-e h’ yul-o war-a?}
  ◊glb{%int fox-%cat . voice-%cat what-%dir}
  ◊glfree{What does the fox say?}
}

◊subsection{Topical clauses}

◊term{Topical clauses} introduce a topical noun phrase for the contained subclause. They consist of the topic, followed by ◊i{◊ecl1{la}} and the subclause:

◊gloss/x[#:lang "art-x-ecl1"]{
  ◊glfree{◊ecl1{malanku la h’ ce kalta h’ yoe h’ ce siwa.}}
  ◊gla{malank-u la h’ ce kalt-a h’ yo-e h’ ce siw-a.}
  ◊glb{kitten-%dir %top . %loc floor-%dir . leg-%cat . %loc above-%dir}
  ◊glfree{The kitten jumped up.}
}

◊subsection{Nesting clauses}

A clause appearing where a direct-case noun phrase is expected is interpreted as a content clause by default.

Operators have a defined ◊term{precedence} that determines how clauses are grouped by default, with juxtaposition of noun phrases being assigned to 0:

◊table/x[#:options (table-options
    #:caption ◊@{Operator precedence in ECL1.}
  )
  #:id "opprec"]{
  Precedence & Type of operator
  0 & Juxtaposition of noun phrases
  ◊rowspan[2]{−2} & Catalyst of reactive clause
                  & Relational particle of stative clause
  −3 & The continuous particle ◊i{◊ecl1{syok}}
  −4 & The interrogative particle ◊i{◊ecl1{kes}}
  −5 & The topical particle ◊i{◊ecl{la}}
}

Two occurrences of operators of the same precedence are considered to have indeterminate precedence with respect to each other; barring any rules allowing for chaining, their precedence relative to each other must be disambiguated via explicit grouping.

ECL1 groups subclauses using a technique similar to ◊link["https://en.wikipedia.org/wiki/Principia_Mathematica#An_introduction_to_the_notation_of_%22Section_A_Mathematical_Logic%22_(formulas_%E2%9C%B11%E2%80%93%E2%9C%B15.71)"]{◊cite[#:lang "la"]{Principia Mathematica}’s dot notation}. Each delimiter has an integer ◊term{order}; the primitive delimiters are as follows:

◊table/x[#:options (table-options
    #:caption ◊@{Primitive grouping delimiters in ECL1.}
  )
  #:id "primgd"]{
  Order & Form
  2 & ◊ecl1{p’}
  1 & ◊ecl1{s’}
  0 & ◊ecl1{h’}
  −1 & ◊ecl1{tŗ}
  −2 & ◊ecl1{sŗ}
  −3 & ◊ecl1{kŗ}
}

Delimiters of further order are expressed by combining the primitive delimiters in balanced senary, with the least significant digit coming first: e.g. 7 → ◊i{◊ecl1{s’x’}}, 10 → ◊i{◊ecl1{sŗp’}}. A sequence of ◊i{◊ecl1{◊var{C}ŗ}} + ◊i{◊ecl1{h’}} is reduced to ◊i{◊ecl1{◊var{C}r’}}.

To parse a sentence, first find the highest order of the delimiters used. Convert each delimiter of that order to ◊i{(} if immediately after an operator, ◊i{)} if immediately before an operator, or ◊i{)(} if not adjacent to any operator. Add opening parentheses to the start of the sentence or closing parentheses to the end as needed. Perform this recursively on any subclauses.

Examples:

◊gloss/x[#:lang "art-x-ecl1"]{
  ◊glfree{◊ecl1{ako h’ samora mukato tleka.}}
  ◊gla{ak-o h’ samor-a mukat-o tlek-a.}
  ◊glb{person-%cat . window-%dir hammer-%cat smithereens-%dir}
  ◊glfree{The person breaks the window with a hammer.}
}

◊gloss/x[#:lang "art-x-ecl1"]{
  ◊glfree{◊ecl1{syok h’ orsu ya kalta h’ te h’ ce s’ te unu.}}
  ◊gla{syok h’ ors-u ya kalt-a h’ te h’ ce s’ te un-u.}
  ◊glb{%cont . water-%dir %loc.%neg floor-%dir . %cat . %loc : %cat nothingness-%dir}
  ◊glfree{It has stopped raining.}
}

◊section{Morphology}

◊subsection{Nouns}

Nouns have the following form: ◊b{<stem> + [numeral] + [demonstrative] + <case>}.

The stem spans from ◊var{s} to ◊var{o}.

The case marker determines the role of the noun. Importantly, it distinguishes between non-catalysts and catalysts:

◊table/x[#:options (table-options
    #:caption ◊@{Case markers in ECL1.}
  )
  #:id "casesuffix"]{
  Case \ Paradigm & I & II & III
  Direct & ◊ecl1{-a} & ◊ecl1{-i} & ◊ecl1{-u}
  Catalytic & ◊ecl1{-o} & ◊ecl1{-e} & ◊ecl1{-in}
  Possessive & ◊ecl1{-am} & ◊ecl1{-im} & ◊ecl1{-om}
}

◊section{Lexicon}

◊subsection{Compounding}

Two (or more) nouns can be joined together to form a compound noun. The first noun is the ‘modifier’ and uses the direct case ending; the second noun is the ‘head’. This process is productive, most often being used with a relational noun as the head. For instance, ◊i{◊ecl1{luma}} ‘cloud’ and ◊i{◊ecl1{siwa}} ‘location above’ can be combined into ◊i{◊ecl1{lumasiwa}} ‘location above the clouds’. Note that ◊i{oginiþe cfarðerþ} must be resolved: ◊i{◊ecl1{orsu}} ‘water’ + ◊i{◊ecl1{swata}} ‘surface’ → ◊i{◊ecl1{or◊b{t}uswata}} ‘surface of water’.

◊subsection{Derivation}

◊subsubsection{Diminuitives}

For denoting offspring: Noun → noun, appending ◊i{◊ecl1{-nku}}.

◊subsection{List of words}

◊dl{
  ◊dt{◊ecl1{aka}}◊dd{◊i{n.} person}◊;
  ◊dt{◊ecl1{hatyu}}◊dd{◊i{n.} robin}◊;
  ◊dt{◊ecl1{kahu}}◊dd{◊i{n.} sun}◊;
  ◊dt{◊ecl1{kalta}}◊dd{◊i{n.} ground, floor}◊;
  ◊dt{◊ecl1{luma}}◊dd{◊i{n.} cloud}◊;
  ◊dt{◊ecl1{mala}}◊dd{◊i{n.} cat}◊;
  ◊dt{◊ecl1{malanku}}◊dd{◊i{n.} kitten}◊;
  ◊dt{◊ecl1{maxa}}◊dd{◊i{n.} light}◊;
  ◊dt{◊ecl1{mukata}}◊dd{◊i{n.} hammer}◊;
  ◊dt{◊ecl1{orsu}}◊dd{◊i{n.} water}◊;
  ◊dt{◊ecl1{peni}}◊dd{◊i{n.} fox}◊;
  ◊dt{◊ecl1{samora}}◊dd{◊i{n.} window}◊;
  ◊dt{◊ecl1{siwa}}◊dd{◊i{n.} location above something}◊;
  ◊dt{◊ecl1{swata}}◊dd{◊i{n.} surface on top of something}◊;
  ◊dt{◊ecl1{tleka}}◊dd{◊i{n.} smithereens}◊;
  ◊dt{◊ecl1{unu}}◊dd{◊i{n.} nothingness}◊;
  ◊dt{◊ecl1{wara}}◊dd{◊i{n.} what?}◊;
  ◊dt{◊ecl1{yoi}}◊dd{◊i{n.} leg}◊;
  ◊dt{◊ecl1{yula}}◊dd{◊i{n.} voice, ability to speak}◊;
}
