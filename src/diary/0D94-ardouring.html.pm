#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{+merlan #flirora’s music production diary}
◊define-meta[date]{2022-10-19}

◊section[#:id "0d94"]{◊render-date{2022-10-19}}

With a whiff of ◊render-date{2022-10-18}.

Installed tkPacman to quickly go over all the LV2 plugins available in the Arch repositories, then installed Ardour and a handful of plugins.

◊make-figure["images/herewegoagain.png" #:alt "A screenshot of the “Welcome to Ardour” window, with somewhat off-kilter text."]{Here we go again.}

Started a new project called ◊codevar{vivian_0x00} and opened the ◊link["https://manual.ardour.org/toc/"]{official manual} and ◊link["https://brunoruviaro.github.io/ardour4-tutorial/"]{Bruno Ruviaro’s tutorial} as it’s been a long time since I last used this program. I also have the PDF of the vivian_0x00 score open.

First thoughts: the contrast for disabled items in the menu is much too low. Also, there’s no light theme available.

I plotted out the first 18 measures. Not entirely satisfied with the patch for the bass line, and I’m encountering noise issues with the lead.

◊section[#:id "0d95"]{◊render-date{2022-10-20}}

Apparently, Ardour 7.0 was only released on ◊render-date{2022-10-14}, so I was lucky enough to happen to finish writing lyrics after then.

Spent a lot of time chasing down the bugs I encountered yesterday. First, Ardour sometimes doesn’t seem to save settings in Surge 1.9 (at least for its LV2 version) properly, sometimes going as far as crashing on load. Switching to Surge XT for all the tracks took a bit of work but solved these problems, as well some of the noise issues.

I also found that Ardour would hang upon exiting. I suspected that it might have to do with the following error message printed: ◊samp{'client != NULL' failed at ../pipewire/pipewire-jack/src/pipewire-jack.c:6188 jack_client_stop_thread()}. I submitted a ◊link["https://github.com/Ardour/ardour/pull/741"]{pull request} to fix that particular message, but it didn’t resolve the hanging issue. Fortunately, ◊link["https://tracker.ardour.org/view.php?id=8768"]{this bug is known}, and it’s relatively benign.

Anyway, continued to the end of Chorus 1b for the instrumental. Still a bit lackluster in my opinion.

◊section[#:id "0d96"]{◊render-date{2022-10-21}}

Added drum parts, as well as going further into the song. Meanwhile, I happened to find ◊link["https://tracker.ardour.org/view.php?id=9019"]{another bug}, which was quickly fixed.

◊section[#:id "0d98"]{◊render-date{2022-10-23}}

Continuing to work on the instrumental.

◊section[#:id "0d99"]{◊render-date{2022-10-24}}

Finished the instrumental; now tuning the .svp again with it.
