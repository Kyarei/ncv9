#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{The case governed by locational relationals}
◊define-meta[date]{2021-10-05}

When this entry is written, relationals that show location, such as ◊l1{es} ◊trans{inside} or ◊l1{desa} ◊trans{below} govern the dative case:

◊gloss/x{
  ◊glfree{◊nc{nôra serin tfeldarnirþ ginas desa doaŋaras lerþil eleþes mâroþ.}}
  ◊gla{nôr-a ser-in tfeldarn-irþ gin-as desa do-aŋar-as lerþ-il eleþ-es mâr-o-þ.}
  ◊glb{small-%rel.%nom,%nom.%cel seed-%nom.%pl endure-%ser snow-%dat.%sg under %caus-warm-%rel.%nom,%dat.%cel spring-%gen.%sg sun-%dat.%sg wait-3%pl-%past}
  ◊glfree{The little seeds waited patiently under the snow for the warm spring sun.}
}

In ŊCv7, on the other hand, such relationals governed the locative case.

I have been noticing that the locative case is not used much in ŊCv9. This trend is especially unsettling since the locative forms of nouns tend to have interesting stem changes. To verify this hypothesis, I used the following Bash command to find mentions of each case in glosses in the Pollen markup files: ◊kbd{for c in nom acc dat gen loc inst abess sembl; do echo -n "$c: "; grep -E "%$c\\b" src/texts/◊var{<file name>}.html.pm | wc -l; done}

◊table/x[#:options (table-options #:caption "The number of times each case was mentioned in glosses of various sentence sets. These count any mention of a case in glosses, such as in declined nouns and in infinitive particles.") #:id "case-counts"]{
  Case & CSTC (#1 – #67) & Zaslon & Moyashi
  Nominative & 44 & 19 & 21
  Accusative & 18 & 49 & 45
  Dative & 22 & 16 & 9
  Genitive & 12 & 16 & 3
  Locative & 14 & 9 & 1
  Instrumental & 5 & 0 & 0
  Abessive & 0 & 0 & 0
  Semblative & 0 & 0 & 0
}

Note the difference between the CSTC and the apple sentence sets: in the latter, the subject in many sentences is pronominal, such that most of the Ŋarâþ Crîþ translations lack an explicit subject. On the other hand, many sentences in the CSTC have non-pronominal subjects, so the nominative case is used more often in these sentences.

Still deciding whether to align ŊCv9 behavior with ŊCv7 in this respect.
