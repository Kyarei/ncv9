#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{The NCS blog has opened!}
◊define-meta[date]{2013-05-26}

◊archive-note["https://necarasso-cryssesa.blogspot.com/2013/05/the-ncs-blog-has-opened.html"]{the Necarasso Cryssesa blog}

(Sorry for not posting this in the language.)

A blog fora ◊i{[sic]} constructed language has opened! In the future, we will add links to later versions of the documentation, as well as make some witty comments.
