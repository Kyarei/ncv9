#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Appendix to the Ŋarâþ Crîþ submission for the Segments #04 translation challenge}
◊define-meta[date]{2022-01-22}

The following example sentences were originally part of the explanation for the translation but have been cut from the final version. (Retypeset into Pollen from the LaTeX source.)

◊section{Usage of ◊l1{porat} versus ◊l1{vrelat}}

The usage of ◊l1{porat} ◊trans{◊S has a large cross-section; ◊S has a large body mass} as opposed to ◊l1{vrelat} ◊trans{◊S (lamina) is thick; ◊S (voice) is deep} was mentioned in (4) of the article. The following examples were originally provided but were left out of the final article for brevity.

◊gloss/x[#:id "porat1"]{
  ◊glfree{◊nc{edeson a gcatlat selcit melco porelon c·jajanaros catlelpeþ.}}
  ◊gla{edes-on a g\catl-at selc-it melc-o por-elon c·ja-janar-os catl-el-pe-þ.}
  ◊glb{assignment-%acc.%sg %inf.%loc (%inf\)complete-%inf knowingly_fail_to_fulfill-%inf parallel_parent-%nom.%sg thick-%rel.%nom,%inst.%ter %inst-stick-%inst.%sg hit-3%sg.%inf-1%sg-%past}
  ◊glfree{Whenever I neglected my duty to do my homework, my (mother/father) would hit me with a thick rod.}
}

◊gloss/x[#:id "porat2"]{
  ◊glfree{◊nc{lê g*corten porerþ avanto fesenvagarpo.}}
  ◊gla{lê g\*cort-en por-erþ avant-o fe-sen-va-garpo.}
  ◊glb{this.%cel pipe-%gen.%sg thickness-%nom.%sg avanta-%nom.%pl eight-two-thirteen-%approx.16²}
  ◊glfree{The thickness (i.e. diameter) of this pipe is ◊${8.2D_{16} \times 16^{-2}} ◊i{avanto}.}
}

◊gloss/x[#:id "vrelat1"]{
  ◊glfree{◊nc{vandals nedo vrelen rešitan elpalta.}}
  ◊gla{vand-als nedo vrel-en rešit-an elp-al-ta.}
  ◊glb{summer-%dat.%sg despite thick-%rel.%nom,%acc.%cel cloak-%acc.%sg wear-3%sg.%inv-%past}
  ◊glfree{Although it was summer, she was wearing a thick cloak.}
}

◊gloss/x[#:id "vrelat2"]{
  ◊glfree{◊nc{celmas vrele so’moc le nemiren cengrit garasle.}}
  ◊gla{celm-as vrel-e so=’moc le nem-iren cengr-it gar-as-le.}
  ◊glb{window-%nom.%sg thick-3%sg if=also %imp apple-%acc.%pl throw-%inf refrain-2%sg-%past}
  ◊glfree{Even if the window is thick, don’t throw apples at it.}
}

◊section{Usage of ◊l1{serpeþ} versus ◊l1{espel} versus ◊l1{varon}}

The usage of ◊l1{serpeþ} ◊trans{the collective actions that make up ◊|gen#|’s way of life} versus ◊l1{espel} ◊trans{the period from ◊|gen#|’s birth to ◊|gen#|’s death; the experience that ◊|gen#| has during their existence} and ◊l1{varon} ◊trans{the state of being alive rather than dead; vital force causing ◊|gen#| to be alive} was mentioned in (12) of the article. Again, the following examples were originally provided but omitted from the final version.

◊gloss/x[#:id "espel1"]{
  ◊glfree{◊nc{respos’or arnendan enfehenrotocjan nêrgaþ.}}
  ◊gla{resp-os=’or arnend-an en-fe-henroto-cjan nêrg-a-þ.}
  ◊glb{ESPEL-%loc.%sg=%poss.3.%hum music-%acc.%col three-eight-%approx.16²-%cl.composition create-3%sg-%past}
  ◊glfree{During his lifetime, he composed about nine hundred pieces of music.}
}

◊gloss/x[#:id "espel2"]{
  ◊glfree{◊nc{le lenser; melco aspos dovretepe!}}
  ◊gla{le lens-er; melc-o asp-os do-vret-e-pe!}
  ◊glb{%imp help-2%pl parallel_parent-%nom.%sg ESPEL-%dat.%sg %caus-dislike-3%sg-1%sg}
  ◊glfree{Help, my (mom/dad) is making me hate my life!}
}

◊gloss/x[#:id "varon1"]{
  ◊glfree{◊nc{a sâna ŋiłit varos’moc rille mitreþ.}}
  ◊gla{a sân-a ŋił-it var-os=’moc rille mitr-e-þ.}
  ◊glb{%inf.%loc bear-%nom.%sg shout-%inf VARON-%dat.%di=also on_behalf_of run-1%sg-%past}
  ◊glfree{When the bear roared, I ran for as much as my life.}
}

◊gloss/x[#:id "varon2"]{
  ◊glfree{◊nc{ŋjârevi varos elteþ tfel ardan senralta.}}
  ◊gla{ŋjâr-evi var-os elt-eþ tfel ard-an senr-al-ta.}
  ◊glb{farmer-%gen.%pl VARON-%dat.%di river-%dat.%sg across dragon-%nom.%sg threaten-3%sg.%inv-%past}
  ◊glfree{The farmers’ lives were threatened by the dragon across the river (i.e. the dragon could kill the farmers).}
}

◊gloss/x[#:id "serpeth1"]{
  ◊glfree{◊nc{ŋjârevi serpeþes nasinora senralta.}}
  ◊gla{ŋjâr-evi serp-eþes nasinor-a senr-al-ta.}
  ◊glb{farmer-%gen.%pl SERPEÞ-%dat.%di drought-%nom.%sg threaten-3%sg.%inv-%past}
  ◊glfree{The farmers’ livelihoods were threatened by the drought (i.e. the drought would reduce the amount of crops they could grow).}
}


