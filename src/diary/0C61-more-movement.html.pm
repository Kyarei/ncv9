#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Movement in the land of modifiers}
◊define-meta[date]{2021-12-16}

◊link["0BED-quantifier-scoping.html"]{Previously,} I proposed a model in which each argument of a sentence is moved out of the ‘deep’ structure in order to ascertain how quantifier scoping worked. However, I looked only at simple ‘fish eat flowers’-type sentences in that post. In particular, I didn’t look at verbal or nominal adjuncts.

Let’s start with a sentence, shall we?

◊gloss/x[#:id "mmrng"]{
  ◊glfree{◊nc{mervos mjôr ragor navan gcecþal.}}
  ◊gla{merv-os mjôr-∅ rag-or nav-an g\cecþ-al.}
  ◊glb{large-%rel.%nom,%nom.%ter wolf-%nom.%sg tall-%rel.%nom,%acc.%hum human-%acc.%sg %pfv\bite-3%sg.%inv}
  ◊glfree{The big wolf bites the tall human.}
}

In terms of word order, we have to put the verb, ◊l1{gcecþal}, at the end of the sentence. In addition, ◊l1{mervos} must precede ◊l1{mjôr}, and ◊l1{ragor} must precede ◊l1{navan}. Otherwise, however, we are free to put the words in whatever order we please. The following sentences are valid permutations of ◊xref/p{mmrng}:

◊enum{
  ◊item{◊nc{mervos mjôr ragor navan gcecþal.}}
  ◊item{◊nc{ragor navan mervos mjôr gcecþal.}}
  ◊item{◊nc{mervos ragor mjôr navan gcecþal.}}
  ◊item{◊nc{ragor mervos navan mjôr gcecþal.}}
  ◊item{◊nc{mervos ragor navan mjôr gcecþal.}}
  ◊item{◊nc{ragor mervos mjôr navan gcecþal.}}
}

The deep structure of ◊xref/p{mmrng} is the following (with intermediate projections not shown for convenience):

◊syntax-tree[#:caption ◊@{The deep structure of ◊xref/p{mmrng}.}
`(gICP
  (NP
    (Rel ,(labeled-node 'trace-1 (trace 1)) (VP (V "mervos")))
    (N ,(labeled-node 'np-mjôr "mjôr")))
  (VP
    (NP 
      (Rel ,(labeled-node 'trace-2 (trace 2)) (VP (V "ragor")))
      (N ,(labeled-node 'np-navan "navan")))
    (V "gcecþal")))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'np-mjôr)
    (stxadd-arrow 'trace-2 'np-navan)))]

Again, it doesn’t really matter whether there are any intermediate nodes between the gICP and the V.

What does Ord1 look like? We could simply move the whole noun phrase ◊l1{mervos mjôr}, followed by ◊l1{ragor navan}. But this is an oversimplification, as there are valid orders (Ord3 – Ord6) that can’t be derived this way.

◊syntax-tree[#:caption ◊@{◊xref/p{mmrng} after moving the whole noun phrases.}
`(
  gICP
  ,(labeled-node 'np-acc
    (boxed-node
      `(NP 
        (Rel ,(labeled-node 'trace-2 (trace 2)) (VP (V "ragor")))
        (N ,(labeled-node 'np-navan "navan")))))
  (
    gICP
    ,(labeled-node 'np-nom
      (boxed-node
        `(NP
          (Rel ,(labeled-node 'trace-1 (trace 1)) (VP (V "mervos")))
          (N ,(labeled-node 'np-mjôr "mjôr")))))
    (gICP
      (NP
        ,(labeled-node 'trace-3 (trace 3))
      (VP
        ,(labeled-node 'trace-4 (trace 4))
        (V "gcecþal"))))))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'np-mjôr)
    (stxadd-arrow 'trace-2 'np-navan)
    (stxadd-arrow 'trace-3 'np-nom)
    (stxadd-arrow 'trace-4 'np-acc)))]

To get Ord3 – Ord 6, we need to do something with those pesky modifiers before moving the main noun phrases out of the clause. First, we move out ◊l1{mervos}:

◊syntax-tree[#:caption ◊@{The deep structure of ◊xref/p{mmrng}, after ◊l1{mervos} is moved out.}
`(gICP
  ,(labeled-node 'mod-mervos
    (boxed-node
      `(Rel ,(labeled-node 'trace-1 (trace 1)) (VP (V "mervos")))))
  (gICP
    (NP
      ,(labeled-node 'trace-3 (trace 3))
      (N ,(labeled-node 'np-mjôr "mjôr")))
    (VP
      (NP 
        (Rel ,(labeled-node 'trace-2 (trace 2)) (VP (V "ragor")))
        (N ,(labeled-node 'np-navan "navan")))
      (V "gcecþal"))))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'np-mjôr)
    (stxadd-arrow 'trace-2 'np-navan)
    (stxadd-arrow 'trace-3 'mod-mervos)))]

Now we can move out the noun phrase containing ◊l1{mjôr} because it has no separable modifiers:

◊syntax-tree[#:caption ◊@{The deep structure of ◊xref/p{mmrng}, arter ◊l1{mervos} and ◊l1{mjôr} are moved out.}
`(gICP
  ,(labeled-node 'mod-mervos
    (boxed-node
      `(Rel ,(labeled-node 'trace-1 (trace 1)) (VP (V "mervos")))))
  (gICP
    ,(labeled-node 'noun-mjôr
      (boxed-node
        `(NP
          ,(labeled-node 'trace-3 (trace 3))
           (N ,(labeled-node 'np-mjôr "mjôr")))))
    (gICP
      ,(labeled-node 'trace-4 (trace 4))
      (VP
        (NP 
          (Rel ,(labeled-node 'trace-2 (trace 2)) (VP (V "ragor")))
          (N ,(labeled-node 'np-navan "navan")))
        (V "gcecþal")))))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'np-mjôr)
    (stxadd-arrow 'trace-2 'np-navan)
    (stxadd-arrow 'trace-3 'mod-mervos)
    (stxadd-arrow 'trace-4 'noun-mjôr)))]

We do the same for the other noun phrase.

◊syntax-tree[#:caption ◊@{The structure of Ord1.}
`(gICP
  ,(labeled-node 'mod-mervos
    (boxed-node
      `(Rel ,(labeled-node 'trace-1 (trace 1)) (VP (V "mervos")))))
  (gICP
    ,(labeled-node 'noun-mjôr
      (boxed-node
        `(NP
          ,(labeled-node 'trace-3 (trace 3))
          (N ,(labeled-node 'np-mjôr "mjôr")))))
    (gICP
      ,(labeled-node 'mod-ragor
        (boxed-node
          `(Rel ,(labeled-node 'trace-2 (trace 2)) (VP (V "ragor")))))
      (gICP
        ,(labeled-node 'noun-navan
          (boxed-node
            `(NP
              ,(labeled-node 'trace-5 (trace 5))
              (N ,(labeled-node 'np-navan "navan")))))
        (gICP
          ,(labeled-node 'trace-4 (trace 4))
          (VP
            ,(labeled-node 'trace-6 (trace 6))
            (V "gcecþal")))))))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'np-mjôr)
    (stxadd-arrow 'trace-2 'np-navan)
    (stxadd-arrow 'trace-3 'mod-mervos)
    (stxadd-arrow 'trace-4 'noun-mjôr)
    (stxadd-arrow 'trace-5 'mod-ragor)
    (stxadd-arrow 'trace-6 'noun-navan)))]

This theory works with any sentence. Here’s what Ord6 looks like:

◊syntax-tree[#:caption ◊@{The structure of Ord6.}
`(gICP
  ,(labeled-node 'mod-ragor
    (boxed-node
      `(Rel ,(labeled-node 'trace-2 (trace 2)) (VP (V "ragor")))))
  (gICP
    ,(labeled-node 'mod-mervos
      (boxed-node
        `(Rel ,(labeled-node 'trace-1 (trace 1)) (VP (V "mervos")))))
    (gICP
      ,(labeled-node 'noun-mjôr
        (boxed-node
          `(NP
            ,(labeled-node 'trace-4 (trace 4))
            (N ,(labeled-node 'np-mjôr "mjôr")))))
      (gICP
        ,(labeled-node 'noun-navan
          (boxed-node
            `(NP 
              ,(labeled-node 'trace-3 (trace 3))
              (N ,(labeled-node 'np-navan "navan")))))
        (gICP
          ,(labeled-node 'trace-5 (trace 5))
          (VP
            ,(labeled-node 'trace-6 (trace 6))
            (V "gcecþal")))))))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'np-mjôr)
    (stxadd-arrow 'trace-2 'np-navan)
    (stxadd-arrow 'trace-3 'mod-ragor)
    (stxadd-arrow 'trace-4 'mod-mervos)
    (stxadd-arrow 'trace-5 'noun-mjôr)
    (stxadd-arrow 'trace-6 'noun-navan)))]

From the example above, we can propose the following rules:

◊items{
  ◊item{All arguments and nominal adjuncts of a verb must be moved out.}
  ◊item{All separable modifiers to a constituent must be moved out before the constituent itself (such as ◊l1{mervos} in ◊l1{mervos mjôr}, but not, for instance, ◊l1{om} in ◊l1{om mjôr}).}
}

It’s straightforward to come up with sentences in which these rules break down. Take this sentence:

◊gloss/x[#:id "lvvav"]{
  ◊glfree{◊nc{lê vrelen vrêman #ageþne vaðeþ.}}
  ◊gla{lê vrel-en vrêm-an #ageþn-e vað-e-þ.}
  ◊glb{this.%cel thick-%rel.%nom,%acc.%cel book-%acc.%sg (name)-%nom.%sg buy-3%sg-%past}
  ◊glfree{This thick book was bought by #ageþne.}
}

with the following deep structure:

◊syntax-tree[#:caption ◊@{The deep structure of ◊xref/p{lvvav}.}
`(gICP
  (NP (N "#ageþne"))
  (VP
    (NP
      (Dem lê)
      (N′
        (Rel ,(labeled-node 'trace-1 (trace 1)) (VP (V "vrelen")))
        (N ,(labeled-node 'noun-vrêman "vrêman"))))
    (V "vaðeþ")))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'noun-vrêman)))]

The problem is that ◊l1{vrelen} has to be moved out before the NP node containing ◊l1{lê vrelen vreman}, while ◊l1{lê} is not moved out at all. As a result, ◊l1{lê} is constrained to be adjacent to ◊l1{vrêman}.

The fix here is easy: just make it optional to move out separable modifiers.

Next, Ŋarâþ Crîþ has separable ◊em{post}modifiers – namely, long numerals. To account for them, we would have to retain them in the deep structure even after the noun phrase they reside in are moved out. This sounds like a Royal Pain™, so I’ll pass on it.

Perhaps separable modifiers don’t move like verb arguments, but are rather moved in a ‘postprocessing’ phase, where they can be moved in any way that respects their order relative to their heads and doesn’t change the head of any modifier.

Last of all, let’s return to the original purpose of the NP-movement model: formalizing the semantics of quantifiers. What if a quantifier occurs deeper in a noun phrase?

◊gloss/x[#:id "shnccelh"]{
  ◊glfree{◊nc{šinen navif cinčef carþcenvu eþit łanu.}}
  ◊gla{šin-en nav-if cinč-ef carþ-cenv-u eþ-it łan-u.}
  ◊glb{all-%gen.%sg human-%gen.%gc name-%loc.%gc carþ-letter-%nom.%gc exist-%inf must-3%gc}
  ◊glfree{All names of people must contain a ◊i{carþ}.}
}

with the following deep structure:

◊syntax-tree[#:caption ◊@{The deep structure of ◊xref/p{shnccelh}.}
`(gICP
  (NP (N "carþcenvu"))
  (VP
    (V′
      (NPl
        (NPg
          (Det "šinen")
          (N "navif"))
        (N "cinčef"))
      (V′
        (Vinf "eþit")
        (V "łanu")))))
#:additions
(stxtree-add
  #:arrows (list))]

Assume that the determiner ◊l1{šinen} plus a noun phrase acts like the pronoun ◊l1{šino} itself, just with a restricted domain. Then we have to move the NPg node for it to have effect, even though it is not separable. If we assume that the NPg node moves in this way, then we have to move the NPl node out immediately afterward:

◊syntax-tree[#:caption ◊@{The deep structure of ◊xref/p{shnccelh}, after moving out the locative NP.}
`(gICP
  ,(labeled-node 'np-šinen-navif
    (boxed-node
      `(NPg
        (Det "šinen")
        (N "navif"))))
  (gICP
    ,(labeled-node 'np-cinčef
      (boxed-node
        `(NPl
          ,(labeled-node 'trace-1 (trace 1))
          (N "cinčef"))))
    (gICP
      (NP (N "carþcenvu"))
      (VP
        (V′
          ,(labeled-node 'trace-2 (trace 2))
          (V′
            (Vinf "eþit")
            (V "łanu")))))))
#:additions
(stxtree-add
  #:arrows (list
    (stxadd-arrow 'trace-1 'np-šinen-navif)
    (stxadd-arrow 'trace-2 'np-cinčef)))]

This model works, but I don’t find it particularly elegant, with its exception for inseparable modifiers containing quantifiers. I’m now out of willpower to touch anything related to theoretical syntax for the next few days, so I’ll wrap it up for now.
