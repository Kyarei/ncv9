#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Quantifier scoping in Ŋarâþ Crîþ}
◊define-meta[date]{2021-08-22}

The question of quantifier scoping has plagued Ŋarâþ Crîþ since the v7 days. Consider the following example:

◊gloss/x[#:id "qs1"]{
  ◊glfree{◊nc{šinof ħelit rjotu.}}
  ◊gla{šin-of ħel-it rjot-u.}
  ◊glb{all-%nom.%gc do_this-%inf cannot-3%gc}
  ◊glfree[#:prefix "(a) "]{It is not possible for everyone to do this. = Not everyone can do this.}
  ◊glfree[#:prefix "(b) "]{For everyone, it is not possible for them to do this. = No one can do this.}
}

The two interpretations depend on whether an auxiliary verb or a quantifier has a higher scope: (a) has the auxiliary above the quantifier, and (b) has the quantifier above the auxiliary.

This order could vary depending on how ◊l1{rjotat} was defined. In Ŋarâþ Crîþ v7, ◊l1{rjotat} was defined as “◊Š fails to, is unable to, cannot ◊|T|”, favoring (b) over (a). If it had been defined as “◊T is not possible” instead, then the definition might have led to interpretation (a).

This question is reminiscent of the interpretation of the English sentence ‘everyone cannot do this’: does it mean ‘not everyone can do this’ or ‘no one can do this’? (Personally, I don’t use the ambiguous version at all.)

Note that the differences in the two interpretations are not important for some auxiliaries:

◊gloss/x[#:id "qs2"]{
  ◊glfree{◊nc{šinof ħelit cenmiru.}}
  ◊gla{šin-of ħel-it cenmir-u.}
  ◊glb{all-%nom.%gc do_this-%inf seem_to-3%gc}
  ◊glfree[#:prefix "(a) "]{It seems that everyone does this.}
  ◊glfree[#:prefix "(b) "]{For everyone, it seems that they do this.}
}

◊gloss/x[#:id "qs3"]{
  ◊glfree{◊nc{šine ħelit ħari.}}
  ◊gla{šin-e ħel-it ħar-i.}
  ◊glb{all-%nom.%pl do_this-%inf do_again-3%pl}
  ◊glfree[#:prefix "(a) "]{It seems that everyone is doing this again.}
  ◊glfree[#:prefix "(b) "]{For everyone, it seems that they are doing this again.}
}

The problem is the most significant for negative verbs, such as ◊l1{rjotat} ◊trans{fail to, cannot}, ◊l1{pečit} ◊trans{avoid}, or ◊l1{tersat} ◊trans{refuse to}.

What if the quantifier is in an object position?

◊gloss/x[#:id "qs4"]{
  ◊glfree{◊nc{šinai lemsat rjota.}}
  ◊gla{šin-ai lems-at rjot-a.}
  ◊glb{all-%dat.%pl help-%inf cannot-1%sg}
  ◊glfree[#:prefix "(a) "]{There are some people I can’t help.}
  ◊glfree[#:prefix "(b) "]{There is no one I can help.}
}

Initially, I thought that (a) was the more natural interpretation, suggesting that subjects and objects were treated differently in Ŋarâþ Crîþ, but this hypothesis is complicated by the fact that Ŋarâþ Crîþ does not distinguish between verbal and clausal coordination:

◊gloss/x[#:id "coordv"]{
  ◊glfree{◊nc{lârinčin nôrime mitra.}}
  ◊gla{lârinč-in nôr-ime mitr-a.}
  ◊glb{kitten-%nom.%sg small-and fast-3%sg}
  ◊glfree{The kitten is small and fast.}
}

◊gloss/x[#:id "coordc"]{
  ◊glfree{◊nc{#saþo marime #flirora vareša.}}
  ◊gla{#saþ-o mar-ime #fliror-a vareš-a.}
  ◊glb{%name-%nom.%sg say-and %name-%nom.%sg listen-3%sg}
  ◊glfree{#saþo speaks and #flirora listens.}
}

◊; TODO: figure out a solution to have math in both HTML and PDF targets
Fortunately, however, the ŊCv7 grammar, clearly states that “[t]he pronouns ◊l1{šino} ◊trans{all} and ◊l1{nema} ◊trans{all} are special: they transform predicates such that ◊var{P}(◊nc{šino}) ⟺ ∀◊var{x} : ◊var{P}(◊var{x}) and ◊var{P}(◊nc{nema}) ⟺ ∃◊var{x} : ◊var{P}(◊var{x}).” In other words, (b) is the correct interpretation for both ◊xref/p{qs1} and ◊xref/p{qs4}.

This statement, however, neglects the possibility of other noun phrases preceding a quantifier:

◊gloss/x[#:id "qs5"]{
  ◊glfree{◊nc{#saþo šinai lemsat rjote.}}
  ◊gla{#saþ-o šin-ai lems-at rjot-e.}
  ◊glb{%name-%nom.%sg all-%dat.%pl help-%inf cannot-3%sg}
  ◊glfree[#:prefix "(a) "]{There are some people #saþo can’t help.}
  ◊glfree[#:prefix "(b) "]{There is no one #saþo can help.}
}

◊section{Argument movement}

Recently, I’ve been thinking of a model in which nominal arguments are moved out of core clause. Let’s see how this applies to this sentence:

◊gloss/x[#:id "daedalvs"]{
  ◊glfree{◊nc{telu tovrafen mênču.}}
  ◊gla{tel-u tovr-afen mênč-u.}
  ◊glb{fish-%nom.%gc flower-%acc.%gc eat-3%gc}
  ◊glfree{Fish eat flowers.}
}

Before any movement, the sentence looks roughly like this:

◊syntax-tree[#:caption ◊@{◊xref/p{daedalvs}, before any movement.}
`(
  gICP
    (NP (N "telu"))
    (VP
      (NP (N "tovrafen"))
      (V "mênču"))
)]

The particular structure of the unmoved sentence isn’t important here, but I had to use an arbitrary representation.

The first noun phrase to be moved is also the first one that appears in the sentence – in this case, ◊l1{telu}.

◊syntax-tree[#:caption ◊@{◊xref/p{daedalvs}, after ◊l1{telu} is moved.}
`(
  gICP
    (NP (N ,(labeled-node 'np-telu "telu")))
    (gICP
      ,(labeled-node 'trace-1 (trace 1))
      (VP
        (NP (N "tovrafen"))
        (V "mênču")))
)
#:additions
(stxtree-add
  #:arrows (list (stxadd-arrow 'trace-1 'np-telu)))]

Finally, we move the other noun phrase.

◊syntax-tree[#:caption ◊@{◊xref/p{daedalvs}, after ◊l1{tovrafen} is moved.}
`(
  gICP
    (NP (N ,(labeled-node 'np-telu "telu")))
    (gICP
      (NP (N ,(labeled-node 'np-tovrafen "tovrafen")))
      (gICP
        ,(labeled-node 'trace-1 (trace 1))
        (VP
          ,(labeled-node 'trace-2 (trace 2))
          (V "mênču"))))
)
#:additions
(stxtree-add
  #:arrows (list (stxadd-arrow 'trace-1 'np-telu)
                 (stxadd-arrow 'trace-2 'np-tovrafen)))]

If the arguments were moved out in the other order, then we would end up with ◊l1{tovrafen telu mênču}, shown below.

◊syntax-tree[#:caption ◊@{◊xref/p{daedalvs}, but with arguments moved in the opposite order.}
`(
  gICP
    (NP (N ,(labeled-node 'np-tovrafen "tovrafen")))
    (gICP
      (NP (N ,(labeled-node 'np-telu "telu")))
      (gICP
        ,(labeled-node 'trace-2 (trace 2))
        (VP
          ,(labeled-node 'trace-1 (trace 1))
          (V "mênču"))))
)
#:additions
(stxtree-add
  #:arrows (list (stxadd-arrow 'trace-2 'np-telu)
                 (stxadd-arrow 'trace-1 'np-tovrafen)))]

Then ◊xref/p{qs1} has the following structure:

◊syntax-tree[#:caption ◊@{The structure of ◊xref/p{qs1}.}
`(
  gICP
    (NP (N ,(labeled-node 'np-šinof "šinof")))
    (gICP
      ,(labeled-node 'trace-1 (trace 1))
      (VP (V "ħelit") (VP (V "rjotu"))))
)
#:additions
(stxtree-add
  #:arrows (list (stxadd-arrow 'trace-1 'np-šinof)))]

We can generalize ŊCv7’s rule about quantifiers to account for movement: ⟦◊nc{šino}◊sub{◊var{i}} [◊sub{◊var{i}} ◊var{α}]⟧◊sup{◊var{a}} = ∀◊var{x} ∈ ◊var{D} : ⟦◊var{α}⟧◊sup{◊var{a}/[◊var{i}=◊var{x}]}. (Ditto for ◊nc{nema}.)

This is also consistent with the rules on nested quantifiers:

◊gloss/x[#:id "all-then-exists"]{
  ◊glfree{◊nc{šine nemer racro.}}
  ◊gla{šin-e nem-er racr-o.}
  ◊glb{all-%nom.%pl any-%acc.%pl know-3%pl}
  ◊glfree{All of them know someone out of them. = For all ◊var{x}, there exists ◊var{y} such that ◊var{x} knows ◊var{y}.}
}

◊syntax-tree[#:caption ◊@{The structure of ◊xref/p{all-then-exists}.}
`(
  gICP
    (NP (N ,(labeled-node 'np-šine "šine")))
    (gICP
      (NP (N ,(labeled-node 'np-nemer "nemer")))
      (gICP
        ,(labeled-node 'trace-1 (trace 1))
        (VP ,(labeled-node 'trace-2 (trace 2))
          (V "racro"))))
)
#:additions
(stxtree-add
  #:arrows (list (stxadd-arrow 'trace-1 'np-šine)
                 (stxadd-arrow 'trace-2 'np-nemer)))]

◊gloss/x[#:id "exists-then-all"]{
  ◊glfree{◊nc{nemer šine racro.}}
  ◊gla{nem-er šin-e racr-o.}
  ◊glb{any-%acc.%pl all-%nom.%pl know-3%pl}
  ◊glfree{There is someone out of them whom all of them know. = There exists ◊var{y} such that for all ◊var{x}, ◊var{x} knows ◊var{y}.}
}

◊syntax-tree[#:caption ◊@{The structure of ◊xref/p{exists-then-all}.}
`(
  gICP
    (NP (N ,(labeled-node 'np-nemer "nemer")))
    (gICP
      (NP (N ,(labeled-node 'np-šine "šine")))
      (gICP
        ,(labeled-node 'trace-2 (trace 2))
        (VP ,(labeled-node 'trace-1 (trace 1))
          (V "racro"))))
)
#:additions
(stxtree-add
  #:arrows (list (stxadd-arrow 'trace-2 'np-šine)
                 (stxadd-arrow 'trace-1 'np-nemer)))]

Work these out and you’ll get the expected interpretations.
