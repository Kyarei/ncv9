#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{Personal note #0x00}
◊define-meta[date]{2021-12-21}

◊i{This entry was originally written for my personal diary. It has been published to this site on ◊render-date/single{2022-08-15}.}

◊section{The obvious choice is almost always the wrong choice}

I am formalizing a rule of thumb for working on Ŋarâþ Crîþ: if you need to express something in Ŋarâþ Crîþ that you don’t know how to, then:

◊enum{
    ◊item{Identify the most obvious solution.}
    ◊item{Don’t do that.}
}

The following is a justification of this rule.

◊subsection{The obvious solution is biased toward familiarity}

That is, following the obvious solution usually leads to knocking off the languages you’re most familiar with, and then usually not the interesting parts.

◊subsection{The obvious solution is usually broad-spectrum}

Broad-spectrum solutions are usually undesirable as they keep the grammar overly simple. (but sometimes, a non-obvious solution can have a surprisingly wide range of use?)

This is the principal difference between Necarasso Cryssesa and Ŋarâþ Crîþ. When I worked on Necarasso Cryssesa, I wanted to avoid ‘half-assing’ the grammar; for example, I didn’t see gender as useful for keeping track of multiple referents because it would be less useful if there were two or more referents of the same gender and I saw obviation as a more general solution to the problem.

For the same reason, Necarasso Cryssesa had a systematic way to derive adverbs and a general-purpose negative marker, both of which are absent in Ŋarâþ Crîþ in favor of more patchwork approaches. (Okay, the prohibitive used a special negative in NCS6.)

Put differently, Necarasso Cryssesa tried to simplify grammar, while Ŋarâþ Crîþ veers away from that goal.

Necarasso Cryssesa also strove to reduce redundancy. Thus it had no initial consonant mutations as seen in Ŋarâþ Crîþ because some onsets would not be mutated, preventing mutations alone from being a reliable marker of anything.

On a sidenote, there are at least two types of linguistic redundancy:

◊items{
    ◊item{Redundancy of morphemes: multiple morphemes in an utterance coding for the same information i.e. agreement}
    ◊item{Redundancy of features: multiple features with overlapping uses.}
}

◊subsection{Insert third reason here}

It would be nice to have a third justification for this rule to complete the rule of three, but I can’t think of one right now.

◊subsection{Caveats}

You still have to make sure that any changes you make are somewhat plausible, and you should also make sure they’re interesting.

◊section{Time to add an extra stem (or two) for all verbs?}

This would be the L stem used for nouns derived from verbs and could be retrofitted into use for existing inflections:

◊items{
    ◊item{1st- and 2nd- person plural finite forms}
    ◊item{locative-, instrumental-, and abessive-hcase participle forms (or vary the set by genus and species; how would this interact with impersonator stems?; and note that this is the obvious thing to do)}
    ◊item{perhaps replace some of the existing particle + infinitive combinations for nominalized verbs with synthetic forms}
}
