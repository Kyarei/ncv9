#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Shiva}
◊define-meta[date]{2023-01-11 – ?}
◊define-meta[book-author]{+merlan #flirora}

Reforming Ŋarâþ Crîþ morphophonology ◊em{again}. *sigh*

◊strong{This project is ◊link["/diary/0E36-shiva-backburner.html"]{backburnered}!}

◊section[#:id "status"]{Current status}

I outlined this topic briefly in ◊link["https://www.reddit.com/r/conlangs/comments/1015tr8/is_there_a_feature_that_you_regret_adding_to_your/j30krjj/"]{one of my comments in /r/conlangs}. To summarize, noun declension is based on an N–L–S framework. This adds the need for a stem that is used only for one rarely used case. The L stem, which is ◊em{almost always} distinct from N, is also rarely used outside of the locative case.

Another issue is that while Ŋarâþ Crîþ currently contains some deduplication rules, these do not encompass all cases of ◊i{oginiþe cfarðerþ} (objectionable repetition), and many such cases have to be solved on an ◊i{ad hoc} basis.

It is also infeasible to use the letter sum in verb conjugations, as any prefix such as ◊l0{do-} or ◊l0{es-} will affect it. This could be solved by introducing the notion of a preverb, which includes such prefixes, and not counting them toward any letter sum.

Participle inflection is also complex without much benefit. Perhaps we could (1) inflect them like nouns (possibly having different endings based on hgender), or (2) have fewer distinctions in hcase than in nominal case (for example, folding the locative, instrumental, and abessive hcases together).

Also, I stumbled upon ◊link["https://www.cam.ac.uk/system/files/rajpopat_phd_thesis_15_dec_2022.pdf"]{Rishi Rajpopat’s Ph.D. thesis} and got the hankering to reform Ŋarâþ Crîþ’s morphophonology as a result.

◊section[#:id "oc"]{On ◊i{oginiþe cfarðerþ}}

◊strong{For disambiguation, this document uses the following symbols in place of the hyphen: ◊l0{÷} on ◊var{s}, ◊l0{←} on ◊var{g}, ◊l0{→} on ◊var{o}, and ◊l0{¬} on ◊var{n}. The colon is also sometimes used to clarify syllable boundaries.}

I think ◊i{oginiþe cfarðerþ} should be a possibility for most consonants in Ŋarâþ Crîþ. I foresee consonants falling into one of the following categories:

◊items{
    ◊item{◊b{Class I}: CVC qualifies as OC. ◊l0{f}, ◊l0{þ}, ◊l0{h}, ◊l0{v}, ◊l0{ð}, ◊l0{ħ} from v9e, but also ◊l0{p}, ◊l0{g}, ◊l0{m}, ◊l0{ŋ}, and so on. Some of these consonants, such as ◊l0{þ}, might better fall into a subcategory in which CrVC and CVrC count as OC.}
    ◊item{◊b{Class II}: CVCV or VCVC, where both Vs are the same, qualifies as OC. ◊l0{s}, ◊l0{l}, ◊l0{n}, ◊l0{r}, ◊l0{ł}, ◊l0{t}, and so on. Note that in v9e, ◊l0{r} and ◊l0{ł} are deduplicated on Layer 3, not on 1.}
}

In Ŋarâþ Crîþ, ◊i{oginiþe cfarðerþ} is resolved using deduplication rules. These rules operate on junctures; thus, OC can stay if it exists in stems of foreign words such as ◊l1{*@vavel}. However, it is desirable to avoid having any other instances of OC. It is also desirable to, unlike v9e, avoid changing the initial letter of a word. If OC occurs word initially, then we should change the second occurrence of the duplicate consonant rather than the first.

The requirement to avoid creating any new OC poses a new problem: ◊b{deduplication can itself cause new instances of OC.}

◊subsection[#:id "oc-propagation"]{Deduplication propagation}

Consider a stem such as ◊l0{ca:ta:s→} which gains a suffix ◊l0{←as}, giving ◊l0{ca:ta:s→as}. This would have an instance of a VCVC OC of a class II consonant and is resolved in some way; suppose the first of the ◊l0{s}es becomes a ◊l0{t}. This in turn gives ◊l0{catatas}, which would have an instance of a VCVC OC of the class II consonant ◊l0{t}. Suppose we resolve this similarly by changing the first ◊l0{t} to become ◊l0{d}. This gives the final form ◊l1{cadatas}.

If we want to truncate the suffix ◊l0{-as} from ◊l1{cadatas}, however, then we have to yield not only the obvious ◊l1{cadat→} and the slightly less obvious ◊l1{cadas→} but also ◊l1{catas→} – that is, we have to look back beyond the last consonant of the stem.

Implementing truncation in the presence of deduplication propagation is probably complicated, and we need to have a well-defined model of how it works. The obvious answer is to move the juncture to one side of the occurrence of the repeated consonant to be changed, on the opposite side to the unchanging consonant. That is, ◊l0{catas→as} becomes ◊l0{cata÷tas}, which in turn becomes ◊l0{ca÷datas}, ending the propagation. Of course, we have to be careful with the deduplication rules to avoid infinite loops.

Even in v9e, not all deduplication rules fit pattern of changing one of the occurrences of the duplicate consonant because of phonotactics. The deduplication rule for ◊l0{f} at least limits itself to the two assemblage units surrounding a vowel, but the rule for ◊l0{þ} also has the potential to change the coda preceding the first occurrence of ◊l0{þ←} or ◊l0{cþ←}.

◊; This model gives us an algorithm to truncate a suffix and return all possible stems before deduplication. For each possible juncture of a word (from ◊l1{cadatas}: ◊l0{c←adatas}, ◊l0{c→adatas}, ◊l0{ca¬datas}, ◊l0{ca÷datas}, …, ◊l0{cadata¬s}):

◊; ◊enum{
◊;     ◊item{If the juncture is immediately before a consonant, then look at that consonant and the next one}
◊;     ◊item{For each possibility}
◊; }

◊subsection[#:id "oc-look"]{Look before you leap}

An alternative to deduplication propagation is a ‘look-before-you-leap’ approach, where deduplication rules check the context surrounding the consonant to be changed and chooses a consonant that does not cause additional OC. In this instance, ◊l0{catas→as} would change the first ◊l0{s} into a consonant other than ◊l0{t} to avoid causing OC with the preceding ◊l0{←ata¬}.

◊subsection[#:id "oc-mutation"]{What happens with mutated consonants?}

Some consonants, such as ◊l0{m·}, are written with one letter (in this case, ◊l1{m}) but are pronounced like another (◊l1{v}). I propose treating mutated letters as both their base letters and their homophonous letters for the purposes of determining ◊i{oginiþe cfarðerþ}. That being said, I’m not yet decided on what they should be changed to as opposed to the letters they are acting as.

This treatment will enable initial mutation to add new instances of OC: consider ◊l1{meva} → !!◊l1{m·eva}. We could try to deduplicate OC arising this way, but another possibility is to leave in instances of OC arising this way.

Eclipsis can also add a consonant where none existed; namely, by adding ◊l0{g←} to words without an initial consonant. This can trigger OC with another ◊l0{g}.

◊section[#:id "assemblage"]{On assemblages}

Assemblage types matter:

◊blockquote[
    "/grammar/phonology/layer0.html"
    ◊@{– ◊link["/grammar/phonology/layer0.html"]{◊cite{Ŋarâþ Crîþ v9e grammar}}}
]{
    Note that deduplication happens before any canonicalization; for instance, appending the syllables ◊l0{reþ} and ◊l0{eþ} together gives ◊l0{reþeþ}, not ◊l0{reteþ} (although appending the stem ◊l0{reþ} to the suffix ◊l0{eþ} ◊em{does} give ◊l0{reteþ}).
}

I’m not sure what the value is for or against having this quirk. Right now, it seems like an artifact of how concatenation is defined. In the current rules, bridge resolution is done after deduplication, as it was deemed more important to avoid invalid bridges than to avoid ◊i{oginiþe cfarðerþ}. Currently, deduplication can produce an invalid bridge in one situation: ◊l0{¬cþ:ħV:ħ←} becomes **◊l0{¬cþ:gV:ħ←} (⇒ ◊l0{¬þ:gV:ħ←}). Bridge resolution, however, can form OC in multiple circumstances, such as ◊l0{¬s÷sV:þ←} ⇒ !!◊l0{¬þV:þ←}. It might be worth performing deduplication after bridge resolution, dealing with the ◊l0{¬cþ:ħV:ħ←} case specially.

Additionally, bridge repair (in particular, canonicalization) does not move the ‘hyphen’ on the assemblage level: even if deduplication happened after bridge resolution, the latter would turn ◊l0{reþ÷eþ} into ◊l0{re÷þeþ} instead of ◊l0{re:þ←eþ}. Hyphen-moving, however, doesn’t solve cases such as ◊l0{þas÷sren} and ◊l0{fas÷siþ} – any option of where to move the hyphen in ◊l0{s÷s} coalescing to ◊l0{þ} will create an instance of ◊l0{þ}-OC in at least one of these examples. A more elaborate model would place hyphens around the bridge: ◊l0{re÷þeþ} ⇒ ◊l0{re¬:þ←eþ}, ◊l0{þas÷sren} ⇒ ◊l0{þa¬:þr←en}, ◊l0{fas÷siþ} ⇒ ◊l0{fa¬:þ←iþ}. This model implies that truncating on the syllabic state requires placing the candidate junctures around the bridge that would have been created and then performing a double truncation with the remaining juncture rules.

The assemblage structure makes it awkward to express deduplication rules, which conceptually work on letters, but this might be considered an advantage, as a naïve formulation such as ‘replace ◊l0{-fVf-} with ◊l0{-tVf-}’ runs into problems with ◊l0{tf←} initials. Working with assemblages encourages us to deal with these situations explicitly. (Of course, one can forget to add a rule for ◊l0{tf←} initials altogether.)

◊section[#:id "nary"]{On ◊var{N}-ary concatenation}

Currently, Ŋarâþ Crîþ v9e supports the notion of binary concatenation. This means that concatenating (◊var{a}, ◊var{b}, ◊var{c}) concatenates ◊var{a} and ◊var{b} first, then the result of that to ◊var{c}.

Another possibility for concatenating multiple morphemes is to join them all in one go. This means that in this example, any juncture rules applied between ◊var{a} and ◊var{b} would have access to the contents of ◊var{c}. This is more complex than repeated binary concatenation but has the advantage of being able to use word-global information (such as stress or syllable position within a word).

◊section[#:id "relationals"]{On relationals}

Relationals would have two absolute stems: an adnominal one and an adverbial one. They would be divided into four categories:

◊items{
    ◊item{◊term{a-relationals}: the adverbial form is the default, with the adnominal form getting ◊l0{←a}}
    ◊item{◊term{i-relationals}: the adnominal form is the default, with the adverbial form getting ◊l0{←i}}
    ◊item{Some relationals have both the ◊l0{←a} suffix on the adnominal form and the ◊l0{←i} suffix on the adverbial.}
    ◊item{A few relationals do something else.}
}
