#lang pollen

◊define-meta[title]{◊@{0x02: ◊cite[((lang "art-x-ncs-v9"))]{naldenôr}}}
◊define-meta[date]{2024-08-07 – 2024-12-31}

◊dl[#:class "infobox"]{
    ◊dt{Title}◊dd{◊cite[#:lang (ŋc-code 9)]{naldenôr} (◊cite{Boundary Realm})}◊;
    ◊dt{Lyrics}◊dd{+merlan #flirora}◊;
    ◊dt{Music}◊dd{+merlan #flirora}◊;
    ◊dt{Vocals}◊dd{Mai (Synthesizer V Studio)}◊;
    ◊dt{Video}◊dd{+merlan #flirora}◊;
    ◊dt{Link}◊dd{◊link["https://youtu.be/ErMgtf9lTOU"]{YouTube}}◊;
}

◊section{Lyrics}

◊poem[#:lang (ŋc-code 9)]{
pevanaþas caðjanen anoþ emitracljaþ
seljen civreþełen sacei arigan’ce creðisa.
gralseþas setoða cforens limes’pe cfarðaþ
reltas nîs arðaþ tfôrton elvareħe.

creten’ac tfilren’pe cehiþ vêsros vôri gcrôneve
fêsema coþros eis cerciren ancâŋi nareca.
soltepriþes lê gcoðir elsendraþa cenþ’pe
łilicþ ronþen darłiþ mirłit senlal.

serfei elaþa neðas šadarga cfidicta
mjêrveca secþro þonal mora.
nemen neðeł crefora tfarsi ar sorve;
nemen neðeł crentos nîs secþres išiła.
reltena sênra darnon edesen civreþe
loran drênans crelciþ cjani narval se.

minaerþ ciþit mitriþ
noldeþa onos cjaše se;
lê cþasos m·eniłos roc pelca
eši ves so on vescþit oras viþca.

eslis cþorelt šesos
ivedit precþa łanasage
vereljen łirlan cfeþliþ
sorþa celmâns dtlerflal.

rintet pindorenivilermeþo fêtecto
pecþantrenþ fraljełen tljorþþens’ce gcericeveca
cihatoþ cfersen sênen cþisil marcþin noldas
pelhasime inoras ar ganþesta.

sernen ħecilca tê mêva velvios eara canrari
v·esvilt es sohrei garivan cajos calana’pe
cîntiþ cliŋot c·lačifos eis prôsaþa cþonłas
caðjanen šino eletor viłcaþ.

ondelt menat geðulu;
naðasels mproteuluþ.
cjašit d·enecilt’êcþ ortla;
clałto avonas tecsa.

tôrþ cfarðiten sielvermeþa ðen
nosriþ mjonelþa pegrimeca
eril vrogos tovren tełavels mincþa
fetjan’moc gamos se.

minaerþ ciþit mitriþ
noldeþa onos cjaše se;
lê cþasos m·eniłos roc pelca
eši ves so on vescþit oras viþca.

eslis cþorelt šesos
ivedit precþa łanasage
vereljen łirlan cfeþliþ
sorþa celmâns dtlerflal.

šelmilþaf intaras m·orilt endriłos roc
edesen coþoravonas mesit łana se.
arelo tê vpiþtols calana dosêrčirþ
ŋgacjopreveca tamoþ nelčos tfôri ndarłel.

łerlime acþasel šencþil naðaserþ dtosal;
sênnoten gelšidit ceraþþes lemêns dogjaverecþ.
eles caf crecþror erces teser fjoltas teþrirþ
tełor’ac docþêhlen respen ħecþoris.

vlêteser gelgačirþ on ŋiłit avona ŋecþatlit
on melsit’ce elriłiniþ crenŋan arnennes remen.
arðon el folþos sotel enmon anel sênlelt
entresþatas tê cforens nareca.

gristelcaf om samen avonał movu se.
vesþan lê gasanon šesos varacre.
}

◊subsection{Gloss}

◊gloss/x{
  ◊glfree{◊nc{pevanetoþ caðjanen anoþ emitracljaþ seljen civreþełen sacei arigan’ce creðisa.}}
  ◊gla{pe-van-etoþ caðjan-en an-oþ e-mitra-clj-aþ selj-en civreþ-ełen sac-ei arig-an’=ce creðis-a.}
  ◊glb{1%sg-look_at-%rel.%dat,%dat.%ter world-%gen.%di sky-%dat.%di far<%ddt>-%ser afternoon-%gen.%sg rising_air-%gen.%gc sun-%gen.%di warmth-%acc.%di feel-1%sg}
  ◊glfree{As I grow more distant from the world’s sky that I look at, I feel the rising air of the afternoon and the warmth of the sunlight.}
}

◊gloss/x{
  ◊glfree{◊nc{gralseþas setoða cforens limes’pe cfarðaþ reltas nîs arðaþ tfôrton elvareħe.}}
  ◊gla{grals-eþas setoð-a cfor-ens lim-es=’pe cfarð-aþ relt-as nîs arð-aþ tfôr-ton elvareħe.}
  ◊glb{leave_behind-%rel.%dat,%dat.%cel fiction-%gen.%sg melody-%dat.%sg head-%loc.%sg=%poss.1 echo-%ser blood_vessel-%dat.%co through rush-%ser arm-%acc.%du liven-3%sg}
  ◊glfree{The fictional melody left aside echoes in my head and flows through my veins, livening my arms.}
}

◊gloss/x{
  ◊glfree{◊nc{creten’ac tfilren’pe cehiþ vêsros vôri gcrôneve fêsema coþros eis cerciren ancâŋi nareca.}}
  ◊gla{cret-en=’ac tfilr-en=’pe ceh-iþ vêsr-os vôri g\crôn-eve fês-ema coþr-os eis cercir-en ancâŋ-i narec-a.}
  ◊glb{wave-%nom.%co=%poss.3%cel heart-%acc.%sg=%poss.1 arrive-%ser energy-%dat.%di full_of.%adv (%inf\)fill-%inf.%loc 3%gc-different_from history-%dat.%di from.%adn stranger-%gen.%sg story-%dat.%sg evoke-3%sg.%pfv}
  ◊glfree{When its waves reach my heart and fill it with energy, it evokes a story of a stranger from another timeline.}
}

◊gloss/x{
  ◊glfree{◊nc{soltepriþes lê gcoðir elsendraþa cenþ’pe łilicþ ronþen darłiþ mirłit senlal.}}
  ◊gla{soltepr-iþes lê g\coð-ir elsendr-aþa cenþ=’pe łil-icþ ronþ-en darł-iþ mirł-it senl-al.}
  ◊glb{pipe_in-%rel.%dat,%dat.%cel this.%cel emotion-%dat.%pl overwhelm-%rel.%acc,%nom.%cel self.%nom.%sg=%poss.1 cloud-%sembl.%gc shape-%acc.%sg extend-%ser hold_onto-%inf try_to-1%sg.%inv}
  ◊glfree{Overwhelmed by the emotions being piped in, I try to reach and hold onto its cloud-like shape.}
}

◊gloss/x{
  ◊glfree{◊nc{serfei elaþa neðas šadarga cfidicta mjêrveca secþro þonal mora.}}
  ◊gla{serf-ei elaþa neð-as šadarg-a cfid-icta mjêrv-eca secþr-o þonal mor-a.}
  ◊glb{autumn-%gen.%di first.%loc.%cel day-%loc.%sg canopy-%gen.%sg leaf-%sembl.%st tunic-%inst.%sg breeze-%dat.%di among.%adv descend-1%sg}
  ◊glfree{Like a canopy’s leaf on the first day of autumn, I descend through the breeze with my tunic.}
}

◊gloss/x{
  ◊glfree{◊nc{nemen neðeł crefora tfarsi ar sorve;}}
  ◊gla{nem-en neð-eł crefor-a tfars-i ar sorv-e;}
  ◊glb{some-%gen.%sg day-%loc.%gen strong_wind-%nom.%di faraway_place-%dat.%sg toward carry-3%sg}
  ◊glfree{Some days, the strong wind takes me far;}
}

◊gloss/x{
  ◊glfree{◊nc{nemen neðeł crentos nîs secþres išiła.}}
  ◊gla{nem-en neð-eł crent-os nîs secþres išił-a.}
  ◊glb{some-%gen.%sg day-%loc.%gen ear-%dat.%du through breeze-%nom.%di whisper-3%sg}
  ◊glfree{Other days, the gentle wind whispers in my ears.}
}

◊gloss/x{
  ◊glfree{◊nc{reltena sênra darnon edesen civreþe loran drênans crelciþ cjani narval se.}}
  ◊gla{relten-a sênr-a darn-on e-desen civreþ-e lor-an drên-ans crelc-iþ cjan-i narv-al se.}
  ◊glb{mist-%acc.%co raise-%rel.%nom,%nom.%cel long-%rel.%nom,%acc.%pl 1%sg-below.%adn rising_wind-%nom.%di hair-%acc.%co encourage-%rel.%nom,%dat.%cel mess_up-%ser poem-%dat.%sg sing-3%sg.%inv %mir}
  ◊glfree{The mist-raising wind below me messes up my long hair and sings me an encouraging poem.}
}

◊gloss/x[#:id "chorus-start"]{
  ◊glfree{◊nc{minaerþ ciþit mitriþ noldeþa onos cjaše se;}}
  ◊gla{mina-erþ ciþ-it mitr-iþ nold-eþa on-os cjaš-e se;}
  ◊glb{alone-%ser arrow-%sembl.%sg fast-%ser boundary-%abess.%sg sky-%loc.%di fall-1%sg %mir}
  ◊glfree{Alone, I fall in the limitless sky as quickly as an arrow;}
}

◊gloss/x{
  ◊glfree{◊nc{lê cþasos m·eniłos roc pelca eši ves so on vescþit oras viþca.}}
  ◊gla{lê cþas-os m·en-iłos roc pelca eši ves so on vescþ-it or-as viþca.}
  ◊glb{this.%cel gradient-%dat.%sg see-%inf.%dat.%subj in_order_to %pr.1%sg.%inst here.%loc.%sg exist.2%sg if %inf.%acc beautiful-%inf think-2%sg %cond}
  ◊glfree{If you were here with me to see this gradient, you would think that it was beautiful.}
}

◊gloss/x[#:id "chorus-end"]{
  ◊glfree{◊nc{eslis cþorelt šesos ivedit precþa łanasage vereljen łirlan cfeþliþ sorþa celmâns dtlerflal.}}
  ◊gla{esl-is cþor-elt šes-os ived-it precþ-a łanasag-e verelj-en łirl-an cfeþl-iþ sorþ-a celm-âns d\tlerfl-al.}
  ◊glb{wide-%rel.%nom,%loc.%di surroundings-%loc.%di always-%loc.%di water_surface-%sembl.%sg shift.%rel.%nom,%nom.%cel pattern_of_clouds-%nom.%sg alternate_reality-%gen.%sg light-%acc.%co reflect-%ser imagination-%gen.%di window-%acc.%sg %pfv\reveal-3%sg.%inv}
  ◊glfree{The pattern of clouds in the vast expanse, constantly shifting like the water’s surface, reflects the light of an alternate reality and reveals the window of imagination.}
}

◊gloss/x{
  ◊glfree{◊nc{rintet pindorenivilermeþo fêtecto pecþantrenþ fraljełen tljorþþens’ce gcericeveca}}
  ◊gla{rint-et pin-do-re-ni-vil-ermeþo fê-tecto pe-cþantr-enþ fralj-ełen tlencþ-þens=’ce g\ceric-eveca}
  ◊glb{rirens-%sembl.%pl eleven-five-ten-nine-one-16¹.%approx 3%gc-before 1%sg-value-%rel.%dat.%acc.%cel optimism-%acc.%gc wish-%acc.%gc=and (%inf\)forget-%inf.%inst}
  ◊glfree{143.875 seconds ago, having forgotten the hope and wishes that I valued,}
}

◊gloss/x{
  ◊glfree{◊nc{cihatoþ cfersen sênen cþisil marcþin noldas pelhasime inoras ar ganþesta.}}
  ◊gla{cihat-oþ cfers-en sênen cþis-il marcþ-in nold-as pelhas-ime inor-as ar ganþes-ta.}
  ◊glb{crag-%dat.%sg rust-%rel.%nom,%gen.%cel above.%adn steel-%gen.%di bridge-%gen boundary-%dat.%sg look_down-and void-%dat.%sg toward %pfv\step-1%sg.%pst.%pfv}
  ◊glfree{I looked down from the edge of the rusted steel bridge above the crags and stepped off.}
}

◊gloss/x{
  ◊glfree{◊nc{sernen ħecilca tê mêva velvios eara canrari v·esvilt es sohrei garivan cajos calana’pe cîntiþ cliŋot c·lačifos eis prôsaþa cþonłas caðjanen šino eletor viłcaþ.}}
  ◊gla{sernen ħec-ilca tê mêv-a velv-ios e-ara canr-ari v·esv-ilt es sohr-ei gariv-an caj-os calan-a=’pe cînt-iþ cliŋ-ot c·lač-ifos eis prôsaþa cþonł-as caðjan-en šin-o el-etor viłc-a-þ.}
  ◊glb{moss-%gen.%di odor-%inst.%di that.%cel rain-%nom.%co wet-%rel.%acc,%nom.%ter 1%sg-toward.%adn insult-%dat.%pl recall-%inf.%dat inside basalt-%gen.%di fragile-%rel.%nom,%acc.%sg ground-%nom.%di body-%acc.%sg=%poss.1 collide-%ser dry_plant_matter-%sembl.%sg break-%inf.%dat.%subj from.%adn fourth-%loc.%cel instant-%loc.%sg world-%gen.%di all-%nom.%sg 1%sg-in_front_of flipped-3%sg-%pst}
  ◊glfree{The fourth instant from when, while the insults toward me flash in front of my eyes, my frail body would hit the basalt ground, wet from the moss-smelling rain, and be shattered by it like a piece of dry plant matter, the entire world flipped before my eyes.}
}

◊gloss/x{
  ◊glfree{◊nc{ondelt menat geðulu;}}
  ◊gla{ond-elt men-at geð-u-lu;}
  ◊glb{now-%loc.%di see-%inf cannot-3%gc-3%sg.%ter}
  ◊glfree{Now it (the ground) is no longer visible;}
}

◊gloss/x{
  ◊glfree{◊nc{naðasels mproteuluþ.}}
  ◊gla{naðas-∅-els m\prote-u-lu-þ.}
  ◊glb{blue-%qual-%dat.%di %pfv\replace-3%gc-3%sg.%ter-%pst}
  ◊glfree{It had been replaced by blueness.}
}

◊gloss/x{
  ◊glfree{◊nc{cjašit d·enecilt’êcþ ortla;}}
  ◊gla{cjaš-it d·enec-ilt=’êcþ ortl-a;}
  ◊glb{fall-%inf continue-%inf.%dat=%poss.%refl sense_by_proprioception-1%sg}
  ◊glfree{I feel myself continuing to fall;}
}

◊gloss/x{
  ◊glfree{◊nc{clałto avonas tecsa.}}
  ◊gla{clałto avonas tecsa.}
  ◊glb{cool-%rel.%nom,%nom.%sg wind-%dat.%di touch-1%sg}
  ◊glfree{I feel the cool wind.}
}

◊gloss/x{
  ◊glfree{◊nc{tôrþ cfarðiten sielvermeþa ðen nosriþ mjonelþa pegrimeca}}
  ◊gla{tôrþ-∅ cfarð-iten si-elverm-eþa ðen nosr-iþ mjon-elþa pegrim-eca}
  ◊glb{inner_mind-%nom.%sg repeat-%rel.%dat,%nom.%cel %app.%loc-belittle-%rel.%dat,%nom.%cel occurrence.%nom.%pl deep-%ser number-%abess.%sg cause_to_feel_pain-but}
  ◊glfree{Though the countless times I’ve been belittled, repeated by my mind, are deep and cause pain,}
}

◊gloss/x{
  ◊glfree{◊nc{eril vrogos tovren tełavels mincþa fetjan’moc gamos se.}}
  ◊gla{er-il vrog-os tovr-en tełav-els mincþa fetj-an=’moc g\am-os se.}
  ◊glb{rainbow-%gen.%sg peak-%loc.%sg flower-%gen.%co pleasant_scent-%dat.%di considering that_idea-%acc.%sg=also %pfv\unimportant-3%gc.%inv %mir}
  ◊glfree{Even that matters little in the perspective of the scent of the flowers at the peak of the rainbow.}
}

◊b{◊xref/p{chorus-start} – ◊xref/p{chorus-end} are repeated here.}

◊gloss/x{
  ◊glfree{◊nc{šelmilþaf intaras m·orilt endriłos roc edesen coþoravonas mesit łana se.}}
  ◊gla{šelmi-lþaf intar-as m·or-ilt endr-iłos roc e-desen coþoravon-as mes-it łan-a se.}
  ◊glb{end-%abess.%gc void-%loc.%sg descend-%inf.%dat exit-%inf.%dat.%subj in_order_to 1%sg-below.%adn windstorm-%dat.%di enter-%inf must-1%sg %mir}
  ◊glfree{To end this endless falling in space, I’ll have to enter the windstorm below me, it seems.}
}

◊gloss/x{
  ◊glfree{◊nc{arelo tê vpiþtols calana dosêrčirþ ŋgacjopreveca tamoþ nelčos tfôri ndarłel.}}
  ◊gla{arel-o tê vpiþt-ols calan-a do-sêrč-irþ ŋ\gacjopr-eveca tam-oþ nelč-os tfôr-i n\darł-el.}
  ◊glb{dim-%rel.%nom,%dat.%sg that.%cel anomaly-%dat.%sg body-%acc.%sg %caus-point_to-%ser (%inf\)clench_teeth-%inf.%inst fear-%dat.%di dye-%acc,%dat.%ter arm-%dat.%sg %pfv\extend-1%sg.%inv}
  ◊glfree{I aim my body toward the faint anomaly and, clenching my teeth, reach out my arm colored with fear.}
}

◊gloss/x{
  ◊glfree{◊nc{łerlime acþasel šencþil naðaserþ dtosal;}}
  ◊gla{łerl-ime acþas-el šencþ-il naðas-∅-erþ d\tos-al;}
  ◊glb{shake-and colorful-%rel.%nom,%gen.%sg sky-%gen.%sg blue-%qual-%nom.%di %pfv\fade-3%sg.%inv}
  ◊glfree{The blueness of the swaying, colorful sky fades away;}
}

◊gloss/x{
  ◊glfree{◊nc{sênnoten gelšidit ceraþþes lemêns dogjaverecþ.}}
  ◊gla{sênnot-en gelšid-it ceraþþ-es lem-êns do-gjaverc-∅.}
  ◊glb{ascend-%rel.%nom,%nom.%cel shadow-%sembl.%co strong_wind-%nom.%di head-%acc.%sg %caus-be_damaged_by_strong_winds-3%sg}
  ◊glfree{The rising shadowy gust batters my head.}
}

◊gloss/x{
  ◊glfree{◊nc{eles caf crecþror erces teser fjoltas teþrirþ tełor’ac docþêhlen respen ħecþoris.}}
  ◊gla{el-es caf crecþr-or erc-es tes-er fjolt-as teþr-irþ teł-or=’ac docþêhl-en resp-en ħecþor-is.}
  ◊glb{sun-%dat.%sg in_place_of radiant-%rel.%nom,%nom.%pl cold-%rel.%nom,%loc.%sg lightning-%nom.%co space-%dat.%sg call-%ser bifurcation-%nom.%pl=%poss.3%cel %caus-comfortable-%rel.%nom,%acc.%sg fog-%acc.%di penetrate-3%pl.%inv}
  ◊glfree{The dazzling lightning taking place of the sun calls from the cold space, and its forks pierce the comforting fog.}
}

◊gloss/x{
  ◊glfree{◊nc{vlêteser gelgačirþ on ŋiłit avona ŋecþatlit on melsit’ce elriłiniþ crenŋan arnennes remen.}}
  ◊gla{vlêtes-er gelgač-irþ on ŋił-it avon-a ŋecþatl-it on mels-it=’ce elriłin-iþ crenŋ-an arnend-nes rem-en.}
  ◊glb{thunderstorm-%nom.%co boom-%ser %inf.%acc roar-%inf wind-%nom.%di forest_spirit-%sembl.%sg %inf.%acc sing-%inf=and intertwine-%ser harmonious-%rel.%nom,%acc.%cl music-%acc.%st make-3%du}
  ◊glfree{The booming roar of the thunderstorms and the ◊i{ŋecþatla}-like singing of the wind intertwine and create a harmonious song.}
}

◊gloss/x{
  ◊glfree{◊nc{arðon el folþos sotel enmon anel sênlelt entresþatas tê cforens nareca.}}
  ◊gla{arð-on el folþ-os sot-el enm-on an-el sênl-elt entresþ-atas tê cfor-ens narec-a.}
  ◊glb{rush-%rel.%nom,%acc.%ter this.%ter moment-%loc.%sg calm-%rel.%nom,%gen.%sg downpour-%nom.%sg sky-%gen.%di top-%loc.%di encounter-%acc,%dat.%cel that.%cel melody-%dat.%sg evoke-3%sg.%pfv}
  ◊glfree{At this moment, the raging downpour reminds me of the melody I encountered at the top of the calm sky.}
}

◊gloss/x{
  ◊glfree{◊nc{gristelcaf om samen avonał movu se.}}
  ◊gla{grist-elcaf om sam-en avon-ał mov-u se.}
  ◊glb{defect-%inst.%gc that.%ter dream-%gen.%sg wind-%nom.%gc float-3%gc %mir}
  ◊glfree{That imperfect dream floats in the wind.}
}

◊gloss/x{
  ◊glfree{◊nc{vesþan lê gasanon šesos varacre.}}
  ◊gla{vesþ-an lê g\asan-on šes-os varacr-e.}
  ◊glb{wave-%rel.%nom,%acc.%sg this.%cel memory-%acc.%sg always-%loc.%di remember-1%sg}
  ◊glfree{I will remember this waving memory forever.}
}
