#lang pollen

◊define-meta[title]{◊@{0x00: ◊cite[((lang "art-x-ncs-v9"))]{naðasels nelčon tfełor}}}
◊define-meta[date]{2022-06-26 – 2022-10-29}

◊dl[#:class "infobox"]{
    ◊dt{Title}◊dd{◊cite[#:lang (ŋc-code 9)]{naðasels nelčon tfełor} (◊cite{The Road Stained Blue})}◊;
    ◊dt{Lyrics}◊dd{+merlan #flirora}◊;
    ◊dt{Music}◊dd{+merlan #flirora}◊;
    ◊dt{Vocals}◊dd{Yamine Renri (Synthesizer V Studio)}◊;
    ◊dt{Video}◊dd{+merlan #flirora}◊;
    ◊dt{Link}◊dd{◊link["https://youtu.be/k4ndOV1yfcw"]{YouTube}}◊;
}

◊section{Lyrics}

◊poem[#:lang (ŋc-code 9)]{
enleħes nemen osmos senâr cadirin veðaþ;
relcor’ac artfaþor cerclon’ce ndošorcôta.
mêvas þon gelelca nôrels prômeca magreleþ;
cjâteþo enôri p·easnelsifos perclalta.

arantil casmel poðes siercełes tê vpîstelt
gadosos lantai meþos minaeþ nelalta.
ša eši met senłari nîs drênit deneste se?
samoþ jas gasnelseþ so’moc feljan eniljacin.

cintos cenm·orþas ndrôneve’pe lê gasane
meðas nîs eti šesos pedrênit vfeŋoris.
«cretil ineðit lê meðanneł ariga’moc
gescasgos» reþ racre.

naðesos anor jonas łorcþit ħare;
jorniłirþ cercli mevas’ce menat pentu se.
ênsêna orgit noldeþa inora;
cajoþ il cerþel mêven tełaverþ.

esnaren serfei cinras vandil šelmilt
secþro þon cfiþarin eši crešit pentu se.
arelo telcþeł vesre elin łirla
tevit mel ontrica erens crêþaf.

tfaþos car erjatfełoþ sa vfełcat cþonłas
falþel aspel’pe elesi vilsel.
maretel šelmilt gcarispeve’pe
ša penas atrecit gennelel se?

envas imîr neðeł annel došorcre so
riłiþerin dosorva so
elret flen fêmîr sortaþan šino
cerectês is censit marðel.

feljan racriþ narviłos roc lê gcjani nêrge;
aspoþ’pe mîr’moc c·erifos roc cþere.
eti crešit cfjoþelt mjonelþa meðałes nîs
mivełen cenvat nevla.

naðesos anor jonas łorcþit ħare;
jorniłirþ cercli mevas’ce menat pentu se.
ênsêna orgit noldeþa inora;
cajoþ il cerþel mêven tełaverþ.

esnaren serfei cinras vandil šelmilt
secþro þon cfiþarin eši crešit pentu se.
arelo telcþeł vesre elin łirla
tevit mel ontrica erens crêþaf.

falþon’po fêmîr elêþ grisfials ar morarþ
tacrinas vetłac·eâns nesmâeftês ndotosal.
ea łirlen el relcasor’moc menat ŋgeðos;
pejasnełor ceriþ šino meða nenso’ce.

ai enven sodas erłen sartan drênime
šadarget łana motos cþasos domenat ħare.
le en seljo lirnat en têmit’ce maoþ lef
afčaþans lê genvas’moc melraþ.

naðesos anor jonas łorcþit ħare;
jorniłirþ cercli mevas’ce menat pentu se.
ênsêna orgit noldeþa inora;
cajoþ il cerþel mêven tełaverþ.

esnaren serfei cinras vandil šelmilt
secþro þon cfiþarin eši crešit pentu se.
arelo telcþeł vesre elin łirla
tevit mel ontrica erens crêþaf.

caðjanen eltêns tfel cfatles teltos il
terałei *galama anagrat cenmire se.
telmaðirþ cehit geðates aline
ar sêrnime mitrit denestâs tal!
}

◊subsection{Gloss}

◊gloss/x{
  ◊glfree{◊nc{enleħes nemen osmos senâr cadirin veðaþ;}}
  ◊gla{enle-ħes nem-en osm-os sen-âr cad-irin veð-a-þ;}
  ◊glb{far-%rel.%nom,%loc.%sg some-%gen.%sg dream-%loc.%sg fire-%nom.%sg island-%acc.%sg raze-3%sg-%past}
  ◊glfree{Far away, in a certain dream, a fire razed an island;}
}

◊gloss/x{
  ◊glfree{◊nc{relcor’ac artfaþor cerclon’ce ndošorcôta.}}
  ◊gla{relc-or=’ac artfaþ-or cercl-on=’ce n\do-šorcr-ta.}
  ◊glb{wing-%nom.%sg=%poss.3.%cel city-%acc.%pl mountain-%acc.%co=and %pfv\%caus-burn-3%sg.%past.%pfv}
  ◊glfree{Its flames burned both cities and mountains.}
}

◊gloss/x{
  ◊glfree{◊nc{mêvas þon gelelca nôrels prômeca magreleþ;}}
  ◊gla{mêv-as þon gel-elca nôr-els prôm-eca magr-e-leþ;}
  ◊glb{rain-%dat.%co among friend-%inst.%sg small-%nom,%inst.%sg boat-%inst.%sg escape-3%sg.%cel-%past}
  ◊glfree{I escaped with a friend in the rain with a small boat;}
}

◊gloss/x{
  ◊glfree{◊nc{cjâteþo enôri p·easnelsifos perclalta.}}
  ◊gla{cjâþ-eþo enôr-i p·e-asnels-ifos percl-al-ta.}
  ◊glb{separate-%rel.%dat,%dat.%cel world-%dat.%sg 1%sg-return-%inf.%dat.%subj not_know-how-1%sg.%inv-%past}
  ◊glfree{I didn’t know how we’d return to the world we were torn away from.}
}

◊gloss/x{
  ◊glfree{◊nc{arantil casmel poðes siercełes tê vpîstelt}}
  ◊gla{arant-il casm-el poð-es si-erc-ełes tê v\pîst-elt}
  ◊glb{long_time-%gen.%di dream-%gen.%sg another-%loc.%sg %appl.%loc-cold-%rel.%dat,%loc.%sg that.%cel country-%loc.%sg}
  ◊glfree{In another long dream, in that frigid land,}
}

◊gloss/x{
  ◊glfree{◊nc{gadosos lantai meþos minaeþ nelalta.}}
  ◊gla{gados-os lant-ai meþos mina-eþ nel-al-ta.}
  ◊glb{giant-%rel.%nom,%dat.%pl tower-%dat.%pl around alone-%ser wander-1%sg-%past}
  ◊glfree{I wandered alone around the giant towers.}
}

◊gloss/x{
  ◊glfree{◊nc{ša eši met senłari nîs drênit deneste se?}}
  ◊gla{ša eši met senł-ari nîs drên-it denest-e se?}
  ◊glb{%int here.%loc.%sg how_many half_month-%dat.%pl through awake-%inf continue-1%sg %rhet}
  ◊glfree{How many months will I continue to be awake here for?}
}

◊gloss/x{
  ◊glfree{◊nc{samoþ jas gasnelseþ so’moc feljan eniljacin.}}
  ◊gla{sam-oþ jas g\asnels-e-þ so=’moc felj-an enilj-a-cin.}
  ◊glb{dream-%dat.%sg from %pfv\return-1%sg-%past if=also this_idea-%acc.%sg ask-1%sg-%refl}
  ◊glfree{Even when I have returned from the dream, I ask myself that.}
}

◊gloss/x{
  ◊glfree{◊nc{cintos cenm·orþas ndrôneve’pe lê gasane}}
  ◊gla{cintos cenm·orþ-as n\drôn-eve=’pe lê g\asan=e}
  ◊glb{morning-%loc.%sg personal_room-%loc.%sg awake-%inf.%loc=%poss.1 this.%cel memory-%nom.%pl}
  ◊glfree{When I wake up in my room in the morning,}
}

◊gloss/x{
  ◊glfree{◊nc{meðas nîs eti šesos pedrênit vfeŋoris.}}
  ◊gla{með-as nîs eti šes-os pe-drên-it v\feŋ-oris.}
  ◊glb{night-%dat.%sg through %inf.%sembl.%subj always-%loc.%sg 1%sg-awake-%inf %pfv\fade_away-3%pl.%inv}
  ◊glfree{These memories fade away as if I were awake through the night.}
}

◊gloss/x{
  ◊glfree{◊nc{«cretil ineðit lê meðanneł ariga’moc gescasgos» reþ racre.}}
  ◊gla{«cret-il ineð-it lê meðat-neł arig-a=’moc g\es-casg-os» reþ racre.}
  ◊glb{wave-%gen.%di white-%sembl.%di this.%cel evening-%gen.%gc warmth-%nom.%sg=also %pfv\%inch-cool_off-3%gc %quot.%acc.%ind know-1%sg}
  ◊glfree{I know that even the transient warmth of the evening will cool off.}
}

◊gloss/x[#:id "chorus-start"]{
  ◊glfree{◊nc{naðesos anor jonas łorcþit ħare;}}
  ◊gla{naðes-os an-or jon-as łorcþ-it ħar-e;}
  ◊glb{blue-%rel.%nom,%nom.%ter sky-%nom.%di now-%loc.%di clear-%inf again-3%sg.%pfv}
  ◊glfree{The blue sky is now clear again;}
}

◊gloss/x{
  ◊glfree{◊nc{jorniłirþ cercli mevas’ce menat pentu se.}}
  ◊gla{jornił-irþ cercl-i mev-as=’ce men-at pent-u se.}
  ◊glb{stand-%ser mountain-%dat.%co sea-%dat.%sg=and see-%inf able_to-3%gc %mir}
  ◊glfree{Standing up, both mountains and sea can be seen.}
}

◊gloss/x{
  ◊glfree{◊nc{ênsêna orgit noldeþa inora;}}
  ◊gla{ên-sêna org-it nold-eþa inor-a;}
  ◊glb{1%pl.%incl-above ice-%sembl.%di boundary-%abess.%sg void-%nom.%sg}
  ◊glfree{Above us lies an ice-like boundless void;}
}

◊gloss/x{
  ◊glfree{◊nc{cajoþ il cerþel mêven tełaverþ.}}
  ◊gla{caj-oþ il cerþ-el mêv-en tełav-erþ.}
  ◊glb{ground-%dat.%di on_top_of remnant-%gen.%sg rain-%gen.%co pleasant_scent-%nom.%sg}
  ◊glfree{On the ground is the scent of the rain that had stopped.}
}

◊gloss/x{
  ◊glfree{◊nc{esnaren serfei cinras vandil šelmilt}}
  ◊gla{es-nar-en serf-ei cinr-as vand-il šelm-ilt}
  ◊glb{%inch-ripe-%rel.%nom,%acc.%cel autumn-%gen.%di start-%loc.%sg summer-%gen.%sg end-%loc.%sg}
  ◊glfree{At the start of autumn and end of summer,}
}

◊gloss/x{
  ◊glfree{◊nc{secþro þon cfiþarin eši crešit pentu se.}}
  ◊gla{secþr-o þon cfiþ-arin eši crešit pentu se.}
  ◊glb{breeze-%dat.%di among leaf-%acc.%co here.%loc.%sg hear-%inf able_to-3%gc %mir}
  ◊glfree{The ripening leaves can be heard here in the breeze.}
}

◊gloss/x[#:id "chorus-end"]{
  ◊glfree{◊nc{arelo telcþeł vesre elin łirla tevit mel ontrica erens crêþaf.}}
  ◊gla{arel-o telcþ-eł vesr-e el-in łirl-a tev-it mel ontr-ica er-ens crêþam-∅.}
  ◊glb{indistinct-%rel.%nom,%dat.%sg bloom-%rel.%nom,%sembl.%cel strong-%rel.%nom,%nom.%pl sun-%gen.%sg light-%nom.%co flower-%sembl.%co many color-%inst.%pl rainbow-%dat.%sg project-3%sg}
  ◊glfree{The sun’s intense light projects a faint rainbow with many colors like the blooming flowers.}
}

◊gloss/x{
  ◊glfree{◊nc{tfaþos car erjatfełoþ sa vfełcat cþonłas}}
  ◊gla{tfaþ-os car erjatfeł-oþ sa v\fełc-at cþonł-as}
  ◊glb{town-%dat.%sg outside railway-%dat.%sg %inf.%loc.%subj move_past-%inf instant-%loc.%sg}
  ◊glfree{The moment I pass by the railway outside the town,}
}

◊gloss/x{
  ◊glfree{◊nc{falþel aspel’pe elesi vilsel.}}
  ◊gla{falþel asp-el=’pe eles-i vils-el.}
  ◊glb{short_time-%gen.%sg lifespan-%gen.%sg=%poss.1 year-%dat.%pl count-1%sg.%inv}
  ◊glfree{I count the years of my short life.}
}

◊gloss/x{
  ◊glfree{◊nc{maretel šelmilt gcarispeve’pe}}
  ◊gla{maret-el šelm-ilt g\car-isp-eve=’pe}
  ◊glb{fate-%gen.%di end-%loc.%sg %term-exist-%inf.%loc=%poss.1}
  ◊glfree{At its inevitable end, when I cease to exist,}
}

◊gloss/x{
  ◊glfree{◊nc{ša penas atrecit gennelel se?}}
  ◊gla{ša penas atrec-it g\ennel-el se?}
  ◊glb{%int %pr.1%sg.%loc conquer-%inf %pfv\have_done-1%sg.%inv %rhet}
  ◊glfree{What will I have achieved?}
}

◊gloss/x{
  ◊glfree{◊nc{envas imîr neðeł annel došorcre so}}
  ◊gla{env-as i-mîr neð-eł ant-nel do-šorcr-e so}
  ◊glb{day-%dat.%sg %adn-after day-%loc.%gc time-%acc.%di %caus-burn-1%sg if}
  ◊glfree{If I waste time day after day,}
}

◊gloss/x{
  ◊glfree{◊nc{riłiþerin dosorva so}}
  ◊gla{riłiþ-erin do-sorv-a so}
  ◊glb{lazy-%qual.%acc.%sg %caus-wash_away-1%sg if}
  ◊glfree{If I let laziness wash me away,}
}

◊gloss/x{
  ◊glfree{◊nc{elret flen fêmîr sortaþan šino cerectês is censit marðel.}}
  ◊gla{elr-et flen fê-mîr sortaþan šin-o cerec-tês is cens-it marð-el.}
  ◊glb{year-%sembl.%pl 256 3%gc-after be_one_of-%rel.%acc,%nom.%cel all-%nom.%sg forget.%patient-%dat.%sg %rot0 equal-%inf inevitably-3%sg.%inv}
  ◊glfree{Then all I’m fated to be in 256 years is someone forgotten.}
}

◊gloss/x{
  ◊glfree{◊nc{feljan racriþ narviłos roc lê gcjani nêrge;}}
  ◊gla{felj-an racr-iþ narv-iłos roc lê g\cjan-i nêrg-e;}
  ◊glb{this_idea-%acc.%sg know-%ser sing-%inf.%dat.%subj %ben this.%cel poem-%dat.%sg compose-1%sg}
  ◊glfree{Knowing this, I compose this poem to be sung;}
}

◊gloss/x{
  ◊glfree{◊nc{aspoþ’pe mîr’moc c·erifos roc cþere.}}
  ◊gla{asp-oþ=’pe mîr=’moc c·er-ifos roc cþer-e.}
  ◊glb{lifetime-%dat.%sg=%poss.1 after=also remain-%inf.%dat.%subj %ben write-1%sg}
  ◊glfree{I write it so that it may remain even after my lifetime.}
}

◊gloss/x{
  ◊glfree{◊nc{eti crešit cfjoþelt mjonelþa meðałes nîs mivełen cenvat nevla.}}
  ◊gla{eti creš-it cfjoþ-elt mjon-elþa með-ałes nîs miveł-en cenv-at nevl-a.}
  ◊glb{%inf.%sembl.%subj hear-%inf leaf-%loc.%sg number-%abess.%sg night-%dat.%gc through word-%acc.%gc write-%inf do_repeatedly-1%sg}
  ◊glfree{Through countless nights, I repeatedly write words on the page so they may be heard.}
}

◊b{◊xref/p{chorus-start} – ◊xref/p{chorus-end} are repeated here.}

◊gloss/x{
  ◊glfree{◊nc{falþon’po fêmîr elêþ grisfials ar morarþ}}
  ◊gla{falþ-on=’po fê-mîr el-êþ grisfi-als ar mor-arþ}
  ◊glb{short_time-%acc.%sg=by 3%gc-after sun-%nom.%sg horizon-%dat.%sg toward descend-%ser}
  ◊glfree{Soon, the sun will set into the horizon,}
}

◊gloss/x{
  ◊glfree{◊nc{tacrinas vetłac·eâns nesmâeftês ndotosal.}}
  ◊gla{tacrin-as vetłac·e-âns nesmâev-tês n\do-tos-al.}
  ◊glb{darken-%rel.%nom,%dat.%sg salmon_color-%acc.%di sky-%dat.%di %pfv\%caus-dissolve-3%sg.%inv}
  ◊glfree{Dissolving orange into the darkening sky.}
}

◊gloss/x{
  ◊glfree{◊nc{ea łirlen el relcasor’moc menat ŋgeðos;}}
  ◊gla{ea łirl-en el relc-asor=’moc men-at ŋ\geð-os;}
  ◊glb{then light-%gen.%co this.%ter wing-%dat.%pl=also see-%inf %pfv\cannot-3%gc}
  ◊glfree{Then even these wings of light will not be visible,}
}

◊gloss/x{
  ◊glfree{◊nc{pejasnełor ceriþ šino meða nenso’ce.}}
  ◊gla{pe-jasl-nełor cer-iþ šin-o með-a nen-so=’ce.}
  ◊glb{1%sg-guide-%gerundv.%attr.%ter remain-%rel.%nom,%nom.%cel all-%nom.%sg moon-%nom.%sg star-%nom.%co=and}
  ◊glfree{Leaving only the moon and stars to guide me.}
}

◊gloss/x{
  ◊glfree{◊nc{ai enven sodas erłen sartan drênime}}
  ◊gla{ai env-en sod-as erł-en sart-an drên-ime}
  ◊glb{but day-%gen.%sg next-%loc.%sg gold-%gen.%sg ring-%acc.%sg awake-and}
  ◊glfree{But the golden ring will rise again the next day,}
}

◊gloss/x{
  ◊glfree{◊nc{šadarget łana motos cþasos domenat ħare.}}
  ◊gla{šadarg-et łan-a mot-os cþas-os do-men-at ħar-e.}
  ◊glb{canopy-%sembl.%pl cloud-%gen.%co backside-%loc.%di gradient-%dat.%sg %caus-see-%inf again-3%sg.%pfv}
  ◊glfree{Showing a gradient behind the clouds like the canopies.}
}

◊gloss/x{
  ◊glfree{◊nc{le en seljo lirnat en têmit’ce maoþ lef}}
  ◊gla{le en selj-o lirn-at en têm-it=’ce ma-oþ lef}
  ◊glb{%imp %inf.%gen afternoon-%nom.%sg bright-%inf %inf.%gen precipitate-%inf=and which-%dat.%sg orthogonal_to}
  ◊glfree{Whether the afternoon is sunny or rainy,}
}

◊gloss/x{
  ◊glfree{◊nc{afčaþans lê genvas’moc melraþ.}}
  ◊gla{afčaþ-ans lê g\env-as=’moc melr-aþ.}
  ◊glb{hope-%rel.%acc,%dat.%cel this.%cel day-%dat.%sg=also savor-1%pl}
  ◊glfree{Let’s enjoy this hoped-for day.}
}

◊b{◊xref/p{chorus-start} – ◊xref/p{chorus-end} are repeated here.}

◊gloss/x{
  ◊glfree{◊nc{caðjanen eltêns tfel cfatles teltos il}}
  ◊gla{caðjan-en elt-êns tfel cfatl-es telt-os il}
  ◊glb{world-%gen.%di river-%dat.%sg across intimidate-%rel.%nom,%dat.%cel cliff-%dat.%co on_top}
  ◊glfree{On top of the formidable cliffs beyond the world’s river}
}

◊gloss/x{
  ◊glfree{◊nc{terałei *galama anagrat cenmire se.}}
  ◊gla{terał-ei *galam-a anagr-at cenmir-e se.}
  ◊glb{accomplishment-%gen.%di secret-%nom.%sg hide-%inf seem_to-3%sg %mir}
  ◊glfree{Seems to hide the secret to self-accomplishment.}
}

◊gloss/x{
  ◊glfree{◊nc{telmaðirþ cehit geðates aline}}
  ◊gla{telmaðirþ ceh-it geð-ates alin-e}
  ◊glb{fierce-%ser reach-%inf cannot-%rel.%acc,%dat.%cel goal-%dat.%sg}
  ◊glfree{Resolutely, toward the goal that can’t be reached,}
}

◊gloss/x{
  ◊glfree{◊nc{ar sêrnime mitrit denestâs tal!}}
  ◊gla{ar sêrn-ime mitr-it denest-âs tal!}
  ◊glb{toward stand_up-and fast-%inf continue-1%pl.%incl %assert}
  ◊glfree{Let’s rise up and keep running!}
}

◊section{Resource credits}

◊details{
    ◊summary{Music}
    ◊dl[#:class "infobox"]{
        ◊dt{DAW}◊dd{◊link["https://ardour.org/"]{Ardour} 7.0.0}◊;
        ◊dt{Instrument plugins}◊dd{◊link["https://surge-synthesizer.github.io/index.html"]{Surge XT}; DrumSynth (◊link["https://github.com/distrho/distrho-ports"]{DISTRHO Ports} version)}◊;
        ◊dt{Other plugins}◊dd{MDA RezFilter (◊link["https://gitlab.com/drobilla/mda-lv2"]{LV2 port}); ◊link["https://lsp-plug.in/index.php"]{LSP} plugins}◊;
    }
}
