#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Re:curs}
◊define-meta[date]{2023-05-29 – ?}
◊define-meta[book-author]{+merlan #flirora}

Describing Ŋarâþ Crîþ in Ŋarâþ Crîþ.

◊section[#:id "goals"]{Goals}

◊items{
    ◊item{Invent Ŋarâþ Crîþ terminology for Ŋarâþ Crîþ grammar}
    ◊item{Eventually write a bilingual grammar of Ŋarâþ Crîþ in Ŋarâþ Crîþ and English}
}

The rewrite of the bilingual grammar might coincide with drastic changes in how Ŋarâþ Crîþ works.

◊section[#:id "challenges"]{Challenges}

Most of this is conceptually straightforward; however, the current Ŋarâþ Crîþ v9 grammar uses mathematical notation in some sections (such as ◊link["/grammar/phonology/layer0.html"]{Layer 0: the assemblage structure}). Glosses might also need some special treatment.

In addition, we need to implement constructs for laying out bilingual texts, such as parallel two-column translations and figures with bilingual captions.

◊section[#:id "checklist"]{Checklist of terminology}

In the future, the glossary might be reinstated to store Ŋarâþ Crîþ terms for its concepts.

◊subsection[#:id "checklist-orthography"]{Orthography and phonology}

◊items{
    ◊item{Layer 0:}
    ◊item{Layer 1:}
    ◊item{Layer 2w:}
    ◊item{Layer 2w*:}
    ◊item{Layer 3w:}
    ◊item{Layer 4w:}
    ◊item{Layer 2s:}
    ◊item{Layer 3s:}
    ◊item{Grapheme:}
    ◊item{Glyph:}
    ◊item{Letter: ◊i{◊nc{cenvos}}}
    ◊item{True letter:}
    ◊item{USR letter:}
    ◊item{Marker:}
    ◊item{Punctuation: ◊i{◊nc{dono}}}
    ◊item{Digit:}
    ◊item{Letter number:}
    ◊item{Letter sum:}
    ◊item{Numquote:}
    ◊item{Backreference:}
    ◊item{Assemblage:}
    ◊item{State machine:}
    ◊item{State:}
    ◊item{Syllabic state:}
    ◊item{Onset state:}
    ◊item{Glide state:}
    ◊item{Nuclear state:}
    ◊item{Terminal state:}
    ◊item{Manifested grapheme phrase:}
    ◊item{Plain letter:}
    ◊item{Base letter:}
    ◊item{Consonant:}
    ◊item{Vowel:}
    ◊item{Semivowel:}
    ◊item{Effective plosive or fricative:}
    ◊item{Tone (Layer 0 – 2s):}
    ◊item{Hatted vowel:}
    ◊item{Unhatted vowel:}
    ◊item{Initial:}
    ◊item{Medial:}
    ◊item{Coda:}
    ◊item{Simple coda:}
    ◊item{Complex coda:}
    ◊item{Onset:}
    ◊item{Initial:}
    ◊item{Bridge:}
    ◊item{Canonical (bridge):}
    ◊item{◊var{ξ}-transformation:}
    ◊item{Deduplication:}
    ◊item{Concatenation:}
    ◊item{Stem fusion:}
    ◊item{Phoneme:}
    ◊item{Phone:}
    ◊item{Stress:}
    ◊item{Tone (Layer 3s):}
    ◊item{Tone accounting unit:}
    ◊item{Kerning:}
    ◊item{Ligature:}
}

◊subsection[#:id "checklist-syntax"]{Syntax}

◊subsection[#:id "checklist-morphology"]{Morphology}

◊items{
    ◊item{Consonant mutation:}
    ◊item{Lenition: ◊i{◊nc{arðerþ}} (◊i{◊nc{arðat}})}
    ◊item{Eclipsis: ◊i{◊nc{sacrinerþ}} (◊i{◊nc{sacrinit}})}
}

◊subsection[#:id "checklist-lexicon"]{Lexicon}
