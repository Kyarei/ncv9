#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Elaine}
◊define-meta[date]{2022-04-02 – 2022-06-14}
◊define-meta[book-author]{+merlan #flirora}

◊blockquote[
    "https://en.wikipedia.org/wiki/Elaine_(legend)"
    ◊@{— ◊cite/inbook{Elaine (legend)} in ◊link["https://en.wikipedia.org/wiki/Elaine_(legend)"]{◊cite{Wikipedia}}}]{
  ◊p{◊b{Elaine} is a name shared by several different female characters in Arthurian legend.}
}

Reforming Ŋarâþ Crîþ morphophonology.

◊section[#:id "current-state"]{The current state of Ŋarâþ Crîþ morphophonology}

The phonotactics of Ŋarâþ Crîþ distinguish between ◊term{simple} and ◊term{complex} codas, where complex codas may occur only at the end of a word. If morphology causes a complex coda to occur word-medially (such as by adding a suffix), and the bridge containing this coda cannot be reinterpreted as one without a complex coda, then a set of ◊term{coda simplification rules} takes place to remove the word-medial complex coda.

During Project Caladrius, it has been observed that some bridges, such as ◊l0{-cð-} and ◊l0{-fp-} rarely occur, to the degree that stem fusion with stems ending in such bridges are difficult to define. During Ŋarâþ Crîþ Corner #0x00, I have proposed the idea of restricting the set of valid bridges to a subset of all coda–onset combinations. To find what bridges are worth saving, I decided to analyze frequencies of bridges in a corpus of ŊCv9 text, but this has not yet provided much insight.

Originally, Project Caladrius was meant to revise the noun declension system, but the proposal of an operation that would make a stem have a valid coda such that a suffix beginning with a consonant could be attached to it, and the refinement of the idea into the current notion of ◊term{stem fusion}, has expanded the scope of the project and made me question if the current morphophonology of Ŋarâþ Crîþ was adequate.

◊section[#:id "bridges"]{Burning bridges}

If a statistical analysis is not insightful, then reasoning from first principles to find which bridges should be valid might be. Here are the first three that come up in my mind:

◊enum{
  ◊item{Lenition in the onset does not affect the validity of a bridge.}
  ◊item{The presence of ◊l0{j} in the onset does not affect the validity of a bridge.}
  ◊item{All bridges that can be interpreted as having a coda that is null, ◊l0{-r}, or ◊l0{-l} are valid.}
}

That is, if ◊l0{-lt-} is valid as an onset, then ◊l0{-lt·-}, ◊l0{-ltj-}, and ◊l0{-lt·j-} are valid as well. Note that this does not say anything about the validity of the homophonous ◊l0{-lþ-}. Indeed, there might exist invalid bridges that are homophonous with valid bridges.

After that, there are two ways to approach the problem:

◊items{
  ◊item{The synchronic approach: find out which bridges don’t sound too awkward and keep those.}
  ◊item{The diachronic approach: assume that all coda–onset combinations were once valid bridges, then use sound changes to narrow the set. Has the advantage of informing us how illegal bridges created by affixation are treated.}
}

◊section[#:id "ends"]{Odds and ends}

◊make-figure["elaine-boundary-diagram.svg" #:alt "The different types of boundaries."]{The different types of boundaries in Ŋarâþ Crîþ: ◊term{syllabic} between syllables, ◊term{glide} between the initial and the medial, ◊term{onset} between the onset and the nucleus, and ◊term{nuclear} between the nucleus and the coda.}

Project Caladrius has formalized the notion of a ◊term{stem} – namely, as a sequence of one or more syllables plus an onset. That is, the beginning of the stem marks a syllable boundary, while the end of a stem marks the boundary between an onset and the following nucleus.

Some operations are possible on some types of ends but not others. For instance, ending a word or adding a suffix such as ◊l0{-ten} is not possible on an onset end, but adding a suffix such as ◊l0{-a} is. (In this case, stem fusion is supposed to provide equivalents of the first two operations on an onset end.)

Affixes also have starts and ends. Prefixes always have syllabic starts, and suffixes always have syllabic ends. A sequence of letters making an affix might be compatible with multiple types of starts or ends; for instance, ◊l0{-a} may have either a syllabic or onset start, depending on the context.

Additionally, there is another type of end called a ◊term{terminal end}, to which morphemes with syllabic starts cannot be attached. This end is produced by the presence of a complex coda at the end - that is, sword-medial complex codas are never produced in the first place.

◊section[#:id "bridge-rules"]{Detailed rules for valid bridges}

We reproduce the three initial rules proposed here, with some elaborations:

◊enum{
  ◊item{Lenition in the onset does not affect the validity of a bridge. (This rule, however, does not establish a further relationship between the results of resolving two bridges that differ only by lenition in the onset. It also does not prevent lenition from affecting ◊em{canonicality}.)}
  ◊item{The presence of ◊l0{j} in the onset does not affect the validity of a bridge.}
  ◊item{All bridges that can be interpreted as having a coda that is null, ◊l0{-r}, or ◊l0{-l} are valid.}
  ◊item{◊b{New:} If a bridge with a complex initial ◊var{I} is valid, then the bridge with an initial containing only the first consonant of ◊var{I} is also valid.}
}

The following subsections describe situations in which bridges are invalid and must be repaired during concatenation. A bridge that is repaired might need further repair by later rules.

Importantly, bridge repair might cause earlier segments to change, and it is not idempotent: ◊l0{-sð-} is repaired to ◊l0{-ss-}, but ◊l0{-ss-} is repaired to ◊l0{-þ-}.

Also note that ◊l0{-cþ} is now promoted to a simple coda.

◊subsection[#:id "bridge-rules-coalesce"]{Coalescence of ◊l0{-tš-}}

The bridge ◊l0{-tš-} is changed to ◊l0{-č-}.

◊subsection[#:id "bridge-rules-fortition"]{Fortition of ◊l0{h-} and ◊l0{ħ-}}

The onset ◊l0{h-} is fortited to ◊l0{c-} after ◊l0{-s}, ◊l0{-þ}, ◊l0{-rþ}, ◊l0{-t}, ◊l0{-c}, ◊l0{-f}, or ◊l0{-cþ}. ◊l0{hr-} and ◊l0{hl-} are fortited analogously.

The onset ◊l0{ħ-} is fortited to ◊l0{g-} after ◊l0{-t}, ◊l0{-c}, and ◊l0{-f}. ◊l0{ħr-} and ◊l0{ħl-} are fortited analogously.

◊subsection[#:id "bridge-rules-metathesis"]{Metathesis of ◊l0{t} before ◊l0{c} or ◊l0{g}}

◊l0{-tc-} and ◊l0{-tg-} are metathesized to ◊l0{-ct-} and ◊l0{-cd-}, respectively. Likewise, ◊l0{-tcr-}, ◊l0{-tcl-}, ◊l0{-tgr-}, and ◊l0{-tgl-} are metathesized to ◊l0{-ctr-}, ◊l0{-ctl-}, ◊l0{-cdr-}, and ◊l0{-cdl-}.

Similar bridges with lenited onsets, such as ◊l0{-tc·-} and ◊l0{-tg·r-} are treated analogously, with the resulting onset remaining lenited.

◊l0{t} is deleted before ◊l0{cf}, ◊l0{cþ}, ◊l0{cš}, ◊l0{cš}, ◊l0{gv}, and ◊l0{gð}, devoicing the last two of these.

◊subsection[#:id "bridge-rules-assimilation-n"]{Nasal assimilation}

For these rules, ◊l0{m·} is counted as a nasal, even though it is pronounced as a fricative.

◊l0{-t} before a nasal onset assimilates to ◊l0{-n}.

◊l0{-c} before a nasal onset assimilates to ◊l0{-ŋ}. This is further corrected: ◊l0{-aŋ}, ◊l0{-oŋ}, and ◊l0{-uŋ} to ◊l0{-or}, and ◊l0{-eŋ} and ◊l0{-iŋ} to ◊l0{-jor} (coalescing the ◊l0{j} if necessary), and analogously with the hatted vowels. As a special case, ◊l0{-cŋ-} is repaired to ◊l0{-ŋ-} instead.

◊subsection[#:id "bridge-rules-denasalization"]{Denasalization of ◊l0{ŋ-}}

After ◊l0{-s}, ◊l0{-þ}, ◊l0{-rþ}, ◊l0{-f}, and ◊l0{cþ}, ◊l0{ŋ-} is denasalized to ◊l0{g-}.

◊subsection[#:id "bridge-rules-devoicing"]{Devoicing of ◊l0{v-} and ◊l0{ð-}}

After ◊l0{-þ}, ◊l0{-rþ}, ◊l0{-t}, ◊l0{-c}, ◊l0{-f}, and ◊l0{-cþ}, ◊l0{v-} devoices to ◊l0{f-} and ◊l0{ð-} devoices to ◊l0{þ-}. Additionally, ◊l0{ð-} is devoiced after ◊l0{-s}.

This process occurs analogously for the onsets ◊l0{vr-}, ◊l0{vl-}, ◊l0{ðr-}, and ◊l0{ðl-}, except that ◊l0{-þ} is deleted before ◊l0{vr-} and ◊l0{vl-} instead. Additionally, ◊l0{-rþCR-} onsets (with R = ◊l0{r} or ◊l0{l} and C = ◊l0{v} or ◊l0{ð}) are corrected to ◊l0{-RC-}.

As usual, similar rules apply to lenited onsets: ◊l0{v·} devoices to ◊l0{f·}, and ◊l0{ð·} is replaced with a copy of the preceding consonant.

◊subsection[#:id "bridge-rules-assimilation-ths"]{Assimilation of ◊l0{s} after ◊l0{þ}}

After a ◊l0{þ}, ◊l0{s} is replaced with ◊l0{þ}. Additionally, ◊l0{ss} is coalesced into ◊l0{þ}, unless it is not followed by a consonant and the latter ◊l0{s} arose from a ◊l0{ð} in the previous step.

◊subsection[#:id "bridge-rules-degemination"]{Degemination before another consonant}

◊l0{þ}, ◊l0{t}, ◊l0{c}, and ◊l0{f} are degeminated before another consonant in the onset; for instance, ◊l0{-ffr-} is corrected to ◊l0{-fr-}, and ◊l0{-ccs-} is corrected to ◊l0{-cs-}. ◊l0{td} is degeminated to ◊l0{d}, and ◊l0{cg} is degeminated to ◊l0{g}.

This rule also applies when the second instance of the degeminated consonant is lenited, in which case the first copy of the consonant is elided: ◊l0{-tt·l-} → ◊l0{-t·l-}.

◊subsection[#:id "bridge-rules-shorten4"]{Partial coda elision of bridges with ◊l0{-rþ} and ◊l0{-cþ} codas}

If the coda is ◊l0{-rþ}, then it becomes ◊l0{-r} before a fricative followed by ◊l0{r} or ◊l0{l}, or before the onsets ◊l0{cf-}, ◊l0{cþ-}, ◊l0{cs-}, ◊l0{cš-}, ◊l0{tf-}, and ◊l0{dv-}. Before any other two-letter onset, it becomes ◊l0{-þ}.

If the coda is ◊l0{-cþ}, then it is maintained before the onsets ◊l0{þ-}, ◊l0{š-}, ◊l0{m·-}, ◊l0{t-}, ◊l0{ħ-}, ◊l0{m·-}, or ◊l0{t·-}. Before ◊l0{cf-}, ◊l0{cþ-}, ◊l0{cs-}, ◊l0{cš-}, or ◊l0{tf-}, or before any of the onsets consisting of ◊l0{þ}, ◊l0{š}, or ◊l0{ħ} followed by ◊l0{r} or ◊l0{l}, the onset loses its first consonant, and ◊l0{cs-} additionally becomes ◊l0{þ-}. In all other cases, the coda becomes ◊l0{-þ}.

◊section[#:id "layer-0-to-1"]{A revised view of the layer-0-to-1 pipeline}

Morphemes are no longer viewed as raw MGP sequences; rather, they are viewed structurally. Some constraints apply to all morphemes:

◊items{
  ◊item{all codas that do not end a word must be simple}
  ◊item{all bridges must be valid}
  ◊item{there are no instances of ◊l0{ji}, ◊l0{jî}, or ◊l0{ju}}
}

When morphemes are joined together, repair processes must maintain these invariants at the join boundary. Morphemes that end in complex codas are simply forbidden from having anything appended to them. The second invariant is maintained by applying bridge correction when two morphemes are joined at a syllable boundary, and the third invariant is maintained by eliding the ◊l0{j} if necessary when joining at an onset boundary.

In addition, there are environments that may naturally occur within a morpheme but are repaired away when created by appending morphemes. For now, these include deduplication targets such as ◊l0{-vav-} and ◊l0{-ðoð-}, which arise when joining morphemes at a glide, onset, or nuclear boundary.

Complex coda simplification will no longer need to occur from layer 0 to 1, since complex codas cannot have anything appended to them in the first place.

More formally, we adopt the following notation:

◊items{
  ◊item{◊${\mathcal{S}_{xy}} is the set of all morphemes with start type ◊${x} and end type ◊${y}, with ◊${x} and ◊${y} being one of ◊${s} (syllabic), ◊${g} (glide), ◊${o} (onset), ◊${n} (nuclear), or ◊${\omega} (terminal).
    ◊items{
      ◊item{◊${\mathcal{S}_{xy}^{n}} is the subset of ◊${\mathcal{S}_{xy}} whose elements undergo ◊${n} cycles from ◊${x} back to ◊${x}.
        ◊items{
          ◊item{Ex: ◊${\mathcal{S}_{xx}^{0}} contains only the empty string for all boundary types ◊${x}.}
          ◊item{Ex: ◊${\mathcal{S}_{sg}^{0}} is the set of all initials.}
          ◊item{Ex: ◊${\mathcal{S}_{no}^{1}} includes ◊l0{stafc} but not ◊l0{þþj} or ◊l0{tatag}.}
        }
      }
    }
  }
  ◊item{Given ◊${\alpha \in \mathcal{S}_{xy}} and ◊${\beta \in \mathcal{S}_{yz}}, ◊${\alpha\cat\beta \in \mathcal{S}_{xz}} is the result of appending ◊${\alpha} and ◊${\beta}, performing repair processes as necessary.
    ◊items{
      ◊item{Ex: if ◊${\alpha = \cv{feva} \in \mathcal{S}_{ss}} and ◊${\beta = \cv{ve} \in \mathcal{S}_{ss}}, then ◊${\alpha\cat\beta = \cv{fenave} \in \mathcal{S}_{ss}}.}
      ◊item{This operation is also defined for ◊${\alpha \in \mathcal{S}_{xo}} and ◊${\beta \in \mathcal{S}_{gz}}, in which case the glides at the end of ◊${\alpha} and the start of ◊${\beta} are merged.}
      ◊item{◊${\alpha\beta} is the result of appending ◊${\alpha} and ◊${\beta} ◊em{without} performing any repair processes.}
    }
  }
  ◊item{◊${\Iota = \mathcal{S}_{sg}^{0}} (the capital letter iota) is the set of all initials.}
  ◊item{◊${\Mu = \mathcal{S}_{go}^{0} = \{\epsilon_{\Mu}, \cv{j}\}} (capital mu) is the set of all glides.}
  ◊item{◊${\Nu = \mathcal{S}_{on}^{0}} (capital nu) is the set of all vowels.}
  ◊item{◊${\Kappa = \mathcal{S}_{ns}^{0}} (capital kappa) is the set of all simple codas.}
  ◊item{◊${\Omega = \mathcal{S}_{n\omega}^{0}} is the set of all codas, simple or complex.}
  ◊item{◊${\Gamma = \mathcal{S}_{ng}^{0}} is the set of all coda–onset pairs. The glide is not included because stems ending in ◊l0{j} are treated specially in stem fusion.}
  ◊item{◊${\Pi} is the set of effective plosives and fricatives – that is, the set of consonants that can form an initial when followed by ◊l0{l} or ◊l0{r}.}
  ◊item{◊${\Tau \subset \mathcal{S}_{so} \setminus \mathcal{S}_{so}^0} is the set of valid stems. A stem must contain at least one syllable, and its final onset must not contain a lenited consonant.}
}

◊section[#:id "stem-fusion"]{Stem fusion}

The difference from Project Caladrius is that the stem and the result are treated structurally instead of linearly. The notation is also different – given a stem ◊${\tau \in \Tau}:

◊items{
  ◊item{◊${\tau^{\epsilon} \in \mathcal{S}_{s\omega}} is the result of fusing ◊${\tau} with a null consonant.}
  ◊item{◊${\tau^{\theta} \in \mathcal{S}_{so}} is the result of fusing ◊${\tau} with a non-null consonant ◊${\theta \in \{\cv{n}, \cv{t}, \cv{þ}\}}.}
}

The following variables are used: ◊${\Sigma_{xy} \in \mathcal{S}_{xy}}, ◊${\gamma \in \Gamma}, ◊${\iota \in \Iota}, ◊${\nu \in \Nu}, ◊${\kappa \in \Kappa}, ◊${\omega \in \Omega}, ◊${\theta \in \{\cv{n}, \cv{t}, \cv{þ}\}}.

As usual, earlier rules take precedence over later ones. In addition, we use a shorthand for C-invariant rules: a rule such as ◊${\tau \leadsto \sigma}, where ◊${\tau \in \Tau} and ◊${\sigma \in \mathcal{S}_{ss}} is interpreted as the rules ◊${\tau^\epsilon = \sigma_{s\omega}} and ◊${\tau^\theta = \sigma \cat \theta}.

Another shorthand used in this document is ◊${\tau \curvearrowright \tau'}, which implies ◊${\tau^\epsilon = (\tau')^\epsilon} and ◊${\tau^\theta = (\tau')^\theta}.

◊subsection[#:id "fusion-j"]{Stems ending in ◊l0{j}}

◊align-math{
  \Sigma_{si} \cv{j} &\leadsto \Sigma_{si} \cat \cv{i} \tag{FinalJ}
}

From now on, any explicit instances of ◊${\epsilon_{\Mu}} will be omitted.

◊subsection[#:id "fusion-onset-alias"]{Onset aliasing}

◊align-math{
  (\Sigma_{ss} \iota)^\epsilon &= (\Sigma_{ss} \cat \cv{s})^\epsilon \os{\(\iota \in \{\cv{t}, \cv{d}\}\)} \tag{Alias}
}

◊subsection[#:id "fusion-coda"]{Valid codas}

◊align-math{
  (\Sigma_{sn} \gamma)^\epsilon &= \Sigma_{sn} \cat \gamma_\Omega \os{\(\gamma \in \Omega\)} \notag \\
  (\Sigma_{sn} \gamma)^\theta &= \Sigma_{sn} \cat \gamma_\Kappa \cat \theta \os{\(\gamma \in \Kappa\)} \tag{ValidCoda}
}

◊subsection[#:id "fusion-degemination"]{Degemination}

◊align-math{
  \Sigma_{sn} \kappa \iota &\leadsto \Sigma_{sn} \cat \delta(\kappa) \os{\(\kappa = \iota\) and \(|\kappa| = 1\)} \tag{Degeminate}
}

where ◊${|\kappa|} is the number of manifested grapheme phrases in ◊${\kappa} and

◊$${
  \delta(\kappa) = \begin{cases}
    \cv{l} \os{\(\kappa = \cv{r}\)} \\
    \cv{þ} \os{\(\kappa = \cv{s}\)} \\
    \kappa   \osamgen
  \end{cases}
}

◊subsection[#:id "fusion-epenthesis"]{Vowel epenthesis}

◊align-math{
  \Sigma_{sn} \kappa \iota &\leadsto \Sigma_{sn} \epsilon_\Kappa \kappa_\iota \cat \cv{e}_\Nu \cat \iota_\Kappa \os{\(\iota \in \Iota \cap \Kappa\), \(\kappa \in \{\cv{r}, \cv{l}\}\)} \tag{Epenthesis-LC}
}

◊subsection[#:id "fusion-nasal"]{Nasal merging}

◊align-math{
  \Sigma_{sn} \cv{n}_\Kappa \cv{d}_\Iota &\curvearrowright \Sigma_{sn} \cv{n}_\Iota \notag \\
  \Sigma_{sn} \cv{n}_\Kappa (\iota^1 \iota^2)_\Iota &\curvearrowright \Sigma_{sn} \cv{n} (\iota^2)_\Iota \os{\(\iota^1 = \cv{d}\)} \notag \\

  \Sigma_{sn} \cv{n}_\Kappa \cv{d}_\Iota &\curvearrowright \Sigma_{sn} \cv{ŋ}_\Iota \notag \\
  \Sigma_{sg} \mu \nu \cv{n}_\Kappa (\iota^1 \iota^2)_\Iota &\curvearrowright \Sigma_{sg} \xi(\mu \nu) \cv{r} (\iota^2)_\Iota \os{\(\iota^1 \in \{\cv{c}, \cv{g}\}\)} \tag{NasalMerge1} \\
  (\Sigma_{sn} \epsilon_\Kappa \cv{m})^\cv{n} &= \Sigma_{sn} \cv{nm} \notag \\
  (\Sigma_{ss} \cv{m})^\cv{n} &= \Sigma_{ss} \cat \cv{ôm} \notag \\
  (\Sigma_{ss} \cv{n})^\cv{n} &= \Sigma_{ss} \cat \cv{enn} \notag \\
  (\Sigma_{so} \nu \kappa \iota)^\cv{n} &= \Sigma_{so} \chi(\nu) \kappa \cat \iota^{\cv{dðv}/\cv{nnm}} \os{\(\iota \in \{\cv{d}, \cv{ð}, \cv{v}\}\)} \tag{NasalMerge2}
}

◊${\xi: \mathcal{S}_{gn}^0 \rightarrow \mathcal{S}_{gn}^0} is the ◊i{ξ}-transformation; i.e.

◊$${
  \xi(\mu \nu) = \begin{cases}
    \mu \cv{o} \os{\(\nu \in \{\cv{a}, \cv{o}, \cv{u}\}\)} \\
    \mu \cv{ô} \os{\(\nu \in \{\cv{â}, \cv{ô}\}\)} \\
    \cv{jo} \os{\(\nu \in \{\cv{e}, \cv{i}\}\)} \\
    \cv{jô} \os{\(\nu \in \{\cv{ê}, \cv{î}\}\)}
  \end{cases}
}

◊${\chi: \Nu \rightarrow \Nu} inverts the tone of a vowel; i.e.

◊$${
  \chi(\nu) = \nu^{\cv{aeioâêîô}/\cv{âêîôaeio}}
}

◊subsection[#:id "fusion-obstruent"]{Obstruent merging}

◊align-math{
  \Sigma_{sn} \cv{rþ} \iota &\curvearrowright \Sigma_{sn} \cv{r} \cat \iota \os{\(\varphi(\cv{þ}, \iota)\)} \notag \\
  \Sigma_{sn} \cv{cþ} \iota &\curvearrowright \Sigma_{sn} \cv{c} \cat \iota \os{\(\varphi(\cv{þ}, \iota)\)} \notag \\
  \Sigma_{sn} \kappa \iota &\curvearrowright \Sigma_{sn} \epsilon \cat \iota \os{\(\varphi(\kappa, \iota)\)} \tag{FricMerge1} \\
  (\Sigma_{ss} (\iota^1 \iota^2)_\Iota)^\theta &= (\Sigma_{ss} \cat \iota^1)^\theta \os{\(\varphi(\iota^2, \theta)\)} \notag \\
  (\Sigma_{ss} \iota)^\theta &= \Sigma_{ss} \cat \theta \os{\(\varphi(\iota, \theta)\)} \tag{FricMerge2}
}

where

◊align-math{
  \varphi(\kappa, \iota) \iff& (\kappa, \iota) \in F \lor [\exists \pi \in \Pi, \rho \in \{\cv{r}, \cv{l}\} : \iota = \pi\rho \land (\kappa, \pi) \in F] \notag \\
  F = \{ & (\cv{f}, \cv{þ}), (\cv{þ}, \cv{þ}), (\cv{t}, \cv{þ}), (\cv{d}, \cv{þ}) \notag \\
         & (\cv{f}, \cv{f}), (\cv{v}, \cv{f}), (\cv{þ}, \cv{f}), \notag \\
         & (\cv{t}, \cv{t}), (\cv{t}, \cv{n}) \} \notag
}

◊subsection[#:id "fusion-final-devoice"]{Final devoicing}

◊align-math{
  (\Sigma_{ss} \iota)^\epsilon &= [\Sigma_{ss} h(\iota)]^\epsilon \os{\(\iota \in \{\cv{v}, \cv{m}, \cv{d}, \cv{ð}\}\)} \notag \\
  (\Sigma_{ss} \iota)^\theta &= [\Sigma_{ss} h(\iota)]^\theta \os{\(\iota \in \{\cv{v}, \cv{m}, \cv{d}, \cv{ð}\}\)} \os{\(\theta \in \{\cv{t}, \cv{þ}\}\)} \tag{FinalDevoice}
}

where

◊$${
  h(\iota) = \iota^{\cv{vmdð}/\cv{fflþ}}
}

◊subsection[#:id "fusion-cl"]{Stems ending in consonant–liquid onsets}

Let ◊${\rho \in \{\cv{r}, \cv{l}\}} and ◊${\pi \in \Pi}. Then ◊${\pi\rho \in \Iota}.

◊align-math{
  \Sigma_{sn} \epsilon_\Kappa \iota \nu \epsilon_\Kappa (\pi \rho) &\leadsto \Sigma_{sn} \iota_{\Kappa} \cat \pi_{\Iota} \cat \nu \cat \rho_{\Kappa} \os{\(\iota \in \{\epsilon, \cv{s}, \cv{n}, \cv{l}\}\)} \tag{Cl-Meta} \\
  (\Sigma_{sn} \epsilon_{\Kappa} (\cv{t}_\Pi \rho))^\cv{t} &= \Sigma_{sn} \cat \cv{r}_\Kappa \cat \cv{t} \notag \\
  (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{t} &= (\Sigma_{sn} \epsilon_{\Kappa} \pi_\Iota)^\epsilon \triangleleft \cv{d} \tag{Cl-NoCoda-T} \\
  (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{n} &= \Sigma_{sn} (\iota^{\cv{dc}/\cv{rs}})_\Kappa \cat \cv{ŋ} \os{\(\pi \in \{\cv{d}, \cv{c}\}\)} \notag \\
  (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{n} &= (\Sigma_{sn} \epsilon_\Kappa \pi_\Iota)^\cv{n} \tag{Cl-NoCoda-N} \\
  (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{þ} &= \Sigma_{sn} \cat \cv{r}_\Kappa \cat \pi \os{\(\pi \in \{\cv{þ}, \cv{ð}\}\)} \notag \\
  (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{þ} &= (\Sigma_{sn} \epsilon_\Kappa \pi_\Iota)^\epsilon \triangleleft \cv{ð} \tag{Cl-NoCoda-Þ} \\
  (\Sigma_{ss} (\pi \cv{r}))^\epsilon &= (\Sigma_{ss} \pi_\Iota) \cat \cv{ôr} \notag \\
  (\Sigma_{ss} (\pi \cv{l}))^\epsilon &= (\Sigma_{ss} \pi_\Iota) \cat \cv{êl} \tag{Cl-Nil} \\
  (\Sigma_{ss} (\pi \rho))^\theta &= (\Sigma_{ss} \pi_\Iota) \cat \cv{ê}_{gs} \cat \theta \os{\(\theta \in \{\cv{t}, \cv{þ}\}\)} \tag{Cl-TÞ} \\
  (\Sigma_{ss} (\pi \rho))^\cv{n} &= (\Sigma_{ss} \pi_\Iota) \cat \cv{ô}_{gs} \cat \cv{n} \tag{Cl-N}
}

where for any consonant ◊${c} and coda ◊${\omega \in \Omega},

◊$${
  (\Sigma_{xn} \omega) \triangleleft c =
    (\Sigma_{xn} \lfloor\omega\rfloor) \cat c_\Iota
}

and ◊${\lfloor\cdot\rfloor : \Omega \rightarrow \Kappa} denotes the operation of taking the maximal prefix of a coda that is a simple coda.

◊subsubsection[#:id "fusion-l"]{Stems ending in ◊l0{r} or ◊l0{l}}

◊align-math{
  (\Sigma_{ss} \cv{r})^\epsilon &= (\Sigma_{ss} \epsilon_\Iota) \cat \cv{ôr} \notag \\
  (\Sigma_{ss} \cv{l})^\epsilon &= (\Sigma_{ss} \epsilon_\Iota) \cat \cv{êl} \tag{L-Nil} \\
  (\Sigma_{ss} \rho_\Iota)^\theta &= (\Sigma_{ss} \rho_\Iota) \cat \cv{ê}_{gs} \cat \theta \os{\(\theta \in \{\cv{t}, \cv{þ}\}\)} \tag{L-TÞ} \\
  (\Sigma_{sn} \cv{n}_\Kappa \rho_\Iota)^\cv{n} &= (\Sigma_{sn} \cv{n}_\Kappa) \cat \cv{n} \notag \\
  (\Sigma_{ss} \rho_\Iota)^\cv{n} &= (\Sigma_{ss} \rho_\Iota) \cat \cv{ô}_{gs} \cat \cv{n} \tag{L-N}
}

◊subsection[#:id "fusion-sh"]{Stems ending in ◊l0{š}, ◊l0{ł}, or ◊l0{č}}

◊align-math{
  \Sigma_{sn} \kappa \iota &\leadsto \Sigma_{sn} I(\kappa \iota) \cv{i}_{gs} \os{\(\iota \in \{\cv{š}, \cv{ł}, \cv{č}, \cv{cš}\}\)} \tag{Šłč}
}

where ◊${I: \Gamma \rightarrow \Gamma} is defined as

◊$${
  I(\gamma) = \begin{cases}
    \cv{rþ} \os{\(\gamma = \cv{rþš}\)} \\
    \cv{rt} \os{\(\gamma = \cv{rþč}\)} \\
    \cv{þč} \os{\(\gamma = \cv{cþš}\)} \\
    \cv{ł} \os{\(\gamma = \cv{lł}\)} \\
    \cv{č} \os{\(\gamma = \cv{tč}\)} \\
    \gamma \osamgen
  \end{cases}
}

◊subsection[#:id "fusion-c"]{Stems ending in ◊l0{c} or ◊l0{g}}

◊align-math{
  (\Sigma_{sn} \kappa \cv{c})^\epsilon &= \Sigma_{sn} \cat \kappa^{\cv{sþ}/\cv{[cþ][cþ]}} \notag \\
  (\Sigma_{ss} \cv{c})^\theta &= \Sigma_{ss} \cat \theta^{\cv{tnþ}/\cv{tŋ[cþ]}} \tag{Cþ} \\
  (\Sigma_{so} \nu \epsilon_\Kappa \cv{g})^\epsilon &= \Sigma_{so} \nu \epsilon_\Gamma \cv{i} \epsilon_\Omega \os{\(\nu \in \{\cv{a}, \cv{e}, \cv{â}, \cv{ê}\}\)} \notag \\
  (\Sigma_{so} \nu \epsilon_\Kappa \cv{g})^\epsilon &= \Sigma_{so} \nu \cv{s} \tag{G-Nil} \\
  (\Sigma_{ss} \cv{g})^\epsilon &= \Sigma_{ss} \cat \cv{i}_{s\omega} \tag{G} \\
  (\Sigma_{sn} \kappa \cv{g})^\theta &= \Sigma_{sn} \kappa \cat \cv{i}_{ss} \cat \theta \os{\(\kappa \in W\)} \notag \\
  (\Sigma_{ss} \cv{c})^\theta &= \Sigma_{ss} \cat \theta^{\cv{tnþ}/\cv{dŋ[gð]}} \tag{Gð}
}

where ◊${W} is the set of codas that end in a voiceless consonant.

◊subsection[#:id "fusion-p"]{Stems ending in ◊l0{p}}

◊align-math{
  \Sigma_{so} \nu \epsilon_\Kappa \cv{p} &\leadsto \Sigma_{so} \nu \cv{f} \os{\(\nu \in \{\cv{e}, \cv{ê}\}\)} \notag \\
  \Sigma_{so} \nu \epsilon_\Kappa \cv{p} &\leadsto \Sigma_{so} \nu \epsilon_\Gamma \cv{e} \epsilon_\Kappa \tag{P-Nil} \\
  \Sigma_{sn} \cv{n}_\Kappa \cv{p} &\leadsto \Sigma_{sn} \epsilon_\Kappa \cat \cv{me}_{ss} \notag \\
  \Sigma_{ss} \cv{p} &\leadsto \Sigma_{ss} \cat \cv{e}_{ss} \tag{P}
}

◊subsection[#:id "fusion-h"]{Stems ending in ◊l0{h}}

◊align-math{
  (\Sigma_{sn} \cv{n}_\Kappa \cv{h}_\Iota)^\epsilon &= \Sigma_{sn} \cv{ns} \notag \\
  (\Sigma_{sn} \cv{r}_\Kappa \cv{h}_\Iota)^\epsilon &= \Sigma_{sn} \cv{ls} \notag \\
  (\Sigma_{sg} \mu \nu \kappa \cv{h}_\Iota)^\epsilon &= \Sigma_{so} \xi(\mu \nu) (\kappa^{\epsilon/\cv{s}})_\Omega \notag \\
  (\Sigma_{ss} \cv{h})^\theta &= \Sigma_{ss} \cat \cv{t} \tag{H}
}

◊subsection[#:id "fusion-hst"]{Stems ending in ◊l0{ħ}}

◊align-math{
  \Sigma_{sn} \cv{c}_\Kappa \cv{ħ}_\Iota &\curvearrowright \Sigma_{sn} \epsilon_\Kappa \cv{g}_\Iota \notag \\
  (\Sigma_{sn} \epsilon_\Kappa \cv{ħ}_\Iota)^\epsilon &= \Sigma_{sn} \cv{s} \notag \\
  \Sigma_{ss} \cv{ħ}_\Iota &\leadsto \Sigma_{ss} \tag{Ħ}
}

◊subsection[#:id "fusion-ng"]{Stems ending in ◊l0{ŋ}}

◊align-math{
  \Sigma_{sg} \mu \nu \kappa \cv{ŋ} &\leadsto \Sigma_{sg} \xi(\mu \nu) \cv{r} \os{\(\kappa \in \{\epsilon, \cv{n}\}\)} \tag{Ŋ-N} \\
  (\Sigma_{sn} \kappa \cv{ŋ})^\epsilon &= \Sigma_{sn} \cv{lôr}_{n\omega} \os{\(\kappa \in \{\cv{r}, \cv{l}\}\)} \notag \\
  (\Sigma_{sn} \kappa \cv{ŋ})^\theta &= \Sigma_{sn} \cv{l}_\Kappa \cat \theta \os{\(\kappa \in \{\cv{r}, \cv{l}\}\)} \tag{Ŋ-Rl}
}

◊subsection[#:id "fusion-cc"]{Stems ending in any other onset with two consonants}

◊align-math{
  [\Sigma_{sn} \epsilon_\Kappa (\iota^1 \iota^2)_\Iota]^\theta &= \Sigma_{sn} \epsilon_\Kappa (\iota^1 \iota^2)_\Iota \cv{î} \cat \theta \notag \\
  \Sigma_{ss} (\iota^1 \iota^2)_\Iota &\curvearrowright \Sigma_{ss} (\iota^1)_\Iota \cv{i} \epsilon_\Kappa \iota^2 \tag{Ccc}
}

◊subsection[#:id "fusion-bycoda"]{Coda-based rules}

◊details{
  ◊summary{◊b{NB:} by this point, the only possible onsets at the end of the stem are ◊l0{n s þ f}.}

  ◊items{
    ◊item{All two-consonant onsets have already been handled.}
    ◊item{◊l0{c ŋ š r l ł g p č h ħ} handled by their respective rules.}
    ◊item{◊l0{v m d ð} handled by final devoicing.}
    ◊item{◊l0{t} handled by onset aliasing for fusion with ◊${\epsilon} and by obstruent merging for fusion with ◊${\theta}.}
  }
}

◊details{
  ◊summary{By observation, the only possible codas in the final bridge at this point are ◊l0{s n þ rþ t f}.}

  ◊items{
    ◊item{The empty coda is obviously eliminated, as all of ◊l0{n s þ f} are valid simple codas.}
    ◊item{◊l0{r}: ◊l0{rþ} is a valid coda; other cases handled by (Epenthesis-LC)}
    ◊item{◊l0{l} handled by (Epenthesis-LC); in case of fusion with ◊${\epsilon}, ◊l0{lþ} and ◊l0{lt} are valid complex codas}
    ◊item{◊l0{c}: ◊l0{-cn-} is an invalid bridge in the first place; ◊l0{-cs-}, ◊l0{-cþ-}, and ◊l0{-cf-} are interpreted as having an empty coda and a complex onset.}
    ◊item{◊l0{cþ}: ◊l0{-cþn-}, ◊l0{-cþs-}, and ◊l0{-cþf-} not valid. ◊l0{-cþþ-} handled by obstruent merging.}
  }

  Some codas are limited to certain onsets at this point:

  ◊items{
    ◊item{◊l0{t} can be followed only by ◊l0{s}: ◊l0{-tn-} not valid, ◊l0{-tf-} canonicalizes to a null coda, and ◊l0{-tþ-} handled by obstruent merging.}
    ◊item{◊l0{þ rþ} followed only by ◊l0{n}: neither can precede ◊l0{s}. ◊l0{þþ rþþ} handled by degemination or obstruent merging. ◊l0{þf rþf} handled by obstruent merging.}
    ◊item{If fusing with a null consonant, ◊l0{n} is followed only by ◊l0{f}: ◊l0{-ns} and ◊l0{-nþ} are already valid complex codas, ◊l0{-nn} handled by degemination.}
  }
}

◊subsubsection[#:id "fusion-ts"]{The bridge ◊l0{-ts-}}

◊align-math{
  \Sigma_{sn} \cv{ts}_\Gamma &\leadsto \Sigma_{sn} \cv{s} \tag{Ts}
}

◊subsubsection[#:id "fusion-sthrthf"]{The codas ◊l0{-s}, ◊l0{-þ}, ◊l0{-rþ}, and ◊l0{-f}}

◊align-math{
  \Sigma_{so} \nu \cv{rþ} \iota &\curvearrowright \Sigma_{so} \eta(\nu) \cv{r} \cat \iota \tag{Coda-Rþ} \\
  \Sigma_{so} \nu \kappa \iota &\curvearrowright \Sigma_{so} \eta(\nu) \epsilon \cat \iota \os{\(\kappa \in \{\cv{s}, \cv{þ}, \cv{f}\}\)} \tag{Coda-Sþf}
}

◊subsubsection[#:id "fusion-coda-n"]{The coda ◊l0{-n}}

◊align-math{
  \Sigma_{sn} \cv{nf}_\Gamma &\leadsto \Sigma_{sn} \cv{f} \notag \\
  \Sigma_{sn} \cv{n}_\Kappa \iota &\leadsto \Sigma_{sn} \cv{n} \tag{Coda-N}
}

◊subsection[#:id "fusion-properties"]{Properties of stem fusion}

Fusion with ◊l0{t} is invariant only when the final onset of the stem is ◊l0{t-}.

Fusion with ◊l0{n} is invariant only when the final bridge of the stem is ◊l0{-nn-}.

Fusion with ◊l0{þ} is invariant only when the final onset of the stem is ◊l0{þ-} or ◊l0{cþ-}.

◊section[#:id "future"]{Future work}

The first complete draft of stem fusion is specified and implemented in f9i, but it needs testing with real-world input to assess what needs to be fixed.

◊section[#:id "fusion-obsolete-rules"]{Obsolete rules}

Rules too complicated for their own good.

◊subsection[#:id "fusion-fvm1"]{Stems ending in ◊l0{f}, ◊l0{v}, or ◊l0{m} (first pass)}

This pass applies only to onsets ending in these consonants that are not preceded by a nonempty coda.

◊align-math{
  (\Sigma_{sn} \epsilon_\Kappa \iota)^\epsilon &= \Sigma_{sn} \cv{f} \os{\(\iota \in \{\cv{f}, \cv{v}, \cv{m}, \cv{tf}, \cv{dv}, \cv{cf}, \cv{gv}\}\)} \notag \\
  (\Sigma_{sn} \epsilon_\Kappa \cv{cf})^\cv{þ} &= \Sigma_{sn} \cv{cþ} \notag \\
  (\Sigma_{sn} \epsilon_\Kappa \cv{gv})^\cv{þ} &= \Sigma_{sn} \cv{gð} \notag \\
  (\Sigma_{sn} \epsilon_\Kappa \iota)^\theta &=  \Sigma_{sn} \cv{f} \cat \theta \os{\(\iota \in \{\cv{f}, \cv{v}, \cv{m}, \cv{tf}, \cv{dv}, \cv{cf}, \cv{gv}\}\)} \tag{Fvm1}
}

◊subsection[#:id "fusion-ptc"]{Stems ending in ◊l0{p}, ◊l0{t}, or ◊l0{c}}

For ◊${\iota^* \in \{\cv{p}, \cv{t}, \cv{c}\}}:

◊align-math{
  \Sigma_{sg} \mu \nu \cv{n}_\Kappa \cv{c}_\Iota &\leadsto \Sigma_{so} \xi(\mu \nu) \cv{r} \notag \\
  \Sigma_{sn} \cv{c}_\Kappa \cv{t}_\Iota &\leadsto \Sigma_{sn} \cv{cþ} \notag \\
  (\Sigma_{sn} \cv{n}_\Kappa \cv{t}_\Iota)^\epsilon &= \Sigma_{sn} \cv{ns} \tag{Ptc-Special} \\
  \Sigma_{sn} \epsilon_\Kappa \iota^* &\leadsto \Sigma_{sn} \cat H(\iota^*) \tag{Ptc-NoCoda} \\
  \Sigma_{so} \nu \kappa \iota^* &\leadsto \Sigma_{so} \chi(\nu) \cat H(\iota^*) \os{\(\kappa \in \{\cv{s}, \cv{f}, \cv{þ}\}\)} \tag{Ptc-Sfth} \\
  \Sigma_{so} \nu \cv{rþ}_\Kappa \iota^* &\leadsto \Sigma_{so} \chi(\nu) \cat H''(\iota^*) \tag{Ptc-Rth} \\
  (\Sigma_{ss} \iota^*)^\cv{þ} &= \Sigma_{ss} \cat \cv{cþ} \tag{Ptc-Þ} \\
  (\Sigma_{ss} \iota^*)^\cv{n} &= \Sigma_{ss} \cat (\iota^*)^{\cv{ptc}/\cv{mdŋ}} \tag{Ptc-N} \\
  (\Sigma_{ss} \iota^*)^\cv{t} &= \Sigma_{ss} \cat \cv{t} \tag{Ptc-T} \\
  (\Sigma_{sn} \kappa \iota^*)^\epsilon &= \Sigma_{sn} \kappa_\Omega \os{\(\kappa \in \{\cv{c}, \cv{t}, \cv{cþ}\}\)} \tag{Ptc-Nil1} \\
  (\Sigma_{sn} \kappa \cv{p})^\epsilon &= \Sigma_{sn} \cv{lt} \os{\(\kappa \in \{\cv{r}, \cv{n}, \cv{l}\}\)} \tag{Ptc-Nil2}
}

where ◊${H, H'': \{\cv{p}, \cv{t}, \cv{c}\} \rightarrow \Kappa} are defined as

◊align-math{
  H(\iota^*) &= (\iota^*)^{\cv{ptc}/\cv{fþ[cþ]}} \\
  H''(\iota^*) &= (\iota^*)^{\cv{ptc}/\cv{f[rþ]þ}}
}

◊subsection[#:id "fusion-dg"]{Stems ending in ◊l0{d} or ◊l0{g}}

For ◊${\iota^* \in \{\cv{d}, \cv{g}\}}:

◊align-math{
  (\Sigma_{sn} \epsilon_\Kappa \iota^*)^\cv{þ} &= \Sigma_{sn} \epsilon_\Kappa (\iota^*)^{\cv{dg}/\cv{ðħ}} \tag{Dg-Nil-Þ} \\
  \Sigma_{sn} \epsilon_\Kappa \iota^* &\leadsto \Sigma_{sn} (\iota^*)^{\cv{dg}/\cv{tc}}_\Kappa \tag{Dg-Nil} \\
  (\Sigma_{sn} \cv{r}_\Kappa \cv{d}_\Iota)^\epsilon &= \Sigma_{sn} \cv{l} \notag \\
  (\Sigma_{sn} \cv{r}_\Kappa \cv{d}_\Iota)^\theta &= \Sigma_{sn} \cv{l} \cat B(\cv{d}, \theta) \tag{D-r} \\
  (\Sigma_{sn} \kappa \cv{d}_\Iota)^\epsilon &= \Sigma_{sn} (\kappa \cv{t})_\Omega \os{\(\kappa \in \{\cv{s}, \cv{l}\}\)} \tag{D-sl} \\
  (\Sigma_{sn} \kappa \cv{d}_\Iota)^\epsilon &= \Sigma_{sn} \kappa_\Omega \tag{D-WithNil} \\
  (\Sigma_{sn} \kappa \cv{g}_\Iota)^\epsilon &= \Sigma_{sn} \kappa_\Omega \os{\(\kappa \in \{\cv{r}, \cv{l}\}\)} \tag{G-Rl} \\
  (\Sigma_{sn} \kappa \cv{g}_\Iota)^\epsilon &= (\Sigma_{sn} \kappa \cat \cv{c})^\epsilon \tag{G-WithNil} \\
  (\Sigma_{ss} \iota^*)^\theta &= \Sigma_{ss} \cat B(\iota^*, \theta) \tag{Gd}
}

where

◊$${
  B(\iota^*, \theta) = \begin{cases}
    \theta^{\cv{tnþ}/\cv{dnð}} \os{\(\iota^* = \cv{d}\)} \\
    \theta^{\cv{tnþ}/\cv{gŋħ}} \os{\(\iota^* = \cv{g}\)}
  \end{cases}
}

◊subsection[#:id "fusion-cths"]{Stems ending in ◊l0{f}, ◊l0{þ}, or ◊l0{s}}

◊align-math{
  (\Sigma_{ss} \iota)^\epsilon &= (\Sigma_{ss} \cat \iota^{\cv{[cf][cs][tf]}/\cv{cst}})^\epsilon \os{\(\iota \in \{\cv{cf}, \cv{cs}, \cv{tf}\}\)} \notag \\
  (\Sigma_{ss} \iota)^\theta &= (\Sigma_{ss} \cat \iota^{\cv{[cf][cs][tf]}/\cv{cst}})^\theta \os{\(\iota \in \{\cv{cf}, \cv{cs}, \cv{tf}\}\)} \tag{Sfc} \\
  \Sigma_{sn} \kappa \iota &\leadsto \Sigma_{sn} \iota_\Kappa \os{\(\kappa \in \{\cv{s}, \cv{f}, \cv{þ}\}\)} \tag{Sfth1}
}

◊section[#:id "external-links"]{External links}

◊items{
  ◊item{The ◊link["https://gitlab.com/Kyarei/f9i/-/tree/elaine-sm"]{elaine-sm} branch of f9i for testing changes made by Project Elaine}
}
