#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type article]
◊define-meta[title]{Project Caladrius (oudated material)}
◊define-meta[date]{2022-01-30 – 2022-06-02}
◊define-meta[book-author]{+merlan #flirora}

◊strong{As of cþD0A, the following sections are outdated due to the initial release of ◊link["elaine.html"]{Project Elaine}.}

◊section[#:id "issues"]{Outstanding issues with current draft}

◊items{
  ◊item{first-declension ◊l0{-e} nouns have identical acc.sg and gen.pl forms if the nominative stem is not eclipseable}
  ◊item{second-declension ◊l0{-iþ} nouns have identical nom.du and sembl.gc forms if the N and S stems are the same}
  ◊item{second-declension ◊l0{-eþ} and ◊l0{-aþ} nouns have identical nom.sg and dat.gc forms (temporarily worked around this issue; might find a different fix later)}
}

◊section[#:id "declension-model"]{Declension model}

There are three types of components when it comes to declensions: ◊term{constants}, ◊term{stems}, and ◊term{themes}, which should be familiar from the existing chapter on nouns.

The definition of a ◊term{stem} is formalized: it is now defined as zero or more syllables followed by an onset that does not contain a lenited consonant.

The main thematic vowel of nouns, which is found in the nominative singular, is denoted by ◊nc{◊ṫ[0]} (the Greek letter theta, for ◊i{◊mark{th}eme}). Some nouns have a locative thematic vowel, which is denoted by ◊nc{◊ṫ[1]}, for ◊i{◊mark{l}ocative}.

The thematic consonant (or consonant cluster), if any, is denoted by ◊nc{◊ṫ[2]}, for ◊lang["grc"]{◊i{◊mark{σ}ῠ́μφωνος}}.

◊subsection[#:id "declension-xforms"]{Transformations}

Instead of a hardcoded set of derivatives, Project Caladrius has a concept of ◊term{transformations} on grapheme sequences. Transformations are denoted by superscripted terms. The following basic transformations are defined:

◊items{
  ◊item{◊nc{◊ẋ{◊var{λ}/◊var{μ}}}: Replace the vowels in ◊var{λ} with the corresponding ones in ◊var{μ}.}
  ◊item{◊nc{◊ẋ{◊var{λ}×◊var{μ}}}: Apply an ◊term{exchange transformation} on a vowel: ◊var{λ} → ◊var{μ}; ◊var{μ} → ◊var{λ}.}
  ◊item{◊nc{◊ẋ{‹◊var{λ}/◊var{μ}}}: Replace this vowel with ◊var{μ} when the previous vowel (if any) is ◊var{λ}.}
  ◊item{◊nc{◊ẋ{~◊var{σ}}}: Apply the transformation ◊var{σ} over both normal and hatted vowels.}
  ◊item{◊nc{◊ẋ{◊var{σ} · ◊var{τ}}}: Apply the transformations ◊var{σ} and ◊var{τ}, in that order.}
}

Other transformations can be expressed in terms of these:

◊items{
  ◊item{◊nc{◊ẋ{◊ẋpl}} = ◊nc{◊ẋ{~aoe/oei}}}
  ◊item{◊nc{◊ẋ{◊ẋgen}} = ◊nc{◊ẋ{~aoe/eei}}}
  ◊item{◊nc{◊ẋ{◊ẋgenr}} = ◊nc{◊ẋ{~aoei/iiee}} = ◊nc{◊ẋ{◊ẋgen · ~e×i}}}
  ◊item{◊nc{◊ẋ{◊ẋfr}} = ◊nc{◊ẋ{~o/a}}}
  ◊item{◊nc{◊ẋ{◊ẋfr2}} = ◊nc{◊ẋ{~o/e}}}
  ◊item{◊nc{◊ẋ{◊ẋfr3}} = ◊nc{◊ẋ{~oi/ee}}}
  ◊item{◊nc{◊ẋ{◊ẋht}} = ◊nc{◊ẋ{aeio/âêîô}}}
  ◊item{◊nc{◊ẋ{◊ẋdh}} = ◊nc{◊ẋ{âêîô/aeio}}}
  ◊item{◊nc{◊ẋ{◊ẋng}} = ◊nc{◊ẋ{~aeio/o[jo][jo]o}}}
}

Transformations can also be applied to stems:

◊items{
  ◊item{◊nc{◊ẋ{%◊var{σ}}} applies ◊var{σ} to the last vowel of the stem.}
  ◊; ◊item{◊nc{◊ẋ{◊ẋscd}} applies the ◊term{coda transformation}.}
}

◊subsection[#:id "declension-fusion"]{Fusion}

Moreover, the concept of ◊term{fusion} between a stem and a (possibly null) consonant is introduced. It is denoted by the symbol ◊l0{×} (not to be confused with the ◊i{njor}). For most final bridges of stems, fusion is ◊term{C-invariant}; that is, the result is a mutated version of the stem that does not depend on C, followed by a morpheme boundary then C itself.

The following sections outline the rules for determining the result of the fusion. For the purposes of this section, ◊l0{m} is not considered a valid coda. In the notation used here:

◊items{
  ◊item{◊${\sigma} denotes a syllable}
  ◊item{◊${\iota} denotes an initial}
  ◊item{◊${\omega} denotes an onset}
  ◊item{◊${\nu} denotes a nucleus}
  ◊item{◊${\kappa} denotes a coda}
  ◊item{◊${\gamma} denotes a bridge}
  ◊item{◊${\lambda} denotes a consonant with which a stem can be fused (◊em{not} an empty string!)}
  ◊item{◊${\epsilon} denotes a null consonant}
}

◊subsubsection[#:id "fusion-j"]{Stems ending in ◊l0{j}}

◊align-math{
  \sigma^* \iota \text{j} \times \lambda &= \sigma^* \iota \text{i-} \lambda \tag{FinalJ}
}

If ◊ṡ{X} ends with a ◊l0{j}, then fusion is C-invariant with the terminal ◊l0{j} changed to ◊l0{i}. In this document, earlier rules take precedence over later ones.

◊subsubsection[#:id "fusion-coda"]{Valid codas}

◊align-math{
  \sigma^* \omega \nu \gamma \times \lambda &= \sigma^* \omega \nu \gamma \text{-} \lambda \text{ if $\gamma$ is a valid coda} \tag{ValidCoda}
}

If the final bridge of ◊ṡ{X} is a valid coda (simple or complex), then fusion is C-invariant with the stem unchanged.

◊subsubsection[#:id "fusion-degemination"]{Degemination}

◊align-math{
  \sigma^* \omega \nu \text{rr} \times \lambda &= \sigma^* \omega \nu \text{l-} \lambda \tag{DegeminateR} \\
  \sigma^* \omega \nu \text{ss} \times \lambda &= \sigma^* \omega \nu \text{þ-} \lambda \tag{DegeminateS} \\
  \sigma^* \omega \nu \kappa \kappa \times \lambda &= \sigma^* \omega \nu \kappa \text{-} \lambda \text{ if $\kappa$ is a valid coda and $|\kappa| = 1$} \tag{Degeminate}
}

If the bridge consists of two of the same consonant that alone is considered a valid coda, then fusion is C-invariant and one of the copies of the consonant is elided. As a special case, ◊l0{rr} is reduced to ◊l0{l} and ◊l0{ss} to ◊l0{þ} instead.

◊subsubsection[#:id "fusion-cl"]{Stems ending in consonant + liquid onsets}

When ◊${\iota} is a consonant that can form an onset with ◊l0{r} or ◊l0{l} and ◊${\mu \in \{\text{r}, \text{l}\}}:

◊align-math{
  \sigma^* \omega' \nu' \omega \nu \iota \mu \times \lambda &= \sigma^* \omega' \nu' \omega \iota \nu \mu \text{-} \lambda \text{ if $\omega \in \{\varepsilon, \text{s}, \text{n}, \text{l}\}$} \tag{Cl-Meta} \\
  \sigma^* \omega \nu \text{t} \mu \times \text{t} &= \sigma^* \omega \nu \text{r-t} \tag{Cl-NoCoda-WithT-t} \\
  \sigma^* \omega \nu \iota \mu \times \text{t} &= (\sigma^* \omega \nu \iota \times \varepsilon) \text{-d} \tag{Cl-NoCoda-WithT} \\
  \sigma^* \omega \nu \iota \mu \times \text{t} &= \sigma^* \omega \nu \iota^{\text{dc/rs}} \text{-ŋ} \text{ if $\iota \in \{\text{d}, \text{c}\}$} \tag{Cl-NoCoda-WithN-dc} \\
  \sigma^* \omega \nu \iota \mu \times \text{t} &= \sigma^* \omega \nu \iota \times \text{n} \tag{Cl-NoCoda-WithN} \\
  \sigma^* \omega \nu \iota \mu \times \text{þ} &= \sigma^* \omega \nu \text{r-} \iota \tag{Cl-NoCoda-WithÞ-þð} \text{ if $\iota \in \{\text{þ}, \text{ð}\}$} \\
  \sigma^* \omega \nu \iota \mu \times \text{þ} &= (\sigma^* \omega \nu \iota \times \varepsilon) \text{-ð} \tag{Cl-NoCoda-WithÞ} \\
  \sigma^* \iota \text{r} \times \varepsilon &= \sigma^* \iota \text{ôr} \tag{Cl-r-WithNil} \\
  \sigma^* \iota \text{l} \times \varepsilon &= \sigma^* \iota \text{êl} \tag{Cl-l-WithNil} \\
  \sigma^* \iota \mu \times \lambda &= \sigma^* \iota \text{ê-} \lambda \text{ if $\lambda \in \{\text{t}, \text{þ}\}$} \tag{Cl-WithTÞ} \\
  \sigma^* \iota \mu \times \text{n} &= \sigma^* \iota \text{ô-n} \tag{Cl-WithN}
}

Note that these rules do not apply to, for example, stems that end in ◊l0{-nr}. However, the last four rules do apply when ◊${\iota = \varepsilon}, albeit with a special case, in which case they reduce to

◊align-math{
  \sigma^* \text{r} \times \varepsilon &= \sigma^* \text{ôr} \tag{L-r-WithNil} \\
  \sigma^* \text{l} \times \varepsilon &= \sigma^* \text{êl} \tag{L-l-WithNil} \\
  \sigma^* \mu \times \lambda &= \sigma^* \text{ê-} \lambda \text{ if $\lambda \in \{\text{t}, \text{þ}\}$} \tag{L-WithTÞ} \\
  \sigma^* \omega \nu \text{n} \mu \times \text{n} &= \sigma^* \omega \nu \text{n-n} \tag{L-WithN-n} \\
  \sigma^* \mu \times \text{n} &= \sigma^* \text{ô-n} \tag{L-WithN}
}

◊subsubsection[#:id "fusion-c"]{Stems ending in ◊l0{c}}

◊align-math{
  \sigma^* \omega \nu \text{þc} \times \lambda &= \sigma^* \omega \nu \text{cþ-} \lambda \tag{FinalC-þ} \\
  \sigma^* \omega \nu \text{nc} \times \varepsilon &= \sigma^* \omega \nu^{\xi} \text{r} \tag{FinalC-n-WithNil} \\
  \sigma^* \omega \nu \text{nc} \times \text{t} &= \sigma^* \omega \nu \text{n-c} \tag{FinalC-n-WithT} \\
  \sigma^* \omega \nu \text{nc} \times \text{n} &= \sigma^* \omega \nu \text{nci-n} \tag{FinalC-n-WithN} \\
  \sigma^* \omega \nu \kappa \text{c} \times \varepsilon &= \sigma^* \omega \nu \kappa \text{ec} \tag{FinalC-WithNil} \\
  \sigma^* \omega \nu \kappa \text{c} \times \text{t} &= \sigma^* \omega \nu \kappa \text{-t} \tag{FinalC-WithT} \\
  \sigma^* \omega \nu \kappa \text{c} \times \text{n} &= \sigma^* \omega \nu \kappa \text{-ŋ} \tag{FinalC-WithN} \\
  \sigma^* \omega \nu \kappa \text{c} \times \text{þ} &= \sigma^* \omega \nu \kappa \text{-cþ} \tag{FinalC-WithÞ}
}

If ◊ṡ{X} ends with ◊l0{þc}, then fusion is C-invariant and the final two consonants of the stem are metathesized.

If ◊ṡ{X} ends with ◊l0{nc}, then fusion is ◊em{not} C-invariant. In this case, ◊ṡ{X} fuses with the null consonant by considering the intermediate form ◊l0{-Vŋ}, which is finally realized as ◊l0{-or} or ◊l0{-jor}. It fuses with ◊l0{t} by eliding the ◊l0{t}, although the morpheme boundary is considered to occur ◊em{before} the ◊l0{c}. ◊ṡ{X} fuses with ◊l0{n} by inserting ◊l0{i} after the stem, and with ◊l0{þ} by leaving the stem unchanged but moving the morpheme boundary before the ◊l0{c}.

In other cases in which ◊ṡ{X} ends with ◊l0{c}, ◊ṡ{X} fuses with the null consonant by inserting ◊l0{e} before the ◊l0{c}. It fuses with ◊l0{t} by eliding the ◊l0{c}, with ◊l0{n} by eliding the ◊l0{c} and replacing the ◊l0{n} with ◊l0{ŋ}, and with ◊l0{þ} by leaving the stem unchanged but moving the morpheme boundary before the ◊l0{c}.

Note that the cases when the coda of the last full syllable is ◊l0{c} or empty are already covered by previous rules.

◊subsubsection[#:id "fusion-n"]{Stems ending in ◊l0{n}}

◊align-math{
  \sigma^* \text{n} \times \lambda &= \sigma^* \text{en-} \lambda \tag{FinalN}
}

If ◊ṡ{X} ends with ◊l0{n}, then fusion is C-invariant and ◊l0{e} is inserted before the final ◊l0{n}.

Note that the cases when the coda of the last full syllable is ◊l0{n} or empty are already covered by previous rules.

◊subsubsection[#:id "fusion-ng"]{Stems ending in ◊l0{ŋ}}

◊align-math{
  \sigma^* \omega \nu \text{ŋ} \times \lambda &= \sigma^* \omega \nu^{\xi} \text{r-} \lambda \tag{FinalŊ-nil} \\
  \sigma^* \omega \nu \text{lŋ} \times \lambda &= \sigma^* \omega \nu \text{lôr-} \lambda \tag{FinalŊ-l} \\
  \sigma^* \omega \nu \text{rþŋ} \times \lambda &= \sigma^* \omega \nu \text{þôr-} \lambda \tag{FinalŊ-rþ} \\
  \sigma^* \omega \nu \kappa \text{ŋ} \times \lambda &= \sigma^* \omega \nu \kappa \text{ôr-} \lambda \tag{FinalŊ}
}

If ◊ṡ{X} ends with ◊l0{ŋ}, then fusion is C-invariant. If the preceding syllable has an empty coda, then its rime and the following onset are replaced with ◊l0{or} or ◊l0{jor}. If the preceding syllable has a nonempty coda, then the following onset is replaced with ◊l0{ôr}; the codas ◊l0{r} and ◊l0{rþ} are additionally changed to ◊l0{l} and to ◊l0{þ}.

◊subsubsection[#:id "fusion-s"]{Stems ending in ◊l0{s}}

◊align-math{
  \sigma^* \omega \nu \kappa \text{s} \times \lambda &= \sigma^* \omega \nu \text{þ-} \lambda \text{ if $\kappa \in \{\text{þ}, \text{f}\}$} \tag{FinalSToÞ} \\
  \sigma^* \omega \nu \kappa \text{s} \times \lambda &= \sigma^* \omega \nu \text{rþ-} \lambda \text{ if $\kappa \in \{\text{r}, \text{rþ}\}$} \tag{FinalSToRÞ} \\
  \sigma^* \omega \nu \text{ts} \times \lambda &= \sigma^* \omega \nu \text{s-} \lambda \tag{FinalS-t}
}

If ◊ṡ{X} ends with ◊l0{s}, then fusion is C-invariant. The bridges ◊l0{þs} and ◊l0{fs} become ◊l0{þ}, ◊l0{rs} and ◊l0{rþs} become ◊l0{rþ}, ◊l0{ts} becomes ◊l0{s}, and ◊l0{cs} becomes ◊l0{cþ}.

Note that the cases when the coda of the last full syllable is ◊l0{s}, ◊l0{n}, ◊l0{l}, or empty are already covered by previous rules.

◊subsubsection[#:id "fusion-thdh"]{Stems ending in ◊l0{þ} or ◊l0{ð}}

For ◊${\iota \in \{\text{þ}, \text{ð}\}}:

◊align-math{
  \sigma^* \omega \nu \text{ð} \times \lambda &= \sigma^* \omega \nu \text{þ-} \lambda \tag{FinalÐ-nil} \\
  \sigma^* \omega \nu \kappa \iota \times \lambda &= \sigma^* \omega \nu \text{rþ-} \lambda \text{ if $\kappa \in \{\text{r}, \text{rþ}\}$} \tag{FinalÞÐ-r} \\
  ◊; \sigma^* \omega \nu \text{n} \text{þ} \times \lambda &= \sigma^* \omega \nu \text{nþ-} \lambda \tag{FinalÞ-n} \\
  \sigma^* \omega \nu \text{n} \text{ð} \times \lambda &= \sigma^* \omega \nu \text{n-} \lambda \tag{FinalÐ-n} \\
  \sigma^* \omega \nu \text{þ} \text{ð} \times \lambda &= \sigma^* \omega \nu \text{þ-} \lambda \tag{FinalÐ-þ} \\

  \sigma^* \omega \nu \text{s} \iota \times \varepsilon &= \sigma^* \omega \nu \text{seþ} \tag{FinalÞÐ-s-WithNil} \\
  \sigma^* \omega \nu \text{s} \iota \times \text{t} &= \sigma^* \omega \nu \text{þ-t} \tag{FinalÞÐ-s-WithT} \\
  \sigma^* \omega \nu \text{sþ} \times \text{n} &= \sigma^* \omega \nu \text{sþe-n} \tag{FinalÞ-s-WithN} \\
  \sigma^* \omega \nu \text{sð} \times \text{n} &= \sigma^* \omega \nu \text{se-nd} \tag{FinalÐ-s-WithN} \\
  \sigma^* \omega \nu \text{s} \iota \times \text{þ} &= \sigma^* \omega \nu \text{s-} \iota \tag{FinalÞÐ-s-WithÞ} \\

  \sigma^* \omega \nu \text{lþ} \times \varepsilon &= \sigma^* \omega \nu \text{rþ} \tag{FinalÞ-l-WithNil} \\
  \sigma^* \omega \nu \text{lð} \times \varepsilon &= \sigma^* \omega \nu \text{lðe} \tag{FinalÐ-l-WithNil} \\
  \sigma_0 \sigma^* \iota' \mu \nu \text{l} \iota \times \lambda &= \sigma_0 \sigma^* \iota' \text{l} \mu \nu \text{þ-} \lambda \text{ if $\lambda \in \{\text{t}, \text{n}\}$ and $\iota' \text{l}$ is a valid onset} \notag \\
  \tag{FinalÞÐ-l-WithTNMeta} \\
  \sigma^* \omega \nu \text{l} \iota \times \text{t} &= \sigma^* \omega \nu \text{l-} \iota^{\text{þð/td}} \tag{FinalÞÐ-l-WithT} \\
  \sigma^* \omega \nu \text{l} \iota \times \text{n} &= \sigma^* \omega \nu \text{l} \iota \text{e-n} \tag{FinalÞÐ-l-WithN} \\
  \sigma^* \omega \nu \text{l} \iota \times \text{þ} &= \sigma^* \omega \nu \text{l-þ} \tag{FinalÞÐ-l-WithÞ} \\
  \sigma^* \omega \nu \text{tþ} \times \varepsilon &= \sigma^* \omega \nu \text{þ} \tag{FinalÞ-t-WithNil} \\
  \sigma^* \omega \nu \text{tð} \times \varepsilon &= \sigma^* \omega \nu \text{ðe} \tag{FinalÐ-t-WithNil} \\
  \sigma^* \omega \nu \text{t} \iota \times \text{t} &= \sigma^* \omega \nu \text{þ-} \iota^{\text{þð/td}} \tag{FinalÞÐ-t-WithT} \\
  \sigma^* \omega \nu \text{tþ} \times \text{n} &= \sigma^* \omega \nu \text{þn-} \tag{FinalÞ-t-WithN} \\
  \sigma^* \omega' \nu' \gamma \mu \nu \text{tð} \times \text{n} &= \sigma^* \omega' \nu' \gamma \text{ð} \mu \nu \text{n-} \text{ if $\gamma$ is a valid coda} \tag{FinalÐ-t-WithNMeta} \\
  \sigma^* \omega \nu \text{tð} \times \text{n} &= \sigma^* \omega \nu \text{ðen-} \tag{FinalÐ-t-WithN} \\
  \sigma^* \omega \nu \text{t} \iota \times \text{þ} &= \sigma^* \omega \nu \text{t-} \iota \tag{FinalÞÐ-t-WithÞ} \\
  \sigma^* \omega \nu \text{cð} \times \varepsilon &= \sigma^* \omega \nu \text{cþ} \tag{FinalÐ-c-WithNil} \\
  \sigma^* \omega \nu \text{cð} \times \lambda &= \sigma^* \omega \nu \text{cþ-} \lambda^{\text{tnþ/dnð}} \tag{FinalÐ-c-WithCons} \\
  \sigma^* \omega \nu \text{f} \iota \times \lambda &= \sigma^* \omega \nu \text{f-} \lambda \tag{FinalÞÐ-f}
}

Note that the cases when the final onset is ◊l0{þ} and the coda of the last full syllable is ◊l0{r}, ◊l0{n}, ◊l0{c}, ◊l0{þ}, or empty are already covered by previous rules.

◊subsubsection[#:id "fusion-cs"]{Stems ending in ◊l0{cs} or ◊l0{cþ} clusters}

For ◊${\mu \in \{\text{s}, \text{þ}\}}:

◊align-math{
  \sigma^* \omega \nu \text{c} \mu \times \lambda &= \sigma^* \omega \nu \text{cþ-} \lambda \tag{FinalCS-nil} \\
  \sigma^* \omega \nu \kappa \text{c} \mu \times \lambda &= \sigma^* \omega \nu \text{cþ-} \lambda \text{ if $\kappa \in \{\text{r}, \text{s}, \text{þ}, \text{rþ}, \text{c}\}$} \tag{FinalCS-r} \\
  \sigma^* \omega \nu \text{nc} \mu \times \lambda &= \sigma^* \omega \nu^\xi \text{rþ-} \lambda \tag{FinalCS-n} \\
  \sigma^* \omega \nu \text{lc} \mu \times \lambda &= \sigma^* \omega \nu \text{ls-} \lambda \tag{FinalCS-l} \\
  \sigma^* \omega \nu \text{tc} \mu \times \lambda &= \sigma^* \omega \nu \text{tô} \mu \text{-} \lambda \tag{FinalCS-t} \\
  \sigma^* \omega \nu \text{fc} \mu \times \lambda &= \sigma^* \omega \nu \text{fî} \mu \text{-} \lambda \tag{FinalCS-f}
}

◊subsubsection[#:id "fusion-final-i-epenth"]{Final ◊l0{i}-epenthesis}

◊align-math{
  \sigma^* \omega \nu \text{rþš} \times \lambda &= \sigma^* \omega \nu \text{rþi-} \lambda \tag{FinalIEpenthesis-rþš} \\
  \sigma^* \omega \nu \text{rþč} \times \lambda &= \sigma^* \omega \nu \text{rti-} \lambda \tag{FinalIEpenthesis-rþč} \\
  \sigma^* \omega \nu \text{lł} \times \lambda &= \sigma^* \omega \nu \text{lði-} \lambda \tag{FinalIEpenthesis-lł} \\
  \sigma^* \omega \nu \text{tš} \times \lambda &= \sigma^* \omega \nu \text{či-} \lambda \tag{FinalIEpenthesis-tš} \\
  \sigma^* \omega \nu \text{tč} \times \lambda &= \sigma^* \omega \nu \text{či-} \lambda  \tag{FinalIEpenthesis-tč} \\
  \sigma^* \iota \times \lambda &= \sigma^* \iota \text{i-} \lambda \text{ if $\iota \in \{\text{š}, \text{ł}, \text{č}, \text{cš}\}$} \tag{FinalIEpenthesis}
}

If ◊ṡ{X} ends with any of ◊l0{š}, ◊l0{ł}, or ◊l0{č}, then fusion is C-invariant with ◊l0{i} inserted after the stem. In some cases, the bridge might be altered for smoother pronunciation (see rules above).

◊subsubsection[#:id "fusion-fvm"]{Stems ending in ◊l0{f}, ◊l0{v}, or ◊l0{m}}

For ◊${\iota \in \{\text{f}, \text{v}, \text{m}\}}:

◊align-math{
  \sigma^* \omega \nu \iota \times \lambda &= \sigma^* \omega \nu \text{f-} \lambda \tag{FinalFVM-nil} \\
  \sigma^* \omega \nu \text{f} \iota \times \lambda &= \sigma^* \omega \nu \text{f-} \lambda \tag{FinalFVM-f} \\
  \sigma^* \omega \nu \text{nm} \times \lambda &= \sigma^* \omega \nu \text{n-} \lambda \tag{FinalM-n} \\
  \sigma^* \omega \nu \kappa \text{m} \times \lambda &= \sigma^* \omega \nu \text{n} \kappa \text{-} \lambda \text{ if $\kappa \in \{\text{t}, \text{c}\}$} \tag{FinalM-tc} \\
  \sigma^* \omega \nu \kappa \iota \times \lambda &= \sigma^* \omega \nu \kappa \text{ef-} \lambda \tag{FinalFVM}
}

◊subsubsection[#:id "fusion-g"]{Stems ending in ◊l0{g}}

◊align-math{
  \sigma^* \omega \nu \text{g} \times \lambda &= g(\sigma^* \omega \nu, \lambda) \tag{FinalG}
}

Gee, that was helpful!

◊subsubsection[#:id "fusion-p"]{Stems ending in ◊l0{p}}

◊subsubsection[#:id "fusion-h"]{Stems ending in ◊l0{h}}

◊align-math{
  \sigma^* \omega \nu \kappa \text{h} \times \lambda &= \sigma^* \omega \nu^{\eta} \kappa \text{-} \lambda \tag{FinalH}
}

If ◊ṡ{X} ends with ◊l0{h}, then fusion is C-invariant. In this case, ◊l0{h} is elided and the vowel of the last full syllable in ◊ṡ{X} is hatted.

◊subsubsection[#:id "fusion-gh"]{Stems ending in ◊l0{ħ}}

◊align-math{
  \sigma^* \omega \nu \text{cħ} \times \lambda &= g(\sigma^* \omega \nu, \lambda)  \tag{FinalĦ-c} \\
  \sigma^* \omega \nu \kappa \text{ħ} \times \lambda &= \sigma^* \omega \nu \kappa \text{-} \lambda \tag{FinalĦ}
}

If ◊ṡ{X} ends with ◊l0{ħ} without a preceding ◊l0{c} coda, then fusion is C-invariant. In this case, ◊l0{ħ} is elided.

If ◊ṡ{X} ends with ◊l0{ħ} with a preceding ◊l0{c} coda, then fusion occurs as if the bridge were ◊l0{g} instead. (The function ◊${g} is defined later.)

◊section[#:id "decl1p"]{The first declension (penultimate)}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[1]} can be either ◊l0{a} or ◊l0{e}, but is more frequently ◊l0{a}}
  ◊item{◊nc{◊ṡ{L}} must be different from ◊nc{◊ṡ{N}}; the most common difference is to change the final vowel of ◊nc{◊ṡ{N}}}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l})}
}

◊table/x[#:options (table-options #:caption ◊@{
  Common declensions for first-declension nouns.
  The nominative dual form depends on the thematic vowel ◊l0{◊ṫ[0]}: ◊l0{a} → ◊l0{◊ṡ{N}ac}, ◊l0{o} → ◊l0{◊ṡ{N}in}, and ◊l0{e}, ◊l0{i} → ◊l0{◊ṡ{N}ic}.
  (*): if ◊l0{◊ṡ{N}} ends with ◊l0{j}, then replace it with ◊l0{l}.
} #:placement 'forcehere) #:id "declensions-1-common"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & * & * & * & * & ◊nc{◊ṡ{N}◊ṫ[0]f}
  Accusative & * & ◊nc{◊ṡ{N}ôr} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]nþ} & ◊nc{◊ṡ{N}◊ṫ[0]fen}
  Dative & * & * & * & * & ◊nc{◊ṡ{N}◊ṫ[0]fes}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}n} / ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}jôr} (*) & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgenr}n} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{N}if}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]s} & ◊nc{◊ṡ{L}◊ṫ[1]ce◊ẋ{‹e/i}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋpl}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{L}◊ṫ[1]fe◊ẋ{‹e/i}s}
  Instrumental & ◊nc{◊ṡ{L}eca} & ◊nc{◊ṡ{L}ecca} & ◊nc{◊ṡ{L}ica} & ◊nc{◊ṡ{L}inca} & ◊nc{◊ṡ{L}efca}
  Abessive & ◊nc{◊ṡ{L}eþa} & ◊nc{◊ṡ{L}ecþa} & ◊nc{◊ṡ{L}iþa} & ◊nc{◊ṡ{L}inþa} & ◊nc{◊ṡ{L}efþa}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊subsection[#:id "decl1p-v"]{Vowel-final nouns}

◊l0{/} in these tables means either ◊trans{I don’t know which one I should choose} or ◊trans{it depends on things}.

◊table/x[#:options (table-options #:caption "Common declensions for vowel-final nouns." #:placement 'forcehere) #:id "declensions-v-common"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]} & * & * & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}l} & ◊nc{◊ṡ{N}◊ṫ[0]f}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]n} & ◊nc{◊ṡ{N}ôr} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]nþ} & ◊nc{◊ṡ{N}◊ṫ[0]fen}
  Dative & ◊colspan*[2 'merged]{◊nc{◊ṡ{N}◊ṫ[0]s}} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}i} & ◊nc{◊ṡ{N}◊ṫ[0]rin} & ◊nc{◊ṡ{N}◊ṫ[0]fes}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for vowel-final nouns.} #:placement 'forcehere) #:id "declensions-v-variable"]{
  Ending & ◊nc{-a} & ◊nc{-o} & ◊nc{-e}
  Nominative dual & ◊nc{◊ṡ{N}ac} & ◊nc{◊ṡ{N}in} & ◊nc{◊ṡ{N}ic}
  Nominative plural & ◊nc{◊ṡ{N}o} & ◊nc{◊ṡ{N}an} & ◊nc{◊ṡ{N}i}
}

◊subsection[#:id "decl1p-vi"]{Vowel + ◊l0{i} nouns}

Not currently in ŊCv9, but projected to house mainly personal and place names.

◊subsection[#:id "decl1p-vs"]{Vowel + ◊l0{s} nouns}

Note that in Caladrius, the locative to semblative cases are inflected identically across all first-declension nouns.

◊table/x[#:options (table-options #:caption ◊@{Common declensions for vowel + ◊l0{-s} nouns.} #:placement 'forcehere) #:id "declensions-vs-common"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]s} & * & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ṫ[0]rin} & ◊nc{◊ṡ{N}◊ṫ[0]f}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}ns} & ◊nc{◊ṡ{N}ôr} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]nþ} & ◊nc{◊ṡ{N}◊ṫ[0]fen}
  Dative & ◊nc{◊ṡ{N}o} & ◊nc{◊ṡ{N}◊ṫ[0]þ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}ri} & ◊nc{◊ṡ{N}◊ṫ[0]sin} & ◊nc{◊ṡ{N}◊ṫ[0]fes}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for vowel + ◊l0{-s} nouns.} #:placement 'forcehere) #:id "declensions-vs-variable"]{
  Ending & ◊nc{-as} & ◊nc{-es}
  Nominative dual & ◊nc{◊ṡ{N}ac} & ◊nc{◊ṡ{N}ic}
  ◊; Nominative plural & ◊nc{◊ṡ{N}es} & ◊nc{◊ṡ{N}is}
}

◊subsection[#:id "decl1p-vth"]{Vowel + ◊l0{þ} nouns}

◊table/x[#:options (table-options #:caption ◊@{Common declensions for vowel + ◊l0{-þ} nouns.} #:placement 'forcehere) #:id "declensions-vth-common"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}þ} & * & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ṫ[0]si◊ẋ{‹i/e}n} & ◊nc{◊ṡ{N}◊ṫ[0]f}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}ns} & ◊nc{◊ṡ{N}ôr} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]nþ} & ◊nc{◊ṡ{N}◊ṫ[0]fen}
  Dative & ◊nc{◊ṡ{N}es} & ◊nc{◊ṡ{N}◊ṫ[0]þ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}ri} & ◊nc{◊ṡ{N}◊ṫ[0]þin} & ◊nc{◊ṡ{N}◊ṫ[0]fes}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for vowel + ◊l0{-þ} nouns.} #:placement 'forcehere) #:id "declensions-vth-variable"]{
  Ending & ◊nc{-âþ} & ◊nc{-ôþ} & ◊nc{-êþ} & ◊nc{-îþ}
  Nominative dual & ◊nc{◊ṡ{N}ac} & ◊nc{◊ṡ{N}in} & ◊nc{◊ṡ{N}ic} & ◊nc{◊ṡ{N}ic}
  ◊; Nominative plural & ◊nc{◊ṡ{N}es} & ◊nc{◊ṡ{N}es} & ◊nc{◊ṡ{N}is} & ◊nc{◊ṡ{N}is}
}


◊section[#:id "decl2p"]{The second declension (penultimate)}

Have a separate G stem.

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[1]} can be either ◊l0{e} or ◊l0{i}}
  ◊item{◊nc{◊ṡ{G}} can be same as or different from ◊nc{◊ṡ{N}}}
  ◊item{◊nc{◊ṡ{L}} can potentially be the same as ◊nc{◊ṡ{N}} but is usually different from it. The difference is usually more substantial than a change in the final vowel of the stem.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l})}
}

◊subsection[#:id "decl2p-ic"]{◊l0{-in} and ◊l0{-is} nouns}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for ◊l0{-in} and ◊l0{-is} nouns.
} #:placement 'forcehere) #:id "declensions-2p-ic"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}ec◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}a◊ṫ[2]} & ◊nc{◊ṡ{N}e◊ṫ[2]◊ẋ{ns/dc}◊ṫ[0]n} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}e} & ◊nc{◊ṡ{N}ec} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}eħin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}ns}
  Dative & ◊nc{◊ṡ{N}ês} & ◊nc{◊ṡ{N}ecþ} & ◊nc{◊ṡ{N}erþ} & ◊nc{◊ṡ{N}erin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}þ}
  Genitive & ◊nc{◊ṡ{G}en} & ◊nc{◊ṡ{G}jôr} & ◊nc{◊ṡ{G}eþ} & ◊nc{◊ṡ{G}ens} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}st}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]lt} & ◊nc{◊ṡ{L}◊ṫ[1]lt◊ṫ[0]c} & ◊nc{◊ṡ{G}◊ṫ[1]◊ẋ{e×i}lt} & ◊nc{◊ṡ{G}◊ṫ[1]lten} & ◊nc{◊ṡ{L}◊ṫ[1]f}
  Instrumental & ◊nc{◊ṡ{L}◊ṫ[1]lca} & ◊nc{◊ṡ{L}◊ṫ[1]lhac} & ◊nc{◊ṡ{L}◊ṫ[1]lco} & ◊nc{◊ṡ{L}◊ṫ[1]lcen} & ◊nc{◊ṡ{L}◊ṫ[1]lcaf}
  Abessive & ◊nc{◊ṡ{L}◊ṫ[1]lþa} & ◊nc{◊ṡ{L}◊ṫ[1]lþac} & ◊nc{◊ṡ{L}◊ṫ[1]lþo} & ◊nc{◊ṡ{L}◊ṫ[1]lþen} & ◊nc{◊ṡ{L}◊ṫ[1]lþaf}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊subsection[#:id "decl2p-vr"]{Vowel + ◊l0{r} nouns}

◊l0{-êr}, ◊l0{-îr}

◊section[#:id "decl2u"]{The second declension (ultimate)}

Lack a separate G stem.

Similar guidelines to the penultimate case, except that you don’t need to create a separate G stem.

◊nc{◊ṫ[2]} is one of ◊l0{r l þ rþ}. The following transformations are defined for ◊nc{◊ṫ[2]}:

◊table/x[#:options (table-options #:caption ◊@{Transformations for the thematic consonant for second-declension ultimate nouns.} #:placement 'forcehere) #:id "declensions-2u-tc-xforms"]{
  ◊nc{◊ṫ[2]} & ◊nc{r} & ◊nc{l} & ◊nc{þ} & ◊nc{rþ}
  ◊nc{◊ṫ[2]◊ẋ{◊ẋ*len}} & ◊nc{r} & ◊nc{r} & ◊nc{s} & ◊nc{r}
  ◊nc{◊ṫ[2]◊ẋ{◊ẋ*mod}} & ◊nc{ð} & ◊nc{ð} & ◊nc{s} & ◊nc{r}
  ◊; ◊nc{◊ṫ[2]◊ẋ{◊ẋ*vcd}} & ◊nc{r} & ◊nc{r} & ◊nc{ð} & ◊nc{ð}
}

◊table/x[#:options (table-options #:caption ◊@{
  Common declensions for second-declension ultimate nouns. (If an entry consists of two forms separated by a pipe, then the former applies to ◊l0{-r} and ◊l0{-l} nouns, and the latter applies to ◊l0{-þ} and ◊l0{-rþ} nouns.)
} #:placement 'forcehere) #:id "declensions-2u-common"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}◊ṫ[0]c} | * & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}in} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]rin} | ◊nc{◊ṡ{N}◊ṫ[0]ðin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]jôr} & ◊nc{◊ṡ{N}e◊ṫ[2]◊ẋ{◊ẋ*len}i} & ◊nc{◊ṡ{N}e◊ṫ[2]◊ẋ{◊ẋ*len}inþ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}nþ}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]ls} | ◊nc{◊ṡ{N}◊ṫ[0]þe◊ẋ{‹e/i}s} & ◊nc{◊ṡ{N}elci} | * & ◊nc{◊ṡ{N}a◊ṫ[2]◊ẋ{◊ẋ*len}i} & ◊nc{◊ṡ{N}◊ṫ[0]lsen} | * & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}þ(ar)}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}i} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}ci} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}vi} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}ħin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}st}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]lt} & ◊nc{◊ṡ{L}◊ṫ[1]lt◊ṫ[0]c} & ◊nc{◊ṡ{N}◊ṫ[1]◊ẋ{e×i}lt} & ◊nc{◊ṡ{N}◊ṫ[1]lten} & ◊nc{◊ṡ{L}◊ṫ[1]f}
  Instrumental & ◊nc{◊ṡ{L}◊ṫ[1]lca} & ◊nc{◊ṡ{L}◊ṫ[1]lhac} & ◊nc{◊ṡ{L}◊ṫ[1]lco} & ◊nc{◊ṡ{L}◊ṫ[1]lcen} & ◊nc{◊ṡ{L}◊ṫ[1]lcaf}
  Abessive & ◊nc{◊ṡ{L}◊ṫ[1]lþa} & ◊nc{◊ṡ{L}◊ṫ[1]lþac} & ◊nc{◊ṡ{L}◊ṫ[1]lþo} & ◊nc{◊ṡ{L}◊ṫ[1]lþen} & ◊nc{◊ṡ{L}◊ṫ[1]lþaf}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊subsection[#:id "decl2u-vr"]{Vowel + ◊l0{r} nouns}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for vowel + ◊l0{r} nouns.
} #:placement 'forcehere) #:id "declensions-2u-vr"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]c} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ṫ[0]ðin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]rin} & ◊nc{◊ṡ{N}◊ṫ[0]rjôr} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}◊ṫ[0]rinþ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}nþ}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]ls} & ◊nc{◊ṡ{N}elci} & ◊nc{◊ṡ{N}ari} & ◊nc{◊ṡ{N}◊ṫ[0]lsen} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}þ(ar)}
}

◊subsection[#:id "decl2u-vl"]{Vowel + ◊l0{l} nouns}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for vowel + ◊l0{l} nouns.
} #:placement 'forcehere) #:id "declensions-2u-vl"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]l} & ◊nc{◊ṡ{N}◊ṫ[0]c} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ṫ[0]ðin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]rin} & ◊nc{◊ṡ{N}◊ṫ[0]ljôr} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}◊ṫ[0]rinþ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}nþ}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]ls} & ◊nc{◊ṡ{N}elci} & ◊nc{◊ṡ{N}ari} & ◊nc{◊ṡ{N}◊ṫ[0]lsen} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}þ(ar)}
}

◊subsection[#:id "decl2u-vth"]{Vowel + ◊l0{þ} nouns}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for vowel + ◊l0{þ} nouns.
} #:placement 'forcehere) #:id "declensions-2u-vth"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]þ} & ◊nc{◊ṡ{N}◊ṫ[0]cþ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ṫ[0]sin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]ðin} & ◊nc{◊ṡ{N}◊ṫ[0]þjôr} & ◊nc{◊ṡ{N}esi} & ◊nc{◊ṡ{N}◊ṫ[0]sinþ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}nþ}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]þe◊ẋ{‹e/i}s} & ◊nc{◊ṡ{N}ecþi} & ◊nc{◊ṡ{N}asi} & ◊nc{◊ṡ{N}◊ṫ[0]þen} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}þar}
}

◊subsection[#:id "decl2u-vrth"]{Vowel + ◊l0{rþ} nouns}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for vowel + ◊l0{rþ} nouns.
} #:placement 'forcehere) #:id "declensions-2u-vrth"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]rþ} & ◊nc{◊ṡ{N}◊ṫ[0]rþ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ṫ[0]rin} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]ðin} & ◊nc{◊ṡ{N}◊ṫ[0]rþjôr} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}◊ṫ[0]rinþ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}nþ}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]rþe◊ẋ{‹e/i}s} & ◊nc{◊ṡ{N}ercþi} & ◊nc{◊ṡ{N}ari} & ◊nc{◊ṡ{N}◊ṫ[0]rþen} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}þar}
}

◊section[#:id "decl3"]{The third declension}

◊subsection[#:id "decl3-os"]{◊l0{-os} nouns}

◊subsection[#:id "decl3-or"]{◊l0{-or} nouns}

◊subsection[#:id "decl3-on"]{◊l0{-on} nouns}

◊subsection[#:id "decl3-el"]{◊l0{-el} nouns}

◊section[#:id "decl4"]{The fourth declension}

◊section[#:id "rationale"]{Rationale}

◊subsection[#:id "rationale-8cases"]{Why these eight cases?}

They were there since the start of ŊCv9. In ŊCv7, there were twelve cases, but because of argument packing in verbs, the ablative, allative, prolative, and semblative I cases were rarely used and were removed in ŊCv9.

◊subsection[#:id "rationale-5nums"]{Why these five numbers?}

ŊCv7 had singular, dual, and plural numbers, as it was since VE²ENCS. The generic was added early in ŊCv9 to make things more interesting. Later on, the singulative was added along with the concept of clareþ in order to clarify the fact that some nouns lacked a useful interpretation for dual and plural numbers, as well as to make things more interesting.

◊subsection[#:id "rationale-1p-vth-hat"]{Why are the nominative singular endings of vowel + ◊l0{þ} nouns hatted?}

To force stress on the penultimate syllable.

◊subsection[#:id "rationale-2p-dat-hat"]{Why is the dative singular ending for second-declension ◊l0{-in} and ◊l0{-is} nouns ◊l0{-ês} instead of ◊l0{-es}?}

To avoid syncretism with the nominative generic.

◊section[#:id "examples"]{Examples}

◊subsection[#:id "ferna"]{◊l1{ferna, firnas, fenlit} (0/Ip(V))}

◊declension-table/split[◊@{Current declensions for ◊l1{ferna} ◊trans{child}, with locative collective form ◊l1{firnas} and semblative collective form ◊l1{fenlit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  ferna fernac ferno fernel fernaf ◊;
  fernan fernate ferner fernanþ fernafen ◊;
  fernas fernas fernai fernarin fernafes ◊;
  fernen vfernec vfernin vfernens fernif ◊;
  firnas firnesac firnos firnens firnaf ◊;
  firneca firnehac firnecta firnegen firnecef ◊;
  firneþa firneþac firnaþa firneðen firneþef ◊;
  fernit fernicta fernet fernicta fernicþ ◊;
}

◊declension-table/split[◊@{Declensions for ◊l1{ferna} ◊trans{child}, with locative collective form ◊l1{firnas} and semblative collective form ◊l1{fenlit} under Project Caladrius. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  ferna fernac ferno fernel fernaf ◊;
  fernan fernôr fernar fernanþ fernafen ◊;
  fernas fernas fernai fernarin fernafes ◊;
  fernen vfernjôr vfernin vfernens fernif ◊;
  firnas firnaces firnos firnens firnafes ◊;
  firneca firnecca firnica firninca firnefca ◊;
  firneþa firnecþa firniþa firninþa firnefþa ◊;
  fernit fernet fernet fernicta fernicþ ◊;
}

◊subsection[#:id "agethne"]{◊l1{ageþne, agoþnas, ageþnit} (0/Ip(V))}

◊declension-table/split[◊@{Current declensions for ◊l1{ageþne} ◊trans{virtue}, with locative collective form ◊l1{agoþnes} and semblative collective form ◊l1{ageþnit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  ageþne ageþnec ageþni ageþnil ageþnef ◊;
  ageþnen ageþnete ageþnir ageþnenþ ageþnefen ◊;
  ageþnes ageþnes ageþnei ageþnerin ageþnefes ◊;
  ageþnin gageþnic gageþnin gageþnins ageþnif ◊;
  agoþnes agoþnesac agoþnis agoþnins agoþnef ◊;
  agoþneca agoþnehac agoþnecta agoþnegen agoþnecif ◊;
  agoþneþa agoþneþac agoþneþa agoþneðen agoþneþif ◊;
  ageþnit ageþnicte ageþnet ageþnicte ageþnicþ ◊;
}

◊declension-table/split[◊@{Declensions for ◊l1{ageþne} ◊trans{virtue}, with locative collective form ◊l1{agoþnas} and semblative collective form ◊l1{ageþnit} under Project Caladrius. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  ageþne ageþnic ageþni ageþnil ageþnef ◊;
  ageþnen ageþnôr ageþner ageþnenþ ageþnefen ◊;
  ageþnes ageþnes ageþnei ageþnerin ageþnefes ◊;
  ageþnin gageþnjôr gageþnen gageþnins ageþnef ◊;
  agoþnas agoþnaces agoþnos agoþnens agoþnafes ◊;
  agoþneca agoþnecca agoþnica agoþninca agoþnefca ◊;
  agoþneþa agoþnecþa agoþniþa agoþninþa agoþnefþa ◊;
  ageþnit ageþnet ageþnet ageþnicte ageþnicþ ◊;
}

◊subsection[#:id "tfatho"]{◊l1{tfaþo, tfoþes, tfadit} (0/Ip(V))}

◊declension-table/split[◊@{Current declensions for ◊l1{tfaþo} ◊trans{village, town}, with locative collective form ◊l1{tfoþos} and semblative collective form ◊l1{tfadit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  tfaþo tfaþoc tfaþe tfaþel tfaþof ◊;
  tfaþon tfaþate tfaþer tfaþonþ tfaþofen ◊;
  tfaþos tfaþos tfaþai tfaþorin tfaþofes ◊;
  tfaþen tfaþec tfaþin tfaþens tfaþif ◊;
  tfoþos tfoþesac tfoþes tfoþens tfoþof ◊;
  tfoþeca tfoþehac tfoþecta tfoþegen tfoþecef ◊;
  tfoþeþa tfoþeþac tfoþeþa tfoþeðen tfoþeþef ◊;
  tfadit tfadicto tfadet tfadicto tfadicþ ◊;
}

◊declension-table/split[◊@{Declensions for ◊l1{tfaþo} ◊trans{village, town}, with locative collective form ◊l1{tfoþes} and semblative collective form ◊l1{tradit} under Project Caladrius. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  tfaþo tfaþin tfaþan tfaþel tfaþof ◊;
  tfaþon tfaþôr tfaþor tfaþonþ tfaþofen ◊;
  tfaþos tfaþos tfaþai tfaþorin tfaþofes ◊;
  tfaþen tfaþjôr tfaþin tfaþens tfaþif ◊;
  tfoþes tfoþecis tfoþis tfoþins tfoþefis ◊;
  tfoþeca tfoþecca tfoþica tfoþinca tfoþefca ◊;
  tfoþeþa tfoþecþa tfoþiþa tfoþinþa tfoþefþa ◊;
  tfadit tfadet tfadet tfadicto tfadicþ ◊;
}

◊subsection[#:id "eltes"]{◊l1{eltes, ilteþ, eldit} (1/Ip(Vs))}

◊declension-table/split[◊@{Current declensions for ◊l1{eltes} ◊trans{river}, with locative collective form ◊l1{ilteþ} and semblative collective form ◊l1{eldit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  eltes eltec eltis elterin eltef ◊;
  elten elteste eltir eltenþ eltefen ◊;
  elteþ elteþe elteri eltesin eltefes ◊;
  eltin gelticþ geltin geltins eltif ◊;
  ilteþ ilteþac iltiþ iltinþ iltef ◊;
  iltecþa iltecþac iltecta iltecþen iltecþif ◊;
  ilteþþa ilteþþac ilteþþa ilteþþen ilteþþif ◊;
  eldit eldiste eldet eldiste eldicþ ◊;
}

◊declension-table/split[◊@{Declensions for ◊l1{eltes} ◊trans{river}, with locative collective form ◊l1{iltes} and semblative collective form ◊l1{eldit} under Project Caladrius. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  eltes eltic eltis elterin eltef ◊;
  eltêns eltôr elter eltenþ eltefen ◊;
  elto elteþ elteri eltesin eltefes ◊;
  eltin geltjôr gelten geltins eltif ◊;
  iltes iltecis iltis iltens iltefis ◊;
  ilteca iltecca iltica iltinca iltefca ◊;
  ilteþa iltecþa iltiþa iltinþa iltefþa ◊;
  eldit eldet eldet eldicte eldicþ ◊;
}

◊subsection[#:id "lethin"]{◊l1{leþin, lsersen, liselt, leðit} (4/IIp(iC))}

◊declension-table/split[◊@{Current declensions for ◊l1{leþin} ◊trans{bluebird}, with genitive collective form ◊l1{lersen}, locative collective form ◊l1{liselt}, and semblative collective form ◊l1{leðit}. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  leþin leþecin leþjan leþedin leþef ◊;
  leþe leþec leþeri leþeħin leþefen ◊;
  leþes leþecþo leþerþ leþerin leþefes ◊;
  lersen lersenco lerseþ lersens lersefin ◊;
  liselt liseltic lersilt lerselten lisef ◊;
  liselca liselhac liselco liselcen lisecef ◊;
  liselþa liselþac lisilþa liselþen liseþef ◊;
  leðit leðinte leðet leðinte leðicþ ◊;
}

◊declension-table/split[◊@{Declensions for ◊l1{leþin} ◊trans{bluebird}, with genitive collective form ◊l1{lersen}, locative collective form ◊l1{liselt}, and semblative collective form ◊l1{leðit} under Project Caladrius. (For illustration purposes, this table assumes that nouns can be declined for all numbers.)} nc]{
  ◊; The comments are required. Don't remove them!
  leþin leþecin leþan leþedin leþes ◊;
  leþe leþec leþeri leþeħin leþens ◊;
  leþês leþecþ leþerþ leþerin leþeþ ◊;
  lersen lersjôr lerseþ lersens leþest ◊;
  liselt liseltic lersilt lerselten lisef ◊;
  liselca liselhac liselco liselcen liselcaf ◊;
  liselþa liselþac lisilþo liselþen liselþaf ◊;
  leðit leðet leðet leðicte leðicþ ◊;
}

◊subsection[#:id "full-texts"]{Full texts}

◊subsubsection[#:id "ft-sq0"]{Square Poem #0x00}

◊l1{ondis} in the original is an error; it should have been ◊l1{ondelt} originally.

◊nc{
  venełan ◊del{erionoros} ◊ins{???} sivandełas ◊del{crîði} ◊ins{???}
  ◊mark{rana} nalfo. ◊mark{mêven} ◊del{łan} ◊ins{???}
  vesro; ħespe; ◊mark{čercas}; ◊del{ondis} ◊ins{ondelt}
  ◊del{cfjoþelþa} ◊ins{cfjoþeþa} ◊mark{lora} ◊mark{aveneca} gendroris.
}
