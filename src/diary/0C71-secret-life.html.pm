#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{The secret life of verbs}
◊define-meta[date]{2022-01-01}

As of cþC71, Ŋarâþ Crîþ verbs have five principal parts:

◊enum[#:type 'Roman]{
  ◊item{The infinitive (of course)}
  ◊item{Any present-tense finite form (subject to vowel affection)}
  ◊item{Any past-tense finite form (subject to vowel affection)}
  ◊item{Any nominative-rcase relative form (subject to vowel affection and stem impersonation)}
  ◊item{Any relative form with rcase other than nominative (subject to vowel affection and stem impersonation)}
}

The finite forms depend on the second and third principal parts, and the relative forms depend on the fourth and fifth parts. All other forms, such as nominalized and converbal forms, depend only on the first part.

The second and third principal parts are constrained to deviate from the first in a narrow range of ways. The fourth and fifth, on the other hand, have arbitrary freedom to differ from the first, although they rarely exercise it.

The need for nouns derived from verbs has made appealing the idea of adding additional principal parts, namely to act as the L and S stems of such nouns. In my personal diary, I proposed the following additional uses for such principal parts:

◊items{
  ◊item{1st- and 2nd- person plural finite forms}
  ◊item{locative-, instrumental-, and abessive-hcase participle forms (or vary the set by genus and species; how would this interact with impersonator stems?; and note that this is the obvious thing to do)}
  ◊item{perhaps replace some of the existing particle + infinitive combinations for nominalized verbs with synthetic forms}
}

Verbs can be grouped by ◊term{class}, showing their degree of irregularity: more irregular verbs are assigned to higher classes. In the current system, there are five classes, from lowest to highest:

◊items{
  ◊item{At the bottom is ◊term{class ω}, whose conjugations can be fully predicted from the infinitive alone (if the species of the verb is known). Examples: ◊l1{magrit}, ◊l1{asfit}.}
  ◊item{Verbs in ◊term{class δ} have vowel affection only. Examples: ◊l1{sârit}, ◊l1{ciþnit}.}
  ◊item{Verbs in ◊term{class γ} have at most vowel affection and stem impersonation. Examples: ◊l1{cemat}, ◊l1{selcit}.}
  ◊item{Verbs in ◊term{class β} have arbitrary principal parts. There are yet no verbs in this class, as the mechanisms for changing principal parts are currently absolute, rather than a tendency for the majority of the verbs.}
  ◊item{Verbs in ◊term{class α} are truly irregular: they have conjugations that cannot be predicted even by all of the principal parts. Examples: ◊l1{eþit}, ◊l1{telit}.}
}

Reworking verb conjugation would change the class structure.

◊section{Coming back to verbal conjugation}

The new principal parts would be needed for the following derivations:

◊items{
  ◊item{Agent derivations (both inanimate and animate; for animate, currently L = S = N)}
  ◊item{The patient derivation}
  ◊item{The location derivation (currently S = N and L = ◊code{N.subst_last_vowel("u")})}
  ◊item{The quality derivation (currently uses paradigm-14 regular stem derivation rules)}
}

Thus, we need the following principal parts:

◊enum[#:type 'Roman]{
  ◊item{The infinitive}
  ◊item{Any present-tense finite form (1◊sc{sg} arbitrarily)}
  ◊item{Any past-tense finite form (1◊sc{sg} arbitrarily)}
  ◊item{Any nominative-rcase relative form (◊sc{nom},◊sc{nom}.(◊sc{cel}|◊sc{sg}) arbitrarily)}
  ◊item{Any relative form with rcase other than nominative (◊sc{acc},◊sc{nom}.(◊sc{cel}|◊sc{sg}) arbitrarily)}
  ◊item{L stem for general use}
  ◊item{L stem for location derivation}
  ◊item{S stem for general use}
  ◊item{S stem for quality derivation (don’t question it.)}
}

◊section{What if we brought back the subjunctive?}

We’d use different stems for the subjunctive forms, parallel to the indicative counterparts. Then which forms conjugate for mood? Certainly the finite forms, and probably the participle and nominalized forms.

What mood is used in ◊i{so}-clauses? NCS6 always used the subjunctive before ◊v6{so}, so perhaps we should do the same with our ◊l1{so}. Alternatively, we could arbitrarily use the indicative for all ◊i{so}-clauses.

Do we even need ◊i{so}-clauses anymore, at least for ◊l1{so}? We could, for instance, use the nominalized form in the locative case and subjunctive mood for the same purpose. This leads to a funny situation in which the particle that ◊i{so}-clauses are named after no longer exists.

Thus, we need the following principal parts:

◊enum[#:type 'Roman]{
  ◊item{The infinitive}
  ◊item{The first-person present imperfective indicative form}
  ◊item{The first-person past imperfective indicative form}
  ◊item{The first-person present imperfective subjunctive form (almost always different from II)}
  ◊item{The first-person past imperfective subjunctive form (almost always different from III)}
  ◊item{Any nominative-rcase indicative relative form (◊sc{nom},◊sc{nom}.(◊sc{cel}|◊sc{sg}) arbitrarily)}
  ◊item{Any indicative relative form with rcase other than nominative (◊sc{acc},◊sc{nom}.(◊sc{cel}|◊sc{sg}) arbitrarily)}
  ◊item{Any nominative-rcase subjunctive relative form (◊sc{nom},◊sc{nom}.(◊sc{cel}|◊sc{sg}) arbitrarily; almost always different from VI)}
  ◊item{Any subjunctive relative form with rcase other than nominative (◊sc{acc},◊sc{nom}.(◊sc{cel}|◊sc{sg}) arbitrarily; almost always different from VII)}
  ◊item{Some indicative nominalized form that has been changed from the particle + infinitive formula to a synthetic form}
  ◊item{L stem for general use (perhaps can reuse IV, V, VIII, or IX)}
  ◊item{L stem for location derivation (perhaps can reuse IV, V, VIII, or IX)}
  ◊item{S stem for general use}
  ◊item{S stem for quality derivation}
}

I don’t think we need imperative or interrogative moods.
