#lang pollen

◊(require pollen/pagetree)

◊define-meta[title]{On agreement}
◊define-meta[date]{2022-08-28}

◊section[#:id "ithkuil-fallacy"]{The ‘Ithkuil fallacy’}

I recently came across one of K Klein’s videos, ◊link["https://www.youtube.com/watch?v=3B_uGsgXKdk"]{◊cite/inbook{Why Conjugate Verbs? – The Ithkuil Fallacy}}, which addresses a common misconception about languages – namely, that redundancy is useless. I’ve been guilty of thinking this when I was working on Necarasso Cryssesa v6.

◊section[#:id "head-directionality"]{Agreement and head directionality}

While writing lyrics in Ŋarâþ Crîþ, I found myself writing the start of the line but having to think ahead about what words would appear later in the phrase. For instance, a participle must agree with the head NP, which appears later, possibly affecting its syllable count. Likewise, the case of a noun phrase, as well as any relationals required for it, is dependent on the verb and is not predictable from its semantic role.

This observation implies that agreement is at least sometimes directional. In other words, given a feature expressed across two or more different words, the value of the feature can be said to be a property of one of them (the ◊term{source}), with the others being redundant. This can be depicted as information flowing from the source to the other words.

Consider again the relationship between a participle and the noun it modifies. In the phrase

◊gloss/x[#:id "deer"]{
  ◊glfree{◊nc{rilþas gitas}}
  ◊gla{rilþ-as git-as}
  ◊glb{sleep-%rel.%nom,%dat.%cel deer-%dat.%sg}
  ◊glfree{a sleeping deer (◊sc{dat})}
}

the dative case and celestial gender are considered to be properties of the deer, not of the sleeping. Thus, there is agreement from the noun ◊l1{gitas} to the participle ◊l1{rilþas}. There is another instance of agreement toward ◊l1{rilþas} – namely from the nominative-case trace in the relative clause.

Consider the relationship between the last two words in the following sentence:

◊gloss/x[#:id "mushroom"]{
  ◊glfree{◊nc{crîlþes ivina nalfo.}}
  ◊gla{crîlþ-es ivin-a nalf-o.}
  ◊glb{forest-%loc.%sg mushroom-%nom.%co grow-3%pl}
  ◊glfree{The mushrooms grow in the forest.}
}

The number and person of the noun ◊l1{ivina} flows to ◊l1{nalfo} and manifests as the suffix ◊l1{-o}. However, it happens that agreement also flows in the opposite direction! The nominative case of ◊l1{ivina} alone does not give information about what role it plays; it requires information about the verb to interpret.

The flow of core cases from the verb to the nouns is regarded as agreement because not all verbs accept arguments for all three core cases. Consider the following sentences:

◊gloss/x[#:id "os1"]{
  ◊glfree{◊nc{ilmon selgeþ.}}
  ◊gla{ilm-on selg-e-þ.}
  ◊glb{peel-%acc.%di slip-1%sg-%past}
  ◊glfree{I slipped on the peel.}
}

◊gloss/x[#:id "os2"]{
  ◊glfree{◊nc{ilmoþ naclilte.}}
  ◊gla{ilm-oþ naclid-te.}
  ◊glb{peel-%dat.%di trip-1%sg.%past.%pfv}
  ◊glfree{I tripped over the peel.}
}

◊gloss/x[#:id "os1x"]{
  ◊glfree[#:prefix "** "]{◊nc{ilmoþ selgeþ.}}
  ◊gla{ilm-oþ selg-e-þ.}
  ◊glb{peel-%dat.%di slip-1%sg-%past}
}

◊gloss/x[#:id "os2x"]{
  ◊glfree[#:prefix "** "]{◊nc{ilmon naclilte.}}
  ◊gla{ilm-on naclid-te.}
  ◊glb{peel-%acc.%di trip-1%sg.%past.%pfv}
}

Note also that the case frame can differ even between semantically similar verbs such as the aforementioned ◊l1{selgit} and ◊l1{naclidit}. As a result, this instance of bidirectional agreement is problematic even compared to other languages with both subject marking on verbs and cases. Thankfully, however, in Ŋarâþ Crîþ, verbs are always allowed to take a nominative argument, which is the only one that is allowed to agree in person and number on verbs – object marking on verbs cannot be redundant with explicit accusative or dative arguments. And even though there is an agreement cycle at the word or even the morpheme level, there is none at the feature level.
