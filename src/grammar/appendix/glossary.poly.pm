#lang pollen

◊define-meta[title]{Glossary}
◊define-meta[chapter-type appendix]

Click on a headword to go to the relevant part of the grammar.

◊glossary{
  ◊entry["Layer" "phonology.html" #f "Chapter"]{
    One of the eight representations of Ŋarâþ Crîþ text. Each layer represents Ŋarâþ Crîþ text at a different layer of abstraction and exists in either the written or spoken mode.
    ◊in-ncs{flef}
    ◊in["art-x-v3-arx0as"]{hank}
  }
  ◊entry["Cenvos" "phonology.html" "layer-01" "Section"]{
    The script used by Ŋarâþ Crîþ natively.
    ◊in-ncs{cenvos}
  }
  ◊entry["Grapheme" "phonology.html" "layer-01" "Section"]{
    The basic unit of representation in layers 0 and 1. Includes letters, digits, and punctuation.
    ◊; ◊in-ncs{???}
    ◊in["art-x-v3-arx0as"]{haca}
    ◊in["ja"]{書記素}
  }
  ◊entry["Glyph" "phonology.html" "layer-01" "Section"]{
    The basic unit of representation in layers 2w, 2w*, 3w, and 4w. These represent the characters being written. Glyphs distinguish ligatures and final forms from their constituent letters.
    ◊; ◊in-ncs{???}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
  }
  ◊entry["Letter" "phonology.html" "layer-01" "Section"]{
    Either a ◊term{true letter} or a ◊term{marker}. A glyph that has a letter value.
    ◊in-ncs{cenvos}
    ◊in["art-x-v3-arx0as"]{hac}
  }
  ◊entry["USR Letter" "phonology.html" "layer-01" "Section"]{
    One of ◊l1{w}, ◊l1{x}, ◊l1{y}, or ◊l1{z}. These letters are dedicated to be assigned to phonemes in foreign languages that are absent from Ŋarâþ Crîþ.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
  }
  ◊entry["True letter" "phonology.html" "layer-01" "Section"]{
    A ◊term{letter} that corresponds to a phoneme.
    ◊; ◊in-ncs{cenvos}
    ◊in["art-x-v3-arx0as"]{fohac}
  }
  ◊entry["Marker" "phonology.html" "layer-01" "Section"]{
    A ◊term{letter} that does not correspond to a phoneme but rather has a semantic role.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
  }
  ◊entry["Punctuation" "phonology.html" "layer-01" "Section"]{
    The class of glyphs that are classified as neither letters nor digits. Includes the clause-end punctuation ◊l1{.}, ◊l1{;}, ◊l1{?}, and ◊l1{!}; the clitic boundary mark ◊l1{’}; the lenition mark ◊l1{·}; the grouping brackets ◊l1{{}}; and the quotation marks ◊l1{«»}.
    ◊in-ncs{dono}
    ◊in["art-x-v3-arx0as"]{valia}
    ◊in["ja"]{句読点}
  }
  ◊entry["Clitic" "phonology.html" "layer-01" "Section"]{
    An entity that acts like an affix phonologically but like a separate word syntactically. In Ŋarâþ Crîþ, all clitics are postclitics and are marked with a ◊i{ŋos}, ◊l1{’}.
    ◊; ◊in-ncs{dono}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
    ◊in["ja"]{接語}
  }
  ◊entry["Clitic boundary" "phonology.html" "layer-01" "Section"]{
    The boundary between a clitic and another word, or between two clitics. The boundary marked by the ◊i{ŋos}.
    ◊; ◊in-ncs{dono}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
    ◊; ◊in["ja"]{接語}
  }
  ◊entry["Syntactic word (sword)" "phonology.html" "layer-01" "Section"]{
    A sequence of graphemes separated by either spaces or ◊i{ŋos}.
    ◊; ◊in-ncs{dono}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
    ◊; ◊in["ja"]{接語}
  }
  ◊entry["Orthographic word (oword)" "phonology.html" "layer-01" "Section"]{
    A sequence of graphemes separated by spaces. A clitic is considered to belong to the same oword as the word to which it is attached.
    ◊; ◊in-ncs{dono}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
    ◊; ◊in["ja"]{接語}
  }
  ◊entry["Morpheme boundary" "phonology.html" "layer-01" "Section"]{
    The boundary between two morphemes of the same syntactic word. These are considered significant in layer 0.
    ◊; ◊in-ncs{dono}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
    ◊; ◊in["ja"]{接語}
  }
  ◊entry["Digit" "phonology.html" "layer-01" "Section"]{
    One of the sixteen graphemes that may be used to write short numerals.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
  }
  ◊entry["Manifested grapheme phrase (MGF)" "phonology.html" "phonotactics" "Subsection"]{
    A sequence of graphemes that is said to represent a single phoneme. Either a true letter not followed by a lenition marker (◊term{plain letter}), any of ◊l0{p t d č c g m f v ð} followed by a lenition mark (◊term{lenited letter}), or, word-initially, one of the digraphs ◊l0{mp vp dt nd gc ŋg vf ðþ lł} (◊term{eclipsed letter}).
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
  }
  ◊entry["Base letter" "phonology.html" "phonotactics" "Subsection"]{
    Of a manifested grapheme phrase, the letter before whatever mutation, if any, resulted in the MGF.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{hac}
  }
  ◊entry["Vowel" "phonology.html" "phonotactics" "Subsection"]{
    One of ◊l0{e o a î i ê ô â u}, or the phoneme represented by one of these.
    ◊; ◊in-ncs{cenvos}
    ◊in["art-x-v3-arx0as"]{vesto}
    ◊in["ja"]{母音}
  }
  ◊entry["Effective plosive" "phonology.html" "phonotactics" "Subsection"]{
    A manifested grapheme phrase whose base letter represents a plosive phoneme; that is, whose base letter is any of ◊l0{p t d c g}. Such consonants may appear before ◊l0{r} or ◊l0{l} as an onset.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{vesto}
    ◊; ◊in["ja"]{母音}
  }
  ◊entry["Effective fricative" "phonology.html" "phonotactics" "Subsection"]{
    A manifested grapheme phrase whose base letter represents a fricative phoneme; that is, whose base letter is any of ◊l0{f v þ ð s š h ħ}. Such consonants may appear before ◊l0{r} or ◊l0{l} as an onset.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{vesto}
    ◊; ◊in["ja"]{母音}
  }
  ◊entry["Simple coda" "phonology.html" "phonotactics" "Subsection"]{
    A coda that is simple enough to occur mid-word.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{vesto}
    ◊; ◊in["ja"]{母音}
  }
  ◊entry["Complex coda" "phonology.html" "phonotactics" "Subsection"]{
    A coda that can be pronounced only word-finally. Instances of such codas in the middle of a syntactic word are simplified during the conversion to layer 1, and such instances immediately before a clitic boundary are simplified during the conversion to layer 2.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{vesto}
    ◊; ◊in["ja"]{母音}
  }
  ◊entry["Letter number" "phonology.html" "letternum" "Subsection"]{
    A number assigned to each letter in layer 1.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{vesto}
    ◊; ◊in["ja"]{母音}
  }
  ◊entry["Letter sum" "phonology.html" "letternum" "Subsection"]{
    The sum of the ◊term{letter numbers} of each letter in a word. This value is used for some noun declension paradigms.
    ◊; ◊in-ncs{cenvos}
    ◊; ◊in["art-x-v3-arx0as"]{vesto}
    ◊; ◊in["ja"]{母音}
  }
  ◊entry["Numquote" "phonology.html" "numquotes" "Subsection"]{
    A digit immediately preceding text surrounded by quotation or grouping marks, mainly used for secondary purposes that lack any dedicated punctuation.
    ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{alxkert}
  }
  ◊entry["Tone" "phonology.html" "layer-3" "Section"]{
    The phonological feature associated with the pitch of a syllable. Ŋarâþ Crîþ has two tones: ◊term{high} and ◊term{low}, with the former being more common. This distinction evolved from the absence or presence of creaky voice in Ŋarâþ Crîþ v7.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{eldem}
    ◊in["ja"]{声調}
  }
  ◊entry["Stress" "phonology.html" "layer-3" "Section"]{
    A feature that was present in Ŋarâþ Crîþ v7 but in Ŋarâþ Crîþ v9 is used solely to calculate tone.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{caf}
    ◊in["ja"]{アクセント}
  }
  ◊entry["Tone accounting unit (TAU)" "phonology.html" "layer-3" "Section"]{
    The maximal unit at which tone is calculated. In other words, calculating the tone of a given syllable requires looking only at the syllables in the same TAU.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{caf}
    ◊; ◊in["ja"]{アクセント}
  }
  ◊entry["Consonant mutation" "phonology.html" "mutations" "Section"]{
    The systematic modification of a consonant, triggered by a morphological environment. Ŋarâþ Crîþ has two types of mutations: ◊term{lenition} and ◊term{eclipsis}.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{amisomiyu}
    ◊in["ja"]{子音変異}
  }
  ◊entry["Lenition" "phonology.html" "mutations" "Section"]{
    The mutation in Ŋarâþ Crîþ that tends to turn plosives into fricatives. Some consonants become null under lenition.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{hoomim}
    ◊; ◊in["ja"]{子音変異}
  }
  ◊entry["Total lenition" "phonology.html" "mutations" "Section"]{
    ◊term{Lenition} that includes the deletion of ◊l0{f}, ◊l0{v}, or ◊l0{d}.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{ilmhoomim}
    ◊; ◊in["ja"]{子音変異}
  }
  ◊entry["Partial lenition" "phonology.html" "mutations" "Section"]{
    ◊term{Lenition} that does not delete ◊l0{f}, ◊l0{v}, or ◊l0{d}.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{vaikhoomim}
    ◊; ◊in["ja"]{子音変異}
  }
  ◊entry["Eclipsis" "phonology.html" "mutations" "Section"]{
    The mutation in Ŋarâþ Crîþ that tends to add voice to voiceless consonants and change voiced stops into nasals.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{veltem}
    ◊; ◊in["ja"]{子音変異}
  }
  ◊entry["Kerning" "phonology.html" "kerning" "Subsection"]{
    The alteration of the distance between two glyphs to make their placement less awkward.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{veltem}
    ◊in["ja"]{カーニング}
  }
  ◊entry["Ligature" "phonology.html" "typographical-layers" "Subsection"]{
    A composite of two or more graphemes that are somehow joined together.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Required ligature" "phonology.html" "typographical-layers" "Subsection"]{
    A ligature that is present in layer 2w and is required to be used whenever possible.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Discretionary ligature" "phonology.html" "typographical-layers" "Subsection"]{
    A ligature that is present in layer 2w*. These ligatures are not required to be used, nor can they be derived by simply connecting the ending stroke of one glyph to the starting stroke of another.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Layer-3w ligation" "phonology.html" "typographical-layers" "Subsection"]{
    The joining of the last stroke of a layer-3w glyph with the first stroke of the next. This can happen only when the two glyphs have compatible join types on their respective ends.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Participation (ligation and shaping)" "phonology.html" "typographical-layers" "Subsection"]{
    A layer-2w* glyph is said to ◊term{participate} in typesetting if its shape is eligible to be altered by the process. All letters participate, but no numerals do so, nor does the space.
    ◊; ◊in-ncs{menerevin}
    ◊in["art-x-v3-arx0as"]{fakt}
    ◊in["ja"]{参加}
  }
  ◊entry["Stroke-order variant" "phonology.html" "typographical-layers" "Subsection"]{
    One of the variants of a layer-2w* glyph that differs only in stroke order. All strokes must be preserved, and no loops may be introduced or removed, but the relative stroke order might be different, and some strokes may be written in the reverse direction; furthermore, a stroke may be split at a turn, and two strokes may be joined where one ends and another begins.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Topological variant" "phonology.html" "typographical-layers" "Subsection"]{
    One of the variants of a stroke-order variant, which may join strokes together, cause two different strokes to touch each other when they did not (or vice versa), or introduce or remove loops. Lengthening or shortening strokes to alter ligation properties also falls under this level.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Stylistic variant" "phonology.html" "typographical-layers" "Subsection"]{
    One of the variants of a topological variant, which may modify the strokes of the glyph themselves.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Canonical stroke order" "phonology.html" "typographical-layers" "Subsection"]{
    The most common or accepted stroke order of a layer-2w* glyph.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Vertical ligation" "phonology.html" "vertical-ligation" "Subsection"]{
    The practice of ligating two glyphs that lie on different lines.
    ◊; ◊in-ncs{menerevin}
    ◊; ◊in["art-x-v3-arx0as"]{fakt}
    ◊; ◊in["ja"]{参加}
  }
  ◊entry["Sentence" "syntax.html" #f "Chapter"]{
    A unit of Ŋarâþ Crîþ text terminated by a ◊i{gen}, ◊i{šac}, or ◊i{cjar}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{vok}
    ◊in["ja"]{文}
  }
  ◊entry["Independent clause phrase (ICP)" "syntax.html" #f "Chapter"]{
    A unit of Ŋarâþ Crîþ text terminated by a ◊i{gen}, ◊i{tja}, ◊i{šac}, or ◊i{cjar}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{lestyavsevet}
    ◊in["ja"]{主節句}
  }
  ◊entry["General independent clause phrase (gICP)" "syntax.html" #f "Chapter"]{
    A clause phrase that falls into the typical pattern for Ŋarâþ Crîþ text, consisting of an independent clause and zero or more subordinate clauses.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{leimlestyavsevet}
    ◊in["ja"]{普通主節句}
  }
  ◊entry["Special independent clause phrase (sICP)" "syntax.html" #f "Chapter"]{
    A clause phrase that has a structure different from that of a general independen clause phrase. Special independent clause phrases include ◊term{interjections}, ◊term{vocatives}, and ◊term{data}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{aallestyavsevet}
    ◊in["ja"]{特別主節句}
  }
  ◊entry["Interjection" "syntax.html" #f "Chapter"]{
    A word in the ‘interjection’ part of speech. This constitutes a special independent clause phrase.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{xivi}
    ◊in["ja"]{感動詞}
  }
  ◊entry["Vocative" "syntax.html" #f "Chapter"]{
    A noun phrase in the dative case used to address someone or something. This constitutes a special independent clause phrase.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{xivi}
    ◊; ◊in["ja"]{感動詞}
  }
  ◊entry["Dependent special independent clause phrase (dsICP)" "syntax.html" #f "Chapter"]{
    A special independent clause phrase that requires another ICP in the same sentence, such as ◊l1{cirtel} ◊trans{by the way, incidentally} or ◊l1{olasta} ◊trans{in addition, furthermore, moreover}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{roxkaallestyavsevet}
    ◊in["ja"]{従属特別主節句}
  }
  ◊entry["Independent clause" "syntax.html" "independent" "Section"]{
    A clause at the head of an independent clause phrase. If an independent clause ends in a verb, then that verb is in a finite form.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{lestyav}
    ◊in["ja"]{主節}
  }
  ◊entry["Dependent clause" "syntax.html" "dependent" "Section"]{
    A clause that somehow modifies a constituent of another clause. This cataegory includes ◊term{relative clauses}, ◊term{converbal clauses}, ◊term{◊i{so}-clauses}, and ◊term{nominalized clauses}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{roxkyav}
    ◊in["ja"]{従属節}
  }
  ◊entry["Head particle (headp)" "syntax.html" "particles" "Section"]{
    A particle that occurs at the beginning of an independent clause phrase.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{osnreit}
    ◊in["ja"]{文頭純詞}
  }
  ◊entry["Absolute head particle (aheadp)" "syntax.html" "particles" "Section"]{
    A head particle that always occurs at the beginning of an independent clause phrase.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{anoiosnreit}
    ◊in["ja"]{絶対文頭純詞}
  }
  ◊entry["Conjunct head particle (cheadp)" "syntax.html" "particles" "Section"]{
    A head particle that usually occurs at the beginning of an independent clause phrase but may move after the end of a ◊i{so}-clause.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{noidosnreit}
    ◊in["ja"]{相対文頭純詞}
  }
  ◊entry["Tail particle (tailp)" "syntax.html" "particles" "Section"]{
    A particle that occurs at the end of an independent clause phrase, immediately after the verb (if present).
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{hetreit}
    ◊in["ja"]{文末純詞}
  }
  ◊entry["Polar question" "syntax.html" "questions" "Section"]{
    A question that asks whether or not a statement is true.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{hetreit}
    ◊; ◊in["ja"]{文末純詞}
  }
  ◊entry["Tag question" "syntax.html" "questions" "Section"]{
    A question that asks whether or not a statement is true but leads toward an affirmative answer, asking for confirmation on a statement.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{hetreit}
    ◊; ◊in["ja"]{文末純詞}
  }
  ◊entry[◊@{◊i{Wh}-question} "syntax.html" "questions" "Section"]{
    A question that asks for an item in which an interrogative pro-form appears.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{hetreit}
    ◊; ◊in["ja"]{文末純詞}
  }
  ◊entry["Choice question" "syntax.html" "questions" "Section"]{
    A question that asks for a choice between two or more options.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{hetreit}
    ◊; ◊in["ja"]{文末純詞}
  }
  ◊entry["Datum" "syntax.html" "data-pos" "Section"]{
    A part of speech that is used to convey data. A datum can be used as a special independent clause phrase.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{xivi}
    ◊; ◊in["ja"]{感動詞}
  }
  ◊entry["Compound datum" "syntax.html" "data-pos" "Section"]{
    A datum that is either a list or a key-value list.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{xivi}
    ◊; ◊in["ja"]{感動詞}
  }
  ◊entry["Gender" "nouns.html" #f "Chapter"]{
    A lexical feature of a noun that dictates agreement with certain other words. Also called ◊term{noun class}. Ŋarâþ Crîþ has three genders: ◊term{celestial}, ◊term{terrestrial}, and ◊term{human}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{him}
    ◊in["ja"]{性}
  }
  ◊entry["Number" "nouns.html" #f "Chapter"]{
    A feature of a declined noun indicating how many of the referent is present. Ŋarâþ Crîþ has five numbers: ◊term{direct}, ◊term{dual}, ◊term{plural}, ◊term{singulative}, and ◊term{generic}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{alx}
    ◊in["ja"]{数}
  }
  ◊entry["Generic number" "nouns.html" #f "Chapter"]{
    One of the five numbers of Ŋarâþ Crîþ, which is used on noun phrases that do not refer to a specific referent or referents.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{alx}
    ◊; ◊in["ja"]{数}
  }
  ◊entry["Clareþ" "nouns.html" #f "Chapter"]{
    A lexical feature of a noun that dictates what numbers it may take. Ŋarâþ Crîþ has three claroþ: ◊term{singular}, ◊term{collective}, and ◊term{mass}.
    ◊in-ncs{clareþ}
    ◊; ◊in["art-x-v3-arx0as"]{alx}
    ◊; ◊in["ja"]{数}
  }
  ◊entry["Case" "nouns.html" #f "Chapter"]{
    A feature of a declined noun indicating what role it plays in a sentence. Ŋarâþ Crîþ has eight cases.
    ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{same}
    ◊in["ja"]{格}
  }
  ◊entry["Nominative case" "nouns.html" #f "Chapter"]{
    The case that indicates the subject of a verb phrase.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{solsame}
    ◊in["ja"]{主格}
  }
  ◊entry["Accusative case" "nouns.html" #f "Chapter"]{
    The case that, among other things, indicates the ‘direct object’ of a verb phrase.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yulsame}
    ◊in["ja"]{対格}
  }
  ◊entry["Dative case" "nouns.html" #f "Chapter"]{
    The case that, among other things, indicates the ‘indirect object’ of a verb phrase.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{alsame}
    ◊in["ja"]{与格}
  }
  ◊entry["Genitive case" "nouns.html" #f "Chapter"]{
    The case that shows such things as possession, composition, description, or apposition.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{tilsame}
    ◊in["ja"]{属格}
  }
  ◊entry["Locative case" "nouns.html" #f "Chapter"]{
    The case that shows the location or time of an object or an action.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{kasame}
    ◊in["ja"]{処格}
  }
  ◊entry["Instrumental case" "nouns.html" #f "Chapter"]{
    The case that shows that a noun phrase is used as an instrument or an accompaniment; that is, it has comitative or instrumental function.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{konsame}
    ◊in["ja"]{具格}
  }
  ◊entry["Abessive case" "nouns.html" #f "Chapter"]{
    The case that shows that a noun phrase is ◊em{not} used as an instrument or an accompaniment; that is, it is the negataion of the ◊term{instrumental case}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{vitsame}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Semblative case" "nouns.html" #f "Chapter"]{
    The case that shows semblance in behavior. On a nominalized verb, it translates to ‘such that’, ‘as though’, or ‘to the point that’.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yunsame}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Core case" "nouns.html" #f "Chapter"]{
    A collective term for the nominative, accusative, dative, and genitive cases. In particular, basic personal pronouns lack forms for these cases.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yunsame}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Paradigm (noun declension)" "nouns.html" "declensions" "Section"]{
    A lexical property of a noun that governs how it is declined for case and number.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yunsame}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Paradigm" "nouns.html" "declensions" "Section"]{
    A set of rules by which a lexical item can be inflected.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yunsame}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Component" "nouns.html" "declensions" "Section"]{
    A part from which an inflected form of a word is built.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{miyuvait}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Constant" "nouns.html" "declensions" "Section"]{
    A component that stays the same regardless of the lexical item being inflected.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{miyuvait}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Variable" "nouns.html" "declensions" "Section"]{
    A component that depends on the lexical item being inflected. They can be either ◊term{stems} or ◊term{themes}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{miyuvait}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Stem" "nouns.html" "declensions" "Section"]{
    A ◊term{variable} that usually takes up a substantial part of the inflected form and is considered the essence of a lexical item.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{veyutifl}
    ◊in["ja"]{語幹}
  }
  ◊entry["Theme" "nouns.html" "declensions" "Section"]{
    A ◊term{variable} that is short (almost always one letter long) and might have ◊term{derivatives}.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yunsame}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Derivative" "nouns.html" "declensions" "Section"]{
    A string that can be derived systematically from the value of a theme for a given paradigm.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{leveol}
    ◊; ◊in["ja"]{具格}
  }
  ◊entry["Principal part" "nouns.html" "declensions" "Section"]{
    One of the inflected forms of a lexical entry that can collectively determine all other inflected forms.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{tihmo}
    ◊in["ja"]{主要形}
  }
  ◊entry["Basic personal pronouns" "nouns.html" "pronouns-personal-basic" "Subsubsection"]{
    A set of personal pronouns for first, second, and third persons, the last of which are distinguished by gender. They are defective and lack forms for the ◊term{core cases}.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Possessive clitic" "nouns.html" "pronouns-personal-possessive" "Subsubsection"]{
    A clitic used on a noun phrase to mark possession.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Possessive construction" "nouns.html" "pronouns-personal-possessive" "Subsubsection"]{
    A double-marked construction to show possession, in which the possessee receives a possessive clitic and the possessor receives the clitic ◊l1{=’(e)þ}.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Reflexive pronoun" "nouns.html" "pronouns-personal-reflexive" "Subsubsection"]{
    The personal pronoun ◊nc{cenþ}, which has the meaning of ◊trans{oneself}.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Emphatic pronoun" "nouns.html" "pronouns-personal-emphatic" "Subsubsection"]{
    A pronoun made of a reflexive pronoun with a possessive clitic attached, which acts roughly like a personal pronoun with an independent form but places focus on the referent.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Interrogative pro-form" "nouns.html" "pronouns-interrogative" "Subsection"]{
    One of the pronouns, determiners, or pro-verbs that is used to ask a question.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Demonstrative pro-form" "nouns.html" "pronouns-demonstrative" "Subsection"]{
    One of the pronouns, determiners, or pro-verbs that is used to refer to something in a particular frame of reference.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Quantifier" "nouns.html" "quantification" "Subsection"]{
    A pro-form such as the pronouns ◊l1{šino} ◊trans{all} or ◊l1{nema} ◊trans{some, any}, which acts as a quantifier over anything occuring after it.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Coordinator" "nouns.html" "coordination" "Section"]{
    The conjunction of a coordinated phrase. In Ŋarâþ Crîþ, nominal coordinators appear as clitics and are sometimes fused with pronouns.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊in["ja"]{等位接続詞}
  }
  ◊entry["Coordinand" "nouns.html" "coordination" "Section"]{
    The phrases being joined by a ◊term{coordinator}.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Quotative" "nouns.html" "quotatives" "Section"]{
    A construction that casts a string into a noun describing the string being conveyed (e.g. said or written). In Ŋarâþ Crîþ, this is achieved using a particle after the quotation.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Direct quotative" "nouns.html" "quotatives" "Section"]{
    A quotative that describes speech exactly as it was or is expressed by someone.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Indirect quotative" "nouns.html" "quotatives" "Section"]{
    A quotative that describes speech that is not necessarily the exact words used by someone but has an equivalent meaning.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Predicate" "verbs.html" #f "Chapter"]{
    A part of speech that describes an action or state.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{tihmo}
    ◊; ◊in["ja"]{主要形}
  }
  ◊entry["Verb" "verbs.html" #f "Chapter"]{
    A part of speech that is inflected like a verb. That is, finite forms do not require a scaffolding verb, and adnominal forms require participle conjugations.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yuo}
    ◊in["ja"]{動詞}
  }
  ◊entry["Valency class" "verbs.html" "valency" "Section"]{
    A lexical property of a verb or relational that governs how many arguments it can take and what cases they assume.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{freyuyuo}
    ◊; ◊in["ja"]{助動詞}
  }
  ◊entry["Independent verb (iverb)" "verbs.html" "valency" "Section"]{
    A verb that is not an auxiliary verb.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{freyuyuo}
    ◊; ◊in["ja"]{助動詞}
  }
  ◊entry["Intransitive verb" "verbs.html" "valency" "Section"]{
    A verb that takes only a nominative argument.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{reinoyuo}
    ◊in["ja"]{自動詞}
  }
  ◊entry["Semitransitive verb" "verbs.html" "valency" "Section"]{
    A verb that takes a nominative argument and a dative argument.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yakkokkolyuo}
    ◊; ◊in["ja"]{自動詞}
  }
  ◊entry["Transitive verb" "verbs.html" "valency" "Section"]{
    A verb that takes a nominative argument and an accusative argument.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{kokkolyuo}
    ◊in["ja"]{他動詞}
  }
  ◊entry["Ditransitive verb" "verbs.html" "valency" "Section"]{
    A verb that takes a nominative argument, an accusative argument, and a dative argument.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{arkansyuo}
    ◊in["ja"]{二重他動詞}
  }
  ◊entry["Tense" "verbs.html" "categories" "Section"]{
    A property of a conjugated verb that indicates the time in which an action or state occurred. Ŋarâþ Crîþ has two tenses: ◊term{present} (more precisely, ◊term{nonpast}) and ◊term{past}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{mel}
    ◊in["ja"]{時制}
  }
  ◊entry["Aspect" "verbs.html" "categories" "Section"]{
    A property of a conjugated verb that denotes how the action or state described by the verb extends over time. Ŋarâþ Crîþ has two aspects: ◊term{imperfective} and ◊term{perfective}. The imperfective aspect is used for ongoing (such as progressive or habitual) actions. The perfective aspect is used for completed actions. In conjugation, aspects can be labeled as ◊term{direct} or ◊term{indirect}. The ◊term{direct aspect} is imperfective for the present tense and perfective for the past tense; the ◊term{inverse aspect} is the other aspect.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{nif}
    ◊in["ja"]{相}
  }
  ◊entry["Imperfective aspect" "verbs.html" "categories" "Section"]{
    The aspect used for ongoing (such as progressive or habitual) actions.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{reinnif}
    ◊in["ja"]{非完結相}
  }
  ◊entry["Perfective aspect" "verbs.html" "categories" "Section"]{
    The aspect used for completed actions.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{intnif}
    ◊in["ja"]{完結相}
  }
  ◊entry["Direct aspect" "verbs.html" "categories" "Section"]{
    In the conjugation of verbs, the morphologically unmarked aspect. The direct aspect receives shorter subject suffixes, and the verb is not eclipsed. It is imperfective for the present tense and perfective for the past tense.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{fremnif}
    ◊; ◊in["ja"]{完結相}
  }
  ◊entry["Inverse aspect" "verbs.html" "categories" "Section"]{
    In the conjugation of verbs, the morphologically marked aspect. The inverse aspect receives longer subject suffixes, and the verb is eclipsed. It is perfective for the present tense and imperfective for the past tense.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{flonnif}
    ◊; ◊in["ja"]{完結相}
  }
  ◊entry["Infinitive" "verbs.html" "inflection" "Section"]{
    The primary lemma form of the verb, which in Ŋarâþ Crîþ, ends in ◊l0{-at} or ◊l0{-it}.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{iva}
    ◊in["ja"]{不定詞}
  }
  ◊entry["Vowel affection" "verbs.html" "inflection" "Section"]{
    A process by which the last vowel of the stem in some verbs changes in the present forms, past forms, or both, possibly with different vowels between the tenses.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{vestomiyu}
    ◊; ◊in["ja"]{不定詞}
  }
  ◊entry["Participle" "verbs.html" "participle" "Section"]{
    A verb that is inflected to be used in a relative clause to modify a noun phrase.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊in["ja"]{分詞}
  }
  ◊entry["Common argument" "verbs.html" "participle" "Section"]{
    The argument shared between the relative clause and the matrix clause (the clause in which it is embedded).
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Relative case (rcase)" "verbs.html" "participle" "Section"]{
    The case of the common argument of a relative clause within the relative clause. In Ŋarâþ Crîþ, this can be the nominative, accusative, or dative, as well as the genitives of any of these separately.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Head case (hcase)" "verbs.html" "participle" "Section"]{
    The case of the common argument of a relative clause within the matrix clause. In Ŋarâþ Crîþ, this can be any of the eight cases.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Head gender (hgender)" "verbs.html" "participle" "Section"]{
    The gender of the common argument of a relative clause within the matrix clause.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Head number (hnumber)" "verbs.html" "participle" "Section"]{
    The number of the common argument of a relative clause within the matrix clause.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Genus" "verbs.html" "participle" "Section"]{
    A lexical property of a verb that includes a family of species whose participle forms are conjugated for in a similar way.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Species" "verbs.html" "participle" "Section"]{
    A lexical property of a verb that governs how a verb will be conjugated for its participle forms.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Type I genus" "verbs.html" "participle" "Section"]{
    A genus in which the participle forms are distinguished for hgender but not for hnumber.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Type II genus" "verbs.html" "participle" "Section"]{
    A genus in which the participle forms are distinguished for hnumber but not for hgender.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊; ◊in["ja"]{分詞}
  }
  ◊entry["Impersonator stem" "verbs.html" "participle" "Section"]{
    In some verbs, the participle forms use stems that are different from the possibly vowel-affected infinitive stem. A verb can have separate nominative-rcase and non-nominative-rcase impersonators.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuoayua}
    ◊in["ja"]{真似幹}
  }
  ◊entry["Converb" "verbs.html" "converb" "Section"]{
    A verb used in a converbal clause, which is used adverbially. Its use can often be translated to a verbal coordination.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yuofreyu}
    ◊in["ja"]{動副詞}
  }
  ◊entry[◊@{◊i{So}-clause} "verbs.html" "so-clauses" "Section"]{
    A clause containing an independent verb phrase followed by a ◊term{◊i{so}-particle}.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuofreyu}
    ◊; ◊in["ja"]{動副詞}
  }
  ◊entry[◊@{◊i{So}-particle} "verbs.html" "so-clauses" "Section"]{
    A conjunction such as ◊l1{so} ◊trans{if}, ◊l1{fose} ◊trans{because}, or ◊l1{dôm} ◊trans{to the extent of}, which is placed at the end of a ◊term{◊i{so}-clause}.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuofreyu}
    ◊; ◊in["ja"]{動副詞}
  }
  ◊entry["Nominalized verb phrase" "verbs.html" "nominalized" "Section"]{
    A verb phrase used as a noun. This is formed by using the infinitive form of the verb, preceded by a nominalizing particle.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yuoasa}
    ◊; ◊in["ja"]{動副詞}
  }
  ◊entry["Irregular verb" "verbs.html" "irregular" "Section"]{
    A verb that is not conjugated using the regular rules of conjugation.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊; ◊in["ja"]{動副詞}
  }
  ◊entry["APN-irregular verb" "verbs.html" "irregular-apn" "Subsection"]{
    An irregular verb whose finite forms are specified according to aspect, person, and number.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊; ◊in["ja"]{動副詞}
  }
  ◊entry["Auxiliary verb" "verbs.html" "aux-tour" "Subsection"]{
    A verb whose meaning fuses with that of another verb (the ◊term{target}) and cannot stand without it. It is a type of predicate modifier.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{freyuyuo}
    ◊in["ja"]{助動詞}
  }
  ◊entry["Target" "verbs.html" "aux-tour" "Subsection"]{
    The verb modified by an auxiliary verb.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{lana}
    ◊; ◊in["ja"]{助動詞}
  }
  ◊entry["Shifted subject" "verbs.html" "aux-tour" "Subsection"]{
    An argument selected to have a special role in the meaning of an auxiliary verb, such as carrying the volition for the performance or nonperformance of the target action. This role can be assigned to the nominative, accusative, or dative argument of the target.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊; ◊in["ja"]{動副詞}
  }
  ◊entry["Positive definite auxiliary verb" "verbs.html" "aux-tour" "Subsection"]{
    An auxiliary verb whose truth of the action or state it describes implies that the action or state described by the target is true.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊in["ja"]{正定助動詞}
  }
  ◊entry["Negative definite auxiliary verb" "verbs.html" "aux-tour" "Subsection"]{
    An auxiliary verb whose truth of the action or state it describes implies that the action or state described by the target is false.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊in["ja"]{負定助動詞}
  }
  ◊entry["Indefinite auxiliary verb" "verbs.html" "aux-tour" "Subsection"]{
    An auxiliary verb whose truth of the action or state it describes makes no implication about the truth value of the action or state described by the target.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊in["ja"]{不定助動詞}
  }
  ◊entry["Modal auxiliary verb" "verbs.html" "aux-tour-modal" "Subsection"]{
    An auxiliary verb that shows modality.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Degree auxiliary verb" "verbs.html" "aux-tour-degree" "Subsection"]{
    An auxiliary verb that shows the extent to which the target action or state holds.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Aspectual auxiliary verb" "verbs.html" "aux-tour-aspect" "Subsection"]{
    An auxiliary verb that indicates aspect.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{yuo alzettel}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Relational" "relationals.html" #f "Chapter"]{
    A type of predicate that encodes the relationship between two (or less often three) entities and resembles postpositions.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{soi}
    ◊; ◊in["ja"]{主語}
  }
  ◊entry["Voice" "verbs.html" "voice" "Subsection"]{
    A property of the inflected verb that modifies how the arguments of a verb map to the participants of the corresponding action. In Ŋarâþ Crîþ, voice is considered a predicate modifier.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{xalt}
    ◊in["ja"]{態}
  }
  ◊entry["Causative voice" "verbs.html" "causative-voice" "Subsubsection"]{
    The voice that adds a cause argument to a verb, demoting the other participants.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{sols}
    ◊in["ja"]{使役}
  }
  ◊entry["Applicative voice" "verbs.html" "applicative-voice" "Subsubsection"]{
    The voice that promotes an oblique adjunct to the dative case, demoting the existing dative argument.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{sols}
    ◊in["ja"]{適用態}
  }
  ◊entry["Subject" "relationals.html" #f "Chapter"]{
    The nominative argument of a verb or relational.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{soi}
    ◊in["ja"]{主語}
  }
  ◊entry["Object" "relationals.html" #f "Chapter"]{
    A non-nominative argument of a verb or relational.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{omit}
    ◊in["ja"]{目的語}
  }
  ◊entry["Main object" "relationals.html" #f "Chapter"]{
    In trivalent relationals, the more salient of the two objects.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{lestomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Ancilliary object" "relationals.html" #f "Chapter"]{
    In trivalent relationals, the less salient of the two objects.
    ◊; ◊in-ncs{neris}
    ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Relational of motion" "relationals.html" #f "Chapter"]{
    A relational derived from a spatial relational that indicates motion of one entity to or from another entity.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Attachment" "relationals.html" "attachment" "Section"]{
    Whether an attributive predicate is used adnominally or adverbially.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Bias" "relationals.html" "attachment" "Section"]{
    A lexical property of a relational that determines whether adnominal or adverbial usage is unmarked.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Cast affix" "relationals.html" "modforms" "Section"]{
    A marking used to use a relational in the attachment opposite of its bias. It is a suffix for celestial relationals and a prefix for terrestrial relationals.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Scaffolding verb" "relationals.html" "finforms" "Section"]{
    A form of ◊l1{eþit} or ◊l1{telit} on which a relational can be attached to use it predicatively.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Spatial relational" "relationals.html" "reltour-spatial" "Subsection"]{
    A relational that encodes a spatial relationship between one entity and another. These relationals have two additional relationals (◊term{relationals}) derived from them.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Temporal relational" "relationals.html" "reltour-temporal" "Subsection"]{
    A relational that encodes a temporal relationship between one entity and another.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Syntactic relational" "relationals.html" "reltour-syntactic" "Subsection"]{
    A relational that is defined to be used to support a certain syntactic construction.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Mathematical relational" "relationals.html" "reltour-mathematical" "Subsection"]{
    A relational that encodes a mtahematical relationship between two entities.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Long numeral" "numerals.html" "numerals-long" "Section"]{
    A class of numerals dating back to VE¹ENCS. Absent from NCS5 and NCS6 but present in ŊCv7 and v9. They are longer than the short numerals and cannot be abbreviated using digits. In Ŋarâþ Crîþ v9, they agree for case and sometimes gender, and they are limited to 16.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Long numeral" "numerals.html" "numerals-short" "Section"]{
    A class of numerals introduced in VE⁴ENCS. They are shorter than the long numerals and can be abbreviated using digits. In Ŋarâþ Crîþ v9, they show no agreement but usually require a classifier.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Classifier" "numerals.html" "numerals-short" "Section"]{
    A suffix attached to a short numeral to indicate what kind of objects are being counted.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Rational numeral" "numerals.html" "numerals-rational" "Subsection"]{
    A numeral that denotes a rational number.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Inexact numeral" "numerals.html" "numerals-inexact" "Subsection"]{
    A numeral that denotes a number that is only approximately known. Inexact numerals in Ŋarâþ Crîþ are represented in scientific notation.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Numeric prefix" "numerals.html" "numeric-prefixes" "Section"]{
    One of a set of derivational prefixes for forming a term for something related to an aggregate of definite size of another object. They are based on the prime factorization of the number.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Compounding" "lexicon.html" "compounding" "Section"]{
    A word-formation process in which a noun or a verb is combined with a noun to form a noun whose meaning is related to both of its constituents.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Derivation" "lexicon.html" "derivation" "Section"]{
    A word-formation process in which a noun or a verb is systematically modified to create a new related word.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
  ◊entry["Calculus affix" "lexicon.html" "calculus" "Section"]{
    An affix that given a quantity, derives the derivative or integral of that quantity with respect to a certain variable. In Ŋarâþ Crîþ, they can manifest as suffixes or infixes depending on the nature of the derivation.
    ◊; ◊in-ncs{neris}
    ◊; ◊in["art-x-v3-arx0as"]{roxkomit}
    ◊; ◊in["ja"]{不定助動詞}
  }
}
