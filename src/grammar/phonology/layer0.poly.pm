#lang pollen

◊define-meta[title]{Layer 0: the assemblage structure}

The phonotactics of Ŋarâþ Crîþ can be expressed in terms of a state machine with five states: ◊${s} (◊term{syllabic}), ◊${g} (◊term{glide}), ◊${o} (◊term{onset}), ◊${n} (◊term{nuclear}), and ◊${\omega} (◊term{terminal}). Each transition defined in the state machine has a set of accepted payloads.

◊make-figure["images/elaine-fsm.svg" #:alt "The finite state machine describing the phonotactics of Ŋarâþ Crîþ."]{The finite state machine describing the phonotactics of Ŋarâþ Crîþ.}

A word, or rather the phonotactically relevant part thereof, starts in the syllabic state and ends in the terminal state.

◊items{
    ◊item{The ◊term{syllabic state}, ◊${s}, is reached between syllables. In this state, an initial can be accepted to transition to the glide state.}
    ◊item{The ◊term{glide state}, ◊${g}, is reached immediately after an initial. This state can accept a medial to transition to the onset state.}
    ◊item{From the ◊term{onset state}, ◊${o}, a vowel (also called a nucleus) leads to the nuclear state.}
    ◊item{From the ◊term{nuclear state}, ◊${n}, a simple coda can be accepted to transition back to the syllabic state. Alternatively, a simple or complex coda may be accepted to transition to the terminal state.}
    ◊item{The ◊term{terminal state} is the end state for a word and marks the end of the final syllable. There are no transitions from this state.}
}

The payloads associated with a transition are strings of ◊term{manifested grapheme phrases}. A manifested grapheme phrase is either a true letter not followed by a lenition marker (◊term{plain letter}), any of ◊l0{p t d č c g m f v ð} followed by a lenition mark (◊term{lenited letter}), or, word-initially, one of the digraphs ◊l0{mp vp dt nd gc ŋg vf ðþ lł} (◊term{eclipsed letter}). All other graphemes are ignored for the purposes of phonotactics.

A manifested grapheme phrase has a ◊term{base letter}. The base letter of a plain letter is itself. The base letter of a lenited letter is the letter without the lenition mark. The base letter of an eclipsed letter is the second letter of the digraph.

A vowel is any of ◊l0{e o a î i ê ô â u}. ◊l0{j} is a semivowel. All other manifested grapheme phrases are consonants.

An ◊term{effective plosive} is a manifested grapheme phrase whose base letter is any of ◊l0{p t d c g}. An ◊term{effective fricative} is a manifested grapheme phrase whose base letter is any of ◊l0{f v þ ð s š h ħ}.

A ◊term{hatted vowel} is one of ◊l0{î ê ô â}. All other vowels are ◊term{unhatted vowels}.

An ◊term{initial} is the beginning of a syllable and consists of one of the following:

◊items{
  ◊item{nothing at all}
  ◊item{a single consonant}
  ◊item{an effective plosive or fricative plus ◊l0{r} or ◊l0{l}}
  ◊item{any of ◊l0{cf cþ cs cš gv gð tf dv}; that is, a plosive plus a fricative of the same voicing, such that the plosive has a more retracted place of articulation than the fricative}
}

The set of valid initials is denoted by the capital Greek letter iota, ◊${\Iota}.

A ◊term{medial} may either be empty or ◊l0{j}. The set of medials is denoted by the capital Greek letter mu, ◊${\Mu}.

The set of vowels is denoted by the capital Greek letter nu, ◊${\Nu}.

A coda is either a ◊term{simple coda} or a ◊term{complex coda}. A simple coda is one of ◊l0{s r n þ rþ l t c f cþ ł} or nothing at all. A complex coda is one of ◊l0{st lt ns ls nþ łt m}, with ◊l0{-m} used only in a handful of function words. The set of all simple codas is denoted by the capital Greek letter kappa, ◊${\Kappa}, and the set of all simple or complex codas is denoted by the capital Greek letter omega, ◊${\Omega}.

An ◊term{onset} is an initial plus a medial. A ◊term{bridge} is the coda of one syllable plus the onset of the following syllable.

For disambiguation, this grammar uses the following symbols in place of the hyphen to show the state at the juncture: ◊l0{÷} on ◊var{s}, ◊l0{←} on ◊var{g}, ◊l0{→} on ◊var{o}, and ◊l0{¬} on ◊var{n}. The colon is also sometimes used to clarify syllable boundaries.

◊section[#:id "layer0-pv"]{Validation}

Valid morphemes have addditional criteria that they must satisfy:

◊items{
    ◊item{All bridges must be valid.}
    ◊item{◊l0{j} cannot precede ◊l0{i}, ◊l0{î}, or ◊l0{u}.}
    ◊item{◊l0{h} cannot occur word-initially.}
    ◊item{Conversely, eclipsed letters may only occur word-initially.}
}

A bridge is ◊term{canonical} if it follows the maximal-onset principle; that is, if the onset has the maximal number of consonants for the given sequence of manifested grapheme phrases. For instance, ◊l0{¬n÷t→} and ◊l0{¬r÷þl→} are canonical, but ◊l0{¬c÷þ→} and ◊l0{¬rþ÷l→} are not (as they can be regrouped as ◊l0{¬∅÷cþ→} and to ◊l0{¬r÷þl→}).

A bridge is ◊term{valid} if it can arise as the result of ◊term{repairing} a canonical bridge. Bridge repair is intended to change a bridge that is awkward to pronounce into one that is less so. It has the following properties:

◊enum{
  ◊item{Except when the coda is ◊l0{¬ł}, lenition in the onset does not affect whether bridge repair preserves the bridge.}
  ◊item{The presence of ◊l0{j} in the onset has no influence on bridge repair.}
  ◊item{All bridges with a coda that is empty, ◊l0{¬r}, or ◊l0{¬l} are unaffected by bridge repair.}
  ◊item{If a bridge with a complex initial ◊var{I} is not changed by bridge repair, then the bridge with an initial containing only the first consonant of ◊var{I} is also unchanged.}
}

Importantly, bridge repair is not idempotent: ◊l0{¬sð·→} is repaired to ◊l0{¬ss→}, but ◊l0{¬ss→} is repaired to ◊l0{¬þ→}. In addition, bridge repair might yield the pseudo-coda ◊l0{¬ŋ}, which changes the preceding medial and vowel.

The following sections describe the rules for bridge repair. A bridge that is modified by one rule might be further changed by later rules.

◊subsection[#:id "bridge-rules-coalesce"]{Coalescence of ◊l0{¬tš←}}

The bridge ◊l0{¬tš←} is changed to ◊l0{¬č←}.

◊subsection[#:id "bridge-rules-fortition"]{Fortition of ◊l0{h←} and ◊l0{ħ←}}

The onset ◊l0{h←} is fortited to ◊l0{c←} after ◊l0{¬s}, ◊l0{¬þ}, ◊l0{¬rþ}, ◊l0{¬t}, ◊l0{¬c}, ◊l0{¬f}, ◊l0{¬ł}, or ◊l0{¬cþ}. ◊l0{hr←} and ◊l0{hl←} are fortited analogously.

The onset ◊l0{ħ←} is fortited to ◊l0{g←} after ◊l0{¬t}, ◊l0{¬c}, ◊l0{¬f} or ◊l0{¬ł}. ◊l0{ħr←} and ◊l0{ħl←} are fortited analogously.

◊subsection[#:id "bridge-rules-metathesis"]{Metathesis of ◊l0{t} before ◊l0{c} or ◊l0{g}}

◊l0{¬tc←} and ◊l0{¬tg←} are metathesized to ◊l0{¬ct←} and ◊l0{¬cd←}, respectively. Likewise, ◊l0{¬tcr←}, ◊l0{¬tcl←}, ◊l0{¬tgr←}, and ◊l0{¬tgl←} are metathesized to ◊l0{¬ctr←}, ◊l0{¬ctl←}, ◊l0{¬cdr←}, and ◊l0{¬cdl←}.

Similar bridges with lenited onsets, such as ◊l0{¬tc·←} and ◊l0{¬tg·r←} are treated analogously, with the resulting onset remaining lenited.

◊l0{¬t} is deleted before ◊l0{cf←}, ◊l0{cþ←}, ◊l0{cs←}, ◊l0{cš←}, ◊l0{gv←}, and ◊l0{gð←}, devoicing the last two of these.

◊subsection[#:id "bridge-rules-assimilation-n"]{Nasal assimilation}

For these rules, ◊l0{m·←} is counted as a nasal, even though it is pronounced as a fricative.

◊l0{¬t} before a nasal onset assimilates to ◊l0{¬n}.

◊l0{¬c} before a nasal onset assimilates to the ◊xref["layer0.html" "bridge-rules-ng" "Subsection"]{pseudo-coda ◊l0{¬ŋ}}. As a special case, ◊l0{¬cŋ←} is repaired to ◊l0{¬:ŋ←} instead.

◊subsection[#:id "bridge-rules-denasalization"]{Denasalization of ◊l0{ŋ←}}

After ◊l0{¬s}, ◊l0{¬þ}, ◊l0{¬rþ}, ◊l0{¬f}, ◊l0{¬ł}, or ◊l0{¬cþ}, ◊l0{ŋ←} is denasalized to ◊l0{g←}.

◊subsection[#:id "bridge-rules-lh-fort"]{Fortition of onsets after ◊l0{¬ł}}

At the beginning of onsets following the coda ◊l0{¬ł}, the consonants ◊l0{þ}, ◊l0{t·}, and ◊l0{s} are replaced by ◊l0{t}, and ◊l0{ð} and ◊l0{d·} are replaced by ◊l0{d}.

◊subsection[#:id "bridge-rules-devoicing"]{Devoicing of ◊l0{v←} and ◊l0{ð←}}

After ◊l0{¬þ}, ◊l0{¬rþ}, ◊l0{¬t}, ◊l0{¬c}, ◊l0{¬f}, and ◊l0{¬cþ}, ◊l0{v←} devoices to ◊l0{f←} and ◊l0{ð←} devoices to ◊l0{þ←}. Additionally, ◊l0{ð←} is devoiced after ◊l0{¬s}.

This process occurs analogously for the onsets ◊l0{vr←}, ◊l0{vl←}, ◊l0{ðr←}, and ◊l0{ðl←}, except that ◊l0{¬þ} is deleted before ◊l0{vr←} and ◊l0{vl←} instead (leaving the onset voiced). Additionally, ◊l0{¬rþCR←} onsets (with R = ◊l0{r} or ◊l0{l} and C = ◊l0{v} or ◊l0{ð}) are corrected to ◊l0{¬RC←}.

As usual, similar rules apply to lenited onsets: ◊l0{v·} devoices to ◊l0{f·}, and ◊l0{ð·} is replaced with a copy of the preceding consonant, except in ◊l0{¬łð·r←} and ◊l0{¬łð·l←}, where the ◊l0{ð·} is deleted.

◊subsection[#:id "bridge-rules-assimilation-ths"]{Assimilation of ◊l0{s} after ◊l0{þ}}

After a ◊l0{þ}, ◊l0{s} is replaced with ◊l0{þ}. Additionally, ◊l0{ss} is coalesced into ◊l0{þ}, unless it is not followed by a consonant and the latter ◊l0{s} arose from a ◊l0{ð} in the previous step.

◊subsection[#:id "bridge-rules-lh-to-l"]{Replacement of ◊l0{¬ł} by ◊l0{¬l} in certain onsets}

Before stop–fricative onsets, as well as before any onset that does not start with ◊l0{t}, ◊l0{d}, ◊l0{l}, ◊l0{r}, ◊l0{n}, ◊l0{c}, or a lenited version of one of these consonants, the coda ◊l0{¬ł} becomes ◊l0{¬l}.

◊subsection[#:id "bridge-rules-degemination"]{Degemination before another consonant}

◊l0{þ}, ◊l0{t}, ◊l0{c}, and ◊l0{f} are degeminated before another consonant in the onset; for instance, ◊l0{¬ffr←} is corrected to ◊l0{¬fr←}, and ◊l0{¬ccs←} is corrected to ◊l0{¬cs←}. ◊l0{td} is degeminated to ◊l0{d}, and ◊l0{cg} is degeminated to ◊l0{g}.

This rule also applies when the second instance of the degeminated consonant is lenited, in which case the first copy of the consonant is elided: ◊l0{¬tt·l←} → ◊l0{¬t·l←}.

◊subsection[#:id "bridge-rules-shorten4"]{Partial coda elision of bridges with ◊l0{¬rþ} and ◊l0{¬cþ} codas}

If the coda is ◊l0{¬rþ}, then it becomes ◊l0{¬r} before a fricative followed by ◊l0{r} or ◊l0{l}, or before the onsets ◊l0{cf←}, ◊l0{cþ←}, ◊l0{cs←}, ◊l0{cš←}, ◊l0{tf←}, and ◊l0{dv←}. Before any other two-letter onset, it becomes ◊l0{¬þ}.

If the coda is ◊l0{¬cþ}, then it is maintained before the onsets ◊l0{þ←}, ◊l0{š←}, ◊l0{m←}, ◊l0{t←}, ◊l0{ħ←}, ◊l0{m·←}, or ◊l0{t·←}. Before ◊l0{cf←}, ◊l0{cþ←}, ◊l0{cs←}, ◊l0{cš←}, or ◊l0{tf←}, or before any of the onsets consisting of ◊l0{þ}, ◊l0{š}, or ◊l0{ħ} followed by ◊l0{r} or ◊l0{l}, the onset loses its first consonant, and ◊l0{cs←} additionally becomes ◊l0{þ←}. In all other cases, the coda becomes ◊l0{¬þ}.

◊subsection[#:id "bridge-rules-ng"]{The pseudo-coda ◊l0{¬ŋ}}

Nasal assimilation might produce the pseudo-coda ◊l0{¬ŋ} instead of an actual (simple) coda. In this case, the preceding vowel becomes ◊l0{o} for ◊l0{a o u}, ◊l0{jo} for ◊l0{e i}, ◊l0{ô} for ◊l0{â ô}, and ◊l0{jô} for ◊l0{ê î}, with any glides merging with the preceding glide. The pseudo-coda itself becomes ◊l0{¬r}.

This operation on a glide–vowel pair is common in Ŋarâþ Crîþ and is referred to as the ◊term{◊i{​ξ}-transformation}.

◊section[#:id "concat"]{Concatenation}

Concatenating two morphemes invokes repair processes to maintain validity invariants. In addition, there are environments that may naturally (if rarely) occur within a morpheme but are repaired away when created by appending morphemes.

◊term{Deduplication}, which occurs on concatenation, affects fricatives in the onset position that precede a non-hatted vowel followed by a homophonous manifested grapheme phrase:

◊enum{
    ◊item{The onset ◊l0{f} or ◊l0{tf} followed by a non-hatted vowel then ◊l0{f} or ◊l0{p·} is replaced with ◊l0{t}.}
    ◊item{The onset ◊l0{þ} or ◊l0{cþ} followed by a non-hatted vowel then ◊l0{þ} or ◊l0{t·} is replaced with ◊l0{t}. In addition, a preceding ◊l0{þ} or ◊l0{cþ} coda is replaced with ◊l0{s}, and a preceding ◊l0{rþ} coda is replaced with ◊l0{r}.}
    ◊item{◊l0{h} followed by a non-hatted vowel then ◊l0{h} or ◊l0{c·} is replaced with ◊l0{p}.}
    ◊item{◊l0{v} followed by a non-hatted vowel then ◊l0{v} or ◊l0{m·} is replaced with ◊l0{n}.}
    ◊item{◊l0{ð} followed by a non-hatted vowel then ◊l0{ð} or ◊l0{d·} is replaced with ◊l0{ŋ}.}
    ◊item{◊l0{ħ} followed by a non-hatted vowel then ◊l0{ħ} or ◊l0{g·} is replaced with ◊l0{g}.}
}

These environments of duplicate consonants are called ◊term{oginiþe cfarðerþ}.

Overall, concatenation invokes the following processes in order:

◊enum{
    ◊item{Any new instances of ◊l0{j} before ◊l0{i}, ◊l0{î}, or ◊l0{u} are elided.}
    ◊item{Deduplication rules are applied.}
    ◊item{Newly formed bridges are canonicalized and repaired.}
}

Note that deduplication happens before any canonicalization; for instance, appending the syllables ◊l0{reþ÷} and ◊l0{÷eþ} together gives ◊l0{reþeþ}, not ◊l0{reteþ} (although appending the stem ◊l0{reþ→} to the suffix ◊l0{←eþ} ◊em{does} give ◊l0{reteþ}).

◊section[#:id "stem-fusion"]{Stem fusion}

In Ŋarâþ Crîþ, a ◊term{stem} consists of one or more syllables followed by an onset. In addition, the final onset of a stem must not contain a lenited consonant.

◊term{Stem fusion} describes a set of related processes on a stem. Stem fusion with a null consonant turns a stem into a word (with a terminal end). Stem fusion with a non-null consonant combines a stem with one of ◊l0{t}, ◊l0{n}, or ◊l0{þ} into another stem.

For some stems, stem fusion is ◊term{C-invariant}; that is, it yields a common sequence of syllables followed by whatever the fusion consonant is.

◊details[#:open? #t]{
  ◊summary{Notation used to describe stem fusion}
  ◊items{
    ◊item{◊${\mathcal{S}_{xy}} is the set of all morphemes with start type ◊${x} and end type ◊${y}, with ◊${x} and ◊${y} being one of ◊${s} (syllabic), ◊${g} (glide), ◊${o} (onset), ◊${n} (nuclear), or ◊${\omega} (terminal).◊;
      ◊items{
        ◊item{◊${\mathcal{S}_{xy}^{n}} is the subset of ◊${\mathcal{S}_{xy}} whose elements undergo ◊${n} cycles from ◊${x} back to ◊${x}.◊;
          ◊items{
            ◊item{Ex: ◊${\mathcal{S}_{xx}^{0}} contains only the empty string for all boundary types ◊${x}.}
            ◊item{Ex: ◊${\mathcal{S}_{sg}^{0}} is the set of all initials.}
            ◊item{Ex: ◊${\mathcal{S}_{no}^{1}} includes ◊l0{stafc} but not ◊l0{þþj} or ◊l0{tatag}.}
          }
        }
      }
    }
    ◊item{Given ◊${\alpha \in \mathcal{S}_{xy}} and ◊${\beta \in \mathcal{S}_{yz}}, ◊${\alpha\cat\beta \in \mathcal{S}_{xz}} is the result of appending ◊${\alpha} and ◊${\beta}, performing repair processes as necessary.◊;
      ◊items{
        ◊item{Ex: if ◊${\alpha = \cv{feva} \in \mathcal{S}_{ss}} and ◊${\beta = \cv{ve} \in \mathcal{S}_{ss}}, then ◊${\alpha\cat\beta = \cv{fenave} \in \mathcal{S}_{ss}}.}
        ◊item{This operation is also defined for ◊${\alpha \in \mathcal{S}_{xo}} and ◊${\beta \in \mathcal{S}_{gz}}, in which case the glides at the end of ◊${\alpha} and the start of ◊${\beta} are merged.}
        ◊item{The exact semantics of this operation depends on the types of ◊${\alpha} and ◊${\beta}, not only their values.}
        ◊item{◊${\alpha\beta} is the result of appending ◊${\alpha} and ◊${\beta} ◊em{without} performing any repair processes.}
      }
    }
    ◊item{We also define the following sets:◊;
      ◊items{
        ◊item{◊${\Iota = \mathcal{S}_{sg}^{0}} is the set of all initials.}
        ◊item{◊${\Mu = \mathcal{S}_{go}^{0} = \{\epsilon_{\Mu}, \cv{j}\}} is the set of all glides.}
        ◊item{◊${\Nu = \mathcal{S}_{on}^{0}} is the set of all vowels.}
        ◊item{◊${\Kappa = \mathcal{S}_{ns}^{0}} is the set of all simple codas.}
        ◊item{◊${\Omega = \mathcal{S}_{n\omega}^{0}} is the set of all codas, simple or complex.}
        ◊item{◊${\Gamma = \mathcal{S}_{ng}^{0}} is the set of all coda–onset pairs. The glide is not included because stems ending in ◊l0{j} are treated specially in stem fusion.}
        ◊item{◊${\Pi} is the set of effective plosives and fricatives – that is, the set of consonants that can form an initial when followed by ◊l0{l} or ◊l0{r}.}
        ◊item{◊${\Tau \subset \mathcal{S}_{so} \setminus \mathcal{S}_{so}^0} is the set of valid stems. A stem must contain at least one syllable, and its final onset must not contain a lenited consonant.}
      }
    }
    ◊item{A superscript expression containing a slash is interpreted as a substitution of each element on the left side with the corresponding element on the right side. Square brackets denote strings of more than one character.◊;
      ◊items{◊item{For example, ◊${x^{\cv{snt}/\cv{ð[nd]c}}} means ◊i{‘◊${x}, but with ◊l0{s}, ◊l0{n}, and ◊l0{t} replaced with ◊l0{ð}, ◊l0{nd}, and ◊l0{c}, respectively’} (with any other value of ◊${x} unchanged).}}
    }
    ◊item{Given a stem ◊${\tau \in \Tau}, ◊${\tau^{\epsilon} \in \mathcal{S}_{s\omega}} is the result of fusing ◊${\tau} with a null consonant, and ◊${\tau^{\theta} \in \mathcal{S}_{so}} is the result of fusing ◊${\tau} with a non-null consonant ◊${\theta \in \{\cv{n}, \cv{t}, \cv{þ}\}}. ◊;
      ◊items{
        ◊item{We use a shorthand for C-invariant rules: a rule such as ◊${\tau \leadsto \sigma}, where ◊${\tau \in \Tau} and ◊${\sigma \in \mathcal{S}_{ss}}, is interpreted as the rules ◊${\tau^\epsilon = \sigma_{s\omega}} and ◊${\tau^\theta = \sigma \cat \theta}.}
        ◊item{Another shorthand used in this document is ◊${\tau \curvearrowright \tau'}, which implies ◊${\tau^\epsilon = (\tau')^\epsilon} and ◊${\tau^\theta = (\tau')^\theta}.}
      }
    }
    ◊item{The following variables are used: ◊${\Sigma_{xy} \in \mathcal{S}_{xy}}, ◊${\gamma \in \Gamma}, ◊${\iota \in \Iota}, ◊${\nu \in \Nu}, ◊${\kappa \in \Kappa}, ◊${\omega \in \Omega}, ◊${\theta \in \{\cv{n}, \cv{t}, \cv{þ}\}}.}
    ◊item{Each set of rules is presented first in mathematical notation then paraphrased (roughly) in plain English.◊;
      ◊items{◊item{Earlier rules take precedence over later ones.}}
    }
  }
}

◊subsection[#:id "fusion-j"]{Stems ending in ◊l0{j}}

◊form+expl{
  ◊align-math{
    \Sigma_{si} \cv{j} &\leadsto \Sigma_{si} \cat \cv{i} \tag{FinalJ}
  }
  ◊expl
  Fusion with any stem that ends with ◊l0{j} is C-invariant, with the ◊l0{j} replaced with ◊l0{i}.
}

From now on, any explicit instances of ◊${\epsilon_{\Mu}} will be omitted.

◊subsection[#:id "fusion-onset-alias"]{Onset aliasing}

◊form+expl{
  ◊align-math{
    (\Sigma_{ss} \iota)^\epsilon &= (\Sigma_{ss} \cat \cv{s})^\epsilon \os{\(\iota \in \{\cv{t}, \cv{d}\}\), \(\kappa \ne \cv{ł}\)} \tag{Alias}
  }
  ◊expl
  When fusing a stem that has an onset of ◊l0{t} or ◊l0{d} with an empty consonant, pretend that the onset is ◊l0{s} instead. This does not apply when the coda is ◊l0{¬ł}, as ◊l0{¬łs→} resolves to ◊l0{¬łt→}.
}

◊subsection[#:id "fusion-coda"]{Valid codas}

◊form+expl{
  ◊align-math{
    (\Sigma_{sn} \gamma)^\epsilon &= \Sigma_{sn} \cat \gamma_\Omega \os{\(\gamma \in \Omega\)} \notag \\
    (\Sigma_{sn} \gamma)^\theta &= \Sigma_{sn} \cat \gamma_\Kappa \cat \theta \os{\(\gamma \in \Kappa\)} \tag{ValidCoda}
  }
  ◊expl
  If the final bridge of a stem can be interpreted as a valid simple coda (if the fusion consonant is not null) or as a valid coda (if the fusion consonant is null), then reinterpret it as one and append the fusion consonant.
}

◊subsection[#:id "fusion-degemination"]{Degemination}

◊form+expl{
  ◊align-math{
    \Sigma_{sn} \kappa \iota &\leadsto \Sigma_{sn} \cat \delta(\kappa) \os{\(\kappa = \iota\) and \(|\kappa| = 1\)} \tag{Degeminate}
  }

  where ◊${|\kappa|} is the number of manifested grapheme phrases in ◊${\kappa} and

  ◊$${
    \delta(\kappa) = \begin{cases}
      \cv{l} \os{\(\kappa = \cv{r}\)} \\
      \cv{þ} \os{\(\kappa = \cv{s}\)} \\
      \kappa   \osamgen{}
    \end{cases}
  }
  ◊expl
  If the coda and initial of the final bridge are both one manifested grapheme phrase long and equal to each other, then fusion is C-invariant and the repeated letter is removed. As a special case, ◊l0{¬rr→} becomes ◊l0{¬l←} and ◊l0{¬ss→} becomes ◊l0{¬þ→}.
}

◊subsection[#:id "fusion-epenthesis"]{Vowel epenthesis}

◊form+expl{
  ◊align-math{
    (\Sigma_{sn} \kappa \cv{c})^\epsilon &= \Sigma_{sn} \epsilon_\Kappa \kappa_\iota \cat \cv{ecþ} \os{\(\kappa \in \{\cv{r}, \cv{l}, \cv{ł}\}\)} \notag \\
    \Sigma_{sn} \kappa \iota &\leadsto \Sigma_{sn} \epsilon_\Kappa \kappa_\iota \cat \cv{e}_\Nu \cat \iota_\Kappa \os{\(\iota \in \Iota \cap \Kappa\), \(\kappa \in \{\cv{r}, \cv{l}, \cv{ł}\}\)} \tag{Epenthesis-LC}
  }
  ◊expl
  If the coda of the final bridge is ◊l0{r}, ◊l0{l}, or ◊l0{ł} and its initial can be interpreted as a valid coda, then fusion is C-invariant with a ◊l0{¬:e÷} inserted between the coda and the initial.

  As an exception, when fusing with a null consonant with the final initial being ◊l0{c←}, the final coda of the result is ◊l0{¬cþ} rather than ◊l0{¬c}.
}

◊subsection[#:id "fusion-nasal"]{Nasal merging}

◊form+expl{
  ◊align-math{
    \Sigma_{sn} \cv{n}_\Kappa \cv{d}_\Iota &\curvearrowright \Sigma_{sn} \cv{n}_\Iota \notag \\
    \Sigma_{sn} \cv{n}_\Kappa (\iota^1 \iota^2)_\Iota &\curvearrowright \Sigma_{sn} \cv{n} (\iota^2)_\Iota \os{\(\iota^1 = \cv{d}\)} \notag \\
    \Sigma_{sn} \cv{n}_\Kappa \iota &\curvearrowright \Sigma_{sn} \cv{ŋ}_\Iota \os{\(\iota \in \{\cv{c}, \cv{g}\}\)} \notag \\
    \Sigma_{sg} \mu \nu \cv{n}_\Kappa (\iota^1 \iota^2)_\Iota &\curvearrowright \Sigma_{sg} \xi(\mu \nu) \cv{r} (\iota^2)_\Iota \os{\(\iota^1 \in \{\cv{c}, \cv{g}\}\)} \tag{NasalMerge1}
  }

  ◊${\xi: \mathcal{S}_{gn}^0 \rightarrow \mathcal{S}_{gn}^0} is the ◊i{ξ}-transformation; i.e.

  ◊$${
    \xi(\mu \nu) = \begin{cases}
      \mu \cv{o} \os{\(\nu \in \{\cv{a}, \cv{o}, \cv{u}\}\)} \\
      \mu \cv{ô} \os{\(\nu \in \{\cv{â}, \cv{ô}\}\)} \\
      \cv{jo} \os{\(\nu \in \{\cv{e}, \cv{i}\}\)} \\
      \cv{jô} \os{\(\nu \in \{\cv{ê}, \cv{î}\}\)}
    \end{cases}
  }
  ◊expl
  ◊enum{
    ◊item{If the final bridge has a coda of ◊l0{¬n} and an onset that starts with ◊l0{d}, perform fusion with the first consonant of the onset removed.}
    ◊item{If the final bridge is ◊l0{¬nc→} or ◊l0{¬ng→}, then perform fusion with ◊l0{¬ŋ→} in its place.}
    ◊item{If the final bridge has a coda of ◊l0{¬n} and an onset that starts with ◊l0{c} or ◊l0{g}, perform fusion as if the bridge consisted of ◊l0{r} followed by the second consonant of the onset, with the previous medial and nucleus affected by the ◊i{​ξ}-transformation.}
  }
}

◊form+expl{
  ◊align-math{
    (\Sigma_{sn} \epsilon_\Kappa \cv{m})^\cv{n} &= \Sigma_{sn} \cv{nm} \notag \\
    (\Sigma_{ss} \cv{m})^\cv{n} &= \Sigma_{ss} \cat \cv{ôm} \notag \\
    (\Sigma_{ss} \cv{n})^\cv{n} &= \Sigma_{ss} \cat \cv{enn} \notag \\
    (\Sigma_{so} \nu \kappa \iota)^\cv{n} &= \Sigma_{so} \chi(\nu) \kappa \cat \iota^{\cv{dðv}/\cv{nnm}} \os{\(\iota \in \{\cv{d}, \cv{ð}, \cv{v}\}\)} \tag{NasalMerge2}
  }

  ◊${\chi: \Nu \rightarrow \Nu} inverts the tone of a vowel; i.e.

  ◊$${
    \chi(\nu) = \nu^{\cv{aeioâêîô}/\cv{âêîôaeio}}
  }
  ◊expl
  If the fusion consonant is ◊l0{n}, then:
  ◊enum{
    ◊item{If the final bridge has an empty coda and an onset of ◊l0{m→}, then it metathesizes with the fusion consonant.}
    ◊item{If the final bridge has an onset of ◊l0{m→} but a nonempty coda, then the result has the final initial replaced with ◊l0{÷ô:m→}.}
    ◊item{If the final bridge has an onset of ◊l0{n→}, then the result has the final initial replaced with ◊l0{÷en:n→}.}
    ◊item{If the final bridge has an onset of ◊l0{d→}, ◊l0{ð→}, or ◊l0{v→}, then the result is the stem, but with the final vowel inverted in tone. The final initial is also replaced with ◊l0{m→} if it was ◊l0{v→}, or ◊l0{n→} otherwise.}
  }
}

◊subsection[#:id "fusion-obstruent"]{Obstruent merging}

◊form+expl{
  ◊align-math{
    \Sigma_{sn} \cv{rþ} \iota &\curvearrowright \Sigma_{sn} \cv{r} \cat \iota \os{\(\varphi(\cv{þ}, \iota)\)} \notag \\
    \Sigma_{sn} \cv{cþ} \iota &\curvearrowright \Sigma_{sn} \cv{c} \cat \iota \os{\(\varphi(\cv{þ}, \iota)\)} \notag \\
    \Sigma_{sn} \kappa \iota &\curvearrowright \Sigma_{sn} \epsilon \cat \iota \os{\(\varphi(\kappa, \iota)\)} \tag{FricMerge1} \\
    (\Sigma_{ss} (\iota^1 \iota^2)_\Iota)^\theta &= (\Sigma_{ss} \cat \iota^1)^\theta \os{\(\varphi(\iota^2, \theta)\)} \notag \\
    (\Sigma_{ss} \iota)^\theta &= \Sigma_{ss} \cat \theta \os{\(\varphi(\iota, \theta)\)} \tag{FricMerge2}
  }

  where

  ◊align-math{
    \varphi(\kappa, \iota) \iff& (\kappa, \iota) \in F \lor [\exists \pi \in \Pi, \rho \in \{\cv{r}, \cv{l}\} : \iota = \pi\rho \land (\kappa, \pi) \in F] \notag \\
    F = \{ & (\cv{f}, \cv{þ}), (\cv{þ}, \cv{þ}), (\cv{t}, \cv{þ}), (\cv{d}, \cv{þ}) \notag \\
          & (\cv{f}, \cv{f}), (\cv{v}, \cv{f}), (\cv{þ}, \cv{f}), \notag \\
          & (\cv{t}, \cv{t}), (\cv{t}, \cv{n}) \} \notag
  }
  ◊expl
  These rules concern environments in which the first consonant of the consonant pairs ◊l0{fþ}, ◊l0{þþ}, ◊l0{tþ}, ◊l0{dþ}, ◊l0{ff}, ◊l0{vf}, ◊l0{þf}, ◊l0{tt}, and ◊l0{tn} is removed.

  ◊enum{
    ◊item{If in the final bridge of the stem, the onset is either a single consonant or a consonant–liquid onset, and the last consonant of the coda and the first consonant of the onset make up one of the pairs above, fusion occurs as if the last consonant of the coda were absent.}
    ◊item{If the fusion consonant is not null and the last consonant of the onset and the fusion consonant make up one of the pairs above, then fusion occurs as if the last consonant of the onset were absent.}
  }
}

◊subsection[#:id "fusion-final-devoice"]{Final devoicing}

◊form+expl{
  ◊align-math{
    (\Sigma_{ss} \iota)^\epsilon &= [\Sigma_{ss} h(\iota)]^\epsilon \os{\(\iota \in \{\cv{v}, \cv{m}, \cv{d}, \cv{ð}\}\)} \notag \\
    (\Sigma_{ss} \iota)^\theta &= [\Sigma_{ss} h(\iota)]^\theta \os{\(\iota \in \{\cv{v}, \cv{m}, \cv{d}, \cv{ð}\}\)} \os{\(\theta \in \{\cv{t}, \cv{þ}\}\)} \tag{FinalDevoice}
  }

  where

  ◊$${
    h(\iota) = \iota^{\cv{vmdð}/\cv{fflþ}}
  }

  ◊expl
  If the fusion consonant is not ◊l0{n} and the stem ends with an onset of ◊l0{v←}, ◊l0{m←}, ◊l0{d←}, or ◊l0{ð←}, then fusion occurs as if the final onset were ◊l0{f←}, ◊l0{f←}, ◊l0{l←}, or ◊l0{þ←} instead.
}

◊subsection[#:id "fusion-cl"]{Stems ending in consonant–liquid onsets}

◊form+expl{
  For any consonant ◊${c} and coda ◊${\omega \in \Omega}, define ◊${\triangleleft} as

  ◊$${
    (\Sigma_{xn} \omega) \triangleleft c =
      (\Sigma_{xn} \lfloor\omega\rfloor) \cat c_\Iota
  }

  where ◊${\lfloor\cdot\rfloor : \Omega \rightarrow \Kappa} denotes the operation of taking the maximal prefix of a coda that is a simple coda.

  ◊expl

  To ◊term{inject} a consonant into a sequence of syllables, the last of which might contain a complex coda, remove consonants from the end of the word until it ends with a simple coda, then append the consonant.
}

These rules concern stems ending in an onset consisting of a consonant ◊${\pi} followed by ◊l0{r} or ◊l0{l}.

◊form+expl{
  Let ◊${\rho \in \{\cv{r}, \cv{l}\}} and ◊${\pi \in \Pi}. Then ◊${\pi\rho \in \Iota}.

  ◊align-math{
    \Sigma_{sn} \epsilon_\Kappa \iota \nu \epsilon_\Kappa (\pi \rho) &\leadsto \Sigma_{sn} \iota_{\Kappa} \cat \pi_{\Iota} \cat \nu \cat \rho_{\Kappa} \os{\(\iota \in \{\epsilon, \cv{s}, \cv{n}, \cv{l}\}\)} \tag{Cl-Meta}
  }

  ◊expl

  If the stem has at least two full syllables, the last two full syllables both have empty codas, and the last full syllable has an onset that is empty, ◊l0{s}, ◊l0{n}, or ◊l0{l}, then fusion is C-invariant, with ◊${\pi} moved right after the onset of the last full syllable, which becomes the coda of the preceding syllable.
}

◊form+expl{
  ◊align-math{
    (\Sigma_{sn} \epsilon_{\Kappa} (\cv{t}_\Pi \rho))^\cv{t} &= \Sigma_{sn} \cat \cv{r}_\Kappa \cat \cv{t} \notag \\
    (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{t} &= (\Sigma_{sn} \epsilon_{\Kappa} \pi_\Iota)^\epsilon \triangleleft \cv{d} \tag{Cl-NoCoda-T} \\
    (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{n} &= \Sigma_{sn} (\iota^{\cv{dc}/\cv{rs}})_\Kappa \cat \cv{ŋ} \os{\(\pi \in \{\cv{d}, \cv{c}\}\)} \notag \\
    (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{n} &= (\Sigma_{sn} \epsilon_\Kappa \pi_\Iota)^\cv{n} \tag{Cl-NoCoda-N} \\
    (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{þ} &= \Sigma_{sn} \cat \cv{r}_\Kappa \cat \pi \os{\(\pi \in \{\cv{þ}, \cv{ð}\}\)} \notag \\
    (\Sigma_{sn} \epsilon_{\Kappa} (\pi \rho))^\cv{þ} &= (\Sigma_{sn} \epsilon_\Kappa \pi_\Iota)^\epsilon \triangleleft \cv{ð} \tag{Cl-NoCoda-Þ}
  }

  ◊expl

  Otherwise, if the coda of the last full syllable is empty, then:

  ◊items{
    ◊item{If the fusion consonant is ◊l0{t} and the final onset starts with ◊l0{t}, then the result is the stem but with ◊l0{¬rt←} as the final bridge.}
    ◊item{If the fusion consonant is ◊l0{t} and the final onset starts with any other consonant, then remove the final liquid from the stem and perform fusion on it with the null consonant. Then inject ◊l0{d} into the result.}
    ◊item{If the fusion consonant is ◊l0{n} and the final onset starts with ◊l0{d} or ◊l0{c}, then the result is the stem with the final coda being ◊l0{¬r} or ◊l0{¬s}, respectively, and the final onset being ◊l0{ŋ→}.}
    ◊item{If the fusion consonant is ◊l0{n} and the final onset starts with any other consonant, then fusion occurs as if the liquid were absent.}
    ◊item{If the fusion consonant is ◊l0{þ} and the final onset starts with ◊l0{þ} or ◊l0{ð}, then the result is the stem but with ◊l0{r} as the final coda and the liquid in the final onset removed.}
    ◊item{If the fusion consonant is ◊l0{þ} and the final onset starts with any other consonant, then remove the final liquid from the stem and perform fusion on it with the null consonant. Then inject ◊l0{ð} into the result of doing so.}
  }
}

◊form+expl{
  ◊align-math{
    (\Sigma_{ss} (\pi \cv{r}))^\epsilon &= (\Sigma_{ss} \pi_\Iota) \cat \cv{ôr} \notag \\
    (\Sigma_{ss} (\pi \cv{l}))^\epsilon &= (\Sigma_{ss} \pi_\Iota) \cat \cv{êl} \tag{Cl-Nil} \\
    (\Sigma_{ss} (\pi \rho))^\theta &= (\Sigma_{ss} \pi_\Iota) \cat \cv{ê}_{gs} \cat \theta \os{\(\theta \in \{\cv{t}, \cv{þ}\}\)} \tag{Cl-TÞ} \\
    (\Sigma_{ss} (\pi \rho))^\cv{n} &= (\Sigma_{ss} \pi_\Iota) \cat \cv{ô}_{gs} \cat \cv{n} \tag{Cl-N}
  }

  ◊expl

  If the coda of the last syllable is ◊em{not} empty:

  ◊items{
    ◊item{If the fusion consonant is null, then the result replaces the final ◊l0{r} or ◊l0{l} with ◊l0{←ôr→} or ◊l0{←êl→}, respectively.}
    ◊item{If the fusion consonant is ◊l0{t} or ◊l0{þ}, then the result is the stem without the final liquid, followed by ◊l0{←ê÷} then the fusion consonant.}
    ◊item{If the fusion consonant is ◊l0{n}, then the result is the stem without the final liquid, followed by ◊l0{←ôn→}.}
  }
}

◊subsubsection[#:id "fusion-l"]{Stems ending in ◊l0{÷r→} or ◊l0{÷l→}}

◊form+expl{
  ◊align-math{
    (\Sigma_{ss} \cv{r})^\epsilon &= (\Sigma_{ss} \epsilon_\Iota) \cat \cv{ôr} \notag \\
    (\Sigma_{ss} \cv{l})^\epsilon &= (\Sigma_{ss} \epsilon_\Iota) \cat \cv{êl} \tag{L-Nil} \\
    (\Sigma_{ss} \rho_\Iota)^\theta &= (\Sigma_{ss} \rho_\Iota) \cat \cv{ê}_{gs} \cat \theta \os{\(\theta \in \{\cv{t}, \cv{þ}\}\)} \tag{L-TÞ} \\
    (\Sigma_{sn} \cv{n}_\Kappa \rho_\Iota)^\cv{n} &= (\Sigma_{sn} \cv{n}_\Kappa) \cat \cv{n} \notag \\
    (\Sigma_{ss} \rho_\Iota)^\cv{n} &= (\Sigma_{ss} \rho_\Iota) \cat \cv{ô}_{gs} \cat \cv{n} \tag{L-N}
  }

  ◊expl

  Likewise, there are similar rules when the final onset is ◊l0{÷r→} or ◊l0{÷l→} alone:

  ◊items{
    ◊item{If the fusion consonant is null, then the result replaces the final ◊l0{r} or ◊l0{l} with ◊l0{←ôr→} or ◊l0{←êl→}, respectively.}
    ◊item{If the fusion consonant is ◊l0{t} or ◊l0{þ}, then the result is the stem without the final liquid, followed by ◊l0{←ê÷} then the fusion consonant.}
    ◊item{If the fusion consonant is ◊l0{n}, then the result is the stem without the final liquid, followed by ◊l0{←ôn→}.◊;
      ◊items{◊item{However, if the preceding coda is ◊l0{¬n}, then the result lacks the ◊l0{ô}.}}
    }
  }
}

◊subsection[#:id "fusion-sh"]{Stems ending in ◊l0{š}, ◊l0{ł}, or ◊l0{č}}

◊form+expl{
  ◊align-math{
    \Sigma_{sn} \kappa \iota &\leadsto \Sigma_{sn} I(\kappa \iota) \cv{i}_{gs} \os{\(\iota \in \{\cv{š}, \cv{ł}, \cv{č}, \cv{cš}\}\)} \tag{Šłč}
  }

  where ◊${I: \Gamma \rightarrow \Gamma} is defined as

  ◊$${
    I(\gamma) = \begin{cases}
      \cv{rþ} \os{\(\gamma = \cv{rþš}\)} \\
      \cv{rt} \os{\(\gamma = \cv{rþč}\)} \\
      \cv{þč} \os{\(\gamma = \cv{cþš}\)} \\
      \cv{ł} \os{\(\gamma = \cv{lł}\)} \\
      \cv{č} \os{\(\gamma = \cv{tč}\)} \\
      \gamma \osamgen{}
    \end{cases}
  }

  ◊expl

  If the stem ends in ◊l0{š}, ◊l0{ł}, or ◊l0{č}, then fusion is C-invariant with ◊l0{i} inserted after the stem. However, some bridges are transformed when this occurs: ◊l0{¬rþš→} to ◊l0{¬rþ→}, ◊l0{¬rþč→} to ◊l0{¬rt→}, ◊l0{¬cþš→} to ◊l0{¬þč→}, ◊l0{¬lł→} to ◊l0{¬ł→}, and ◊l0{¬tč→} to ◊l0{¬č→}.
}

◊subsection[#:id "fusion-c"]{Stems ending in ◊l0{c} or ◊l0{g}}

◊form+expl{
  ◊align-math{
    (\Sigma_{sn} \kappa \cv{c})^\epsilon &= \Sigma_{sn} \cat \kappa^{\cv{sþ}/\cv{[cþ][cþ]}} \notag \\
    (\Sigma_{ss} \cv{c})^\theta &= \Sigma_{ss} \cat \theta^{\cv{tnþ}/\cv{tŋ[cþ]}} \tag{Cþ}
  }

  ◊expl

  If the final onset is ◊l0{÷c→} and the fusion consonant is null, then the result is the stem without the final onset and with the final coda replaced with ◊l0{¬cþ} if it was either ◊l0{s} or ◊l0{þ} (otherwise, the final coda is not changed).

  If the final onset is ◊l0{÷c→} and the fusion consonant is not null, then the result is the stem without the final onset, followed by ◊l0{÷t→}, ◊l0{÷ŋ→}, or ◊l0{÷cþ→} for the fusion consonants ◊l0{t}, ◊l0{n}, and ◊l0{þ}, respectively.
}

◊form+expl{
  ◊align-math{
    (\Sigma_{so} \nu \epsilon_\Kappa \cv{g})^\epsilon &= \Sigma_{so} \nu \epsilon_\Gamma \cv{i} \epsilon_\Omega \os{\(\nu \in \{\cv{a}, \cv{e}, \cv{â}, \cv{ê}\}\)} \notag \\
    (\Sigma_{so} \nu \epsilon_\Kappa \cv{g})^\epsilon &= \Sigma_{so} \nu \cv{s} \tag{G-Nil} \\
    (\Sigma_{ss} \cv{g})^\epsilon &= \Sigma_{ss} \cat \cv{i}_{s\omega} \tag{G}
  }
  
  ◊expl

  If the final onset is ◊l0{÷g¬}, the fusion consonant is null, then:

  ◊items{
    ◊item{If the preceding coda is empty and the preceding vowel is ◊l0{a}, ◊l0{e}, ◊l0{â}, or ◊l0{e}, then the result is the stem but with the final onset replaced by ◊l0{i}.}
    ◊item{If the preceding coda is empty and the preceding vowel has any other value, the result is the stem but with the final onset replaced by ◊l0{s}.}
    ◊item{If the preceding coda is not empty, then the result is the stem but with ◊l0{i} added at the end.}
  }
}

◊form+expl{
  ◊align-math{
    (\Sigma_{sn} \kappa \cv{g})^\theta &= \Sigma_{sn} \kappa \cat \cv{i}_{ss} \cat \theta \os{\(\kappa \in W\)} \notag \\
    (\Sigma_{ss} \cv{g})^\theta &= \Sigma_{ss} \cat \theta^{\cv{tnþ}/\cv{dŋ[gð]}} \tag{Gð}
  }

  where ◊${W} is the set of codas that end in a voiceless consonant.

  ◊expl

  If the final onset is ◊l0{÷g→} but the fusion consonant is not null, then:

  ◊items{
    ◊item{If the preceding coda ends with a voiceless consonant, then the result is the stem plus ◊l0{i} and the fusion consonant.}
    ◊item{Otherwise, the result is the stem with the onset replaced with ◊l0{÷d→}, ◊l0{÷ŋ→}, or ◊l0{÷gð→} for a fusion consonant of ◊l0{t}, ◊l0{n}, or ◊l0{þ}, respectively.}
  }
}

◊subsection[#:id "fusion-p"]{Stems ending in ◊l0{p}}

◊form+expl{
  ◊align-math{
    \Sigma_{so} \nu \epsilon_\Kappa \cv{p} &\leadsto \Sigma_{so} \nu \cv{f} \os{\(\nu \in \{\cv{e}, \cv{ê}\}\)} \notag \\
    \Sigma_{so} \nu \epsilon_\Kappa \cv{p} &\leadsto \Sigma_{so} \nu \epsilon_\Gamma \cv{e} \epsilon_\Kappa \tag{P-Nil} \\
    \Sigma_{sn} \cv{n}_\Kappa \cv{p} &\leadsto \Sigma_{sn} \epsilon_\Kappa \cat \cv{me}_{ss} \notag \\
    \Sigma_{ss} \cv{p} &\leadsto \Sigma_{ss} \cat \cv{e}_{ss} \tag{P}
  }

  ◊expl

  If the final onset is ◊l0{÷p→}, then fusion is C-invariant.
  
  ◊items{
    ◊item{If the preceding coda is empty, then the final onset is replaced with ◊l0{f} if the preceding vowel is ◊l0{e} or ◊l0{ê} and with ◊l0{e} otherwise.}
    ◊item{If the preceding coda is ◊l0{¬n}, then the final bridge is replaced by ◊l0{¬:me}.}
    ◊item{If the preceding coda is anything else, then the final onset is replaced with ◊l0{÷e}.}
  }
}

◊subsection[#:id "fusion-h"]{Stems ending in ◊l0{h}}

◊form+expl{
  ◊align-math{
    (\Sigma_{sn} \cv{n}_\Kappa \cv{h}_\Iota)^\epsilon &= \Sigma_{sn} \cv{ns} \notag \\
    (\Sigma_{sn} \cv{r}_\Kappa \cv{h}_\Iota)^\epsilon &= \Sigma_{sn} \cv{ls} \notag \\
    (\Sigma_{sg} \mu \nu \kappa \cv{h}_\Iota)^\epsilon &= \Sigma_{so} \xi(\mu \nu) (\kappa^{\epsilon/\cv{s}})_\Omega \notag \\
    (\Sigma_{ss} \cv{h})^\theta &= \Sigma_{ss} \cat \cv{t} \tag{H}
  }
  ◊expl

  If the final onset is ◊l0{÷h→}, then:

  ◊items{
    ◊item{If the fusion consonant is null, then the result is:◊;
      ◊items{
        ◊item{the stem with the final bridge replaced with ◊l0{¬ns}, if the preceding coda is ◊l0{¬n}}
        ◊item{the stem with the final bridge replaced with ◊l0{¬ls}, if the preceding coda is ◊l0{¬r}}
        ◊item{the stem with the preceding glide and vowel ◊i{ξ}-transformed, if the preceding coda is anything else. In this case, a final coda of ◊l0{¬s} is added if it is empty.}
      }
    }
    ◊item{If the fusion consonant is not null, then the result replaces the final onset with ◊l0{t}.}
  }
}

◊subsection[#:id "fusion-hst"]{Stems ending in ◊l0{ħ}}

◊form+expl{
  ◊align-math{
    \Sigma_{sn} \cv{c}_\Kappa \cv{ħ}_\Iota &\curvearrowright \Sigma_{sn} \epsilon_\Kappa \cv{g}_\Iota \notag \\
    (\Sigma_{sn} \epsilon_\Kappa \cv{ħ}_\Iota)^\epsilon &= \Sigma_{sn} \cv{s} \notag \\
    \Sigma_{ss} \cv{ħ}_\Iota &\leadsto \Sigma_{ss} \tag{Ħ}
  }
  ◊expl
  If the final onset is ◊l0{÷ħ→}, then:

  ◊enum{
    ◊item{If the bridge is ◊l0{¬cħ→}, then fusion occurs as if it were ◊l0{¬g→} instead.}
    ◊item{If the fusion consonant is null and the preceding coda is empty, then the result is the stem, but with the final onset replaced with ◊l0{¬s}.}
    ◊item{Otherwise, fusion is C-invariant, with the final ◊l0{ħ→} being lost.}
  }
}

◊subsection[#:id "fusion-ng"]{Stems ending in ◊l0{ŋ}}

◊form+expl{
  ◊align-math{
    \Sigma_{sg} \mu \nu \kappa \cv{ŋ} &\leadsto \Sigma_{sg} \xi(\mu \nu) \cv{r} \os{\(\kappa \in \{\epsilon, \cv{n}\}\)} \tag{Ŋ-N} \\
    (\Sigma_{sn} \kappa \cv{ŋ})^\epsilon &= \Sigma_{sn} \cv{lôr}_{n\omega} \os{\(\kappa \in \{\cv{r}, \cv{l}\}\)} \notag \\
    (\Sigma_{sn} \kappa \cv{ŋ})^\theta &= \Sigma_{sn} \cv{l}_\Kappa \cat \theta \os{\(\kappa \in \{\cv{r}, \cv{l}\}\)} \tag{Ŋ-Rl}
  }
  ◊expl
  If the final onset is ◊l0{÷ŋ→}, then:
  
  ◊enum{
    ◊item{If the preceding coda is empty or ◊l0{¬n}, then fusion is C-invariant, with the final bridge replaced with ◊l0{¬r} and the preceding glide and vowel ◊i{ξ}-transformed.}
    ◊item{If the fusion consonant is null and the preceding coda is ◊l0{¬r} or ◊l0{¬l}, then the result has the final bridge replaced with ◊l0{¬lôr}.}
    ◊item{If the fusion consonant is not null and the preceding coda is ◊l0{¬r} or ◊l0{¬l}, then the result has the final bridge replaced with ◊l0{¬l÷} plus the fusion consonant.}
    ◊item{No other coda preceding the final onset is possible at this point.}
  }
}

◊subsection[#:id "fusion-cc"]{Stems ending in any other onset with two consonants}

◊form+expl{
  ◊align-math{
    [\Sigma_{sn} \epsilon_\Kappa (\iota^1 \iota^2)_\Iota]^\theta &= \Sigma_{sn} \epsilon_\Kappa (\iota^1 \iota^2)_\Iota \cv{î} \cat \theta \notag \\
    \Sigma_{ss} (\iota^1 \iota^2)_\Iota &\curvearrowright \Sigma_{ss} (\iota^1)_\Iota \cv{i} \epsilon_\Kappa \iota^2 \tag{Ccc}
  }
  ◊expl
  If the final onset has two consonants:

  ◊enum{
    ◊item{If the fusion consonant is not null and the preceding coda is empty, then the result is the stem, followed by ◊l0{î} then the fusion consonant.}
    ◊item{Otherwise, fusion occurs as if ◊l0{i} were inserted between the consonants of the final onset.}
  }
}

◊subsection[#:id "fusion-bycoda"]{Coda-based rules}

◊details{
  ◊summary{By this point, the only possible onsets at the end of the stem are ◊l0{n s þ f}.}

  ◊items{
    ◊item{All two-consonant onsets have already been handled.}
    ◊item{◊l0{c ŋ š r l ł g p č h ħ} handled by their respective rules.}
    ◊item{◊l0{v m d ð} handled by final devoicing.}
    ◊item{◊l0{t} handled by onset aliasing for fusion with ◊${\epsilon} and by obstruent merging for fusion with ◊${\theta}.}
  }
}

◊details{
  ◊summary{By observation, the only possible codas in the final bridge at this point are ◊l0{s n þ rþ t f}.}

  ◊items{
    ◊item{The empty coda is obviously eliminated, as all of ◊l0{n s þ f} are valid simple codas.}
    ◊item{◊l0{r}: ◊l0{rþ} is a valid coda; other cases handled by (Epenthesis-LC)}
    ◊item{◊l0{l} handled by (Epenthesis-LC); in case of fusion with ◊${\epsilon}, ◊l0{lþ} and ◊l0{lt} are valid complex codas}
    ◊item{◊l0{c}: ◊l0{¬c:n→} is an invalid bridge in the first place; ◊l0{¬c:s→}, ◊l0{¬c:þ→}, and ◊l0{¬c:f→} are interpreted as having an empty coda and a complex onset.}
    ◊item{◊l0{cþ}: ◊l0{¬cþn→}, ◊l0{¬cþs→}, and ◊l0{¬cþf→} not valid. ◊l0{¬cþþ→} handled by obstruent merging.}
  }

  Some codas are limited to certain onsets at this point:

  ◊items{
    ◊item{◊l0{t} can be followed only by ◊l0{s}: ◊l0{¬tn→} not valid, ◊l0{¬tf→} canonicalizes to a null coda, and ◊l0{¬tþ→} handled by obstruent merging.}
    ◊item{◊l0{þ rþ} followed only by ◊l0{n}: neither can precede ◊l0{s}. ◊l0{þþ rþþ} handled by degemination or obstruent merging. ◊l0{þf rþf} handled by obstruent merging.}
    ◊item{If fusing with a null consonant, ◊l0{n} is followed only by ◊l0{f}: ◊l0{¬ns} and ◊l0{¬nþ} are already valid complex codas, ◊l0{¬nn→} handled by degemination.}
  }
}

◊subsubsection[#:id "fusion-ts"]{The bridge ◊l0{¬ts→}}

◊form+expl{
  ◊align-math{
    \Sigma_{sn} \cv{ts}_\Gamma &\leadsto \Sigma_{sn} \cv{s} \tag{Ts}
  }
  ◊expl
  If the final bridge is ◊l0{¬ts→}, then fusion is C-invariant, with the final bridge replaced by ◊l0{s}.
}

◊subsubsection[#:id "fusion-sthrthf"]{The codas ◊l0{¬s}, ◊l0{¬þ}, ◊l0{¬rþ}, and ◊l0{¬f}}

◊form+expl{
  ◊align-math{
    \Sigma_{so} \nu \cv{rþ} \iota &\curvearrowright \Sigma_{so} \eta(\nu) \cv{r} \cat \iota \tag{Coda-Rþ} \\
    \Sigma_{so} \nu \kappa \iota &\curvearrowright \Sigma_{so} \eta(\nu) \epsilon \cat \iota \os{\(\kappa \in \{\cv{s}, \cv{þ}, \cv{f}\}\)} \tag{Coda-Sþf}
  }

  where ◊${\eta(\nu) = \nu^{\cv{aeio/âêîô}}}.

  ◊expl

  If the final coda is ◊l0{¬s}, ◊l0{¬þ}, ◊l0{¬rþ}, or ◊l0{¬f}, then fusion occurs as if the preceding vowel is hatted and the final coda loses its last consonant. The following onset is unchanged.
}

◊subsubsection[#:id "fusion-coda-n"]{The coda ◊l0{¬n}}

◊form+expl{
  ◊align-math{
    \Sigma_{sn} \cv{nf}_\Gamma &\leadsto \Sigma_{sn} \cv{f} \notag \\
    \Sigma_{sn} \cv{n}_\Kappa \iota &\leadsto \Sigma_{sn} \cv{n} \tag{Coda-N}
  }
  ◊expl

  If the final coda is ◊l0{¬n}, then fusion is C-invariant, with the bridge replaced with ◊l0{¬f} if the following onset is ◊l0{f←} and with ◊l0{¬n} otherwise.
}

◊subsection[#:id "fusion-properties"]{Properties of stem fusion}

Fusion with ◊l0{t} is invariant (i.e. yields the same stem as the original) only when the final onset of the stem is ◊l0{t→}.

Fusion with ◊l0{n} is invariant only when the final bridge of the stem is ◊l0{¬nn→}.

Fusion with ◊l0{þ} is invariant only when the final onset of the stem is ◊l0{þ→} or ◊l0{cþ→}.
