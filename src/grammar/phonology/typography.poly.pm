#lang pollen

◊define-meta[title]{Layers 2w – 4w: The typography of Ŋarâþ Crîþ}

◊; TODO: improve this intro

In principle, layer 2w is the highest written layer needed to write in Ŋarâþ Crîþ. (Note that there is only one valid layer-2w representation for each layer-1 string; in other words, changing a valid layer-2w string in a way that preserves the layer-1 representation always results in an invalid layer-2w string.) However, speakers of Ŋarâþ Crîþ tend to value aesthetics, even in writing. Thus, a mastery of handwriting beyond layer 2w is considered crucial.

Even though movable type has been available for a long time, prominent parts of printed materials (such as titles) often continued to use plates engraved from handwriting. Eventually, typography and calligraphy were considered parts of the same discipline, leading to typefaces supporting more features from the latter. Even today, logos often opt for lettering over typefaces. Because of this unification, we use the term ◊term{typography} to refer to the discipline of laying out writing in general.

Although a full treatment of Ŋarâþ Crîþ typography is out of scope for this grammar, this section gives an overview of the concerns at hand.

◊section[#:id "kerning"]{Kerning}

Cenvos is a script that absolutely requires kerning. To start, some glyphs such as ◊l2w{e} and ◊l2w{m} have long leftward tails that necessitate kerning with glyphs such as ◊l2w{s} or ◊l2w{o}, which lack descenders, or even some glyphs with descenders such as ◊l2w{j}.

Other glyphs such as ◊l2w{j} and ◊l2w{ê} have shorter leftward descenders that also require kerning with following glyphs.

◊l2w{â} has a descender in the opposite direction; thus, it must kern with certain ◊em{preceding} glyphs.

Diagonal strokes with matching slopes (such as in ◊l2w{âv} or ◊l2w{rj}) should be kerned to bring them closer.

◊make-figure["images/kerning.svg" #:alt "Examples of glyph pairs that require kerning."]{Examples of glyph pairs that require kerning: ◊l2w{es}, ◊l2w{mj}, ◊l2w{jo}, ◊l2w{ên}, ◊l2w{câ}, and ◊l2w{âv}.}

Moreover, even pairs are sometimes insufficient. Since ◊l2w{e} and ◊l2w{i} are kerned so closely, ◊l2w{ei} must itself kern with glyphs such as ◊l2w{s}.

◊make-figure["images/kerning2.svg" #:alt "Kerning of eis and eig."]{Kerning of ◊l2w{eis} and ◊l2w{eig}. In ◊l2w{eis}, ◊l2w{ei} has room to kern with ◊l2w{s}. ◊l2w{ei} obviously cannot kern with ◊l2w{g}; that is, in ◊l2w{eig}, ◊l2w{i} and ◊l2w{g} are spaced ◊em{farther apart} than usual.}

◊section[#:id "typographical-layers"]{Ligation and shaping}

Another important aspect of typography is the use of ligatures (beyond the required ones). The concepts of higher written layers and the hierarchy of graphic variations have been developed to try to formalize this problem.

To explain the idea behind this model, we note that a good ligature will have the end of one glyph near the start of the next. The starting and ending points of a glyph, in turn, depend on the order in which the strokes are written.

Furthermore, natural handwriting tends to join certain strokes together. In some cases, this joining can affect how a glyph ligates; for instance, ◊l3w{◊sym{a1;1}} cannot ligate with the previous character (ligating through the middle would cause a stroke collision with stroke 2 of ◊l3w{◊sym{a1;1}}), but ◊l3w{◊sym{a1;2}}, in which the two strokes are joined without a loop, can do so.

In addition, rapid handwriting often produces stylistic variations of glyphs. For example, ◊l3w{◊sym{i2;1}} (“◊l2w{i} with the stroke going upward”) can often end in a leftward swash at the end of the stroke. Since this deviation does not create any ambiguity, it has been accepted, yielding the stylistic variant ◊l4w{◊sym{i2;1+S}}.

◊make-figure["images/principles1.svg" #:alt "The ideas behind ligation."]{(a) An example of a bad ligature, in which the first glyph ends at the baseline and the second glyph starts at the top line. In the next example, the second glyph starts at the baseline as well, avoiding an awkward joining point. (b) A difference in stroke order (shown with the glyph ◊l2w{a}) can change the starting points (shown as blue dots) and the ending points (shown as red dots) of a glyph. (◊l3w{◊sym{a1;1}} does not have a starting point suitable for ligation.) (c) The first stroke of ◊l3w{◊sym{a1;1}} blocks ligation from a previous glyph, but such a stroke is absent in ◊l3w{◊sym{a1;2}}. (d) The default variant ◊l4w{◊sym{i2;1}} in comparison to ◊l4w{◊sym{i2;1+S}} (both ligated after ◊l4w{◊sym{f1;1}}).}

We now cover the formalism itself. Layers 2w*, 3w, and 4w are aesthetic layers; the writer decides the precise sequence of glyphs to realize a layer-2w string in higher layers. Nonetheless, not all layer-3w or -4w strings are valid, even those that correspond to valid layer-2w strings; for instance, ◊l3w{◊lig{◊sym{s1}◊sym{i1}}} is not a valid realization of ◊l2w{si} because it requires a base-to-top ligation.

Only some glyphs ◊term{participate} in typesetting. Notably, all letters participate, but no numerals do so, nor does the space.

Each participating layer-2w* glyph has a hierarchy of variations as follows:

◊items{
  ◊item{At the top level is the layer-2w* glyph itself.}
  ◊item{These are divided into ◊term{stroke-order variants}, which differ only in stroke order. All strokes must be preserved, and no loops may be introduced or removed, but the relative stroke order might be different, and some strokes may be written in the reverse direction; furthermore, a stroke may be split at a turn, and two strokes may be joined where one ends and another begins. These are denoted with subscript numerals: ◊l2w{a} has variants ◊l3w{◊sym{a1}}, ◊l3w{◊sym{a2}}, and ◊l3w{◊sym{a3}}. Variant 1 is considered the ‘canonical’ variant.}
  ◊item{Each stroke-order variant has one or more ◊term{topological variants}, which may join strokes together, cause two different strokes to touch each other when they did not (or vice versa), or introduce or remove loops. Lengthening or shortening strokes to alter ligation properties also falls under this level. Topological variants are distinguished using lowercase Greek letters. For instance, ◊l3w{◊sym{a1}} has three topological variants: ◊l3w{◊sym{a1;1}}, ◊l3w{◊sym{a1;2}}, ◊l3w{◊sym{a1;3}}. ◊top-var[1] is reserved for the canonical variant, which preserves all strokes, although it is not always the most common variant.}
  ◊item{Each topological variant has one or more ◊term{stylistic variants}, which can modify the strokes of the glyph themselves. For instance, ◊l4w{◊sym{i2;1}} is the topological variant of ◊l2w{i} in which the stroke goes from the base to the top. It has two stylistic variants: ◊l4w{◊sym{i2;1}} is the default one, and ◊l4w{◊sym{i2;1+S}} has a swash to the left at the top of the stroke. Note that the ‘canonical’ stylistic variant has no superscript letter, while the other variants do.}
}

Layer 2w is transliterated using mostly the same symbols as the layer-1 romanization, but required ligatures are notated with an overline (such as in ◊l2w{◊lig{me}} for ◊cenvos{me}), and final forms are written as if they were ligatures with a special ◊nc/w{$} symbol: ◊l2w{◊lig{c$}} for ◊cenvos{c}. Layer 2w* introduces ◊term{discretionary ligatures}, which are similarly marked in our notation. By ◊term{discretionary ligature}, we mean a ligature that the writer may choose to use but is not obligated to do so, and that cannot be derived by simply connecting the ending stroke of one glyph to the starting stroke of another.

Layer 3w works on topological variants. The overline denotes optional ligatures between topological variants; it is now omitted for required  and discretionary ligatures, which are their own layer-2w* glyphs in their own right: ◊l3w{◊lig{◊sym{+1;1}◊sym{me1;1}}◊lig{◊sym{r1;1}◊sym{l2;2}}◊sym{a1;1}◊sym{n1;1} ◊lig{◊sym{#1;1}◊sym{f1;1}◊sym{l2;4}◊sym{i1;2}}◊sym{r1;1}◊lig{◊sym{o1;1}◊sym{r2;1}◊sym{a3;2}}} transliterates a particularly fancy realization of ◊l1{+merlan #flirora}.

◊make-figure["images/est_e_kozet_san.svg" #:alt "#merlan +flirora" #:id "est_e_kozet_san"]{What ◊l3w{◊lig{◊sym{+1;1}◊sym{me1;1}}◊lig{◊sym{r1;1}◊sym{l2;2}}◊sym{a1;1}◊sym{n1;1} ◊lig{◊sym{#1;1}◊sym{f1;1}◊sym{l2;4}◊sym{i1;2}}◊sym{r1;1}◊lig{◊sym{o1;1}◊sym{r2;1}◊sym{a3;2}}} would look like.}

Layer 4w works on stylistic variants. In the transliteration, the overline is used as in 3w.

Layer 3w can be thought of as the ‘ligation layer’; similarly, layer 4w can be thought of as the ‘shaping layer’.

◊xref/l["canonical-stroke-order"]{Table} describes the canonical stroke order of each glyph, and ◊xref/l["stroke-order-variants"]{Table} lists the stroke-order variants.

◊table/x[
  #:options (table-options
    #:caption "Canonical stroke orders for layer-2w* glyphs. (Glyphs in parentheses are discretionary ligatures.)"
    #:colgroup '(c d)
    #:long? #t
    #:pdf-backend 'tabularray
  ) #:id "canonical-stroke-order"
]{
  Glyph & Stroke order
  ◊nc/w{c} & (1) Counterclockwise
  ◊nc/w{e} & (1) From top right to bottom left
  ◊nc/w{n} & (1) From top left to bottom right
  ◊nc/w{ŋ} & (1) From top right to bottom
  ◊nc/w{v} & (1) From right to left
  ◊nc/w{o} & (1) From top to bottom left
  ◊nc/w{s} & (1) From top right to bottom left
  ◊nc/w{þ} & (1) Rightmost stroke from right to left ◊br (2) Leftmost stroke from right to left
  ◊nc/w{š} & (1) From top right to bottom left
  ◊nc/w{r} & (1a) From bottom to top (1b) to left
  ◊nc/w{l} & (1a) ◊nc{r}-stroke from bottom to top (1b) to left ◊br (2) Intersecting stroke from right to left
  ◊nc/w{ł} & (1a) ◊nc{o}-stroke from top to bottom (1b) to left ◊br (2) Intersecting stroke from right to left
  ◊nc/w{m} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Intersecting stroke from right to left
  ◊nc/w{a} & (1) ◊nc{þ}-sloping stroke from left to right ◊br (2) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{f} & (1) Rightmost stroke from right to left ◊br (2) Leftmost stroke from right to left
  ◊nc/w{g} & (1) From top right to bottom
  ◊nc/w{p} & (1) From right to bottom
  ◊nc/w{t} & (1a) ◊nc{v}-stroke from right to top (1b) to left ◊br (2) Vertical stroke from top to bottom
  ◊nc/w{č} & (1) Ascending stroke from top to bottom ◊br (2) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{î} & (1) From bottom right to top left
  ◊nc/w{j} & (1) From top right to bottom left
  ◊nc/w{i} & (1) From top to bottom
  ◊nc/w{d} & (1) ◊nc{þ}-sloping stroke from left to right ◊br (2) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{ð} & (1) Leftmost ◊nc{þ}-sloping stroke from left to right ◊br (2) Rightmost ◊nc{þ}-sloping stroke from left to right ◊br (3) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{h} & (1) From right to left
  ◊nc/w{ħ} & (1) Clockwise, starting and ending at the top
  ◊nc/w{ê} & (1) From top right to bottom left
  ◊nc/w{ô} & (1) From top to bottom
  ◊nc/w{â} & (1) From bottom right to top left
  ◊nc/w{u} & (1) ◊nc{o}-stroke from top to bottom left ◊br (2) Rightmost dot ◊br (3) Leftmost dot
  ◊nc/w{w} & (1) From top to bottom
  ◊nc/w{x} & (1) Stroke with descender, starting from the top-right corner and ending on the descender ◊br (2) Wave stroke, from right to left
  ◊nc/w{y} & (1) From right to left
  ◊nc/w{z} & (1) From right to left
  ◊nc/w{◊lig{c$}} & (1) From right to bottom left
  ◊nc/w{◊lig{ŋ$}} & (1) ◊nc{ŋ}-stroke from top right to bottom ◊br (2) Intersecting stroke from right to left
  ◊nc/w{◊lig{ee}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Overbar from right to left
  ◊nc/w{◊lig{em}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Roof from right to lef
  ◊nc/w{◊lig{me}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Intersecting stroke from right to left ◊br (3) Overbar from right to left
  ◊nc/w{◊lig{mm}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Intersecting stroke from right to left ◊br (3) Roof from right to left
  ◊nc/w{◊lig{jâ}} & (1) ◊nc{j}-stroke from top right to bottom left ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{◊lig{âj}} & (1) ◊nc{â}-stroke from bottom right to top left ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{◊lig{ww}} & (1) ◊nc{w}-stroke, from top to bottom ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{◊lig{xx}} & (1) Stroke with descender, starting from the top-right corner and ending on the descender ◊br (2) Wave stroke, from right to left ◊br (3) Bottom-right tick ◊br (4) Top-left tick
  ◊nc/w{◊lig{yy}} & (1) ◊nc{y}-stroke, from right to left ◊br (2) Tick, from top to bottom
  ◊nc/w{◊lig{zz}} & (1) ◊nc{z}-stroke, from right to left ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{#} & (1) From bottom right to top left
  ◊nc/w{+} & (1) From top right to bottom left
  ◊nc/w{+*} & (1) From top right to bottom left ◊br (2) Vertical stroke from top to bottom ◊br (3) ◊nc{f}-sloping stroke from top right to bottom left ◊br (4) ◊nc{þ}-sloping stroke from bottom right to top left
  ◊nc/w{@} & (1) Vertical stroke from top to bottom ◊br (2) ◊nc{v}-stroke from right to left
  ◊nc/w{*} & (1) Vertical stroke from top to bottom ◊br (2) Horizontal stroke from right to left ◊br (3) ◊nc{f}-sloping stroke from top right to bottom left ◊br (4) ◊nc{þ}-sloping stroke from bottom right to top left
  ◊nc/w{&} & (1) Sinusoid from right to left ◊br (2) Arrowhead
  ◊nc/w{.} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w{;} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w{?} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w{!} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w["{"] & (1) From right to left
  ◊nc/w["}"] & (1) From right to left
  ◊nc/w{«} & (1) From top to bottom
  ◊nc/w{»} & (1) Vertical stroke from top to bottom ◊br (2) Left cornered edge from top to bottom
  ◊nc/w{/} & (1) From bottom, curving at the top toward the left, then descending while crossing to the right half and possibly to the left again
  (◊nc/w{◊lig{ra}}) & (1) Stroke as in ◊l2w{r}, but with the end extending to the descender line ◊br (2) Stroke intersecting the second part of stroke 1
  (◊nc/w{◊lig{ro}}) & (1a) The stem of the ◊l2w{r}-stroke, from bottom to top (1b) A ◊l2w{v}-stroke from right to left
}

◊make-figure["images/stroke-order.svg" #:alt "Canonical stroke orders of layer-2w glyphs."]{Canonical stroke orders of layer-2w glyphs.}

◊make-figure["images/discretionary-ligatures.svg" #:alt "Stroke orders of discretionary ligatures."]{Stroke orders of discretionary ligatures.}

◊table/x[
  #:options (table-options
    #:caption ◊@{Stroke order variants of glyphs, in reference to ◊xref["typography.html" "canonical-stroke-order" "Table"]{the canonical stroke order}. The prime symbol denotes the reverse direction; the plus denotes a fused stroke.}
    #:colgroup '(c rb c c c c c)
    #:long? #t
  ) #:id "stroke-order-variants"
]{
  Glyph & 1 & 2 & 3 & 4 & 5 & 6
  ◊nc{c} & 1
  ◊nc{e} & 1
  ◊nc{n} & 1 & 1◊prime
  ◊nc{ŋ} & 1
  ◊nc{v} & 1
  ◊nc{o} & 1
  ◊nc{s} & 1
  ◊nc{þ} & 1 2
  ◊nc{š} & 1
  ◊nc{r} & 1 & 1a◊prime 1b
  ◊nc{l} & 1 2 & 1a◊prime 1b 2
  ◊nc{ł} & 1 2
  ◊nc{m} & 1 2 & 2◊prime 1 & 1 2◊prime
  ◊nc{a} & 1 2 & 2 1◊prime & 1◊prime 2
  ◊nc{f} & 1 2
  ◊nc{g} & 1
  ◊nc{p} & 1
  ◊nc{t} & 1 2 & 1a+2 1b
  ◊nc{č} & 1 2
  ◊nc{î} & 1
  ◊nc{j} & 1
  ◊nc{i} & 1 & 1◊prime
  ◊nc{d} & 1 2 & 2 1◊prime & 1◊prime 2
  ◊nc{ð} & 1 2 3 & 3 1 2
  ◊nc{h} & 1
  ◊nc{ħ} & 1
  ◊nc{ê} & 1
  ◊nc{ô} & 1
  ◊nc{â} & 1
  ◊nc{u} & 1 2 3
  ◊nc{w} & 1
  ◊nc{x} & 1 2
  ◊nc{y} & 1
  ◊nc{z} & 1
  ◊nc/w{◊lig{c$}} & 1
  ◊nc/w{◊lig{ŋ$}} & 1 2 & 1 2◊prime
  ◊nc/w{◊lig{ee}} & 1 2 & 2 1
  ◊nc/w{◊lig{em}} & 1 2 & 2 1
  ◊nc/w{◊lig{me}} & 1 2 3 & 2◊prime 1 3 & 1 2◊prime 3 & 3 1 2 & 3 2◊prime 1 & 3 1 2◊prime
  ◊nc/w{◊lig{mm}} & 1 2 3 & 2◊prime 1 3 & 1 2◊prime 3 & 3 1 2 & 3 2◊prime 1 & 3 1 2◊prime
  ◊nc/w{◊lig{jâ}} & 1 2
  ◊nc/w{◊lig{âj}} & 1 2
  ◊nc/w{◊lig{ww}} & 1 2
  ◊nc/w{◊lig{xx}} & 1 2 3 4
  ◊nc/w{◊lig{yy}} & 1 2
  ◊nc/w{◊lig{zz}} & 1 2
  ◊nc{#} & 1
  ◊nc{+} & 1
  ◊nc{+*} & 1 2 3 4
  ◊nc{@} & 1 2
  ◊nc{*} & 1 2 3 4
  ◊nc{&} & 1 2
  ◊nc{.} & 1 2
  ◊nc{;} & 1 2
  ◊nc{?} & 1 2
  ◊nc{!} & 1 2
  ◊nc["{"] & 1
  ◊nc["}"] & 1
  ◊nc{«} & 1
  ◊nc{»} & 1 2 & 1+2◊prime
  ◊nc{/} & 1
  (◊nc/w{◊lig{ra}}) & 1 2
  (◊nc/w{◊lig{ro}}) & 1 & 1a◊prime 1b
}

◊table/x[
  #:options (table-options
    #:caption ◊@{Topological variants of glyphs: ligation properties and descriptions. (Stroke numbers are in reference to the stroke-order variant, not the 2w glyph.)}
    #:colgroup '(c c c d d)
    #:long? #t
  ) #:id "topological-variants"
]{
  Glyph & Start join & End join & Description & Use
  ◊nc/w{◊sym{c1;1}} & M & — & Default & Default
  ◊nc/w{◊sym{e1;1}} & Mv & D & Default & Default
  ◊nc/w{◊sym{e1;2}} & Bv & D & Stem shortened to start at base & After glyphs that end at the base
  ◊nc/w{◊sym{n1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{n2;1}} & B & M & Default & Before glyphs that start at the mid
  ◊nc/w{◊sym{ŋ1;1}} & M & Dv & Default & Default
  ◊nc/w{◊sym{v1;1}} & B & B & Default & Default
  ◊nc/w{◊sym{o1;1}} & Tv & M & Default & Default
  ◊nc/w{◊sym{o1;2}} & M & M & Loop on stroke to allow for mid ligation with previous glyph & After glyphs that end at the mid
  ◊nc/w{◊sym{s1;1}} & M & B & Default & Default
  ◊nc/w{◊sym{þ1;1}} & B & M & Default & Default
  ◊nc/w{◊sym{þ1;2}} & B & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{š1;1}} & M & Bv & Default & Default
  ◊nc/w{◊sym{r1;1}} & Dv & B & Default & Default
  ◊nc/w{◊sym{r2;1}} & Mv & B & Default & Rare (◊top-var[2] form is more common), but sometimes after glyphs that end at the mid
  ◊nc/w{◊sym{r2;2}} & Bv & B & Stroke 1 disconnected from 2 (starts at base instead) & After glyphs that end at the base
  ◊nc/w{◊sym{l1;1}} & Dv & M & Default & Default
  ◊nc/w{◊sym{l1;2}} & Dv & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{l2;1}} & Mv & M & Default & Rare (◊top-var[2] form is more common), but sometimes after glyphs that end at the mid
  ◊nc/w{◊sym{l2;2}} & Bv & M & Stroke 1 disconnected from 2 (starts at base instead) & After glyphs that end at the base
  ◊nc/w{◊sym{l2;3}} & Mv & M & Strokes 2 and 3 connected & Rare (◊top-var[4] form is more common), but stylization of ◊top-var[1]
  ◊nc/w{◊sym{l2;4}} & Bv & M & Stroke 1 disconnected from 2 (starts at base instead), and strokes 2 and 3 connected & Stylization of ◊top-var[2]
  ◊nc/w{◊sym{ł1;1}} & Tv & BD & Default & Default
  ◊nc/w{◊sym{ł1;2}} & Tv & BD & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{m1;1}} & Mv & — & Default & Default
  ◊nc/w{◊sym{m2;1}} & — & D & Default & Rare; ◊top-var[2] form is more common
  ◊nc/w{◊sym{m2;2}} & — & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{m3;1}} & Mv & — & Default & Rare; ◊top-var[2] form is more common
  ◊nc/w{◊sym{m3;2}} & Mv & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{a1;1}} & — & D & Default & Default
  ◊nc/w{◊sym{a1;2}} & M & D & Strokes 1 and 2 fused, with 2 beginning where 1 ends (without a loop) & Stylistic (‘italic’ variant)
  ◊nc/w{◊sym{a1;3}} & — & D & Strokes 1 and 2 connected (with a loop) & Stylistic
  ◊nc/w{◊sym{a2;1}} & M & M & Default & After glyphs that end at the mid
  ◊nc/w{◊sym{a2;2}} & M & M & Strokes 1 and 2 connected (rare) & Stylistic
  ◊nc/w{◊sym{a3;1}} & B & D & Default & After glyphs that end at the base
  ◊nc/w{◊sym{a3;2}} & B & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{f1;1}} & M & B & Default & Default
  ◊nc/w{◊sym{f1;2}} & M & B & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{g1;1}} & M & Dv & Default & Default
  ◊nc/w{◊sym{p1;1}} & B & Dv & Default & Default
  ◊nc/w{◊sym{t1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{t2;1}} & B & B & Default & Stylistic
  ◊nc/w{◊sym{č1;1}} & T & B & Default & Default
  ◊nc/w{◊sym{î1;1}} & B & M & Default & Default
  ◊nc/w{◊sym{j1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{i1;1}} & Tv & Bv & Default & Default
  ◊nc/w{◊sym{i1;2}} & M & Bv & Loop on stroke to allow for mid ligation with previous glyph & After glyphs that end at the mid
  ◊nc/w{◊sym{i2;1}} & B & T & Default & After glyphs that end at the base
  ◊nc/w{◊sym{d1;1}} & — & B & Default & Default
  ◊nc/w{◊sym{d2;1}} & M & M & Default & After glyphs that end at the mid
  ◊nc/w{◊sym{d3;1}} & B & B & Default & After glyphs that end at the base
  ◊nc/w{◊sym{ð1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{ð1;2}} & B & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{ð1;3}} & B & — & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{ð1;4}} & B & — & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{ð2;1}} & M & M & Default & After glyphs that end at the mid, or as a stylization
  ◊nc/w{◊sym{ð2;2}} & M & M & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{h1;1}} & M & M & Default & Default
  ◊nc/w{◊sym{ħ1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{ê1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{ê1;2}} & M & — & Stroke bends to the right at the end, preventing linkage with the next glyph & Stylistic
  ◊nc/w{◊sym{ô1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{â1;1}} & D & M & Default & Default
  ◊nc/w{◊sym{u1;1}} & Tv & DB & Default & Default
  ◊nc/w{◊sym{u1;2}} & M & DB & Loop on stroke 1 to allow for mid ligation with previous glyph & After glyphs that end at the mid
  ◊nc/w{◊sym{w1;1}} & M & Dv & Default & Default
  ◊nc/w{◊sym{x1;1}} & M & M & Default & Default
  ◊nc/w{◊sym{y1;1}} & B & B & Default & Default
  ◊nc/w{◊sym{z1;1}} & B & B & Default & Default
  ◊nc/w{◊sym{c$1;1}} & M & D & Default (in practice, final forms have no successor to ligate to) & Default
  ◊nc/w{◊sym{ŋ$1;1}} & M & DB & Default & Default
  ◊nc/w{◊sym{ŋ$2;1}} & M & — & Default & Rare; ◊top-var[2] form is more common
  ◊nc/w{◊sym{ŋ$2;2}} & M & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{ee1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{ee2;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{ee2;2}} & M & D & Strokes 1 and 2 connected (uncommon) & Stylistic
  ◊nc/w{◊sym{em1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{em2;1}} & M & D & Default & Stylistic
  ◊nc/w{◊sym{em2;2}} & M & D & Strokes 1 and 2 connected (uncommon) & Stylistic
  ◊nc/w{◊sym{me1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{me2;1}} & — & M & Default & Stylistic
  ◊nc/w{◊sym{me2;2}} & — & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me3;1}} & Mv & M & Default & Stylistic
  ◊nc/w{◊sym{me3;2}} & Mv & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me3;3}} & — & M & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{me3;4}} & — & M & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{me4;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{me4;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me5;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{me5;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me5;3}} & M & D & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{me5;4}} & M & D & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{me6;1}} & M & — & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{me6;2}} & M & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me6;3}} & M & — & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{me6;4}} & M & — & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{mm1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{mm2;1}} & — & M & Default & Stylistic
  ◊nc/w{◊sym{mm2;2}} & — & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm3;1}} & Mv & M & Default & Stylistic
  ◊nc/w{◊sym{mm3;2}} & Mv & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm3;3}} & — & M & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{mm3;4}} & — & M & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{mm4;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{mm4;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm5;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{mm5;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm5;3}} & M & D & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{mm5;4}} & M & D & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{mm6;1}} & M & — & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{mm6;2}} & M & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm6;3}} & M & — & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{mm6;4}} & M & — & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{jâ1;1}} & M & M & Default & Default
  ◊nc/w{◊sym{âj1;1}} & D & D & Default & Default
  ◊nc/w{◊sym{ww1;1}} & M & — & Default & Default
  ◊nc/w{◊sym{xx1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{yy1;1}} & B & M & Default & Default
  ◊nc/w{◊sym{zz1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{#1;1}} & — & M & Default & Default
  ◊nc/w{◊sym{+1;1}} & — & M & Default & Default
  ◊nc/w{◊sym{+*1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{@1;1}} & Tv & M & Default & Default
  ◊nc/w{◊sym{@1;2}} & M & M & Loop on stroke 1 to allow for mid ligation with previous glyph & After a glyph that ends at the mid
  ◊nc/w{◊sym{*1;1}} & — & M & Default & Default
  ◊nc/w{◊sym{&1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{.1;1}} & MT & — & Default & Default
  ◊nc/w{◊sym{;1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{?1;1}} & MT & — & Default & Default
  ◊nc/w{◊sym{!1;1}} & M & — & Default & Default
  ◊nc/w{◊sym["{1;1"]} & T & Tv & Default & Default
  ◊nc/w{◊sym["}1;1"]} & Bv & B & Default & Default
  ◊nc/w{◊sym{«1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{»1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{»2;1}} & — & — & Default & Stylistic (handwriting variant)
  ◊nc/w{◊sym{/1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{ra1;1}} & Dv & — & Default & Default
  ◊nc/w{◊sym{ro1;1}} & Dv & M & Default & Default
  ◊nc/w{◊sym{ro2;1}} & Mv & M & Default & Rare (◊top-var[2] form is more common), but sometimes after glyphs that end at the mid
  ◊nc/w{◊sym{ro2;2}} & Bv & M & Stroke 1 disconnected from 2 (starts at base instead) & After glyphs that end at the base
}

◊xref/l["topological-variants"]{Table} lists all topological variants with their possible join positions on each side, with ◊i{B} for ◊i{base}, ◊i{M} for ◊i{mid} (or ◊i{mean}), ◊i{T} for ◊i{top} (ascender line), and ◊i{D} for ◊i{descender}. If more than one position is listed, then any one of them can be used. A ◊i{v} suffix on a position indicates that the stroke end at the appropriate side is vertical.

In general, for two topological variants ◊var{a} and ◊var{b} to ligate to each other (in that order), there must exist a position ◊var{C} such that ◊var{a} can join at ◊var{C} endward and ◊var{b} can join at ◊var{C} startward, with at least one end not being vertical.

There are a few exceptions to this rule: any topological variant of ◊l2w{l} can be ligated before ◊l3w{◊sym{i2;1}} (see ◊xref/l["est_e_kozet_san"]{Figure} for an example).

Stylistic variants are much less standardized in comparison, but there are some widely recognized variants:

◊items{
  ◊item{Some topological variants (◊l3w{◊sym{þ1;2}}, ◊l3w{◊sym{j1;1}}, ◊l3w{◊sym{i2;1}}, ◊l3w{◊sym{c$1;1}}, ◊l3w{◊sym{«1;1}}) have an S variant that introduces a swash at the end of the last stroke.}
  ◊item{In the standard forms, ◊l2w{e} and ◊l2w{m} (as well as the required ligatures involving these) have the tail sloping slightly upwards (as it goes to the left). This tail might sometimes bend downwards (the C variant) or even start with a downward slope (the D variant).}
  ◊item{The rightward descending stem of a glyph such as ◊l3w{◊sym{r1;1}} can be shortened (in the H variant) after an ◊l2w{e} or ◊l2w{m} to allow kerning.}
}

◊l2w{’} and ◊l2w{·} are special: they can ligate with ◊em{any} participating glyph on either end, appearing as an extension of the stroke near the ◊l2w{’} or ◊l2w{·}. Nonetheless, such ligation is not particularly common.

The rules over layers 3w and 4w dictate only what is legal, not what is considered beautiful. (Indeed, it is perfectly legal to use the 1◊top-var[1] form of every glyph and abstain from all non-required ligatures.) Nor do they dictate ◊em{how} an eligible pair of glyphs should be ligated. There are some guidelines, however, on what is desirable:

◊items{
  ◊item{Avoid stroke collisions}
  ◊item{Minimize horizontal space}
  ◊item{Minimize effort to write}
  ◊item{Prefer to ligate when possible, but avoid doing so excessively}
  ◊item{Prefer to use the canonical stroke-order}
  ◊item{Prefer to use the most common topological forms}
  ◊item{Vary the particular forms of each letter}
}

◊subsection[#:id "ligation-connotations"]{Connotations associated with choices in layer-4w realization}

Of course, context also plays a role in deciding how to realize text into layer 4w. First, the purpose of the writing has an influence (text meant for children or language learners will be less embellished, and header text tends to be more embellished than body text).

Another part of context is the expressive connotation that the writer wishes to communicate.

◊table/x[#:options (table-options
    #:caption "Expresive connotations associated with choices in layer-4w realization."
    #:first-col-header? #f
    #:colgroup '(c d)
  )]{
  Connotation & Properties of realization
  Elegant, refined & Increased use of ligation in general; use of ‘broken ◊l2w{r}-stroke forms’ such as ◊l3w{◊sym{r2;2}} and ◊l3w{◊sym{l2;2}}
  Rational & Use of the non-H stylistic variants of glyphs such as ◊l3w{◊sym{r1;1}} after ◊l2w{e} or ◊l2w{m} rather than the H variants
  Casual, informal & Use of ◊l3w{◊sym{a1;2}}
}

◊section[#:id "vertical-ligation"]{Vertical ligation}

Another desirable practice is ◊term{vertical ligation}, in which the strokes of two glyphs in different lines are connected. This is naturally difficult even in handwriting, let alone in type!
