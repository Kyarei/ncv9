#lang pollen

◊(require pollen/core pollen/setup)

◊define-meta[title]{Orthography and phonology}
◊define-meta[chapter-type part]

The phonology and orthography of Ŋarâþ Crîþ can be divided into eight layers in two modes (◊term{writing} and ◊term{speaking}):

◊items{
  ◊item{◊term{Layer 0} is the underlying morphographemic representation. Content in this layer exists structurally instead of linearly. In this grammar, text in this layer is written in double square brackets: ◊l0{tanc-a}.}
  ◊item{◊term{Layer 1} is the graphemic representation. This representation is subsequently exported to the spoken and written modes. Text in this layer is written with angle brackets: ◊l1{tanca}.}
  ◊item{◊term{Layer 2w} is the surface glyphic representation. This represents the sequence of Cenvos glyphs that is written, observing required ligatures and final forms. Text in this layer is written with double angle brackets: ◊l2w{tanca}; for a more interesting example, ◊l1{mencoc} becomes ◊l2w{◊lig{me}nco◊lig{c$}}.}
  ◊item{◊term{Layer 2w*} is an intermediate layer between 2w and 3w, in which discretionary ligatures are introduced to 2w text. For instance, ◊l2w{#flirora} can be realized as ◊l2w*{#fli◊lig{ro}◊lig{ra}}.}
  ◊item{◊term{Layer 3w} is the topological representation, showing optional ligatures as well as stroke order variations. Text in this layer is written with double angle brackets: ◊l3w{◊sym{t1;1}◊sym{a1;3}◊sym{n1;1}◊sym{c1;1}◊sym{a1;1}}. More interestingly, ◊l2w{◊lig{me}nco◊lig{c$}} could become ◊l3w{◊sym{me1;1}◊sym{n1;1}◊sym{c1;1}◊lig{◊sym{o1;1}◊sym{c$1;1}}}.}
  ◊item{◊term{Layer 4w} is the presentational representation, adding to 3w variations in the strokes themselves and how strokes within a glyph are joined. Text in this layer is written with double angle brackets: ◊l4w{◊sym{t1;1}◊sym{a1;3}◊sym{n1;1}◊sym{c1;1}◊sym{a1;1}}.}
  ◊item{◊term{Layer 2s} is the phonemic representation. We use slashes for this, as usual: ◊l2{tanka}.}
  ◊item{◊term{Layer 3s} is the phonetic representation, or what is pronounced. We use square brackets for this, as usual: ◊l3{tʰa⁴ɲcʰa²}.}
}

The conversions from 0 to 1, 1 to 2w, and 2s to 3s are functional: each valid input corresponds to exactly one output. The conversion from 1 to 2s is almost so, except when a ◊l1{&} is present. In the opposite direction, the conversions from 4w to 3w, from 3w to 2w*, and from 2w* to 2w are functional. Furthermore, for any conversion, it can be determined whether a given input can be converted into a given output without external information.

In addition, the conversion between 1 and 2w is bijective: valid layer-1 and layer-2w representations can be paired with each other.

◊(when/splice (equal? (current-poly-target) 'html)
  (embed-pagetree "index.ptree"))
