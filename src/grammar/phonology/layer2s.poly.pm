#lang pollen

◊define-meta[title]{Layer 2s and 3s: the spoken layers}

◊section[#:id "layer-2"]{Layer 2s}

◊b{TODO:} deal with complex codas before a clitic boundary

Traditionally, only manifested grapheme phrases are considered to be significant in the conversion from layer 1 to layer 2s. However, other graphemes such as punctuation can affect prosody.

◊table/x[
  #:options (table-options
    #:caption "Layer 1 to layer 2s conversions."
    #:colgroup '(c rb c c)
    #:first-col-header? #f
  )
]{
  MGPs & IPA & MGPs & IPA
  ◊nc{c} & k & ◊nc{p} & p
  ◊nc{e} & e & ◊nc{t} & t
  ◊nc{n nd} & n & ◊nc{č} & t͡ʂ
  ◊nc{ŋ ŋg} & ŋ & ◊nc{î} & ì
  ◊nc{v m· vp} & v & ◊nc{j} & j
  ◊nc{o} & o & ◊nc{i} & i
  ◊nc{s} & s & ◊nc{d dt} & d
  ◊nc{þ t·} & θ & ◊nc{ð d· ðþ} & ð
  ◊nc{š č·} & ʂ & ◊nc{h c·} & x
  ◊nc{r} & ɹ & ◊nc{ħ g·} & ʕ
  ◊nc{l lł} & l & ◊nc{ê} & è
  ◊nc{ł} & ɬ & ◊nc{ô} & ò
  ◊nc{m mp} & m & ◊nc{â} & à
  ◊nc{a} & a & ◊nc{u} & u̜
  ◊nc{f p·} & f & ◊nc{f· v· ð·} & ∅
  ◊nc{g gc} & ɡ
}

In some more conservative dialects, ◊l1{f·} is pronounced as ◊l2{ʍ} and ◊l1{v·} is pronounced as ◊l2{w}.

Layer 2 has a two-way tone contrast between vowels: the high tone (H) is the default, being contrasted with the low tone (L). For historical reasons, the presence or absence of a low tone on a vowel is called [±creaky].

◊section[#:id "layer-3"]{Layer 3s}

The conversion from layer 2s to layer 3s is comparatively more complex.

◊subsection{Segmental changes}

First, the following changes are made:

◊items/dense{
  ◊item{kθ → x͡θ}
  ◊item{ʕ → ħ / V[+creaky] _}
  ◊item{n → m / _ C[+labial]}
  ◊item{n → ɱ / _ C[+labiodental]}
  ◊item{n → n̪ / _ C[+dental]}
  ◊item{n → ɳ / _ C[+retroflex]}
  ◊item{n C₁[+velar] → ɲ C₁[+palatal]}
  ◊item{n → ŋ / _ C[+lateral] V[+front]}
  ◊item{sʂ → ʂː}
  ◊item{C₁={ɹ, ɬ} → w / C₁V _}
  ◊item{l → ɾ / V[+back] _ V}
  ◊item{θ → θ̠ / s_, _s, _ʂ}
  ◊item{ʂj → ʃ}
  ◊item{ʂ → ʃ / _ i}
  ◊item{t͡ʂj → t͡ʃ}
  ◊item{t͡ʂ → t͡ʃ / _ i}
  ◊item{sj → ɕ}
  ◊item{s → ɕ / _ i}
  ◊item{lj → ɬ / C[+plosive, −voiced] _ (often)}
  ◊item{lj → ɮ / C[+plosive, +voiced] _ (sometimes)}
  ◊item{lj → ɮ (rarely)}
  ◊item{C₁[+voiced, +plosive] → C₁[−voiced, +ejective] / ɬ}
  ◊item{C₁[+voiced, +plosive] → C₁[−voiced, −aspirated] / C₂[−voiced]}
}

Plosives in a coda are unreleased. All unvoiced plosives and affricates outside of a coda are aspirated. Voiced plosives are sometimes partially devoiced or breathy-voiced after a vowel of the same word.

The words ◊l1{cenþ’pe} and ◊l1{cenþ’ve} are pronounced as ◊l3{kenθe} and as ◊l3{kenðe}.

◊subsection{Stress}

In order to describe tone, we must introduce the concept of ◊term{“stress”}, which is placed according to the following rules:

◊items{
  ◊item{Syllables with a high tone have a priority over syllables with a low tone – that is, a syllable with a low tone will be selected only if the word in question has only low-tone syllables.}
  ◊item{If the 2nd-to-last syllable has a vowel of ◊l3{i} or ◊l3{ì} and an empty coda, then the syllables are chosen in the order ◊b{3rd-to-last → 2nd-to-last → last → 4th-to-last → … → first}.}
  ◊item{If the coda of the final syllable is either empty or is ◊l3{s} or ◊l3{n}, then:◊;
    ◊items{
      ◊item{If the 3rd-to-last syllable has a nonempty coda but the 2nd-to-last syllable does not, then the syllables are chosen in the order ◊b{3rd-to-last → 2nd-to-last → last → 4th-to-last → … → first}.}
      ◊item{Otherwise, the syllables are chosen in the order ◊b{2nd-to-last → 3rd-to-last → last → 4th-to-last → … → first}.}
    }
  }
  ◊item{If the coda of the final syllable is a complex coda, then the syllables are chosen in the order ◊b{last → 3rd-to-last → 2nd-to-last → 4th-to-last → … → first}.}
  ◊item{If the coda is anything else, then the syllables are chosen from end to start: ◊b{last → 2nd-to-last → 3rd-to-last → … → first}.}
  ◊item{Monosyllabic function words generally lack any stressed syllable.}
}

◊subsection{Tone}

While Ŋarâþ Crîþ has two tone levels phonemically, their realizations in the phonetic level is more complex. It is common to describe phonetic tone using seven levels, from 0 (the lowest) to 6 (the highest). Each syllable has one or more tones.

We introduce the concept of a ◊term{tone accounting unit} (TAU), which is the level at which tones are realized. That is, the tone of a syllable depends only on the contents of the TAU in which it lies. Instances of content words occupy different TAUs from each other, but some function words occupy the same TAU as the preceding or following word (in particular, such words have no stressed syllable and are confined to a relatively fixed position):

◊items{
  ◊item{Head particles, nominalized verb particles, and monosyllabic determiners occupy the same TAU as the following word.}
  ◊item{◊l1{so}, monosyllabic relationals ... occupy the same TAU as the preceding word.}
}

(Stress is accounted by orthographic word, not by TAU.)

First, two adjacent vowels are fused into a diphthong if the vowels are not identical, the first vowel is stressed, the second vowel is ◊l3{i} or ◊l3{u̜}, and the syllable to which the second vowel belongs can be interpreted as having an empty coda. For purposes of tonekeeping, a diphthong is considered to be composed of two different syllables.

In general, unstressed H and L syllables have tone levels 4 and 2, respectively; stressed H and L syllables have tone levels 5 and 1. However, an open H or L syllable before a stressed syllable gets level 3 or 1, respectively, instead. Diphthongs get different values: 65 for HH, 53 for HL, 13 for LH, and 21 for LL.

If two adjacent copies of an identical vowel have the same tone level at this stage, then the one closer to the stressed syllable rises by one tone level and the one farther from it falls by one level.

A tone level of ◊var{n} is then changed into a tone contour in the following situations, unless doing so would result in an out-of-bounds tone level:

◊items{
  ◊item{◊var{n} to (◊var{n} : ◊var{n} + 1): when the coda is ◊l3{st} or ◊l3{x͡θ}}
  ◊item{◊var{n} to (◊var{n} : ◊var{n} − 1): when the coda is ◊l3{rθ} or ◊l3{ns}}
  ◊item{◊var{n} to (◊var{n} + 1 : ◊var{n}): when the nucleus is preceded by two or more voiceless consonants}
}

In addition, other syllables change their tone levels:

◊items{
  ◊item{Raise the tone level by 1 (if it is not already 6) if the coda is a voiceless fricative, or if the coda is ◊l3{x͡θ}.}
  ◊item{Lower the tone level by 1 if the coda is ◊l3{ɹ}.}
  ◊item{Lower the tone level by 1 if the coda is a nasal followed by a voiced obstruent or nasal.}
}

Finally, if all tones have a level of 4 or higher, then the lowest tone (breaking ties by preferring later tones) is lowered to 3, and all other tones in the same syllable are lowered by the same amount. All level-3 tones are then lowered to level 2.

◊subsection[#:id "isochrony"]{Isochrony}

The isochrony of Ŋarâþ Crîþ falls somewhere between syllable and mora timing, where:

◊items{
  ◊item{The body of a syllable is always 1 unit long.}
  ◊item{The coda of a syllable is between 0 and 1 unit long, with the hierarchy ◊l2{t, k < n < l, ɹ < f, s, θ, ɹθ, kθ < st, lt, ns, ls, nθ}.}
  ◊item{Codas are shortened after two consecutive vowels: for instance, the ◊l1{l} in ◊l1{moriel} is pronounced for less time than that in ◊l1{mjarel}.}
}
