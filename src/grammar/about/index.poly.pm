#lang pollen

◊define-meta[title]{What is Ŋarâþ Crîþ?}
◊; The chapter type is intentionally 'chapter rather than 'part.

◊term{Ŋarâþ Crîþ} (◊l3{ŋa⁵ɹa²θ kɹi¹θ}; Cenvos: ◊cenvos{ŋarâþ crîþ}; lit. ◊trans{forest language}) is an artistic constructed language by ◊link["https://flirora.xyz"]{+merlan #flirora} (◊l3{me⁵ɹla²n fli¹ɹo⁵wa⁴}; Cenvos: ◊cenvos{+merlan #flirora}). Originally called ◊term{Necarasso Cryssesa}, it is now on version 9.

The development of the language can be classified into four phases:

◊items{
  ◊item{
    In the first phase (2013 – 2014; VE¹ENCS – VE⁴ENCS), Necarasso Cryssesa was head-initial and had an SVO word order. The language was meant to have an elvish aesthetic. Successive versions of the grammar added more elaborate morphology.
  }
  ◊item{
    The second phase (2014 – 2016; NCS5 – NCS6) made drastic changes to the phonology and grammar in order to make it more like Japanese. For example, these versions had head-final word order and cases, and they lost articles and gender.
  }
  ◊item{
    After a hiatus, the author revisited the language, renaming it to ◊term{Ŋarâþ Crîþ}, with more radical changes than even NCS5. ◊term{ŊCv7} lasted from 2019 to 2021. In particular, the phonology was reworked, adding consonant mutations; agreement was made more plentiful; and gender was re-added (albeit not with a sex distinction).
  }
  ◊item{
    Lastly, the current phase consists of ◊term{ŊCv9}, which is the subject of this website. (Version 8 was skipped because 8 is ◊em{not} a lucky number.)
  }
}

Learn more about the ◊link["https://flirora.xyz/langdocs/reason4v7.html"]{history of Ŋarâþ Crîþ} (main site).

◊section[#:id "changes"]{Changes from Ŋarâþ Crîþ v7}

◊subsection[#:id "changes-phonology"]{Phonology and orthography}

The phonology of ŊCv9 is mostly a successor of ŊCv7’s phonology.

In the area of phonotactics, I have found that I dislike ◊l1{ŋ} as a coda, although I like it in the onset position. Therefore, ◊l0{ŋ} is no longer a valid coda, and final ◊l0{m} is drastically rarer.

ŊCv9 adds ◊l0{f} and ◊l0{ł}, as well as several consonant clusters, as valid codas. Most of the complex codas come from an abandoned ŊCv7 fork (also confusingly named “ŊCv9”), while ◊l0{f} and ◊l0{cþ} are genuinely new. Of course, more conversion rules must be added to handle the appearance of complex codas mid-word.

The “circumflexed vowels”, ◊l1{î ê ô â}, were pronounced with creaky voice in ŊCv7. Therefore, they were often pronounced with a low pitch as well. I have taken advantage of this pronunciation to make ŊCv9 a tonal language. After all, about half of the world’s languages are tonal, but the proportion among my conlangs is much lower.

ŊCv9 adds four true letters, ◊l1{w x y z}, which are not used natively but are reserved for Cenvos orthographies of foreign languages.

Additionally, Project Elaine has revised Ŋarâþ Crîþ’s morphophonology: layer 0 is now viewed structurally as paths through a finite state machine. Bridges are resolved when they are formed from concatenation. In addition, Project Elaine adds the operation of ◊term{stem fusion}.

The properties of kerning and ligation were not documented in ŊCv7 but are in ŊCv9.

◊subsection[#:id "changes-morphology"]{Morphology}

In general, ŊCv9 has more complex morphology than ŊCv7. ŊCv9 inflection can be automated with f9i, as is done to produce inflection tables for dictionary entries.

The ablative, allative, prolative, and semblative cases from ŊCv7 were removed, decreasing the number of cases from 12 to 8. At the same time, the generic and singulative numbers were added, along with the concept of ◊term{clareþ}, which governs which numbers make sense for each noun.

In ŊCv7, the inflectional paradigm of a noun could be deduced from the ending of its lemma. This is not possible in general in ŊCv9. There is also no way to regularly derive non-lemma stems from the lemma form. The inflection of nouns has been organized into six main paradigms, most of which are further divided into sub-paradigms.

In comparison to ŊCv7, ŊCv9 has more complex verb inflections. ◊term{Vitreous} verbs in ŊCv9 resemble the traditional Ŋarâþ Crîþ inflections in finite verbs. While verbs in ŊCv7 were marked for aspect using eclipsis only, ŊCv9 adds different set of person–number affixes for distinguishing aspect in order to make imperfective and perfective forms distinct when the initial consonant is not eclipsable. ◊term{Resinous} verbs, on the other hand, exhibit more fusion in the finite forms, as well as having certain forms that vitreous verbs do not.

Unlike ŊCv7, which has a single paradigm for participle forms, ŊCv9 has three ◊term{genera} according to the distinctions made in head gender and number, each of which is divided into one or more ◊term{species}. Nominalized forms were uniformly made from a case particle plus the infinitive in ŊCv7, but some of these forms in ŊCv9 have been replaced with synthetic forms.

◊term{Relationals} in ŊCv9 function like ŊCv7’s postpositions, but they mark for attachment (either adnominal or adverbial) and can be used predicatively.

The long numerals up to six are now declined for both case and gender, while they were marked for case only in ŊCv7.

◊subsection[#:id "changes-syntax"]{Syntax}

Most of the changes in syntax are either clarifications or new features. The datum, however, has been formalized in ŊCv9.

◊subsection[#:id "changes-lexicon"]{Lexicon}

As inspired by the ◊link["https://ziphil.com/"]{Shaleian} language, the ŊCv9 dictionary makes an effort to provide more precise definitions for words rather than simply providing translations.

Because of the additional restrictions to word forms in ŊCv9, some words are altered from their ŊCv7 forms.
