#lang pollen

◊(require pollen/core pollen/setup)

◊define-meta[title]{Background}
◊define-meta[chapter-type part]

This part is an overview of the place in which Ŋarâþ Crîþ is spoken.

Note that the state of Necarasso Cryssesa v6 and Ŋarâþ Crîþ v7 in Njôro is not necessarily identical to their state as described ◊link["https://gitlab.com/Kyarei/uruwi-clongos/"]{on Earth}, nor is the evolution of Necarasso Cryssesa in Njôro the same as on Earth.

◊(when/splice (equal? (current-poly-target) 'html)
  (embed-pagetree "index.ptree"))
