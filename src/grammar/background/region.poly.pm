#lang pollen

◊define-meta[title]{Ŋarâþ Crîþ in the world}

◊section[#:id "history"]{The history of Ŋarâþ Crîþ}

Languages play an important part of history, and Ŋarâþ Crîþ is no exception.

One of the major langauge families in Crîþja is the ◊term{Sylvic languages}, which can be traced back to present-day central Oripel in c. −1600. The family contains the following languages:

◊items{
    ◊item{South Sylvic (c. −700)}
    ◊item{East Sylvic (c. 300)◊;
    ◊items{
        ◊item{Near-East Sylvic (c. 1100)}
        ◊item{Far-East Sylvic (c. 1100)}
    }}
    ◊item{West Sylvic (c. 300)◊;
    ◊items{
        ◊item{P-West Sylvic (c. 677)◊;
        ◊items{
            ◊item{◊b{Necarasso Cryssesa}}
        }}
        ◊item{C-West Sylvic (c. 677)}
    }}
}

The South Sylvic branch diverged from the rest of the languages around the year −700. The rest of the family split into the East and West branches around 300.

In present-day Oripel and Arcelis, the West Sylvic languages were spoken. These included ◊term{Necarasso Cryssesa}, which would eventually become a prestige language in the region. Around the year 300, Necarasso Cryssesa, like many other West Sylvic languages, was largely head-initial and had two sex-based genders. Unsurprisingly for that time, the society of its speakers was quite patriarchial in ways that would be distasteful to mention in this grammar.

However, by c. 600, the vernacular variety of the Necarasso Cryssesa began to shift toward a head-final word order and gain a primitive case system, while the prestige register remained head-initial.

In 669, Enacssyr Malfa, who was the daughter of Enesor, the then-reigning king of Cressja, fled an arranged marriage and led a revolt against the ruling class. Her movement eventually seized the capital city in 677, leading to what would be known as the Naresa Reformation. Along with making sweeping reforms to the government and society (often in a heavy-handed manner), she enacted major changes to the language. The ◊term{New Form} (◊v6{trespo tegen}), as it was called, was based on the vernacular register and was head-final, but it additionally omitted grammatical gender and standardized the case system. These changes also spread to most of the West Sylvic languages, which would be known as P-West Sylvic languages, with the holdouts being classified as C-West Sylvic languages.

Necarasso Cryssesa underwent further changes until 1500, but it evolved surprisingly little after that point.

Necarasso Cryssesa was written with the ◊term{Old Cenvos Script}, which was invented in the year 0 by a person now referred to as ◊l1{#{nensâħa roħaliþ}} ◊trans{Protector of the Stars}. This script originally was unicameral, but it later gained lowercase letters.

In Ŋarâþ Crîþ, Necarasso Cryssesa is called ◊l1{ŋarâþ crîþ}, as Ŋarâþ Crîþ is considered a continuation of Necarasso Cryssesa despite the vast differences between the two languages. When the two languages must be distinguished, Necarasso Cryssesa is called ◊l1{nema ŋarâþ crîþ}, where ◊l1{nema} is borrowed from Necarasso Cryssesa ◊l1{nema} ◊trans{old} (and not the homophonous word meaning ◊trans{anything}).

The stages of Necarasso Cryssesa are assigned the integers from 1 to 6, inclusive, with 0 sometimes being used for its hypothetical predecessors. Strangely, the earliest stage refers to a time before the Sylvic family split; in fact, stage 1 coincides with the term ◊term{Nevasa}. The start of the Naresa era marks the start of 5, and the start of the Cenþed·rełis era marks the start of 6.

Meanwhile, the East Sylvic languages spread to the east. The first East Sylvic speakers migrated across the Anares Mountains around 1100, creating a split between the Near-East Sylvic and the Far-East Sylvic languages.

◊term{Ŋarâþ Crîþ v7} started to take shape in the late Senârmortos era and became codified with the Asoren–Viriþis alliance. The origin of Ŋarâþ Crîþ is unclear, but it is thought to have originated as a creole between Necarasso Cryssesa and Far-East Sylvic languages, with some influence from non-Sylvic languages and hardcore standardization.

During the Nerita Era, Ŋarâþ Crîþ spread throughout Crîþja, becoming either the dominant language or a minority but prestige language.

Toward the end of the Nerita era, Ŋarâþ Crîþ v7 started to evolve into daughter languages (◊nc{desorin}; sg. ◊nc{desoren}) in each region. However, Ŋarâþ Crîþ continued to be used as a learned language, as well as a ◊i{lingua franca} within the area. The (Society of Ŋarâþ Crîþ) was founded in 3645 to regulate the language, eventually creating ◊term{Ŋarâþ Crîþ v9}.

A surge of nationalism across the continent in the 3800s sparked an interest in the vernacular languages, elevating their status. As a result, some of these languages gained official or co-official status in their respective countries.

◊section[#:id "areas"]{Ŋarâþ Crîþ as spoken today}

The current Ŋarâþ Crîþ-speaking core consists of five countries: ◊term{Sadun} (◊l1{*@sadon}), ◊term{Viriþis}, ◊term{Asoren}, ◊term{Renselis}, and ◊term{Irnines}, with the periphery including ◊term{Lalaþne}, ◊term{Cfârneþf·lantis}, ◊term{Venoscrîþ}, and eastern ◊term{Aviro} and ◊term{Cþaliso}.

This grammar focuses mostly on the standard dialect of Ŋarâþ Crîþ, which is the prestige variety in all countries that recognize Ŋarâþ Crîþ as an official language. Technically, standard Ŋarâþ Crîþ has some differences across different areas, but these differences are minor enough that for the most part, it can be treated as a single variety.

In addition to the standard register, there are many national and regional varieties used for colloquial speech, often coexisting with and influenced by other languages in the area. These form a continuum with standard Ŋarâþ Crîþ.

In Asoren, colloquial Ŋarâþ Crîþ varieties can be classified into northern, southern, and inland groups. The Viriþian dialects can be classified into coastal and inland groups, with northern Asorenese dialects being similar to coastal Viriþian, and the inland Asorenese and Viriþian dialects being similar as well.

The coastal–inland distinction is also present in Sadun but is less pronounced than in Asoren or Viriþis.

Renselian dialects are also divided into coastal and inland groups, with an additional group for the area south of the Teriþos River.

In contrast, the dialects in Irnines are divided into central and peripheral dialects, based on whether they are spoken within the Irneþsarta mountain range or outside it. The peripheral dialects also form a dialect continuum with each other.

◊subsection[#:id "desorin-tecter"]{Other languages in Ŋarâþ Crîþ-speaking areas}

Alongside Ŋarâþ Crîþ exist other languages spoken in these areas. These are classified primarily into ◊term{desorin} (sg. ◊i{desoren}) and ◊term{tecter} (sg. ◊i{tectol}). ◊i{Desorin}, as previously mentioned, refer to languages that are descended from Ŋarâþ Crîþ v7, while ◊i{tecter} refer to languages that were spoken in the area before Ŋarâþ Crîþ was introduced, some of which are still spoken today.

In Asoren, the vernacular ◊term{Ŋarâþ Asoren} (also spoken in Viriþis as ◊term{Ŋarâþ Viriþis}) became the official language in some areas, but Ŋarâþ Crîþ remained official in others. As a result, different areas within the same country have different official languages, and the languages spoken in the household have typically followed whatever the official language is.

The capital of each nation (Cþeflje for Asoren and Vargiel for Viriþis) are officially bilingual. The government of Asoren has made several other cities bilingual, with some controversy.

[TODO: cover other ◊i{desorin} in terms of sociolinguistics]

In all of the regions, the case system of ŊCv7 was simplified or even lost completely, and the dual number was often lost. In some of the ◊i{desorin}, the celestial and terrestrial gender were merged into a ‘non-human’ gender.

◊section[#:id "other-languages"]{Languages spoken elsewhere}

Ŋarâþ Crîþ is only one of the four current hypercentral languages in Njôro (that is, there is no single language with as much influence as English has in this world), with the others being:

◊items{
  ◊item{◊term{(unnamed-l1)} – isolating and head-initial; uses logographic script; base-? numeral system}
  ◊item{◊term{(unnamed-l2)} – both positional and cased nouns; many light verb constructions; base-? numeral system}
  ◊item{◊term{(unnamed-l3)} – ?; base-? numeral system}
}

Almost all education systems teach at least one of these languages as a foreign language.