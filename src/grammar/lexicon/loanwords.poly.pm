#lang pollen

◊define-meta[title]{Loanwords}

Loanwords and foreign names are marked with a ◊i{nef}, ◊l1{*}.

In the case of Ŋarâþ Crîþ, however, what counts as a ‘loanword’ is more complicated than in many other languages. Inherited words are not loanwords, and words borrowed from Necarasso Cryssesa v6 are not considered loanwords, either. Words borrowed from ◊i{desorin} are usually not considered to be loanwords, but recent borrowings from ◊i{tecter} are. Borrowings from other languages are naturally considered loanwords, but calques are not.

Sometimes, a word may be marked with a ◊i{nef} for reasons unrelated to borrowing. For instance, the words ◊l1{*sedapat} ◊trans{female} and ◊l1{*moganit} ◊trans{male} were inherited from an East Sylvic language and were originally written without ◊i{nefs} until the late Senârmortos period. In this case, the ◊i{nefs} seemed to be added in order to discourage these words from being used. Interestingly, the stigma arising from these ◊i{nefs} does not seem to apply to true loanwords.

◊section[#:id "adaptation"]{Adaptation of foreign words}

◊subsection[#:id "adaptation-graphemic"]{Graphemic adaptation}

Generally, when borrowing from languages that use the Cenvos script or a script related to it, and whose orthographies in the script in question do not deviate too far from Ŋarâþ Crîþ usage, Ŋarâþ Crîþ prefers to borrow the word graphemically than phonemically.

[TODO: problems: phonotactics, use of foreign letters (◊l1{w x y z}, ◊l1{² ₂} diacritics)]

◊subsection[#:id "adaptation-phonetic"]{Phonetic adaptation}

◊subsection[#:id "adaptation-morphological"]{Morphological adaptation}

[TODO: particularly nouns]
