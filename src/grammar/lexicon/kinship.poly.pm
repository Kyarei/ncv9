#lang pollen

◊define-meta[title]{Kinship} ◊; TODO: rename to a more general ‘people’ chapter (also discussing different sapient species, gender &c.)

The most common kinship terms in Ŋarâþ Crîþ (◊xref/l["kinship-table"]{Table }) are determined not by the gender of the member, but rather whether it is the same or different as that of oneself. Derived terms are given using the period as used in programming languages (i.e. it should be read as the Japanese ◊ja{の}).

◊table/x[#:options (table-options #:caption "Kinship terms in Ŋarâþ Crîþ." #:first-col-header? #f) #:id "kinship-table"]{
  Term & Abbreviation & Gloss
  ◊nc{melco} & ssP & parent of same gender as self
  ◊nc{tfoso} & osP & parent of opposite gender as self
  ◊nc{nanda} & ssC & child of same gender as self
  ◊nc{laroþ} & osC & child of opposite gender as self
  ◊nc{armo} & ssSb & sibling of same gender as self
  ◊nc{melsas} & osSb & sibling of opposite gender as self
  ◊nc{veliša} & SP & spouse
}
