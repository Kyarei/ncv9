#lang pollen

◊define-meta[title]{Sensation & perception}

◊section[#:id "shape"]{Shape}

Shape can be perceived with different senses.

◊subsection[#:id "size"]{Size}

For overall size, Ŋarâþ Crîþ uses the verbs ◊l1{mervit} ◊trans{large} and ◊l1{nôrit} ◊trans{small}, the latter being colexified with ◊trans{young}. ◊l1{gadosit} is an intensified counterpart to ◊l1{mervit}.

◊section[#:id "vision"]{Vision}

The basic verb for seeing is ◊l1{menat}. ◊l1{vonat} and ◊l1{varmenat} imply a sense of volition on top.

◊subsection[#:id "brightness"]{Brightness}

For ◊trans{bright}, Ŋarâþ Crîþ uses the verb ◊l1{lirnat} for ◊trans{emitting a large amount of light} and ◊l1{csarneat} for ◊trans{well-lit}. The antonym of the former is the verb ◊l1{arelit} (also meaning ◊trans{difficult to see}). and that of the latter is the noun ◊l1{crîna} (also meaning ◊trans{black} or ◊trans{dark in color}).

◊subsection[#:id "colors"]{Color}

Ŋarâþ Crîþ has the six basic color terms. Interestingly, color terms are asymmetric syntactically: only two color terms have both a nominal and verbal form.

◊table/x[#:options (table-options #:caption "Basic color terms in Ŋarâþ Crîþ.") #:id "colors-table"]{
  Color & Noun & Verb
  Transparent & ◊nc{magen} & ◊nc{mirþit}
  Black & ◊nc{crîna} & —
  White & ◊nc{ineþa} & —
  Red & ◊nc{ceaþ} & ◊nc{censit}
  Green or blue & — & ◊nc{naðasit}
  Yellow & ◊nc{tfora} & —
}

Color terms can be used attributively by using the genitive singular forms for nominal forms and the participles for verbal forms.

Verbal color terms can be used predicatively as is. Nominal color terms can be used predicatively by using the relational ◊l1{čil} with the subject being the color and the object being the object with that color. If the colored object is not solid, then the verb ◊l1{eþit} is used with the object in the locative case.

In addition, ◊l1{crîna} and ◊l1{ineþa} are used for the characteristic of being dark or pale in general.

[TODO: RGB and CMY primary color terms]

◊section[#:id "sound"]{Sound}

The basic verb for hearing is ◊l1{crešit}, from which ◊l1{varešit} ◊trans{listen to a person speaking; read carefully} is derived.

◊subsection[#:id "pitch"]{Pitch}

For voices, ◊l1{firit} is used to refer to high pitch (colexified with ◊trans{thin (lamina)}); conversely, ◊l1{vrelat} is used to refer to low pitch (colexified with ◊trans{thick}).

◊subsection[#:id "loudness"]{Loudness}

The main verbs describing loudness are ◊l1{vregit} ◊trans{loud} and ◊l1{cicþit} ◊trans{soft}. Their more extreme counterparts are ◊l1{gelgačit} and the noun ◊l1{išiłte}.

◊subsection[#:id "timbre"]{Timbre}

◊l1{cličit} ◊trans{rough, grating}

◊section[#:id "touch"]{Touch}

◊l1{tecsat} is used for ◊trans{touch} or ◊trans{feel}.

temperature: ◊l1{ercit} ◊trans{cold}

hardness?: ◊l1{nêlit} ◊trans{soft}

texture: ◊l1{cličit} ◊trans{rough}

◊section[#:id "smell"]{Smell}

◊l1{ħacal} is used for ◊trans{smell} or ◊trans{odor}; ◊l1{calit} is to sense it.

◊section[#:id "taste"]{Taste}

◊l1{ifoma} is used for ◊trans{taste} and ◊trans{flavor}; ◊l1{evantat} is to sense it. Ŋarâþ Crîþ does not generally distinguish ◊i{taste} and ◊i{flavor}.

Terms for specific tastes include ◊l1{cełirat} ◊trans{sweet}, ◊l1{gelfat} ◊trans{bitter}, ? ◊trans{salty}, ? ◊trans{sour}, ? ◊trans{umami}, ◊l1{cþîrvit} ◊trans{spicy}, ? ◊trans{minty}, and ? ◊trans{fatty}.

Terms for intensity of taste include ? ◊trans{intense, flavorful} and ? ◊trans{bland}.

◊section[#:id "cognition"]{Cognition}
