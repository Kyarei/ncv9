#lang pollen

◊define-meta[title]{What is Ŋarâþ Crîþ?}

◊term{Ŋarâþ Crîþ} (◊l3{ŋa⁵ɹa²θ kɹi¹θ}; Cenvos: ◊cenvos{ŋarâþ crîþ}; lit. ◊trans{forest language}) is a constructed language by ◊link["https://flirora.xyz"]{+merlan #flirora} (◊l3{me⁵ɹla²n fli¹ɹo⁵wa⁴}; Cenvos: ◊cenvos{+merlan #flirora}). Originally called ◊term{Necarasso Cryssesa}, it is now on version 9.

The development of the language can be classified into four phases:

◊items{
  ◊item{
    In the first phase (2013 – 2014; VE¹ENCS – VE⁴ENCS), Necarasso Cryssesa was head-initial and had an SVO word order. The language was meant to have an elvish aesthetic. Successive versions of the grammar added more elaborate morphology.
  }
  ◊item{
    The second phase (2014 – 2016; NCS5 – NCS6) made drastic changes to the phonology and grammar in order to make it more like Japanese. For example, these versions had head-final word order and cases, and they lost articles and gender.
  }
  ◊item{
    After a hiatus, the author revisited the language, renaming it to ◊term{Ŋarâþ Crîþ}, with more radical changes than even NCS5. ◊term{ŊCv7} lasted from 2019 to 2021. In particular, the phonology was reworked, adding consonant mutations; agreement was made more plentiful; and gender was re-added (albeit not with a sex distinction).
  }
  ◊item{
    Lastly, the current phase consists of ◊term{ŊCv9}, which is the subject of this website. (Version 8 was skipped because 8 is ◊em{not} a lucky number.)
  }
}

Learn more about the ◊link["https://flirora.xyz/langdocs/reason4v7.html"]{history of Ŋarâþ Crîþ} (main site).

◊section[#:id "changes"]{Changes from Ŋarâþ Crîþ v7}

◊subsection[#:id "changes-phonology"]{Phonology and orthography}

The phonology of ŊCv9 is mostly a successor of ŊCv7’s phonology.

In the area of phonotactics, I have found that I dislike ◊l1{ŋ} as a coda, although I like it in the onset position. Therefore, ◊l0{ŋ} is no longer a valid coda, and final ◊l0{m} is drastically rarer.

ŊCv9 adds ◊l0{f}, as well as several consonant clusters, as valid codas. Most of the complex codas come from an abandoned ŊCv7 fork (also confusingly named “ŊCv9”), while ◊l0{f} and ◊l0{cþ} are genuinely new. Of course, more conversion rules must be added to handle the appearance of complex codas mid-word.

The “circumflexed vowels”, ◊l1{î ê ô â}, were pronounced with creaky voice in ŊCv7. Therefore, they were often pronounced with a low pitch as well. I have taken advantage of this pronunciation to make ŊCv9 a tonal language. After all, about half of the world’s languages are tonal, but the proportion among my conlangs is much lower.

ŊCv9 adds four true letters, ◊l1{w x y z}, which are not used natively but are reserved for Cenvos orthographies of foreign languages.

The properties of kerning and ligation were not documented in ŊCv7 but are in ŊCv9.

◊subsection[#:id "changes-morphology"]{Morphology}

The ablative, allative, prolative, and semblative cases from ŊCv7 were removed, decreasing the number of cases from 12 to 8. A generic number was added, however.

In ŊCv7, the inflectional paradigm of a noun could be deduced from the ending of its lemma. This is not possible in general in ŊCv9. There is also no way to regularly derive non-lemma stems from the lemma form.

Verbs in ŊCv7 were marked for aspect using eclipsis only. A different set of person–number affixes for distinguishing aspect has been introduced in ŊCv9 to make imperfective and perfective forms distinct, even when the initial consonant is not eclipsable. There are also multiple paradigms for forming verbal participles.

◊term{Relationals} in ŊCv9 function like ŊCv7’s postpositions, but they mark for attachment (either adnominal or adverbial) and can be used predicatively.

The long numerals up to six are now declined for both case and gender, while they were marked for case only in ŊCv7.

◊subsection[#:id "changes-syntax"]{Syntax}
