#lang pollen

◊define-meta[title]{Syntax}

For most of this text, we will be working with units that are the ◊term{sentence} or lower. A sentence consists of one or more ◊term{independent clause phrases} (ICPs), separated by ◊l1{;}, with a ◊l1{.}, ◊l1{?}, or ◊l1{!} at the end. An independent clause phrase can be a ◊term{general independent clause phrase} (gICP) or a ◊term{special independent clause phrase} (sICP).

A special independent clause phrase is one of an ◊term{interjection}, a ◊term{vocative}, or a ◊xref["syntax.html" "data-pos" "Section"]{◊term{datum}}. An interjection is a word in the “interjection” part of speech; interjections are not inflected. A vocative consists of a noun phrase in the dative case.

There are a few interjections, such as ◊l1{cirtel} ◊trans{by the way, incidentally} and ◊l1{olasta} ◊trans{in addition, furthermore, moreover} that can never appear on their own; they must be followed by another ICP in the same sentence. Others, such as ◊l1{cleli} ◊trans{of course, obviously}, can stand on their own, but when they occur before another ICP, they can naturally be interpreted as modifying it. To put it another way, such interjections on their own imply an ellipsed statement: “of course◊em{, that is the case}”. Such interjections constitute ◊term{dependent special independent clause phrases} (dsICPs) and ◊term{ambidependent special independent clause phrases} (asICPs). Of course, it can be argued that all sICPs are dsICPs or asICPs, as interjections such as ◊l1{meŋco} ◊trans{oh!, I see!, look!, indeed!} are related to what follows them. In any case, the presence of dsICPs and asICPs imply a structural level between the ICP and the sentence.

General independent clause phrases, on the other hand, are structurally more complex. We first look at the case of a single independent clause with no dependent clauses.

◊section[#:id "independent"]{Independent clauses}

An independent clause might or might not have a verb. We first look at the case where a verb is present.

If there is a verb, then it comes at the end of the clause (except before any tail particles). That is, arguments and adjuncts to the verb occur before it. Because Ŋarâþ Crîþ has cases, the relative order of noun phrases in a clause is usually insignificant, but the topic usually precedes the focus, and contrastive foci are often moved immediately after the topic (if any) or immediately before the verb.

Likewise, most modifiers precede their heads. The following do not, however:

◊items{
  ◊item{cardinal (as opposed to an ordinal) numerals}
  ◊item{the second part of most compound nouns}
}

Some types of modifiers agree with their heads and therefore can be moved away from them, as long as their relative order is preserved.

[TODO: order of modifiers]

◊subsection[#:id "independent-verbless"]{Verbless clauses}

Some independent clauses do not have a finite verb at the end. Nonetheless, they are treated as the head of a gICP and therefore can be a part of a ◊i{so}-clause.

◊items{
  ◊item{A clause with a nominative NP ◊var{x} alone implies the existence of whatever ◊var{x} refers to.}
  ◊item{A clause with two nominative NPs ◊var{x} and ◊var{y} equates the referents of ◊var{x} with the referents of ◊var{y}.}
  ◊item{A clause with a nominative NP ◊var{x} and an accusative NP ◊var{y} implies that the referents of ◊var{x} are a subset of the referents of ◊var{y}.}
  ◊item{A clause with a nominative NP ◊var{x} and an a semblative NP ◊var{y} implies that the referents of ◊var{x} are like the referents of ◊var{y} (as described by the semblative case).}
}

Verbless independent clauses can also occur when the primary verb of a clause modified by a converbal clause or a ◊i{so}-clause is the same as that of the subordinate clause and is ellipsed.

◊section[#:id "dependent"]{Dependent clauses}

These clauses are introduced in ◊xref/c["nouns.html" #:type 'inline]{Nouns} and ◊xref/c["verbs.html" #:type 'inline]{Verbs}:

◊items{
  ◊item{◊xref["nouns.html" "quotatives" "Section"]{◊term{Quotatives}}, which use a sentence wrapped inside quotation marks, followed by a particle}
  ◊item{◊xref["verbs.html" "participle" "Section"]{◊term{Relative clauses}}, which use a participle form of a verb}
  ◊item{◊xref["verbs.html" "converb" "Section"]{◊term{Converbal clauses}}, which use a converb}
  ◊item{◊xref["verbs.html" "so-clauses" "Subsection"]{◊term{◊i{So}-clauses}}, which use a gICP (i.e. finite form of a verb) plus a ◊term{◊i{so}-particle}}
  ◊item{◊xref["verbs.html" "nominalized" "Section"]{◊term{Nominalized clauses}}, which use a nominalized verb}
}

In all such clauses, the verb comes at the end of the clause (followed by a ◊term{◊i{so}-particle} for ◊i{so}-clauses).

◊section[#:id "particles"]{Head and tail particles}

Ŋarâþ Crîþ has both head and tail particles, which occur at the extremes of an ICP. ◊term{Absolute head particles} (◊term{aheadps}) appear at the beginning of an ICP:

◊items{
  ◊item{◊l1{ai} ◊trans{but, however} is used to contrast the idea of the clause in question with that of an earlier one.}
  ◊item{◊l1{ea} ◊trans{thus, therefore, in addition} is used to imply that the clause in question is the result of an earlier one, or that the clause in question adds information to an earlier one.}
  ◊item{◊l1{vjor} ◊trans{alternatively} is used to contrast a clause with an earlier alternative.}
}

◊term{Conjunct head particles} (◊term{cheadps}) appear at the beginning of an ICP, but if a so-clause is present, then it may occur at the start of the independent clause proper, immediately after the ◊i{so}-particle:

◊items{
  ◊item{◊l1{ša}, inside an ICP, indicates an interrogative sentence. This particle can also be used at the beginning of a ◊i{so}-clause, in which case it indicates an irrealis modality.}
  ◊item{◊l1{le} indicates an imperative or hortative modality.}
}

◊term{Tail particles} (◊term{tailps}) are used less often than head particles and often serve a pragmatic role. Omitting them can be seen as stoic. Prosodically, the final phoneme of a tail particle is often lengthened.

◊items{
  ◊item{◊l1{šan} indicates a tag question. If this particle is used, then ◊l1{ša} is omitted, but the ◊i{šac} is not.}
  ◊item{◊l1{þal} is used to make assertions. When used with the imperative, it marks a stronger imperative. When this particle appears after a word ending in ◊l1{-þ} but not in ◊l1{-cþ}, then the ending and the particle dissimilate into ◊l1{-s tal}.}
  ◊item{◊l1{se} indicates a rhetorical question or occasionally a mirative mood. Regardless of its use, it is not used with a ◊i{šac}.}
  ◊item{◊l1{viþca} marks the conditional mood.}
}

◊section[#:id "questions"]{Questions}

All questions contain either the cheadp ◊l1{ša} or, in the case of a tag question, the tailp ◊l1{šan}. If the last clause of a sentence is interrogative, then it is terminated by a ◊l1{?}. In colloquial speech, ◊l1{ša} may be omitted, but this is never done in song lyrics.

◊term{Polar questions} ask whether a statement is true and are created using the cheadp ◊l1{ša} on the statement that is questioned. They can be answered using ◊l1{vil} (the statement is true) or ◊l1{ces} (the statement is false).

◊gloss/x{
  ◊glfree{◊nc{ša lê tfoþos gðenuveþ?}}
  ◊gla{ša lê tfoþ-os gðen-u-ve-þ?}
  ◊glb{%int %this.%cel village-%loc.%sg give_birth-3%gc-2%sg-%past}
  ◊glfree{Were you born in this village?}
}

◊term{Tag questions}, which are created using the tailp ◊l1{šan} instead of ◊l1{ša}, are leading toward an affirmative answer. There is no separate way to create a leading question toward a negative answer.

◊term{◊i{Wh}-questions}, in addition to ◊l1{ša}, contain one or interrogative pro-forms, each of which can be an ◊xref["nouns.html" "pronouns-interrogative" "Subsection"]{◊term{interrogative pronoun}}, a noun phrase modified by the interrogative determiner ◊l1{mê} ◊trans{which}, or the pro-verb ◊l1{nepit}. The questioned element stays in its original position.

The following elements can be questioned:

◊items{
  ◊item{all noun phrases that are arguments or adjuncts the main clause}
  ◊item{all objects of relational phrases that are adjuncts to the main clause}
  ◊item{all complements of postpositional phrases that are adjuncts to the main clause}
  ◊item{the second element of commutative nominal coordinate structures and either element of noncommutative nominal coordinate structures, if the entire coordinate phrase could be replaced with an interrogative pronoun}
  ◊item{any verb in the main clause}
}

If an interrogative pronoun is modified, then the domain of answers is similarly restricted.

Answers to ◊i{wh}-questions are given in the same order as the interrogative pro-forms appear, with the same morphological forms.

◊term{Choice questions} list the options that the answer is expected to be selected from. In Ŋarâþ Crîþ, they are a special case of ◊i{wh}-question, in which the interrogative pronoun ◊l1{meel} ◊trans{which one?}, with the choices, joined by the coordinator ◊l1{=’ce}, being a genitive adjunct to that pronoun, is the element being questioned. In this case, ◊l1{meel} can appear wherever an interrogative pronoun could, and the answers take the same form as ◊l1{meel}. ◊l1{meel} is singular if exactly one answer is expected, but plural if there is no such expectation.

◊section[#:id "clitic-moc"]{The clitic ◊l1{=’moc}}

The clitic ◊l1{=’moc} can be translated to Japanese to ◊trans{◊ja{～も}} or to English as ◊trans{also} or ◊trans{even}. It can be applied to many different constituents:

◊items{
  ◊item{noun phrases}
  ◊item{attributive predicate phrases (verb participles, adverbial or adnominal relationals)}
  ◊item{finite predicative phrases?}
  ◊item{the ◊i{so}-particle ◊l1{so}, changing the meaning to ◊trans{even if}}
}

◊section[#:id "data-pos"]{Data}

A ◊term{datum} is one of the following:

◊items{
  ◊item{A short numeral (alone)}
  ◊item{A noun phrase in the nominative case}
  ◊item{An amount of ◊xref["numerals.html" "units-currency" "Subsection"]{currency}, denoted using the ◊l1{9{}} or the ◊l1{*9{}} numquote}
  ◊item{A list whose elements are data, denoted using the ◊l1{3{}} ◊xref["phonology.html" "numquotes" "Subsection"]{numquote} with elements separated by spaces. Elements with spaces on the outer level are grouped with ◊l1{{}}.}
  ◊item{A key-value list whose keys are arbitrary strings (but usually nominative-case noun phrases) and whose values are data. Each key is wrapped inside the ◊l1{3{}} numquote, followed by the corresponding value inside the ◊l1{4{}} numquote. The elements are collectively wrapped inside the ◊l1{2{}} numquote.}
}

A datum that is a list or a key-value list is called a ◊term{compound datum}.

Arbitrary strings can be contained inside a datum by casting them into nouns using a direct ◊xref["nouns.html" "quotatives" "section"]{quotative} particle. Alternatively, the particle ◊l1{neþþo} before a list or key-value list applies the quotative to each element of a list or to each value of a key-value list.

A datum by itself can be used as a sICP to convey the information contained therein. It can also be cast into a noun using a zero genitive construct, involving the noun ◊l1{manveo} ◊trans{datum} immediately followed by the datum. This noun can be replaced with a more specific term that describes the referent of the datum.
