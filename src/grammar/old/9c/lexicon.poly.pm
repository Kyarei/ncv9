#lang pollen

◊define-meta[title]{Lexicon}

Observations on the Ŋarâþ Crîþ lexicon that deserve their own section.

◊section[#:id "compounding"]{Compounding}

A noun can be formed by combining a noun or verb with a noun. The ‘head’ of the compound comes second. If the head noun is neither terrestrial nor a form of ◊l1{vês} ◊trans{system}, then it is lenited. Principal forms are inherited from the head noun.

A verb in a compound has its final ◊l0{-t} dropped.

◊section[#:id "derivation"]{Derivation}

◊subsection[#:id "verb2noun"]{Verb to noun}

◊subsubsection[#:id "verb2noun-agent-inanimate"]{The inanimate agent derivation}

The ◊term{inanimate agent derivation} forms a noun that describes an instrument that performs an action.

◊subsubsection[#:id "verb2noun-agent-animate"]{The animate agent derivation}

The ◊term{animate agent derivation} forms a noun that describes an animate being that performs an action. The final ◊l0{-t} of the infinitive is replaced with ◊l0{-dir}, forming a paradigm-3 noun. The L and S stems are identical to the N stem. Such a derived noun is usually human.

◊subsubsection[#:id "verb2noun-patient"]{The patient derivation}

◊subsubsection[#:id "verb2noun-location"]{The location derivation}

The ◊term{location derivation} forms a noun that describes a location at which an action happens. The final ◊l0{-t} of the infinitive is replaced with ◊l0{-łes}, forming a paradigm-1 noun. The S stem is identical to the N stem; the L stem replaces the final vowel of the N stem with ◊l0{u}.

◊subsubsection[#:id "verb2noun-quality"]{The quality derivation}

The ◊term{quality derivation} forms a noun that describes the action or quality described the verb. The ending of the infinitive is replaced with ◊l0{-erþ}, forming a paradigm-14 noun. In infinitives ending with ◊l0{-aħat} or ◊l0{-a(ħ)it}, such a suffix is replaced with ◊l0{-arþ} instead. In most cases, the L and S stems are automatically derived.

◊subsection[#:id "verb2verb"]{Verb to verb}

◊subsubsection[#:id "aspect"]{Other aspects}

The prefix ◊l0{es-} forms an inceptive or inchoative form of a verb (◊trans{start ~ing}), and ◊l0{car◊|sei|-} forms a cessative (◊trans{stop ~ing}): ◊l1{mitrit} ◊trans{run}; ◊l1{esmitrit} ◊trans{start running}; ◊l1{carmitrit} ◊trans{stop running}. The cessative prefix does not cause any mutation in inflected forms of ◊l1{eþit} or ◊l1{telit}.

The prefix ◊l0{er◊|sei|-} forms a terminative form of a verb (◊trans{finish ~ing}) and is applicable only to lexically telic verbs.

◊subsection[#:id "noun2noun"]{Noun to noun}

◊items{
  ◊item{Augmentative: ◊l0{ar-}}
  ◊item{Diminuitive: ◊l0{-in}, ◊l0{-(n)tin}, ◊l0{-(n)čin}, or ◊l0{e-}}
}

◊subsection[#:id "calculus"]{Calculus}

New words can also be derived by differentiating or integrating existing terms.

◊table/x[#:options (table-options #:caption "Calculus affixes in Ŋarâþ Crîþ.")]{
  With respect to & Derivative & Integral
  Time & ◊sc{ddt} ◊nc{mitra} & ◊sc{idt} ◊nc{arcja}
  Space (1D) & ◊sc{ddx} ◊nc{cþivo} & ◊sc{idx} ◊nc{jando}
  Space (2D) & ◊sc{dda} ◊nc{relen} & ◊sc{ida} ◊nc{senna}
  Space (3D) & ◊sc{ddxv} ◊nc{marša} & ◊sc{idxv} ◊nc{ganto}
  Population & ◊sc{ddp} ◊nc{gille} & ◊sc{idp} ◊nc{grija}
}

Each affix listed has a reciprocal counterpart. For most affixes, this is derived by inverting the tone of the first vowel of the affix (◊l0{-relen-} ◊trans{◊sc{dda}} → ◊l0{-rêlen-} ◊trans{◊sc{dda}.◊sc{rec}; ‘reciprocal of the derivative with respect to 2-dimensional space’}), but the reciprocal of ◊l0{-mitra-} is ◊l0{-genna-}.

◊subsubsection[#:id "calculus-verb"]{Verbs}

Different kinds of verbs can be modified with the calculus affixes by infixing them immediately after the last vowel of the stem. This cancels any vowel affections that the stem undergoes in finite or participle conjugation, but ◊xref["verbs.html" "impersonator" "Section"]{impersonator stems} are preserved. The ◊sc{dda} affix is realized as ◊l0{-relne-} instead in this case (with the reciprocal counterpart being ◊l0{-rêlne-}).

The affixes can be used on stative verbs, turning its meaning from ◊trans{◊S is high in ◊var{y}} to ◊trans{◊S is high in ◊${dy/dt} &c.}:

◊items{
  ◊item{◊l1{ecljat} ◊trans{◊S is far from ◊I}}
  ◊item{◊l1{e◊mark{mitra}cljat} ◊trans{◊S is moving quickly from ◊I}}
  ◊item{◊l1{e◊mark{arcja}cljat} ◊trans{◊S has a high absement from ◊I}}
  ◊item{◊l1{e◊mark{genna}cljat} ◊trans{◊S is moving slowly from ◊I}}
}

◊; expand use in the future?
Active verbs can take only ◊l0{-mitra-} and ◊l0{-genna-}, indicating the speed at which the action is done.

◊subsubsection[#:id "calculus-noun"]{Nouns}

◊subsubsection[#:id "calculus-amina"]{The numeral ◊l1{âmina}}

◊section[#:id "colors"]{Colors}

Ŋarâþ Crîþ has the six basic color terms. Interestingly, color terms are asymmetric syntactically: only two color terms have both a nominal and verbal form.

◊table/x[#:options (table-options #:caption "Colors in Ŋarâþ Crîþ.") #:id "colors-table"]{
  Color & Noun & Verb
  Transparent & ◊nc{magen} & ◊nc{mirþit}
  Black & ◊nc{crîna} & —
  White & ◊nc{ineþa} & —
  Red & ◊nc{ceaþ} & ◊nc{censit}
  Green or blue & — & ◊nc{naðasit}
  Yellow & ◊nc{tfora} & —
}

Color terms can be used attributively by using the genitive singular forms for nominal forms and the participles for verbal forms.

Verbal color terms can be used predicatively as is. Nominal color terms can be used predicatively by using the relational ◊l1{čil} with the subject being the color and the object being the object with that color. If the colored object is not solid, then the verb ◊l1{eþit} is used with the object in the locative case.

◊section[#:id "kinship"]{Kinship terms}

The most common kinship terms in Ŋarâþ Crîþ (◊xref/l["kinship-table"]{Table }) are determined not by the gender of the member, but rather whether it is the same or different as that of oneself. Derived terms are given using the period as used in programming languages (i.e. it should be read as the Japanese ◊ja{の}).

◊table/x[#:options (table-options #:caption "Kinship terms in Ŋarâþ Crîþ." #:first-col-header? #f) #:id "kinship-table"]{
  Term & Gloss
  ◊nc{melco} & parent of same gender as self
  ◊nc{tfoso} & parent of opposite gender as self
  ◊nc{nanda} & child of same gender as self
  ◊nc{laroþ} & child of opposite gender as self
  ◊nc{armo} & sibling of same gender as self
  ◊nc{melsas} & sibling of opposite gender as self
  ◊nc{veliša} & spouse
}
