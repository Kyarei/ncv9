#lang pollen

◊define-meta[title]{Relationals}

In this chapter, we cover relationals.

◊section{Valency and case frame}

All predicates have at least a nominative-case argument (the ◊term{subject}). Most relationals are divalent; the second argument (the ◊term{object}) is dative by default.

Some relationals, such as ◊l1{tfel} ◊trans{◊S is on the other side of ◊O relative to ◊O2}, are trivalent. In this case, one of the objects is termed the ◊term{main object} and the other is called the ◊term{ancilliary object}. In divalent relationals, of course, the main object is the only one involved.

If a relational encodes a spatial relationship between one entity and another, then two additional relationals (◊term{relationals of motion}) are derived: one encoding motion toward the main object (in which case the main object is accusative) and another encoding motion away from the main object (in which case it is abessive).

◊section[#:id "attachment"]{Attachment and relational bias}

A relational can be used ◊term{adnominally} or ◊term{adverbially}, that is, modifying either a noun phrase or a verb phrase. We call this distinction ◊term{attachment}. Adnominal usage simply places the affected noun phrase as the subject of the action depicted by the relational. Adverbial usage, on the other hand, does one of the following: (1) place the action depicted by the VP as the subject of the relational, (2) indicate that as a result of the action, a shifted subject of the VP starts to participate as a subject of the relational.

All relationals have a ◊term{bias} toward either adnominal or adverbial attachment. A relational with adnominal bias takes its lemma form when modifying a noun phrase but changes form when modifying a verb phrase. Conversely, a relational with adverbial bias takes its lemma form when modifying a verb phrase but changes form when modifying a noun phrase.

Relationals of motion always have adverbial bias, even when the base relational has adnominal bias.

◊section[#:id "modforms"]{Modifying forms}

The anatomy of a modifying form is ◊b{[<object prefix>] + <lemma> + [<cast suffix>] + [<motion suffix>]} for celestial relationals and ◊b{[<cast prefix>] + [<object prefix>] + <lemma> + [<motion suffix>]} for terrestrial relationals.

For celestial relationals, the ◊term{cast suffix} is used if the relational has the wrong bias for its usage. It is ◊l0{-en} when an adverbially biased relational is used adnominally, and it is ◊l0{-al} when an adnominally biased relational is used adverbially.

Likewise, the ◊term{cast prefix} is used for terrestrial relationals in the same situation. It is ◊l0{i-} when an adverbially biased relational is used adnominally, and it is ◊l0{as-} when an adnominally biased relational is used adverbially.

The object prefix is like the object affix in verb conjugation; in other words, it is used if the object is pronominal. Nevertheless, it has different forms:

◊table/x[#:options (table-options #:caption "Object affixes." #:placement 'please-here)]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{e(l)-} & ◊nc{ec-} & ◊nc{en-} & ◊nc{ef-}
  1st incl. & & ◊nc{êc-} & ◊nc{ên-} &
  2nd & ◊nc{o-} & ◊nc{oc-} & ◊nc{on-} & ◊nc{of-}
  3rd celestial & ◊nc{er-}
  3rd terrestrial & ◊nc{os-}
  3rd human & ◊nc{an-} & ◊nc{or-} & ◊nc{ran-}
  3rd epicene &  & ◊nc{ac-} & ◊nc{ren-} & ◊nc{fê(s)-}
  Reflexive & ◊colspan[4]{◊nc{ce(n)-}}
  Reciprocal & ◊colspan[4]{◊nc{re(þ)-}}
}

(The consonants in brackets are included only if the lemma starts with ◊l0{e-} or ◊l0{ê-}.)

On relationals of motion, a motion suffix can be added to denote motion. The suffix for motion ◊trans{toward} something is usually ◊l0{-ar}, but when the lemma ends with ◊l0{a} or ◊l0{â} and the cast suffix ◊l0{-en} is absent, then the tone of the final vowel is inverted and ◊l0{-r} is attached.

The suffix for motion ◊trans{away from} something can be either ◊l0{-es}, ◊l0{-as}, or ◊l0{-jas}:

◊items{
  ◊; you fool, ◊l0{-al} can't occur with a suffix of motion
  ◊; ◊item{It is always ◊l0{-es} if the form has the cast suffix ◊l0{-al}.}
  ◊item{It is always ◊l0{-as} if the form has the cast suffix ◊l0{-en}.}
  ◊item{Otherwise, if the lemma ends in a vowel, then it is always ◊l0{-jas}.}
  ◊item{Otherwise, if the last syllable of the lemma has an ◊l0{e} or ◊l0{ê}, then it is always ◊l0{-as}.}
  ◊item{Otherwise, if the last syllable of the lemma has an ◊l0{a} or ◊l0{â}, then it is always ◊l0{-es}.}
  ◊item{Otherwise, it is ◊l0{-es} if bit 1 of the letter sum of the preceding letters is set and ◊l0{-as} if it is unset.}
}

In trivalent relationals, the ancilliary object affixes occur at the end of the relational:

◊table/x[#:options (table-options #:caption "Ancilliary object affixes." #:placement 'please-here)]{
  Person \ Number & Singular & Dual & Plural & Generic
  None & ◊nc{-(e)s}
  1st excl. & ◊nc{-ef} & ◊nc{-ecþ} & ◊nc{-if} & ◊nc{-af}
  1st incl. & & ◊nc{-êcþ} & ◊nc{-îf} &
  2nd & ◊nc{-or} & ◊nc{-ocþ} & ◊nc{-orþ} & ◊nc{-of}
  3rd celestial & ◊nc{-ir}
  3rd terrestrial & ◊nc{-jos}
  3rd human & ◊nc{-ran} & ◊nc{-lor} & ◊nc{-ren}
  3rd epicene &  & ◊nc{-ac} & ◊nc{-erþ} & ◊nc{-∅}
  Reflexive & ◊colspan[4]{◊nc{-lef}}
  Reciprocal & ◊colspan[4]{◊nc{-rin}}
}

(The suffix for an explicit ancilliary object is ◊l0{-s} after a vowel or a ◊l0{-l}.)

If both the main and ancilliary objects are specified as noun phrases to an attributive relational, then the ancilliary object is eclipsed and follows the main object.

◊section[#:id "finforms"]{Finite forms}

A relational can be used predicatively using a finite form that attaches it to a scaffolding verb, either affirmative or negative. Thus the relational acts like a verb syntactically.

The anatomy of the finite form of a relational is ◊b{[<motion prefix>] + <lemma> + <finite form of scaffolding verb>}. The scaffolding verb can be either ◊l1{eþit} (for the affirmative) or ◊l1{telit} (for the negative).

The motion prefix is ◊l0{ar-} for motion ◊trans{toward} something and ◊l0{as-} for for motion ◊trans{away from} something.

The relational ◊l1{es} ◊trans{inside} changes to ◊l1{el} when in a finite form; likewise, ◊l1{car} ◊trans{outside} changes to ◊l1{caþ}.

The object affix on the scaffolding verb refers to the main object in divalent relationals. In trivalent relationals, it refers to the main object by default, but if the main object is explicitly specified as a noun phrase, then the object affix refers to the ancilliary object instead.

If the ancilliary object is specified as a noun phrase, then it is preceded by the particle ◊l1{os} and eclipsed.

If a relational is a target of an auxiliary, then the scaffolding verb contracts to ◊l0{-is} for ◊l1{eþit} and ◊l0{-cest} for ◊l1{telit}. If such a relational previously governed the dative, then it now governs the accusative in this case.

◊section[#:id "nomforms"]{Nominalized forms}

The nominalized form of a verb describes the action referenced by the verb. In contrast, the nominalized form of a relational describes the subject involved in the state described.

The ◊term{nominal lemma} is the lemma form plus a possible motion suffix, followed by ◊l0{-l} if this would end in a vowel.

Celestial relationals add the suffix ◊l0{-er}, resulting in a paradigm 3 noun with the N, L, and S stems being the lemma form (plus a possible motion suffix).

Terrestrial relationals add the suffix ◊l0{-os}, resulting in a paradigm 7 noun with the N and S stems being the lemma form (plus a possible motion suffix) and the L stem being the lemma form plus ◊l0{-av-}.

In either case, a genitive on such a noun indicates the object of the state described.

◊section{Interactions with predicate modifiers}

A noun phrase in the accusative case plus the clitic ◊l1{=’po} is a predicate modifier that acts on relationals implying separation between two objects (spatially or temporally) and describes the degree to which they are separated.

The particle ◊l1{pâ} directly before a relational describing a spatial relationship can be translated as ◊trans{directly} or ◊trans{precisely}. With the relational ◊l1{nîs} describing a span of time over which an action takes place, ◊l1{pâ} implies that the action is continuous.

Prefixing ◊l1{do-} to a relational switches the order of ◊S and ◊|O|; the gender, bias, and governed case are preserved. Such a relational is used only attributively. That is, the same prefix on a finite relational is interpreted as a causative prefix as usual.

◊section[#:id "reltour"]{A tour of relationals}

This section gives an overview of the relationals of Ŋarâþ Crîþ.

◊subsection[#:id "reltour-spatial"]{Spatial relationals}

◊table/x[#:options (table-options #:caption "Spatial relationals in Ŋarâþ Crîþ." #:first-col-header? #f #:placement 'please-here) #:id "spatial-relationals"]{
  Relational & Gloss
  ◊nc{ar} & ◊trans{toward}
  ◊nc{jas} & ◊trans{away from}
  ◊nc{nîs} & ◊trans{through}
  ◊nc{âŋa} & ◊trans{bending toward}
  ◊nc{es} & ◊trans{inside}
  ◊nc{car} & ◊trans{outside of}
  ◊nc{il} & ◊trans{on top of}
  ◊nc{sêna} & ◊trans{above}
  ◊nc{čil} & ◊trans{on (a vertical surface)}
  ◊nc{desa} & ◊trans{below}
  ◊nc{etor} & ◊trans{in front of}
  ◊nc{þon} & ◊trans{in the midst of, in the middle of}
  ◊nc{cþar} & ◊trans{around, surrounding}
  ◊nc{cþarnîs} & ◊trans{revolving around}
  ◊nc{fan} & ◊trans{next to, beside}
  ◊nc{nerła} & ◊trans{between}
  ◊nc{tfel} (3val) & ◊trans{across}
  ◊nc{lef} & ◊trans{perpendicular to}
}

Some words denoting such relations are verbs instead of relationals:

◊items{
  ◊item{◊l1{ecljat} = ◊trans{◊S is far from ◊I}}
  ◊item{◊l1{cþîšat} = ◊trans{◊S is near ◊O}}
}

◊subsection[#:id "reltour-temporal"]{Temporal relationals}

◊table/x[#:options (table-options #:caption "Temporal relationals in Ŋarâþ Crîþ." #:first-col-header? #f #:placement 'please-here) #:id "temporal-relationals"]{
  Relational & Gloss
  ◊nc{tecto} & ◊trans{before}
  ◊nc{mîr} & ◊trans{after}
  ◊nc{nîs} & ◊trans{during, while}
}

Some words denoting such relations are verbs instead of relationals:

◊items{
  ◊item{◊l1{cjašit} = ◊trans{◊S begins at the time of ◊I}}
}

◊subsection[#:id "reltour-syntactic"]{Syntactic relationals}

These relationals are used solely for syntactic support.

◊items{
  ◊item{◊l1{ro} marks the former indirect object when the causative voice is applied on a ditransitive verb.}
  ◊item{◊l1{peŋan} is used to mark the compared object in an equal comparison.}
  ◊item{◊l1{îþ} forms the superlative. It is defined as ◊trans{◊S performs an action to the greatest extent in or among ◊|O|}.}
}

◊subsection[#:id "reltour-mathematical"]{Mathematical relationals}

These relationals denote mathematical relations.

◊items{
  ◊item{◊l1{ema} = ◊trans{other than, not equal to}. Its antonym is the verb ◊l1{censit}.}
  ◊item{◊l1{cor} = ◊trans{not one of}. Its antonym is the verb ◊l1{varit}.}
}

Some words denoting such relations are verbs instead of relationals:

◊items{
  ◊item{◊l1{censit} = ◊trans{◊S is equal to ◊I}}
  ◊item{◊l1{varit} = ◊trans{◊S is one of ◊O}}
  ◊item{◊l1{mirit} = ◊trans{◊S is greater than ◊O by a margin of ◊I}}
  ◊item{◊l1{łavrit} = ◊trans{◊S is less than ◊O by a margin of ◊I}}
}

◊subsection{Other relationals}

◊items{
  ◊item{◊l1{dêt} = ◊trans{instead of}}
  ◊item{◊l1{ton}: ornative = ◊trans{◊S has ◊|O| attached to it as a feature or accessory}}
  ◊item{◊l1{vôr}: ◊trans{◊O is abundant within ◊|S|}}
  ◊item{◊l1{roc} (becoming ◊l1{rille} after the clitic ◊l1{=’moc}) = ◊trans{on behalf of}}
  ◊item{◊l1{nedo} = ◊trans{◊S happens in spite of ◊|O|}}
  ◊item{◊l1{uc} = ◊trans{◊S resembles ◊O in appearance, visually or otherwise}}
  ◊item{◊l1{mesa} and ◊l1{rjas} both translate to ◊trans{between}…}
  ◊item{◊l1{lef}, in addition to its spatial meaning, is used to mean ◊trans{unrelated to}.}
  ◊item{◊l1{es}, in addition to its spatial meaning, is used for the progressive aspect.}
  ◊item{◊l1{desa}, in addition to its spatial meaning, is used to mean ◊trans{regarding} or ◊trans{related to}.}
}
