#lang pollen

◊define-meta[title]{Orthography and phonology}

The phonology and orthography of Ŋarâþ Crîþ can be divided into eight layers in two modes (◊term{writing} and ◊term{speaking}):

◊items{
  ◊item{◊term{Layer 0} is the underlying morphographemic representation. In this grammar, text in this layer is written in double square brackets: ◊l0{tanc-a}.}
  ◊item{◊term{Layer 1} is the graphemic representation. This representation is subsequently exported to the spoken and written modes. Text in this layer is written with angle brackets: ◊l1{tanca}.}
  ◊item{◊term{Layer 2w} is the surface glyphic representation. This represents the sequence of Cenvos glyphs that is written, observing required ligatures and final forms. Text in this layer is written with double angle brackets: ◊l2w{tanca}; for a more interesting example, ◊l1{mencoc} becomes ◊l2w{◊lig{me}nco◊lig{c$}}.}
  ◊item{◊term{Layer 2w*} is an intermediate layer between 2w and 3w, in which discretionary ligatures are introduced to 2w text. For instance, ◊l2w{#flirora} can be realized as ◊l2w*{#fli◊lig{ro}◊lig{ra}}.}
  ◊item{◊term{Layer 3w} is the topological representation, showing optional ligatures as well as stroke order variations. Text in this layer is written with double angle brackets: ◊l3w{◊sym{t1;1}◊sym{a1;3}◊sym{n1;1}◊sym{c1;1}◊sym{a1;1}}. More interestingly, ◊l2w{◊lig{me}nco◊lig{c$}} could become ◊l3w{◊sym{me1;1}◊sym{n1;1}◊sym{c1;1}◊lig{◊sym{o1;1}◊sym{c$1;1}}}.}
  ◊item{◊term{Layer 4w} is the presentational representation, adding to 3w variations in the strokes themselves and how strokes within a glyph are joined. Text in this layer is written with double angle brackets: ◊l4w{◊sym{t1;1}◊sym{a1;3}◊sym{n1;1}◊sym{c1;1}◊sym{a1;1}}.}
  ◊item{◊term{Layer 2s} is the phonemic representation. We use slashes for this, as usual: ◊l2{tanka}.}
  ◊item{◊term{Layer 3s} is the phonetic representation, or what is pronounced. We use square brackets for this, as usual: ◊l3{tʰa⁴ɲcʰa²}.}
}

The conversions from 0 to 1, 1 to 2w, and 2s to 3s are functional: each valid input corresponds to exactly one output. The conversion from 1 to 2s is almost so, except when a ◊l1{&} is present. In the opposite direction, the conversions from 4w to 3w, from 3w to 2w*, and from 2w* to 2w are functional. Furthermore, for any conversion, it can be determined whether a given input can be converted into a given output without external information.

In addition, the conversion between 1 and 2w is bijective: valid layer-1 and layer-2w representations can be paired with each other.

◊section[#:id "layer-01"]{Layers 0, 1, and 2w: Cenvos and its romanization}

◊term{Cenvos}, the native script of Ŋarâþ Crîþ, is written from right to left. This script can be analyzed on two levels: ◊term{graphemes}, which constitute the abstract level and ◊term{glyphs}, which are the characters being written. For instance, Cenvos has one grapheme romanized as ◊l1{c} that corresponds to two different glyphs: the non-final form ◊cenvos{c~} (denoted as ◊l2w{c}) and the final form ◊cenvos{c} (◊l2w{◊lig{c$}}). As another example, the sequence ◊cenvos{me} (◊l1{me} = ◊l2w{◊lig{me}}) consists of one glyph but two graphemes.

In this grammar, we primarily use the romanization, whose symbols largely map one-to-one with Cenvos graphemes. Cenvos has four kinds of graphemes:

◊items{
  ◊item{◊term{True letters} are graphemes that represent sounds.}
  ◊item{◊term{Markers}, while considered letters, do not represent sounds. Instead, they indicate that the words affected are treated specially. They occur on the level of a word and do not actively participate in morphology.}
  ◊item{◊term{Punctuation} includes the clause-end punctuation ◊l1{.}, ◊l1{;}, ◊l1{?}, and ◊l1{!}; the clitic boundary mark ◊l1{’}; the lenition mark ◊l1{·}; the grouping brackets ◊l1{{}}; and the quotation marks ◊l1{«»}.}
  ◊item{◊term{Digits} can be used to write short numerals.}
}

Of course, there is also the space. Layer 0 also contains the morpheme boundary, ◊l0{-}.

◊(define (row c n r)
  `(@! ,(cenvos c) "&" ,(nc n) "&" ,(nc r)))

◊table/x[
  #:options (table-options
    #:caption "The graphemes of Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:colgroup '(c c rb c c rb c c c)
    #:first-col-header? #f
  )
]{
  Cen & Name & Rom & Cen & Name & Rom & Cen & Name & Rom
  ◊pseudoheader{True letters}
  ◊(row "c~" "ca" "c") & ◊(row "m" "ma" "m") & ◊(row "h" "ar" "h")
  ◊(row "e" "e" "e") & ◊(row "a" "a" "a") & ◊(row "ħ" "ħo" "ħ")
  ◊(row "n" "na" "n") & ◊(row "f" "fa" "f") & ◊(row "ê" "ên" "ê")
  ◊(row "ŋ~" "ŋa" "ŋ") & ◊(row "g" "ga" "g") & ◊(row "ô" "ôn" "ô")
  ◊(row "v" "va" "v") & ◊(row "p" "pa" "p") & ◊(row "â" "ân" "â")
  ◊(row "o" "o" "o") & ◊(row "t" "ta" "t") & ◊(row "u" "uħo" "u")
  ◊(row "s" "sa" "s") & ◊(row "č" "ča" "č") & ◊(row "w" "cełaŋa" "w")
  ◊(row "þ" "þa" "þ") & ◊(row "î" "în" "î") & ◊(row "x" "avarte" "x")
  ◊(row "š" "ša" "š") & ◊(row "j" "ja" "j") & ◊(row "y" "priþnos" "y")
  ◊(row "r" "ra" "r") & ◊(row "i" "i" "i") & ◊(row "z" "telrigjon" "z")
  ◊(row "l" "la" "l") & ◊(row "d" "da" "d")
  ◊(row "ł" "ła" "ł") & ◊(row "ð" "ða" "ð")
  ◊pseudoheader{Final forms and ligatures (layer 2w)}
  ◊(row "c" "" (lig "c$")) & ◊(row "me" "" (lig "me")) & ◊(row "ww" "" (lig "ww"))
  ◊(row "ŋ" "" (lig "ŋ$")) & ◊(row "mm" "" (lig "mm")) & ◊(row "xx" "" (lig "xx"))
  ◊(row "ee" "" (lig "ee")) & ◊(row "jâ" "" (lig "jâ")) & ◊(row "yy" "" (lig "yy"))
  ◊(row "em" "" (lig "em")) & ◊(row "âj" "" (lig "âj")) & ◊(row "zz" "" (lig "zz"))
  ◊pseudoheader{Markers}
  ◊(row "#" "carþ" "#") & ◊(row "×" "njor" "+*") & ◊(row "*" "nef" "*")
  ◊(row "+" "tor" "+") & ◊(row "@" "es" "@") & ◊(row "&" "sen" "&")
  ◊pseudoheader{Punctuation}
  ◊(row "." "gen" ".") & ◊(row "'" "ŋos" "’") & ◊(row "«" "fos" "«")
  ◊(row ";" "tja" ";") & ◊(row "·" "łil" "·") & ◊(row "»" "þos" "»")
  ◊(row "?" "šac" "?") & ◊(row "{" "rin" "{") & ◊(row "/" "jedva" "/")
  ◊(row "!" "cjar" "!") & ◊(row "}" "cin" "}") & ◊(row "-" "mivaf·ome" "-")
}

The letters ◊l1{w}, ◊l1{x}, ◊l1{y}, and ◊l1{z} are ◊term{USR letters}. These are used in foreign languages written in Cenvos to represent phonemes that are not approximated by the phonology of Ŋarâþ Crîþ. Each foreign orthography is free to assign them as it pleases.

Cenvos has two graphemes that change form at the end of the word: ◊l1{c} and ◊l1{ŋ}, as well as several ligatures. We do not distinguish these forms in the romanization.

The marker ◊l1{*} is used for foreign words, such as loanwords and foreign names. ◊l1{#} is used to prefix given names. ◊l1{+} is used to prefix surnames passed by native conventions (i.e. from parent to child within the same gender); ◊l1{+*} marks a surname passed using non-native conventions. Place names are prefixed with ◊l1{@}. ◊l1{#}, ◊l1{+}, ◊l1{+*}, and ◊l1{@} can all be used with ◊l1{*}, in which case ◊l1{*} occurs first. Note that ◊l1{+*} is a single letter of its own and not a ligature.

At the start of a word, ◊l1{&} indicates reduplication of an unspecified prefix of the rest of the word. For instance, ◊l1{&cên} can be pronounced as if it were ◊l1{cêcên} or ◊l1{cêncên}. (◊l1{&} occurs after all other markers in this case.) This usage is not productive in standard Ŋarâþ Crîþ, but it appears in a few words, as well as in some idiosyncratic cases. At the middle or the end of a word, or alone, it indicates ellipsis of part or all of the word, most often to abbreviate or censor a word. Lastly, ◊l1{&{}} is used similarly to the ellipsis in Western punctuation.

Markers can be applied to multi-word strings by surrounding the string with the delimiters ◊l1{{}}. In legal language, ◊l1{{}} are also used around phrases to resolve ambiguities.

The sentence punctuation ◊l1{.}, ◊l1{?}, and ◊l1{!} are used as expected. ◊l1{;} is used to separate two independent clause phrases within the same sentence. The quotation marks, ◊l1{«»}, are used around quotations, direct or indirect. A ◊l1{.} at the end of a quotation embedded within another sentence is omitted.

◊l1{’} is used to separate clitics from the rest of the word to which they are attached. ◊l1{·} indicates lenition; it could be described as a “letter modifier”. It is also used as a decimal point: officially, it is used after the most significant digit of an inexact numeral when written with digits, but it also used unofficially to write non-integers.

◊l1{/}, as its derivation from ◊l1{i} suggests, is used to separate the number of ◊i{mjari} from the number of ◊i{edva} when writing currency amounts.

Spaces are placed in the following places:

◊items{
  ◊item{between orthographic words, but not between a clitic and the word to which it is attached}
  ◊item{after (but not before) ◊l1{.}, ◊l1{;}, ◊l1{?}, and ◊l1{!}}
  ◊item{before ◊l1{«} and after ◊l1{»} (but not on the other sides)}
  ◊item{around ◊l1{&{}}}
}

[TODO: cover mentions of letters within the language, corresponding to v7 p17 “When letters or markers are referred to, … but the effects on other glyphs are not standardized”]

Digits are interchangeable with short-form numerals, but not with long-form numerals. They are also written right-to-left in Cenvos, with the most significant digit first: ◊cenvos{2A3} is 0x2A3 = 675.

◊table/x[
  #:options (table-options
    #:caption "The digits of Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:first-col-header? #f
  )
]{
  Cen & # & Cen & # & Cen & # & Cen & #
  ◊cenvos{0} & ◊nc{0} & ◊cenvos{1} & ◊nc{1} & ◊cenvos{2} & ◊nc{2} & ◊cenvos{3} & ◊nc{3}
  ◊cenvos{4} & ◊nc{4} & ◊cenvos{5} & ◊nc{5} & ◊cenvos{6} & ◊nc{6} & ◊cenvos{7} & ◊nc{7}
  ◊cenvos{8} & ◊nc{8} & ◊cenvos{9} & ◊nc{9} & ◊cenvos{A} & ◊nc{A} & ◊cenvos{B} & ◊nc{B}
  ◊cenvos{C} & ◊nc{C} & ◊cenvos{D} & ◊nc{D} & ◊cenvos{E} & ◊nc{E} & ◊cenvos{F} & ◊nc{F}
}

◊subsection[#:id "phonotactics"]{Phonotactics}

We express the phonotactic rules of Ŋarâþ Crîþ in terms of layer 0.

A ◊term{manifested grapheme phrase} is either a true letter not followed by a lenition marker (◊term{plain letter}), any of ◊l0{p t d č c g m f v ð} followed by a lenition mark (◊term{lenited letter}), or, word-initially, one of the digraphs ◊l0{mp vp dt nd gc ŋg vf ðþ lł} (◊term{eclipsed letter}). All other graphemes are ignored for the purposes of phonotactics.

A manifested grapheme phrase has a ◊term{base letter}. The base letter of a plain letter is itself. The base letter of a lenited letter is the letter without the lenition mark. The base letter of an eclipsed letter is the second letter of the digraph.

A vowel is any of ◊l0{e o a î i ê ô â u}. ◊l0{j} is a semivowel. All other manifested grapheme phrases are consonants.

An ◊term{effective plosive} is a manifested grapheme phrase whose base letter is any of ◊l0{p t d c g}. An ◊term{effective fricative} is a manifested grapheme phrase whose base letter is any of ◊l0{f v þ ð s š h ħ}.

A word consists of one or more ◊term{syllables}, each of which has an ◊term{initial}, a ◊term{medial}, a ◊term{nucleus}, and a ◊term{coda}. An initial consists of one of the following:

◊items{
  ◊item{nothing at all}
  ◊item{a single consonant}
  ◊item{an effective plosive or fricative plus ◊l0{r} or ◊l0{l}}
  ◊item{any of ◊l0{cf cþ cs cš gv gð tf dv}; that is, a plosive plus a fricative of the same voicing, such that the plosive has a more retracted place of articulation than the fricative}
}

The only valid medial, if present, is ◊l0{j}. A nucleus is a vowel.

A coda is either a ◊term{simple coda} or a ◊term{complex coda}. A simple coda is one of ◊l0{s r n þ rþ l t c f m} or nothing at all. A complex coda is one of ◊l0{st lt ns ls nþ cþ}. While complex codas are allowed in any syllable in layer 0, instances of such codas in the middle of a syntactic word are simplified during the conversion to layer 1, and such instances immediately before a clitic boundary are simplified during the conversion to layer 2. The coda ◊l0{-m} is used in only a few words.

In addition, ◊l0{h} is forbidden word-initially. Doubled consonants and vowels are allowed.

If there is more than one way to split a word into syllables, the maximal-onset principle is used. However, clitic boundaries always start a new syllable.

An ◊term{onset} is an initial plus a medial. A ◊term{bridge} is the coda of one syllable plus the onset of the following syllable.

◊subsection[#:id "conv-01"]{Conversion from layer 0 to layer 1}

The following changes are applied as a part of morphology. They occur only when the subsequence involved in a change (that is, the substring being replaced as well as the environment that triggers the change) crosses a morpheme boundary but not a word boundary. For instance, ◊l0{*@vav-el} becomes ◊l1{*@vavel} instead of ◊l1{*@navel}. For clarity, however, we omit any ◊l0{-}s from the rules below. (These changes apply from left to right.)

◊items/dense{
  ◊item{v → n / _ V[-creaky] {v, m·}}
  ◊item{ð → ŋ / _ V[-creaky] {ð, d·}}
}

Here, “V[-creaky]” means any of ◊l0{e o a i u}.

◊anchor{simplify-complex-clusters}

The following changes are made to simplify complex codas within a syntactic word ◊em{if (and only if) the consonant cluster cannot be reinterpreted to avoid the mid-word complex coda.}

◊items/dense{
  ◊item{stšr → šr}
  ◊item{stšl → šl}
  ◊item{stš → sč}
  ◊item{sts → st}
  ◊item{st → t / _ C[+nasal]}
  ◊item{st → s / _ C}
  ◊item{ltšr → ltr}
  ◊item{ltšl → ltl}
  ◊item{ltš → lč}
  ◊item{lts → ls}
  ◊item{lt → t / _ C[+nasal]}
  ◊item{lt → l / _ C}
  ◊item{ss → þ / {n, l} _}
  ◊item{ns → n / _ C[+obstruent]²}
  ◊item{C[+coronal, -voiced] → ∅ / ns _}
  ◊item{ns C[+coronal, +voiced] → nð}
  ◊item{ns {ħ, g·} → nð}
  ◊item{ns C[+dorsal, -voiced] → nh}
  ◊item{ns C[+dorsal, +voiced] → ŋ / _ V}
  ◊item{ns C[+dorsal, +voiced] → n}
  ◊item{ns C[+labial, -voiced] → nf}
  ◊item{ns C[+labial, +voiced] → nv}
  ◊item{ls → l / _ C[+obstruent]²}
  ◊item{C[+coronal, -voiced] → ∅ / ls _}
  ◊item{ls C[+coronal, +voiced] → lð}
  ◊item{ls {ħ, g·} → lð}
  ◊item{ls C[+dorsal, -voiced] → lh}
  ◊item{ls C[+dorsal, +voiced] → lħ}
  ◊item{ls C[+labial, -voiced] → lf}
  ◊item{ls C[+labial, +voiced] → lv}
  ◊item{nþ → þ / _ C[+obstruent]²}
  ◊item{nþ C[-voiced] → nþ}
  ◊item{nþ C[+voiced] → nð}
  ◊item{cþ → þ / _ C[+obstruent]}
  ◊item{cþ C[+nasal] → nþ}
}

Here, the consonant graphemes are considered to be organized in the following way based on their pronunciations (with voiceless/voiced pairs):

◊table/x[#:options (table-options #:caption "Phonetic features used for complex coda simplification.")]{
  & Labial & Coronal & Dorsal & Other
  Obstruent & ◊nc{p, f, p· / v, vp} & ◊nc{t, č, þ, s, š, ł, t·, č· / d, ð, dt, d·, ðþ} & ◊nc{c, h, c· / g, gc} & ◊nc{/ ħ, g·}
  Nasal & ◊nc{/ m, mp} & ◊nc{/ n, nd} & ◊nc{/ ŋ, ŋg}
  Other & & ◊nc{/ l, lł, r} &
}

Finally, the ◊l0{j} is removed from any instances of ◊l0{ji jî ju}.

◊subsection[#:id "letternum"]{Letter numbering}

Sometimes, an integer must be assigned to each letter. In this case, the assignment shown in the table below is used. Note that numbers are not assigned fully sequentially. Furthermore, this function is valid only for layer 1 graphemes.

◊(define (row2 c n)
  `(@! ,(nc c) "&"
    (cls "figure" ,(my-number->string n 16)) "&"
    (cls "figure" ,(my-number->string n))))

◊table/x[
  #:options (table-options
    #:caption "Letter numbering in Ŋarâþ Crîþ. (The columns are read from left to right.)"
    #:colgroup '(c c rb c c rb c c c)
    #:first-col-header? #f
  )
]{
  Letter & Hex & Dec & Letter & Hex & Dec & Letter & Hex & Dec
  ◊pseudoheader{True letters}
  ◊(row2 "c" 0) & ◊(row2 "m" 32) & ◊(row2 "h" 17)
  ◊(row2 "e" 1) & ◊(row2 "a" 9) & ◊(row2 "ħ" 18)
  ◊(row2 "n" 2) & ◊(row2 "f" 10) & ◊(row2 "ê" 257)
  ◊(row2 "ŋ" 43) &◊(row2 "g" 11)  & ◊(row2 "ô" 260)
  ◊(row2 "v" 3) & ◊(row2 "p" 12) & ◊(row2 "â" 265)
  ◊(row2 "o" 4) & ◊(row2 "t" 13) & ◊(row2 "u" 19)
  ◊(row2 "s" 5) & ◊(row2 "č" 222) & ◊(row2 "w" -1)
  ◊(row2 "þ" 85) &◊(row2 "î" 14)  & ◊(row2 "x" -2)
  ◊(row2 "š" 94) & ◊(row2 "j" 110) & ◊(row2 "y" -3)
  ◊(row2 "r" 6) & ◊(row2 "i" 15) & ◊(row2 "z" -4)
  ◊(row2 "l" 7) & ◊(row2 "d" 16)
  ◊(row2 "ł" 119) & ◊(row2 "ð" 341)
  ◊pseudoheader{Markers}
  ◊(row2 "#" 20) & ◊(row2 "+*" 22) & ◊(row2 "*" 25)
  ◊(row2 "+" 21) & ◊(row2 "@" 23) & ◊(row2 "&" 26)
}

The ◊term{letter sum} of a word is the sum of all of its letters. This value is used in some of the noun declension paradigms.

It is theorized that letter numbers were assigned in the following manner:

◊items{
  ◊item{The basic true letters inherited from ◊term{Necarasso Cryssesa} (i.e. those corresponding to ◊l1{c e n v o s r l m a f g p t î i d h}) received sequential numbers from zero. The number of ◊l1{m} was changed due to superstitions against the number eight.}
  ◊item{◊l1{ŋ þ š ł č ð} received numbers based on what letter pairs (or triplets in the case of ◊l1{ð}) they were based on.}
  ◊item{◊l1{ê}, ◊l1{ô}, and ◊l1{â} were numbered as 256 + base glyph number.}
  ◊item{The other letters and the markers received sequential numbers after ◊l1{h}, skipping 0x18.}
}

◊subsection[#:id "collation"]{Collation}

The true letters and the markers are collated in their respective order, except for ◊l1{&}, which is ignored. Lenited letters are treated as their respective base letters, except when two words differ only by the presence or absence of a lenition mark, in which case the lenited variant is collated after the base letter: ◊l1{saga} < ◊l1{sag·a} < ◊l1{sada} < ◊l1{saħa}. Numerals are collated after all letters.

In a directory of personal names, entries are collated on surnames, with given names considered only when surnames are identical. Headings in such a list include the prefix up to an including the first true letter: ◊l1{+merlan #flirora} would be found under ◊l1{+m}.

Ordered items can be labeled using numerals (starting from 0) or letters. In the latter case, only the letters ◊l1{c e n v o s r l m a f g p t î i d h} are used.

◊subsection[#:id "numquotes"]{Numquotes}

A digit immediately preceding text surrounded by quotation or grouping marks constitutes a ◊term{numquote}. The digit is usually not pronounced in this case. Numquotes are mainly used for secondary purposes that lack any dedicated punctuation.

◊table/x[#:options (table-options
    #:caption "Numquotes in Ŋarâþ Crîþ."
    #:first-col-header? #f
    #:colgroup '(c d)
  )]{
  Numquote & Meaning
  ◊nc{B{}} & Contains parenthetical information: provides supplementary information. The sentence should still make sense without the parenthetical content.
  ◊nc{1{}} & Lists an alias of a referent mentioned by name.
  ◊nc{2{}} & Surrounds a key-value list. Used as such: ◊l1{2{3{&{}} 4{&{}} 3{&{}} 4{&{}}}}
  ◊nc{3{}} & Used for listing a key inside ◊l1{2{}}.
  ◊nc{4{}} & Used for listing a value inside ◊l1{2{}}. When not directly inside a ◊l1{2{}} numquote, marks a list: elements are delimited by spaces, and ◊l1{{}} can be used to insert multi-word elements.
  ◊nc{9{}} & Used to contain abbreviated quantities in the traditional currency system.
  ◊nc{*9{}} & Used to contain abbreviated quantities in a currency system other than the traditional one.
}

◊section[#:id "layer-2"]{Layer 2s}

Before the rest of the conversion to layer 2, the ◊xref["phonology.html" "conv-01" "Subsection"]{complex coda-simplifying changes} are performed to simplify such complex codas before clitic boundaries or at the end of a word. (That is, any occurrences of ◊l1{’} are ignored this time.)

Traditionally, only manifested grapheme phrases are considered to be significant in the conversion from layer 1 to layer 2s. However, other graphemes such as punctuation can affect prosody.

◊table/x[
  #:options (table-options
    #:caption "Layer 1 to layer 2s conversions."
    #:colgroup '(c rb c c)
    #:first-col-header? #f
  )
]{
  MGPs & IPA & MGPs & IPA
  ◊nc{c} & k & ◊nc{p} & p
  ◊nc{e} & e & ◊nc{t} & t
  ◊nc{n nd} & n & ◊nc{č} & t͡ʂ
  ◊nc{ŋ ŋg} & ŋ & ◊nc{î} & ì
  ◊nc{v m· vp} & v & ◊nc{j} & j
  ◊nc{o} & o & ◊nc{i} & i
  ◊nc{s} & s & ◊nc{d dt} & d
  ◊nc{þ t·} & θ & ◊nc{ð d· ðþ} & ð
  ◊nc{š č·} & ʂ & ◊nc{h c·} & x
  ◊nc{r} & ɹ & ◊nc{ħ g·} & ʕ
  ◊nc{l lł} & l & ◊nc{ê} & è
  ◊nc{ł} & ɬ & ◊nc{ô} & ò
  ◊nc{m mp} & m & ◊nc{â} & à
  ◊nc{a} & a & ◊nc{u} & u̜
  ◊nc{f p·} & f & ◊nc{f· v· ð·} & ∅
  ◊nc{g gc} & ɡ
}

Layer 2 has a two-way tone contrast between vowels: the high tone (H) is the default, being contrasted with the low tone (L). For historical reasons, the presence or absence of a low tone on a vowel is called [±creaky].

◊section[#:id "layer-3"]{Layer 3s}

The conversion from layer 2s to layer 3s is comparatively more complex.

First, the following changes are made:

◊items/dense{
  ◊item{kθ → x͡θ}
  ◊item{ʕ → ħ / V[+creaky] _}
  ◊item{n → m / _ C[+labial] }
  ◊item{n → ɱ / _ C[+labiodental]}
  ◊item{n → n̪ / _ C[+dental] }
  ◊item{n → ɳ / _ C[+retroflex]}
  ◊item{n C₁[+velar] → ɲ C₁[+palatal]}
  ◊item{n → ŋ / _ C[+lateral] V[+front]}
  ◊item{sʂ → ʂː}
  ◊item{C₁={ɹ, ɬ} → w / C₁V _}
  ◊item{l → ɾ / V[+back] _ V}
  ◊item{θ → θ̠ / s_, _s}
  ◊item{ʂj → ʃ}
  ◊item{ʂ → ʃ / _ i}
  ◊item{t͡ʂj → t͡ʃ}
  ◊item{t͡ʂ → t͡ʃ / _ i}
  ◊item{C₁[+voiced] → C₁[-voiced, -aspirated] / C₂[-voiced]}
}

Plosives in a coda are unreleased. All unvoiced plosives and affricates outside of a coda are aspirated.

While Ŋarâþ Crîþ has two tone levels phonemically, their realizations in the phonetic level is more complex. It is common to describe phonetic tone using seven levels, from 0 (the lowest) to 6 (the highest). Each syllable has one or more tones.

In order to describe tone, we must introduce the concept of ◊term{“stress”}, which is placed according to the following rules:

◊items{
  ◊item{Syllables with a high tone have a priority over syllables with a low tone – that is, a syllable with a low tone will be selected only if the word in question has only low-tone syllables.}
  ◊item{If the coda of the final syllable is either empty, or it consits of only ◊l3{s} or ◊l3{n}, then the syllables are chosen in the order ◊b{2nd-to-last → 3rd-to-last → last → 4th-to-last → … → first}.}
  ◊item{If the coda of the final syllable is a complex coda, then the syllables are chosen in the order ◊b{last → 3rd-to-last → 2nd-to-last → 4th-to-last → … → first}.}
  ◊item{If the coda is anything else, then the syllables are chosen from end to start: ◊b{last → 2nd-to-last → 3rd-to-last → … → first}.}
  ◊item{Monosyllabic function words generally lack any stressed syllable.}
}

We also introduce the concept of a ◊term{tone accounting unit} (TAU), which is the level at which tones are realized. That is, the tone of a syllable depends only on the contents of the TAU in which it lies. Instances of content words occupy different TAUs from each other, but some function words occupy the same TAU as the preceding or following word (in particular, such words have no stressed syllable and are confined to a relatively fixed position):

◊items{
  ◊item{Head particles, nominalized verb particles, and monosyllabic determiners occupy the same TAU as the following word.}
  ◊item{◊l1{so}, monosyllabic relationals ... occupy the same TAU as the preceding word.}
}

(Stress is accounted by orthographic word, not by TAU.)

First, two adjacent vowels are fused into a diphthong if the vowels are not identical, the first vowel is stressed, the second vowel is ◊l3{i} or ◊l3{u̜}, and the syllable to which the second vowel belongs can be interpreted as having an empty coda. For purposes of tonekeeping, a diphthong is considered to be composed of two different syllables.

In general, unstressed H and L syllables have tone levels 4 and 2, respectively; stressed H and L syllables have tone levels 5 and 1. However, an open H or L syllable before a stressed syllable gets level 3 or 1, respectively, instead. Diphthongs get different values: 65 for HH, 53 for HL, 13 for LH, and 21 for LL.

If two adjacent copies of an identical vowel have the same tone level at this stage, then the one closer to the stressed syllable rises by one tone level and the one farther from it falls by one level.

A tone level of ◊var{n} is then changed into a tone contour in the following situations, unless doing so would result in an out-of-bounds tone level:

◊items{
  ◊item{◊var{n} to (◊var{n} : ◊var{n} + 1): when the coda is ◊l3{st} or ◊l3{x͡θ}}
  ◊item{◊var{n} to (◊var{n} : ◊var{n} − 1): when the coda is ◊l3{rθ} or ◊l3{ns}}
  ◊item{◊var{n} to (◊var{n} + 1 : ◊var{n}): when the nucleus is preceded by two or more voiceless consonants}
}

In addition, other syllables change their tone levels:

◊items{
  ◊item{Raise the tone level by 1 (if it is not already 6) if the coda is a voiceless fricative, or if the coda is ◊l3{x͡θ}.}
  ◊item{Lower the tone level by 1 if the coda is ◊l3{ɹ}.}
  ◊item{Lower the tone level by 1 if the coda is a nasal followed by a voiced obstruent or nasal.}
}

Finally, if all tones have a level of 4 or higher, then the lowest tone (breaking ties by preferring later tones) is lowered to 3, and all other tones in the same syllable are lowered by the same amount. All level-3 tones are then lowered to level 2.

◊subsection[#:id "isochrony"]{Isochrony}

The isochrony of Ŋarâþ Crîþ falls somewhere between syllable and mora timing, where:

◊items{
  ◊item{The body of a syllable is always 1 unit long.}
  ◊item{The coda of a syllable is between 0 and 1 unit long, with the hierarchy ◊l2{t, k < n < l, ɹ < f, s, θ, ɹθ, kθ < st, lt, ns, ls, nθ}.}
  ◊item{Codas are shortened after two consecutive vowels: for instance, the ◊l1{l} in ◊l1{moriel} is pronounced for less time than that in ◊l1{mjarel}.}
}

◊section[#:id "mutations"]{Mutations}

Ŋarâþ Crîþ has two kinds of initial mutations: ◊term{lenition} and ◊term{eclipsis}. Neither kind of mutation has any effect on plosive-fricative onsets or any of ◊l0{r l n ŋ ħ}.

Lenition tends to turn plosives into fricatives and is indicated with a middle dot ◊l0{·} after the consonant affected. In particular, it affects ◊l0{p t d č c g m f v ð}. (See ◊xref["phonology.html" "layer-2" "Section" #:type 'inline]{Layer 2} for pronunciation details.) Partial lenition does not affect any of ◊l0{f v ð}; that is, it does not lenite consonants that would become silent. Unless otherwise qualified, lenition refers to total lenition, which affects ◊l0{f v ð}.

In a word containing ◊l0{&}, both instances of the reduplicated prefix are lenited. For example, ◊l1{&d·enfo} can be pronounced as ◊l3{ðeðenfo} but not as *◊l3{ðedenfo}.

Lenition occurs in the following environments:

◊items{
  ◊item{On the stem in abessive forms of nouns in paradigms 7, 8, 9, 10, and 13}
  ◊item{On a noun modified by ◊l1{šinen} or ◊l1{nemen} when used as determiners, if that noun is not a form of ◊l1{ðên}}
  ◊item{Partially, on a noun modified by ◊l1{ruf} not immediately following it}
  ◊item{Partially, on a noun modified by ◊l1{mê} immediately preceding it}
  ◊item{On a terrestrial noun modified by a participle-form verb belonging to a Type I genus}
  ◊item{To a dative-case nominalized verb phrase as explained in ◊xref["verbs.html" "nominalized" "Section" #:type 'inline]{Nominalized forms}}
  ◊item{Partially, on a verb when receiving the comparative prefixes ◊l0{mir-} or ◊l0{ła-}}
  ◊item{On a classifier attached to the numeral ◊l1{ces} or any numeral ending in ◊l1{ħas} or ◊l1{sreþas}}
  ◊item{On the second item of a compound noun, if it is neither terrestrial nor a form of ◊l1{vês}}
  ◊item{On a verb with the cessative prefix ◊l0{car-} or the terminative prefix ◊l0{er-}}
}

Eclipsis tends to add voice to voiceless consonants and change voiced stops into nasals. It is indicated by prefixing a consonant: ◊l0{t d c g f þ ł} become ◊l0{dt nd gc ŋg vf ðþ lł}, respectively. ◊l0{p} becomes ◊l0{vp} before any of ◊l0{i e u î ê} and ◊l0{mp} elsewhere. If a word starts with a vowel, then it is eclipsed by prefixing ◊l0{g}.

In a word containing ◊l0{&}, only the first instance of the reduplicated prefix is eclipsed. For example, ◊l1{n&denfin} can be pronounced as ◊l3{nedenfin} but not as *◊l3{nenenfin}.

Eclipsis occurs in the following environments:

◊items{
  ◊item{On the genitive dual, plural, and singulative forms of nouns}
  ◊item{On a noun modified by ◊l1{lê} or ◊l1{tê} immediately preceding it}
  ◊item{On a noun modified by ◊l1{dân}}
  ◊item{On a finite form of a verb or relational with perfective aspect}
  ◊item{To a locative, instrumental, or abessive-case nominalized verb phrase that is not an object of a modifying relational, as explained in ◊xref["verbs.html" "nominalized" "Section" #:type 'inline]{Nominalized forms}}
  ◊item{On a short numeral modified by ◊l1{ceþe}}
}

Lenition can happen on any syllabic onset of a word, but eclipsis is limited to word-initial positions.

In this documentation, lenition is sometimes marked with an empty circle ◊|sei|, and eclipsis with an filled circle ◊|uru|. Partial lenition is marked with an empty triangle ◊|sei2|.

◊section[#:id "loanwords"]{Loanwords}

Almost all loanwords in Ŋarâþ Crîþ are nouns. [TODO: we are reworking nouns]

Generally, when borrowing from languages that use the Cenvos script or a script related to it, and whose orthographies in the script in question do not deviate too far from Ŋarâþ Crîþ usage, Ŋarâþ Crîþ prefers to borrow the word graphemically than phonemically.

◊section[#:id "typography"]{The typography of Ŋarâþ Crîþ}

◊; TODO: improve this intro

In principle, layer 2w is the highest written layer needed to write in Ŋarâþ Crîþ. (Note that there is only one valid layer-2w representation for each layer-1 string; in other words, changing a valid layer-2w string in a way that preserves the layer-1 representation always results in an invalid layer-2w string.) However, speakers of Ŋarâþ Crîþ tend to value aesthetics, even in writing. Thus, a mastery of handwriting beyond layer 2w is considered crucial.

Even though movable type has been available for a long time, prominent parts of printed materials (such as titles) often continued to use plates engraved from handwriting. Eventually, typography and calligraphy were considered parts of the same discipline, leading to typefaces supporting more features from the latter. Even today, logos often opt for lettering over typefaces. Because of this unification, we use the term ◊term{typography} to refer to the discipline of laying out writing in general.

Although a full treatment of Ŋarâþ Crîþ typography is out of scope for this grammar, this section gives an overview of the concerns at hand.

◊subsection[#:id "kerning"]{Kerning}

Cenvos is a script that absolutely requires kerning. To start, some glyphs such as ◊l2w{e} and ◊l2w{m} have long leftward tails that necessitate kerning with glyphs such as ◊l2w{s} or ◊l2w{o}, which lack descenders, or even some glyphs with descenders such as ◊l2w{j}.

Other glyphs such as ◊l2w{j} and ◊l2w{ê} have shorter leftward descenders that also require kerning with following glyphs.

◊l2w{â} has a descender in the opposite direction; thus, it must kern with certain ◊em{preceding} glyphs.

Diagonal strokes with matching slopes (such as in ◊l2w{âv} or ◊l2w{rj}) should be kerned to bring them closer.

◊make-figure["images/kerning.svg" #:alt "Examples of glyph pairs that require kerning."]{Examples of glyph pairs that require kerning: ◊l2w{es}, ◊l2w{mj}, ◊l2w{jo}, ◊l2w{ên}, ◊l2w{câ}, and ◊l2w{âv}.}

Moreover, even pairs are sometimes insufficient. Since ◊l2w{e} and ◊l2w{i} are kerned so closely, ◊l2w{ei} must itself kern with glyphs such as ◊l2w{s}.

◊make-figure["images/kerning2.svg" #:alt "Kerning of eis and eig."]{Kerning of ◊l2w{eis} and ◊l2w{eig}. In ◊l2w{eis}, ◊l2w{ei} has room to kern with ◊l2w{s}. ◊l2w{ei} obviously cannot kern with ◊l2w{g}; that is, in ◊l2w{eig}, ◊l2w{i} and ◊l2w{g} are spaced ◊em{farther apart} than usual.}

◊subsection[#:id "typographical-layers"]{Ligation and shaping}

Another important aspect of typography is the use of ligatures (beyond the required ones). The concepts of higher written layers and the hierarchy of graphic variations have been developed to try to formalize this problem.

To explain the idea behind this model, we note that a good ligature will have the end of one glyph near the start of the next. The starting and ending points of a glyph, in turn, depend on the order in which the strokes are written.

Furthermore, natural handwriting tends to join certain strokes together. In some cases, this joining can affect how a glyph ligates; for instance, ◊l3w{◊sym{a1;1}} cannot ligate with the previous character (ligating through the middle would cause a stroke collision with stroke 2 of ◊l3w{◊sym{a1;1}}), but ◊l3w{◊sym{a1;2}}, in which the two strokes are joined without a loop, can do so.

In addition, rapid handwriting often produces stylistic variations of glyphs. For example, ◊l3w{◊sym{i2;1}} (“◊l2w{i} with the stroke going upward”) can often end in a leftward swash at the end of the stroke. Since this deviation does not create any ambiguity, it has been accepted, yielding the stylistic variant ◊l4w{◊sym{i2;1+S}}.

◊make-figure["images/principles1.svg" #:alt "The ideas behind ligation."]{(a) An example of a bad ligature, in which the first glyph ends at the baseline and the second glyph starts at the top line. In the next example, the second glyph starts at the baseline as well, avoiding an awkward joining point. (b) A difference in stroke order (shown with the glyph ◊l2w{a}) can change the starting points (shown as blue dots) and the ending points (shown as red dots) of a glyph. (◊l3w{◊sym{a1;1}} does not have a starting point suitable for ligation.) (c) The first stroke of ◊l3w{◊sym{a1;1}} blocks ligation from a previous glyph, but such a stroke is absent in ◊l3w{◊sym{a1;2}}. (d) The default variant ◊l4w{◊sym{i2;1}} in comparison to ◊l4w{◊sym{i2;1+S}} (both ligated after ◊l4w{◊sym{f1;1}}).}

We now cover the formalism itself. Layers 2w*, 3w, and 4w are aesthetic layers; the writer decides the precise sequence of glyphs to realize a layer-2w string in higher layers. Nonetheless, not all layer-3w or -4w strings are valid, even those that correspond to valid layer-2w strings; for instance, ◊l3w{◊lig{◊sym{s1}◊sym{i1}}} is not a valid realization of ◊l2w{si} because it requires a base-to-top ligation.

Only some glyphs ◊term{participate} in typesetting. Notably, all letters participate, but no numerals do so, nor does the space.

Each participating layer-2w* glyph has a hierarchy of variations as follows:

◊items{
  ◊item{At the top level is the layer-2w* glyph itself.}
  ◊item{These are divided into ◊term{stroke-order variants}, which differ only in stroke order. All strokes must be preserved, and no loops may be introduced or removed, but the relative stroke order might be different, and some strokes may be written in the reverse direction; furthermore, a stroke may be split at a turn, and two strokes may be joined where one ends and another begins. These are denoted with subscript numerals: ◊l2w{a} has variants ◊l3w{◊sym{a1}}, ◊l3w{◊sym{a2}}, and ◊l3w{◊sym{a3}}. Variant 1 is considered the ‘canonical’ variant.}
  ◊item{Each stroke-order variant has one or more ◊term{topological variants}, which may join strokes together, cause two different strokes to touch each other when they did not (or vice versa), or introduce or remove loops. Lengthening or shortening strokes to alter ligation properties also falls under this level. Topological variants are distinguished using lowercase Greek letters. For instance, ◊l3w{◊sym{a1}} has three topological variants: ◊l3w{◊sym{a1;1}}, ◊l3w{◊sym{a1;2}}, ◊l3w{◊sym{a1;3}}. ◊top-var[1] is reserved for the canonical variant, which preserves all strokes, although it is not always the most common variant.}
  ◊item{Each topological variant has one or more ◊term{stylistic variants}, which can modify the strokes of the glyph themselves. For instance, ◊l4w{◊sym{i2;1}} is the topological variant of ◊l2w{i} in which the stroke goes from the base to the top. It has two stylistic variants: ◊l4w{◊sym{i2;1}} is the default one, and ◊l4w{◊sym{i2;1+S}} has a swash to the left at the top of the stroke. Note that the ‘canonical’ stylistic variant has no superscript letter, while the other variants do.}
}

Layer 2w is transliterated using mostly the same symbols as the layer-1 romanization, but required ligatures are notated with an overline (such as in ◊l2w{◊lig{me}} for ◊cenvos{me}), and final forms are written as if they were ligatures with a special ◊nc/w{$} symbol: ◊l2w{◊lig{c$}} for ◊cenvos{c}. Layer 2w* introduces ◊term{discretionary ligatures}, which are similarly marked in our notation. By ◊term{discretionary ligature}, we mean a ligature that the writer may choose to use but is not obligated to do so, and that cannot be derived by simply connecting the ending stroke of one glyph to the starting stroke of another.

Layer 3w works on topological variants. The overline denotes optional ligatures between topological variants; it is now omitted for required  and discretionary ligatures, which are their own layer-2w* glyphs in their own right: ◊l3w{◊lig{◊sym{+1;1}◊sym{me1;1}}◊lig{◊sym{r1;1}◊sym{l2;2}}◊sym{a1;1}◊sym{n1;1} ◊lig{◊sym{#1;1}◊sym{f1;1}◊sym{l2;4}◊sym{i1;2}}◊sym{r1;1}◊lig{◊sym{o1;1}◊sym{r2;1}◊sym{a3;2}}} transliterates a particularly fancy realization of ◊l1{+merlan #flirora}.

◊make-figure["images/est_e_kozet_san.svg" #:alt "#merlan +flirora" #:id "est_e_kozet_san"]{What ◊l3w{◊lig{◊sym{+1;1}◊sym{me1;1}}◊lig{◊sym{r1;1}◊sym{l2;2}}◊sym{a1;1}◊sym{n1;1} ◊lig{◊sym{#1;1}◊sym{f1;1}◊sym{l2;4}◊sym{i1;2}}◊sym{r1;1}◊lig{◊sym{o1;1}◊sym{r2;1}◊sym{a3;2}}} would look like.}

Layer 4w works on stylistic variants. In the transliteration, the overline is used as in 3w.

Layer 3w can be thought of as the ‘ligation layer’; similarly, layer 4w can be thought of as the ‘shaping layer’.

◊xref/l["canonical-stroke-order"]{Table} describes the canonical stroke order of each glyph, and ◊xref/l["stroke-order-variants"]{Table} lists the stroke-order variants.

◊table/x[
  #:options (table-options
    #:caption "Canonical stroke orders for layer-2w* glyphs. (Glyphs in parentheses are discretionary ligatures.)"
    #:colgroup '(c d)
    #:long? #t
  ) #:id "canonical-stroke-order"
]{
  Glyph & Stroke order
  ◊nc/w{c} & (1) Counterclockwise
  ◊nc/w{e} & (1) From top right to bottom left
  ◊nc/w{n} & (1) From top left to bottom right
  ◊nc/w{ŋ} & (1) From top right to bottom
  ◊nc/w{v} & (1) From right to left
  ◊nc/w{o} & (1) From top to bottom left
  ◊nc/w{s} & (1) From top right to bottom left
  ◊nc/w{þ} & (1) Rightmost stroke from right to left ◊br (2) Leftmost stroke from right to left
  ◊nc/w{š} & (1) From top right to bottom left
  ◊nc/w{r} & (1a) From bottom to top (1b) to left
  ◊nc/w{l} & (1a) ◊nc{r}-stroke from bottom to top (1b) to left ◊br (2) Intersecting stroke from right to left
  ◊nc/w{ł} & (1a) ◊nc{o}-stroke from top to bottom (1b) to left ◊br (2) Intersecting stroke from right to left
  ◊nc/w{m} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Intersecting stroke from right to left
  ◊nc/w{a} & (1) ◊nc{þ}-sloping stroke from left to right ◊br (2) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{f} & (1) Rightmost stroke from right to left ◊br (2) Leftmost stroke from right to left
  ◊nc/w{g} & (1) From top right to bottom
  ◊nc/w{p} & (1) From right to bottom
  ◊nc/w{t} & (1a) ◊nc{v}-stroke from right to top (1b) to left ◊br (2) Vertical stroke from top to bottom
  ◊nc/w{č} & (1) Ascending stroke from top to bottom ◊br (2) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{î} & (1) From bottom right to top left
  ◊nc/w{j} & (1) From top right to bottom left
  ◊nc/w{i} & (1) From top to bottom
  ◊nc/w{d} & (1) ◊nc{þ}-sloping stroke from left to right ◊br (2) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{ð} & (1) Leftmost ◊nc{þ}-sloping stroke from left to right ◊br (2) Rightmost ◊nc{þ}-sloping stroke from left to right ◊br (3) ◊nc{f}-sloping stroke from right to left
  ◊nc/w{h} & (1) From right to left
  ◊nc/w{ħ} & (1) Clockwise, starting and ending at the top
  ◊nc/w{ê} & (1) From top right to bottom left
  ◊nc/w{ô} & (1) From top to bottom
  ◊nc/w{â} & (1) From bottom right to top left
  ◊nc/w{u} & (1) ◊nc{o}-stroke from top to bottom left ◊br (2) Rightmost dot ◊br (3) Leftmost dot
  ◊nc/w{w} & (1) From top to bottom
  ◊nc/w{x} & (1) Stroke with descender, starting from the top-right corner and ending on the descender ◊br (2) Wave stroke, from right to left
  ◊nc/w{y} & (1) From right to left
  ◊nc/w{z} & (1) From right to left
  ◊nc/w{◊lig{c$}} & (1) From right to bottom left
  ◊nc/w{◊lig{ŋ$}} & (1) ◊nc{ŋ}-stroke from top right to bottom ◊br (2) Intersecting stroke from right to left
  ◊nc/w{◊lig{ee}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Overbar from right to left
  ◊nc/w{◊lig{em}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Roof from right to lef
  ◊nc/w{◊lig{me}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Intersecting stroke from right to left ◊br (3) Overbar from right to left
  ◊nc/w{◊lig{mm}} & (1) ◊nc{e}-stroke from top right to bottom left ◊br (2) Intersecting stroke from right to left ◊br (3) Roof from right to left
  ◊nc/w{◊lig{jâ}} & (1) ◊nc{j}-stroke from top right to bottom left ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{◊lig{âj}} & (1) ◊nc{â}-stroke from bottom right to top left ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{◊lig{ww}} & (1) ◊nc{w}-stroke, from top to bottom ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{◊lig{xx}} & (1) Stroke with descender, starting from the top-right corner and ending on the descender ◊br (2) Wave stroke, from right to left ◊br (3) Bottom-right tick ◊br (4) Top-left tick
  ◊nc/w{◊lig{yy}} & (1) ◊nc{y}-stroke, from right to left ◊br (2) Tick, from top to bottom
  ◊nc/w{◊lig{zz}} & (1) ◊nc{z}-stroke, from right to left ◊br (2) Ring clockwise (starting and ending point unspecified)
  ◊nc/w{#} & (1) From bottom right to top left
  ◊nc/w{+} & (1) From top right to bottom left
  ◊nc/w{+*} & (1) From top right to bottom left ◊br (2) Vertical stroke from top to bottom ◊br (3) ◊nc{f}-sloping stroke from top right to bottom left ◊br (4) ◊nc{þ}-sloping stroke from bottom right to top left
  ◊nc/w{@} & (1) Vertical stroke from top to bottom ◊br (2) ◊nc{v}-stroke from right to left
  ◊nc/w{*} & (1) Vertical stroke from top to bottom ◊br (2) Horizontal stroke from right to left ◊br (3) ◊nc{f}-sloping stroke from top right to bottom left ◊br (4) ◊nc{þ}-sloping stroke from bottom right to top left
  ◊nc/w{&} & (1) Sinusoid from right to left ◊br (2) Arrowhead
  ◊nc/w{.} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w{;} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w{?} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w{!} & (1) Main stroke from right to left ◊br (2) Arrowhead
  ◊nc/w["{"] & (1) From right to left
  ◊nc/w["}"] & (1) From right to left
  ◊nc/w{«} & (1) From top to bottom
  ◊nc/w{»} & (1) Vertical stroke from top to bottom ◊br (2) Left cornered edge from top to bottom
  ◊nc/w{/} & (1) From bottom, curving at the top toward the left, then descending while crossing to the right half and possibly to the left again
  (◊nc/w{◊lig{ra}}) & (1) Stroke as in ◊l2w{r}, but with the end extending to the descender line ◊br (2) Stroke intersecting the second part of stroke 1
  (◊nc/w{◊lig{ro}}) & (1a) The stem of the ◊l2w{r}-stroke, from bottom to top (1b) A ◊l2w{v}-stroke from right to left
}

◊make-figure["images/stroke-order.svg" #:alt "Canonical stroke orders of layer-2w glyphs."]{Canonical stroke orders of layer-2w glyphs.}

◊make-figure["images/discretionary-ligatures.svg" #:alt "Stroke orders of discretionary ligatures."]{Stroke orders of discretionary ligatures.}

◊table/x[
  #:options (table-options
    #:caption ◊@{Stroke order variants of glyphs, in reference to ◊xref["phonology.html" "canonical-stroke-order" "Table"]{the canonical stroke order}. The prime symbol denotes the reverse direction; the plus denotes a fused stroke.}
    #:colgroup '(c rb c c c c c)
    #:long? #t
  ) #:id "stroke-order-variants"
]{
  Glyph & 1 & 2 & 3 & 4 & 5 & 6
  ◊nc{c} & 1
  ◊nc{e} & 1
  ◊nc{n} & 1 & 1◊prime
  ◊nc{ŋ} & 1
  ◊nc{v} & 1
  ◊nc{o} & 1
  ◊nc{s} & 1
  ◊nc{þ} & 1 2
  ◊nc{š} & 1
  ◊nc{r} & 1 & 1a◊prime 1b
  ◊nc{l} & 1 2 & 1a◊prime 1b 2
  ◊nc{ł} & 1 2
  ◊nc{m} & 1 2 & 2◊prime 1 & 1 2◊prime
  ◊nc{a} & 1 2 & 2 1◊prime & 1◊prime 2
  ◊nc{f} & 1 2
  ◊nc{g} & 1
  ◊nc{p} & 1
  ◊nc{t} & 1 2 & 1a+2 1b
  ◊nc{č} & 1 2
  ◊nc{î} & 1
  ◊nc{j} & 1
  ◊nc{i} & 1 & 1◊prime
  ◊nc{d} & 1 2 & 2 1◊prime & 1◊prime 2
  ◊nc{ð} & 1 2 3 & 3 1 2
  ◊nc{h} & 1
  ◊nc{ħ} & 1
  ◊nc{ê} & 1
  ◊nc{ô} & 1
  ◊nc{â} & 1
  ◊nc{u} & 1 2 3
  ◊nc{w} & 1
  ◊nc{x} & 1 2
  ◊nc{y} & 1
  ◊nc{z} & 1
  ◊nc/w{◊lig{c$}} & 1
  ◊nc/w{◊lig{ŋ$}} & 1 2 & 1 2◊prime
  ◊nc/w{◊lig{ee}} & 1 2 & 2 1
  ◊nc/w{◊lig{em}} & 1 2 & 2 1
  ◊nc/w{◊lig{me}} & 1 2 3 & 2◊prime 1 3 & 1 2◊prime 3 & 3 1 2 & 3 2◊prime 1 & 3 1 2◊prime
  ◊nc/w{◊lig{mm}} & 1 2 3 & 2◊prime 1 3 & 1 2◊prime 3 & 3 1 2 & 3 2◊prime 1 & 3 1 2◊prime
  ◊nc/w{◊lig{jâ}} & 1 2
  ◊nc/w{◊lig{âj}} & 1 2
  ◊nc/w{◊lig{ww}} & 1 2
  ◊nc/w{◊lig{xx}} & 1 2 3 4
  ◊nc/w{◊lig{yy}} & 1 2
  ◊nc/w{◊lig{zz}} & 1 2
  ◊nc{#} & 1
  ◊nc{+} & 1
  ◊nc{+*} & 1 2 3 4
  ◊nc{@} & 1 2
  ◊nc{*} & 1 2 3 4
  ◊nc{&} & 1 2
  ◊nc{.} & 1 2
  ◊nc{;} & 1 2
  ◊nc{?} & 1 2
  ◊nc{!} & 1 2
  ◊nc["{"] & 1
  ◊nc["}"] & 1
  ◊nc{«} & 1
  ◊nc{»} & 1 2 & 1+2◊prime
  ◊nc{/} & 1
  (◊nc/w{◊lig{ra}}) & 1 2
  (◊nc/w{◊lig{ro}}) & 1 & 1a◊prime 1b
}

◊table/x[
  #:options (table-options
    #:caption ◊@{Topological variants of glyphs: ligation properties and descriptions. (Stroke numbers are in reference to the stroke-order variant, not the 2w glyph.)}
    #:colgroup '(c c c d d)
    #:long? #t
  ) #:id "topological-variants"
]{
  Glyph & Start join & End join & Description & Use
  ◊nc/w{◊sym{c1;1}} & M & — & Default & Default
  ◊nc/w{◊sym{e1;1}} & Mv & D & Default & Default
  ◊nc/w{◊sym{e1;2}} & Bv & D & Stem shortened to start at base & After glyphs that end at the base
  ◊nc/w{◊sym{n1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{n2;1}} & B & M & Default & Before glyphs that start at the mid
  ◊nc/w{◊sym{ŋ1;1}} & M & Dv & Default & Default
  ◊nc/w{◊sym{v1;1}} & B & B & Default & Default
  ◊nc/w{◊sym{o1;1}} & Tv & M & Default & Default
  ◊nc/w{◊sym{o1;2}} & M & M & Loop on stroke to allow for mid ligation with previous glyph & After glyphs that end at the mid
  ◊nc/w{◊sym{s1;1}} & M & B & Default & Default
  ◊nc/w{◊sym{þ1;1}} & B & M & Default & Default
  ◊nc/w{◊sym{þ1;2}} & B & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{š1;1}} & M & Bv & Default & Default
  ◊nc/w{◊sym{r1;1}} & Dv & B & Default & Default
  ◊nc/w{◊sym{r2;1}} & Mv & B & Default & Rare (◊top-var[2] form is more common), but sometimes after glyphs that end at the mid
  ◊nc/w{◊sym{r2;2}} & Bv & B & Stroke 1 disconnected from 2 (starts at base instead) & After glyphs that end at the base
  ◊nc/w{◊sym{l1;1}} & Dv & M & Default & Default
  ◊nc/w{◊sym{l1;2}} & Dv & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{l2;1}} & Mv & M & Default & Rare (◊top-var[2] form is more common), but sometimes after glyphs that end at the mid
  ◊nc/w{◊sym{l2;2}} & Bv & M & Stroke 1 disconnected from 2 (starts at base instead) & After glyphs that end at the base
  ◊nc/w{◊sym{l2;3}} & Mv & M & Strokes 2 and 3 connected & Rare (◊top-var[4] form is more common), but stylization of ◊top-var[1]
  ◊nc/w{◊sym{l2;4}} & Bv & M & Stroke 1 disconnected from 2 (starts at base instead), and strokes 2 and 3 connected & Stylization of ◊top-var[2]
  ◊nc/w{◊sym{ł1;1}} & Tv & BD & Default & Default
  ◊nc/w{◊sym{ł1;2}} & Tv & BD & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{m1;1}} & Mv & — & Default & Default
  ◊nc/w{◊sym{m2;1}} & — & D & Default & Rare; ◊top-var[2] form is more common
  ◊nc/w{◊sym{m2;2}} & — & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{m3;1}} & Mv & — & Default & Rare; ◊top-var[2] form is more common
  ◊nc/w{◊sym{m3;2}} & Mv & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{a1;1}} & — & D & Default & Default
  ◊nc/w{◊sym{a1;2}} & M & D & Strokes 1 and 2 fused, with 2 beginning where 1 ends (without a loop) & Stylistic (‘italic’ variant)
  ◊nc/w{◊sym{a1;3}} & — & D & Strokes 1 and 2 connected (with a loop) & Stylistic
  ◊nc/w{◊sym{a2;1}} & M & M & Default & After glyphs that end at the mid
  ◊nc/w{◊sym{a2;2}} & M & M & Strokes 1 and 2 connected (rare) & Stylistic
  ◊nc/w{◊sym{a3;1}} & B & D & Default & After glyphs that end at the base
  ◊nc/w{◊sym{a3;2}} & B & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{f1;1}} & M & B & Default & Default
  ◊nc/w{◊sym{f1;2}} & M & B & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{g1;1}} & M & Dv & Default & Default
  ◊nc/w{◊sym{p1;1}} & B & Dv & Default & Default
  ◊nc/w{◊sym{t1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{t2;1}} & B & B & Default & Stylistic
  ◊nc/w{◊sym{č1;1}} & T & B & Default & Default
  ◊nc/w{◊sym{î1;1}} & B & M & Default & Default
  ◊nc/w{◊sym{j1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{i1;1}} & Tv & Bv & Default & Default
  ◊nc/w{◊sym{i1;2}} & M & Bv & Loop on stroke to allow for mid ligation with previous glyph & After glyphs that end at the mid
  ◊nc/w{◊sym{i2;1}} & B & T & Default & After glyphs that end at the base
  ◊nc/w{◊sym{d1;1}} & — & B & Default & Default
  ◊nc/w{◊sym{d2;1}} & M & M & Default & After glyphs that end at the mid
  ◊nc/w{◊sym{d3;1}} & B & B & Default & After glyphs that end at the base
  ◊nc/w{◊sym{ð1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{ð1;2}} & B & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{ð1;3}} & B & — & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{ð1;4}} & B & — & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{ð2;1}} & M & M & Default & After glyphs that end at the mid, or as a stylization
  ◊nc/w{◊sym{ð2;2}} & M & M & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{h1;1}} & M & M & Default & Default
  ◊nc/w{◊sym{ħ1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{ê1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{ê1;2}} & M & — & Stroke bends to the right at the end, preventing linkage with the next glyph & Stylistic
  ◊nc/w{◊sym{ô1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{â1;1}} & D & M & Default & Default
  ◊nc/w{◊sym{u1;1}} & Tv & DB & Default & Default
  ◊nc/w{◊sym{u1;2}} & M & DB & Loop on stroke 1 to allow for mid ligation with previous glyph & After glyphs that end at the mid
  ◊nc/w{◊sym{w1;1}} & M & Dv & Default & Default
  ◊nc/w{◊sym{x1;1}} & M & M & Default & Default
  ◊nc/w{◊sym{y1;1}} & B & B & Default & Default
  ◊nc/w{◊sym{z1;1}} & B & B & Default & Default
  ◊nc/w{◊sym{c$1;1}} & M & D & Default (in practice, final forms have no successor to ligate to) & Default
  ◊nc/w{◊sym{ŋ$1;1}} & M & DB & Default & Default
  ◊nc/w{◊sym{ŋ$2;1}} & M & — & Default & Rare; ◊top-var[2] form is more common
  ◊nc/w{◊sym{ŋ$2;2}} & M & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{ee1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{ee2;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{ee2;2}} & M & D & Strokes 1 and 2 connected (uncommon) & Stylistic
  ◊nc/w{◊sym{em1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{em2;1}} & M & D & Default & Stylistic
  ◊nc/w{◊sym{em2;2}} & M & D & Strokes 1 and 2 connected (uncommon) & Stylistic
  ◊nc/w{◊sym{me1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{me2;1}} & — & M & Default & Stylistic
  ◊nc/w{◊sym{me2;2}} & — & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me3;1}} & Mv & M & Default & Stylistic
  ◊nc/w{◊sym{me3;2}} & Mv & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me3;3}} & — & M & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{me3;4}} & — & M & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{me4;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{me4;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me5;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{me5;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me5;3}} & M & D & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{me5;4}} & M & D & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{me6;1}} & M & — & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{me6;2}} & M & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{me6;3}} & M & — & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{me6;4}} & M & — & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{mm1;1}} & Mv & M & Default & Default
  ◊nc/w{◊sym{mm2;1}} & — & M & Default & Stylistic
  ◊nc/w{◊sym{mm2;2}} & — & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm3;1}} & Mv & M & Default & Stylistic
  ◊nc/w{◊sym{mm3;2}} & Mv & M & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm3;3}} & — & M & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{mm3;4}} & — & M & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{mm4;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{mm4;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm5;1}} & M & D & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{mm5;2}} & M & D & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm5;3}} & M & D & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{mm5;4}} & M & D & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{mm6;1}} & M & — & Default & Sometimes after a glyph that ends at the mid
  ◊nc/w{◊sym{mm6;2}} & M & — & Strokes 1 and 2 connected & Stylistic
  ◊nc/w{◊sym{mm6;3}} & M & — & Strokes 2 and 3 connected & Stylistic
  ◊nc/w{◊sym{mm6;4}} & M & — & Strokes 1, 2, and 3 connected & Stylistic
  ◊nc/w{◊sym{jâ1;1}} & M & M & Default & Default
  ◊nc/w{◊sym{âj1;1}} & D & D & Default & Default
  ◊nc/w{◊sym{ww1;1}} & M & — & Default & Default
  ◊nc/w{◊sym{xx1;1}} & M & D & Default & Default
  ◊nc/w{◊sym{yy1;1}} & B & M & Default & Default
  ◊nc/w{◊sym{zz1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{#1;1}} & — & M & Default & Default
  ◊nc/w{◊sym{+1;1}} & — & M & Default & Default
  ◊nc/w{◊sym{+*1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{@1;1}} & Tv & M & Default & Default
  ◊nc/w{◊sym{@1;2}} & M & M & Loop on stroke 1 to allow for mid ligation with previous glyph & After a glyph that ends at the mid
  ◊nc/w{◊sym{*1;1}} & — & M & Default & Default
  ◊nc/w{◊sym{&1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{.1;1}} & MT & — & Default & Default
  ◊nc/w{◊sym{;1;1}} & B & — & Default & Default
  ◊nc/w{◊sym{?1;1}} & MT & — & Default & Default
  ◊nc/w{◊sym{!1;1}} & M & — & Default & Default
  ◊nc/w{◊sym["{1;1"]} & T & Tv & Default & Default
  ◊nc/w{◊sym["}1;1"]} & Bv & B & Default & Default
  ◊nc/w{◊sym{«1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{»1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{»2;1}} & — & — & Default & Stylistic (handwriting variant)
  ◊nc/w{◊sym{/1;1}} & — & — & Default & Default
  ◊nc/w{◊sym{ra1;1}} & Dv & — & Default & Default
  ◊nc/w{◊sym{ro1;1}} & Dv & M & Default & Default
  ◊nc/w{◊sym{ro2;1}} & Mv & M & Default & Rare (◊top-var[2] form is more common), but sometimes after glyphs that end at the mid
  ◊nc/w{◊sym{ro2;2}} & Bv & M & Stroke 1 disconnected from 2 (starts at base instead) & After glyphs that end at the base
}

◊xref/l["topological-variants"]{Table} lists all topological variants with their possible join positions on each side, with ◊i{B} for ◊i{base}, ◊i{M} for ◊i{mid} (or ◊i{mean}), ◊i{T} for ◊i{top} (ascender line), and ◊i{D} for ◊i{descender}. If more than one position is listed, then any one of them can be used. A ◊i{v} suffix on a position indicates that the stroke end at the appropriate side is vertical.

In general, for two topological variants ◊var{a} and ◊var{b} to ligate to each other (in that order), there must exist a position ◊var{C} such that ◊var{a} can join at ◊var{C} endward and ◊var{b} can join at ◊var{C} startward, with at least one end not being vertical.

There are a few exceptions to this rule: any topological variant of ◊l2w{l} can be ligated before ◊l3w{◊sym{i2;1}} (see ◊xref/l["est_e_kozet_san"]{Figure} for an example).

Stylistic variants are much less standardized in comparison, but there are some widely recognized variants:

◊items{
  ◊item{Some topological variants (◊l3w{◊sym{þ1;2}}, ◊l3w{◊sym{j1;1}}, ◊l3w{◊sym{i2;1}}, ◊l3w{◊sym{c$1;1}}, ◊l3w{◊sym{«1;1}}) have an S variant that introduces a swash at the end of the last stroke.}
  ◊item{In the standard forms, ◊l2w{e} and ◊l2w{m} (as well as the required ligatures involving these) have the tail sloping slightly upwards (as it goes to the left). This tail might sometimes bend downwards (the C variant) or even start with a downward slope (the D variant).}
  ◊item{The rightward descending stem of a glyph such as ◊l3w{◊sym{r1;1}} can be shortened (in the H variant) after an ◊l2w{e} or ◊l2w{m} to allow kerning.}
}

◊l2w{’} and ◊l2w{·} are special: they can ligate with ◊em{any} participating glyph on either end, appearing as an extension of the stroke near the ◊l2w{’} or ◊l2w{·}. Nonetheless, such ligation is not particularly common.

The rules over layers 3w and 4w dictate only what is legal, not what is considered beautiful. (Indeed, it is perfectly legal to use the 1◊top-var[1] form of every glyph and abstain from all non-required ligatures.) Nor do they dictate ◊em{how} an eligible pair of glyphs should be ligated. There are some guidelines, however, on what is desirable:

◊items{
  ◊item{Avoid stroke collisions}
  ◊item{Minimize horizontal space}
  ◊item{Minimize effort to write}
  ◊item{Prefer to ligate when possible, but avoid doing so excessively}
  ◊item{Prefer to use the canonical stroke-order}
  ◊item{Prefer to use the most common topological forms}
  ◊item{Vary the particular forms of each letter}
}

◊subsubsection[#:id "ligation-connotations"]{Connotations associated with choices in layer-4w realization}

Of course, context also plays a role in deciding how to realize text into layer 4w. First, the purpose of the writing has an influence (text meant for children or language learners will be less embellished, and header text tneds to be more embellished than body text).

Another part of context is the expressive connotation that the writer wishes to communicate.

◊table/x[#:options (table-options
    #:caption "Expresive connotations associated with choices in layer-4w realization."
    #:first-col-header? #f
    #:colgroup '(c d)
  )]{
  Connotation & Properties of realization
  Elegant, refined & Increased use of ligation in general; use of ‘broken ◊l2w{r}-stroke forms’ such as ◊l3w{◊sym{r2;2}} and ◊l3w{◊sym{l2;2}}
  Rational & Use of the non-H stylistic variants of glyphs such as ◊l3w{◊sym{r1;1}} after ◊l2w{e} or ◊l2w{m} rather than the H variants
  Casual, informal & Use of ◊l3w{◊sym{a1;2}}
}

◊subsection[#:id "vertical-ligation"]{Vertical ligation}

Another desirable practice is ◊term{vertical ligation}, in which the strokes of two glyphs in different lines are connected. This is naturally difficult even in handwriting, let alone in type!
