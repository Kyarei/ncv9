#lang pollen

◊(require pollen/pagetree)

◊define-meta[pdf-type book]
◊define-meta[book-title]{The Ŋarâþ Crîþ v9 grammar}
◊define-meta[book-author]{+merlan #flirora}

◊show-version[]

◊(include-pages-from-pagetree "index.ptree"
    #:ignore (set 'grammar/old/index.html))
