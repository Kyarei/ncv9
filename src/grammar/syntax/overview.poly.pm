#lang pollen

◊define-meta[title]{Overview}

For most of this text, we will be working with units that are the ◊term{sentence} or lower. A sentence consists of one or more ◊term{independent clause phrases} (ICPs), separated by ◊l1{;}, with a ◊l1{.}, ◊l1{?}, or ◊l1{!} at the end. An independent clause phrase can be a ◊term{general independent clause phrase} (gICP) or a ◊term{special independent clause phrase} (sICP).

A special independent clause phrase is one of an ◊term{interjection}, a ◊term{vocative}, a ◊term{probism}, an ◊term{attributed quotation}, or a ◊xref["overview.html" "data-pos" "Section"]{◊term{datum}}.

An ◊term{interjection} is a word in the “interjection” part of speech and is not inflected. There are a few interjections, such as ◊l1{cirtel} ◊trans{by the way, incidentally} and ◊l1{olasta} ◊trans{in addition, furthermore, moreover} that can never appear on their own; they must be followed by another ICP in the same sentence. Others, such as ◊l1{cleli} ◊trans{of course, obviously}, can stand on their own, but when they occur before another ICP, they can naturally be interpreted as modifying it. To put it another way, such interjections on their own imply an ellipsed statement: “of course◊em{, that is the case}”. Such interjections constitute ◊term{dependent special independent clause phrases} (dsICPs) and ◊term{ambidependent special independent clause phrases} (asICPs). Of course, it can be argued that all sICPs are dsICPs or asICPs, as interjections such as ◊l1{menco} ◊trans{oh!, I see!, look!, indeed!} are related to what follows them. In any case, the presence of dsICPs and asICPs imply a structural level between the ICP and the sentence.

A ◊term{vocative} consists of a noun phrase in the dative case and is used to address the referent.

A ◊term{probism} (named after the ◊link["https://en.wikipedia.org/wiki/Appendix_Probi"]{◊lang["la"]{◊cite{Appendix Probi}}}) consists of one or both of a direct ◊xref["../morphology/nouns.html" "quotatives" "Section"]{quotative} noun phrase in the ablative case and another in the accusative case, such that ◊l1{«◊var{Y}» noþa «◊var{X}» ne} is an instruction to use the construction ◊var{X} rather than ◊var{Y}. If both of the quotatives are present, then the ablative one comes first.

An ◊term{attributive quotation} indicates words said by a certain person, as in a piece of dialogue. It consists of a noun phrase in the nominative case followed by a quotation in quotation marks. While it can technically be used in a larger sentence, it usually stands on its own.

General independent clause phrases, on the other hand, are structurally more complex. We first look at the case of a single independent clause with no dependent clauses.

◊section[#:id "independent"]{Independent clauses}

An independent clause might or might not have a verb. We first look at the case where a verb is present.

If there is a verb, then it comes at the end of the clause (except before any tail particles). That is, arguments and adjuncts to the verb occur before it. Because Ŋarâþ Crîþ has cases, the relative order of noun phrases in a clause is usually insignificant, but the topic usually precedes the focus, and contrastive foci are often moved immediately after the topic (if any) or immediately before the verb.

Likewise, most modifiers precede their heads. The following do not, however:

◊items{
  ◊item{cardinal (as opposed to an ordinal) numerals}
  ◊item{the second part of most compound nouns}
}

Some types of modifiers agree with their heads and therefore can be moved away from them, as long as their relative order is preserved.

[TODO: order of modifiers]

◊subsection[#:id "independent-verbless"]{Verbless clauses}

Some independent clauses do not have a finite verb at the end. Nonetheless, they are treated as the head of a gICP and therefore can be a part of a ◊i{so}-clause.

◊items{
  ◊item{A clause with a nominative NP ◊var{x} alone implies the existence of whatever ◊var{x} refers to. In this case, ◊var{x} is usually understood to be new information.}
  ◊item{A clause with two nominative NPs ◊var{x} and ◊var{y} equates the referents of ◊var{x} with the referents of ◊var{y}.}
  ◊item{A clause with a nominative NP ◊var{x} and an accusative NP ◊var{y} implies that the referents of ◊var{x} are a subset of the referents of ◊var{y}.}
  ◊item{A clause with a nominative NP ◊var{x} and an a semblative NP ◊var{y} implies that the referents of ◊var{x} are like the referents of ◊var{y} (as described by the semblative case).}
}

Nominative–nominative and nominative–accusative verbless clauses are often used as a pseudo-clefting construction to place a noun phrase as the focus (see ◊xref/c["infostruct.html" #:type 'inline]{Information structure}):

◊gloss/x{
  ◊glfree{◊nc{nemirin #saþo mênčeþ.}}
  ◊gla{nem-irin #saþ-o mênč-eþ.}
  ◊glb{apple-%acc.%sg %name-%nom.%sg eat-3%sg.%past.%pfv}
  ◊glfree{#saþo ate the apple.}
}
◊gloss/x{
  ◊glfree{◊nc{nemirin mînčac nava #saþo.}}
  ◊gla{nem-irin mînč-ac nav-a #saþ-o.}
  ◊glb{apple-%acc.%sg eat-%rel.%nom,%nom.%hum person-%nom.%sg %name-%nom.%sg}
  ◊glfree{It was #saþo who ate the apple.}
}

Verbless independent clauses can also occur when the primary verb of a clause modified by a converbal clause or a ◊i{so}-clause is the same as that of the subordinate clause and is ellipsed.

◊subsection[#:id "postposed"]{Adjunct postposition}

An adjunct (an adverbial noun phrase, a relational phrase, or a ◊i{so}-clause) or a ◊xref["../morphology/nouns.html" "quotatives" "Section"]{quotation continuation} can be postposed after the verb of an independent clause. If this happens, a ◊i{vas} is inserted before the postposed phrase; there is a space before but not after the ◊i{vas}.

◊section[#:id "dependent"]{Dependent clauses}

These clauses are introduced in ◊xref/c["../morphology/nouns.html" #:type 'inline]{Nouns} and ◊xref/c["../morphology/verbs.html" #:type 'inline]{Verbs}:

◊items{
  ◊item{◊xref["../morphology/nouns.html" "quotatives" "Section"]{◊term{Quotatives}}, which use a sentence wrapped inside quotation marks, followed by a particle}
  ◊item{◊xref["../morphology/verbs.html" "participle" "Section"]{◊term{Relative clauses}}, which use a participle form of a verb}
  ◊item{◊xref["../morphology/verbs.html" "converb" "Section"]{◊term{Converbal clauses}}, which use a converb}
  ◊item{◊xref["../morphology/verbs.html" "so-clauses" "Subsection"]{◊term{◊i{So}-clauses}}, which use a gICP (i.e. finite form of a verb) plus a ◊term{◊i{so}-particle}}
  ◊item{◊xref["../morphology/verbs.html" "nominalized" "Section"]{◊term{Nominalized clauses}}, which use a nominalized verb}
}

In all such clauses, the verb comes at the end of the clause (followed by a ◊term{◊i{so}-particle} for ◊i{so}-clauses).

◊section[#:id "particles"]{Head and tail particles}

Ŋarâþ Crîþ has both head and tail particles, which occur at the extremes of an ICP. ◊term{Absolute head particles} (◊term{aheadps}) appear at the beginning of an ICP:

◊items{
  ◊item{◊l1{ai} ◊trans{but, however} is used to contrast the idea of the clause in question with that of an earlier one.}
  ◊item{◊l1{ea} ◊trans{thus, therefore, in addition} is used to imply that the clause in question is the result of an earlier one, or that the clause in question adds information to an earlier one.}
  ◊item{◊l1{vjor} ◊trans{alternatively} is used to contrast a clause with an earlier alternative.}
}

◊term{Conjunct head particles} (◊term{cheadps}) appear at the beginning of an ICP, but if a so-clause is present, then it may occur at the start of the independent clause proper, immediately after the ◊i{so}-particle:

◊items{
  ◊item{◊l1{ša}, inside an ICP, indicates an interrogative sentence. This particle can also be used at the beginning of a ◊i{so}-clause, in which case it indicates an irrealis modality.}
  ◊item{◊l1{le} indicates an ◊xref["../morphology/verbs.html" "modality-imperative" "Subsection"]{imperative} or hortative modality.}
}

In informal speech, the placement of cheadps is more relaxed: they might, for instance, occur after a nominalized verb phrase or after an oblique noun phrase.

◊term{Tail particles} (◊term{tailps}) are used less often than head particles and often serve a pragmatic role. Omitting them can be seen as stoic. Prosodically, the final phoneme of a tail particle is often lengthened.

◊items{
  ◊item{◊l1{šan} indicates a tag question. If this particle is used, then ◊l1{ša} is omitted, but the ◊i{šac} is not.}
  ◊item{◊l1{þal} is used to make assertions. When used with the imperative, it marks a stronger imperative. When this particle appears after a word ending in ◊l1{-þ} but not in ◊l1{-cþ}, then the ending and the particle dissimilate into ◊l1{-s tal}.}
  ◊item{◊l1{se} indicates a rhetorical question or occasionally a mirative mood. Regardless of its use, it is not used with a ◊i{šac}.}
  ◊item{◊l1{ado} shows that the speaker laments the preceding statement.}
  ◊item{◊l1{viþca} marks the ◊xref["../morphology/verbs.html" "modality-conditional" "Subsection"]{conditional} mood.}
}

◊section[#:id "scope"]{Scope ordering}

Several constructs in Ŋarâþ Crîþ can produce a new scope. These include the universal and existential quantifiers ◊l1{šino} and ◊l1{nema}, numerals, coordinate phrases, and certain auxiliary verbs.

Scopes created by noun phrases follow linear order. In other words, the outermost quantifier corresponds to the outermost level of quantification:

◊gloss/x{
  ◊glfree{◊nc{šine nemar racro.}}
  ◊gla{šin-e nem-ar racr-o.}
  ◊glb{all-%nom.%pl any-%acc.%pl know-3%pl}
  ◊glfree{All of them know someone out of them. = For all ◊var{x}, there exists ◊var{y} such that ◊var{x} knows ◊var{y}.}
}

◊gloss/x{
  ◊glfree{◊nc{nemar šine racro.}}
  ◊gla{nem-ar šin-e racr-o.}
  ◊glb{any-%acc.%pl all-%nom.%pl know-3%pl}
  ◊glfree{There is someone out of them whom all of them know. = There exists ◊var{y} such that for all ◊var{x}, ◊var{x} knows ◊var{y}.}
}

◊gloss/x{
  ◊glfree{◊nc{šine #saþon #môran’te racro.}}
  ◊gla{šin-e #saþ-on #môr-an=’te racr-o.}
  ◊glb{all-%nom.%pl %name-%acc.%sg %name-%acc.%sg=or know-3%pl}
  ◊glfree{All of them know either #saþo or #môra. = For all ◊var{x}, ◊var{x} knows #saþo or #môra.}
}

◊gloss/x{
  ◊glfree{◊nc{#saþon #môran’te šine racro.}}
  ◊gla{#saþ-on #môr-an=’te šin-e racr-o.}
  ◊glb{%name-%acc.%sg %name-%acc.%sg=or all-%nom.%pl know-3%pl}
  ◊glfree{Either all of them know #saþo, or all of them know #môra. = There exists ◊var{y} in {#saþo, #môra} such that for all ◊var{x}, ◊var{x} knows ◊var{y}.}
}

TODO: figure out scope ordering involving auxiliary verbs or subordinate clauses

◊gloss/x[#:id "qs4"]{
  ◊glfree{◊nc{šinai lensat rjota.}}
  ◊gla{šin-ai lens-at rjot-a.}
  ◊glb{all-%dat.%pl help-%inf cannot-1%sg}
  ◊glfree{There is no one I can help. = For all ◊var{x}, I can’t help ◊var{x}.}
}

◊gloss/x[#:id "qs5"]{
  ◊glfree{◊nc{#saþo šinai lensat rjote.}}
  ◊gla{#saþ-o šin-ai lens-at rjot-e.}
  ◊glb{%name-%nom.%sg all-%dat.%pl help-%inf cannot-3%sg}
  ◊glfree[#:prefix "(?a) "]{There are some people #saþo can’t help.}
  ◊glfree[#:prefix "(b) "]{There is no one #saþo can help.}
}

◊gloss/x[#:id "bought"]{
  ◊glfree{◊nc{#saþo šinor vaðit vandraþ.}}
  ◊gla{#saþ-o šin-or vað-it vandr-aþ.}
  ◊glb{%name-%nom.%sg all-%acc.%pl buy-%inf leave_undone-3%sg.%inv}
  ◊glfree[#:prefix "(?a) "]{#saþo didn’t buy everything.}
  ◊glfree[#:prefix "(b) "]{#saþo didn’t buy anything.}
}

◊gloss/x[#:id "magnets"]{
  ◊glfree{◊nc{le morencivistan gostoþ ila fêfana’te inos šesos grenfit peču.}}
  ◊gla{le morencivist-an gost-oþ ila fê-fana=’te in-os šes-os grenf-it peč-u.}
  ◊glb{%imp magnet-%acc.%gen machine-%dat.%sg on.%adn 3%gc-next_to.%adn=or place-%dat.%di always-%loc.%di put-%inf avoid-3%gc}
  ◊glfree{Never put magnets on or near the machine.}
}

◊section[#:id "questions"]{Questions}

All questions contain either the cheadp ◊l1{ša} or, in the case of a tag question, the tailp ◊l1{šan}. If the last clause of a sentence is interrogative, then it is terminated by a ◊l1{?}. In colloquial speech, ◊l1{ša} may be omitted, but this is never done in song lyrics.

◊term{Polar questions} ask whether a statement is true and are created using the cheadp ◊l1{ša} on the statement that is questioned. They can be answered using ◊l1{vil} (the statement is true) or ◊l1{ces} (the statement is false).

◊gloss/x{
  ◊glfree{◊nc{ša lê tfoþos gðenuveþ?}}
  ◊gla{ša lê tfoþ-os gðen-u-ve-þ?}
  ◊glb{%int %this.%cel village-%loc.%sg give_birth-3%gc-2%sg-%past}
  ◊glfree{Were you born in this village?}
}

◊term{Tag questions}, which are created using the tailp ◊l1{šan} instead of ◊l1{ša}, are leading toward an affirmative answer. There is no separate way to create a leading question toward a negative answer.

◊term{◊i{Wh}-questions}, in addition to ◊l1{ša}, contain one or interrogative pro-forms, each of which can be an ◊xref["../morphology/nouns.html" "pronouns-interrogative" "Subsection"]{◊term{interrogative pronoun}}, a noun phrase modified by the interrogative determiner ◊l1{mê} ◊trans{which}, or the pro-verb ◊l1{nepit}. The questioned element stays in its original position.

The following elements can be questioned:

◊items{
  ◊item{all noun phrases that are arguments or adjuncts to the main clause}
  ◊item{all complements of relational phrases that are adjuncts to the main clause}
  ◊item{the second element of commutative nominal coordinate structures and either element of noncommutative nominal coordinate structures, if the entire coordinate phrase could be replaced with an interrogative pronoun}
  ◊item{any verb in the main clause}
}

If an interrogative pronoun is modified, then the domain of answers is similarly restricted.

Answers to ◊i{wh}-questions are given in the same order as the interrogative pro-forms appear, with the same morphological forms.

◊term{Choice questions} list the options that the answer is expected to be selected from. In Ŋarâþ Crîþ, they are a special case of ◊i{wh}-question, in which the interrogative pronoun ◊l1{meel} ◊trans{which one?}, with the choices, joined by the coordinator ◊l1{=’ce}, being a genitive adjunct to that pronoun, is the element being questioned. In this case, ◊l1{meel} can appear wherever an interrogative pronoun could, and the answers take the same form as ◊l1{meel}. ◊l1{meel} is singular if exactly one answer is expected, but plural if there is no such expectation.

◊section[#:id "data-pos"]{Data}

A ◊term{datum} is one of the following:

◊items{
  ◊item{A short numeral (alone)}
  ◊item{A noun phrase in the nominative case}
  ◊item{An amount of ◊xref["../lexicon/numerals.html" "units-currency" "Subsection"]{currency}, denoted using the ◊l1{9{}} or the ◊l1{*9{}} numquote}
  ◊item{A list whose elements are data, denoted using the ◊l1{3{}} ◊xref["../phonology/layer1.html" "numquotes" "Section"]{numquote} with elements separated by spaces. Elements with spaces on the outer level are grouped with ◊l1{{}}.}
  ◊item{A key-value list whose keys are arbitrary strings (but usually nominative-case noun phrases) and whose values are data. Each key is wrapped inside the ◊l1{3{}} numquote, followed by the corresponding value inside the ◊l1{4{}} numquote. The elements are collectively wrapped inside the ◊l1{2{}} numquote.}
}

A datum that is a list or a key-value list is called a ◊term{compound datum}.

Arbitrary strings can be contained inside a datum by casting them into nouns using a direct ◊xref["../morphology/nouns.html" "quotatives" "section"]{quotative} particle. Alternatively, the particle ◊l1{neþþo} before a list or key-value list applies the quotative to each element of a list or to each value of a key–value list. In this case, the outer quotation marks may be omitted (with multi-word phrases being encompassed by grouping brackets).

A datum by itself can be used as a sICP to convey the information contained therein. It can also be cast into a noun using a zero genitive construct, involving the noun ◊l1{manveo} ◊trans{datum} immediately followed by the datum. This noun can be replaced with a more specific term that describes the referent of the datum. If a verbless independent clause ends with a datum under ◊l1{manveo} in the nominative case, then this occurrence of ◊l1{manveo} can be omitted.

◊gloss/x[#:id "manveo-elision"]{
  ◊glfree{◊nc{ociro 2{3{ineþav·rêma} 4{9{B/’}} 3{*srela} 4{9{rł1 mA}} 2{fîðiłir} 3{9{10/2}}}.}}
  ◊gla{ocir-o 2{3{ineþav·rêm-a} 4{9{B/’}} 3{*srel-a} 4{9{rł1 mA}} 2{fîðił-ir} 3{9{10/2}}}.}
  ◊glb{price-%nom.%pl %map:%key:notebook-%nom.%sg %value:%cur:11/- %key:backpack-%nom.%sg %value:%cur:£1 10s %key:broom-%nom.%sg %value:%cur:16/2}
  ◊glfree{The prices are: notebook: 11/-, backpack: £1 10s, broom: 16/2.}
}
