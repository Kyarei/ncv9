#lang pollen

◊define-meta[title]{Relationals}

In this chapter, we cover relationals.

◊section{Valency and case frame}

All predicates have at least a nominative-case argument (the ◊term{subject}). Most relationals are divalent; the second argument (the ◊term{object}) is dative by default.

Some relationals, such as ◊l1{tfel} ◊trans{◊S is on the other side of ◊O relative to ◊O2}, are trivalent. In this case, one of the objects is termed the ◊term{main object} and the other is called the ◊term{ancillary object}. In divalent relationals, of course, the main object is the only one involved.

If a relational encodes a spatial relationship between one entity and another, then two additional relationals (◊term{relationals of motion}) are derived: the ◊term{allative}, encoding motion toward the main object and the ◊term{ablative} encoding motion away from the main object. The complement of an allative relational is inflected in the accusative case. The complement of an ablative relational is dative for third- and sixth-declension nouns and abessive otherwise, although in colloquial language, the locative may be used instead.

When a fifth-declension noun is the object of the relational ◊l1{es} ◊trans{◊S is contained inside ◊|O|; ◊S is in progress of doing ◊|O|} (in static motion), it is inflected in the locative case rather than the dative.

◊section[#:id "attachment"]{Attachment}

A relational can be used ◊term{adverbially} or ◊term{adnominally}, that is, modifying either a verb phrase or a noun phrase. We call this distinction ◊term{attachment}. Adnominal usage simply places the affected noun phrase as the subject of the action depicted by the relational. Adverbial usage, on the other hand, does one of the following: (1) place the action depicted by the VP as the subject of the relational, (2) indicate that as a result of the action, a shifted subject of the VP starts to participate as a subject of the relational.

◊section[#:id "inflection"]{Inflection}

Each relational has an ◊term{absolute} (A), ◊term{adverbial} (V), ◊term{adnominal} (N), and ◊term{conjunct} (C) form. The A, V, and N forms are stems, while the conjunct form is a sequence of one or more simple syllables. When A, V, or N occurs word-finally, it is assumed to be fused with the null consonant.

Most relationals have either V or N be equal to A. Those where A = V are called adverbial relationals, while those with A = N are called adnominal relationals. A few relationals have both V and N be distinct from A.

The consonantal conjunct form of a relation is C, while the vocalic conjunct form is A. However, if A ends in a vowel, then this is removed from the vocalic conjunct form.

◊subsection[#:id "modforms"]{Modifying forms}

A relational that is not a relational of motion takes the form ◊b{[<object prefix>] + <N or V>}.

The object prefix is like the object affix in verb conjugation; in other words, it is used if the object is pronominal. Nevertheless, it has different forms from the verbal object affixes:

◊table/x[#:options (table-options #:caption "Object affixes." #:placement 'please-here)]{
  Person \ Number & Singular & Dual & Plural & Generic
  1st excl. & ◊nc{e(l)-} & ◊nc{ec-} & ◊nc{en-} & ◊nc{ef-}
  1st incl. & & ◊nc{êc-} & ◊nc{ên-} &
  2nd & ◊nc{o-} & ◊nc{oc-} & ◊nc{on-} & ◊nc{of-}
  3rd celestial & ◊nc{er-}
  3rd terrestrial & ◊nc{os-}
  3rd human & ◊nc{an-} & ◊nc{or-} & ◊nc{ran-}
  3rd epicene &  & ◊nc{ac-} & ◊nc{ren-} & ◊nc{fê(s)-}
  Reflexive & ◊colspan[4]{◊nc{ce(n)-}}
  Reciprocal & ◊colspan[4]{◊nc{re(þ)-}}
}

(The consonants in brackets are included only if the stem starts with ◊l0{e-} or ◊l0{ê-}.)

As an exception, a relational at the head of a postposed relational phrase without an object prefix uses the A stem instead of the V stem.

A relational of motion takes the form ◊b{[<object prefix>] + <A> + <motion suffix>}. The motion suffix takes one of the following forms depending on the direction of motion and the attachment of the resulting relational:

◊table/x[#:options (table-options #:caption "Motion suffixes." #:placement 'please-here)]{
  Direction \ Attachment & Adverbial & Adnominal
  Allative & ◊l0{-ar} & ◊l0{-ara}
  Ablative & ◊l0{-jas}* & ◊l0{-eis}
}

The ablative adverbial motion suffix varies depending on the form of A:

◊items{
  ◊item{If A ends in a vowel, then the suffix is always ◊l0{-jas}.}
  ◊item{Otherwise, if the last syllable of A has an ◊l0{e} or ◊l0{ê}, then it is always ◊l0{-as}.}
  ◊item{Otherwise, if the last syllable of A has an ◊l0{a} or ◊l0{â}, then it is always ◊l0{-es}.}
  ◊item{Otherwise, it is ◊l0{-es} if bit 1 of the letter sum of A is set and ◊l0{-as} if it is unset.}
}

If a motion suffix that starts with a vowel follows a stem ending in a vowel of the same quality, then the final vowel of the stem is inverted and the initial vowel of the suffix is omitted.

In trivalent relationals, the ancillary object affixes occur at the end of the relational. If there is no motion suffix, then such a suffix occurs at an onset end:

◊table/x[#:options (table-options #:caption ◊@{
  Ancillary object affixes (for onset ends).
} #:placement 'please-here)]{
  Person \ Number & Singular & Dual & Plural & Generic
  None & ◊nc{-(e)s}
  1st excl. & ◊nc{-ef} & ◊nc{-ecþ} & ◊nc{-if} & ◊nc{-af}
  1st incl. & & ◊nc{-êcþ} & ◊nc{-îf} &
  2nd & ◊nc{-or} & ◊nc{-ocþ} & ◊nc{-orþ} & ◊nc{-of}
  3rd celestial & ◊nc{-ir}
  3rd terrestrial & ◊nc{-jos}
  3rd human & ◊nc{-aren} & ◊nc{-oł} & ◊nc{-ens}
  3rd epicene &  & ◊nc{-ac} & ◊nc{-erþ} & ◊nc{-∅}
  Reflexive & ◊colspan[4]{◊nc{-ef}}
  Reciprocal & ◊colspan[4]{◊nc{-iren}}
}

Otherwise, the ancillary object suffix occurs at a syllabic end and a different set of suffixes is used:

◊table/x[#:options (table-options #:caption ◊@{
  Ancillary object affixes (for syllabic ends).
} #:placement 'please-here)]{
  Person \ Number & Singular & Dual & Plural & Generic
  None & ◊nc{-(e)s}
  1st excl. & ◊nc{-ef} & ◊nc{-ecþ} & ◊nc{-if} & ◊nc{-af}
  1st incl. & & ◊nc{-êcþ} & ◊nc{-îf} &
  2nd & ◊nc{-or} & ◊nc{-ocþ} & ◊nc{-orþ} & ◊nc{-of}
  3rd celestial & ◊nc{-ir}
  3rd terrestrial & ◊nc{-jos}
  3rd human & ◊nc{-ran} & ◊nc{-lor} & ◊nc{-ren}
  3rd epicene &  & ◊nc{-ac} & ◊nc{-erþ} & ◊nc{-∅}
  Reflexive & ◊colspan[4]{◊nc{-lef}}
  Reciprocal & ◊colspan[4]{◊nc{-rin}}
}

After a vowel or a ◊l0{-l}, the suffix for an explicit ancillary object becomes ◊l0{-s}.

If both the main and ancillary objects are specified as noun phrases to an attributive relational, then the ancillary object is eclipsed and follows the main object.

◊subsection[#:id "finforms"]{Finite forms}

A relational can be used predicatively using a finite form that attaches it to a scaffolding verb, either affirmative or negative. Thus the relational acts like a verb syntactically.

The anatomy of the finite form of a relational is ◊b{[<motion prefix>] + <C> + <finite form of scaffolding verb>}. The scaffolding verb can be either ◊l1{eþit} (for the affirmative) or ◊l1{telit} (for the negative).

The motion prefix is ◊l0{ar-} for allative motion and ◊l0{as-} for for ablative motion.

The object affix on the scaffolding verb refers to the main object in divalent relationals. In trivalent relationals, it refers to the main object by default, but if the main object is explicitly specified as a noun phrase, then the object affix refers to the ancillary object instead.

If the ancillary object is specified as a noun phrase, then it is preceded by the particle ◊l1{os} and eclipsed.

If a relational is a target of an auxiliary, then the scaffolding verb contracts to ◊l0{-is} for ◊l1{eþit} and ◊l0{-cest} for ◊l1{telit}. If such a relational previously governed the dative, then it now governs the accusative in this case.

Immediately before a ◊i{so}-particle, the unmarked form of a relational is always used in place of the finite form if ◊|S| is in the third person.

◊subsection[#:id "nomforms"]{Nominalized forms}

The nominalized form of a verb describes the action referenced by the verb. In contrast, the nominalized form of a relational describes the subject involved in the state described and is always a noun with mass clareþ. Relationals are classified by how their nominalized forms are derived.

Type I relationals, which are most often short, common, and describing spatial relationships, give rise to first-declension celestial ◊l0{-e} nouns. Given a nominal multisyllable ◊nc{◊ṡ{B}}, which is ◊nc{◊ṡ{C}} for non-motion relationals and ◊nc{◊ṡ{A}} plus the appropriate adverbial motion affix for relational motions, the stems are ◊l0{◊ṡ{B}tar}, ◊l0{◊ṡ{B}tor}, and ◊l0{◊ṡ{B}tel} and ◊nc{◊ṫ[1]} is ◊l0{e}.

To derive the nominalized form of a type II relational, we define a nominal stem ◊nc{◊ṡ{B}}. For non-motion relationals, this is equal to ◊nc{◊ṡ{A}} if ◊nc{◊ṡ{N}} has more syllables as ◊nc{◊ṡ{A}}, and ◊nc{◊ṡ{N}} otherwise. For relationals of motion, this is ◊nc{◊ṡ{A}} plus the appropriate adnominal motion suffix.

(For both Type I and Type II relationals, the derivation of ◊nc{◊ṡ{B}} for relationals of motion follows the same syncope rules as the attributive forms.)

The resulting noun is a second-declension celestial ◊l0{-er} noun (with ◊nc{◊ṫ[1]} = ◊l0{e}) if the letter sum of ◊nc{◊ṡ{N}} is odd and a third-declension terrestrial ◊l0{-os} noun if the letter sum is even.

If ◊nc{◊ṡ{B}} contains only one full syllable and its final bridge consists of at most one consonant and no ◊l0{j}, then the N stem is ◊l0{◊ṡ{B}al}, the L stem is either ◊l0{◊ṡ{B}el} or ◊l0{◊ṡ{B}il}, and the S stem is ◊l0{◊ṡ{B}al}. In particular, the L stem is ◊l0{◊ṡ{B}il} if the last vowel in ◊nc{◊ṡ{B}} is ◊l0{e} or ◊l0{ê} and ◊l0{◊ṡ{B}el} otherwise.

Otherwise, if ◊nc{◊ṡ{B}} ends with a vowel, then the N stem is ◊l0{◊ṡ{B}s} and the S stem is ◊l0{◊ṡ{B}d}. The L stem is derived from the N stem by doing the following:

◊items{
  ◊item{If the final vowel is ◊l0{a} or ◊l0{e}, then change it to ◊l0{o}.}
  ◊item{If the final vowel is ◊l0{o}, then change it to ◊l0{a}.}
  ◊item{If the final vowel has a low tone, then change it to high.}
  ◊item{Otherwise, change the final bridge to ◊l0{-st-}.}
}

If ◊nc{◊ṡ{B}} has two or more full syllables and does not end with a vowel, then the N stem is ◊l0{◊ṡ{B}}, and the S stem is ◊l0{◊ṡ{B}}. The L stem depends on the motion suffix:

◊items{
  ◊item{If there is no motion suffix, then the L stem is the conjunct form followed by ◊l0{l}.}
  ◊item{If the allative suffix would be used, then the L stem is ◊l0{◊ṡ{A}◊ẋ{þ}er}.}
  ◊item{If the ablative suffix would be used, then the L stem is ◊l0{◊ṡ{A}◊ẋ{t}as}.}
}

A genitive noun phrase modifying a nominalized form of a relational indicates the object of the state described.

◊section{Interactions with predicate modifiers}

A noun phrase in the accusative case plus the clitic ◊l1{=’po} is a predicate modifier that acts on relationals implying separation between two objects (spatially or temporally) and describes the degree to which they are separated. For nouns that are units of measure other than ◊l1{enva} or ◊l1{elva}, using the semblative case has the equivalent effect as ◊l1{=’po}.

The particle ◊l1{pâ} directly before a relational describing a spatial relationship can be translated as ◊trans{directly} or ◊trans{precisely}. With the relational ◊l1{nîs} describing a span of time over which an action takes place, ◊l1{pâ} implies that the action is continuous.

Prefixing ◊l1{do-} to a relational switches the order of ◊S and ◊|O|; the case of the complement remains the same. Such a relational is used only on an attributive or nominalized form. That is, the same prefix on a finite relational is interpreted as a causative prefix as usual.

◊section[#:id "coordination"]{Coordination of relationals}

Attributive relational phrases are coordinated using the ◊xref["nouns.html" "coordination" "Section"]{same clitics for nominal coordination}:

◊gloss/x{
  ◊glfree{◊nc{erinoroþ ila naftes sênen’ce ono leþin nefa valtanis.}}
  ◊gla{erinor-oþ ila naft-es sênen’ce on-o leþ-in nefa valt-anis.}
  ◊glb{branch-%dat.%sg on.%adn pond-%dat.%sg=and above.%adn nest-%dat.%sg bluebird-%nom.%co two.%nom.%cel reside-3%du.%inv}
  ◊glfree{In a nest on a branch, above a pond, lived two bluebirds.}
}

The complement of the relational phrases can be shared by using the third-person generic prefix for the latter relational:

◊gloss/x{
  ◊glfree{◊nc{le morencivistan gostoþ ila fêfana’te inos šesos grenfit peču.}}
  ◊gla{le morencivist-an gost-oþ ila fê-fana=’te in-os šes-os grenf-it peč-u.}
  ◊glb{%imp magnet-%acc.%gen machine-%dat.%sg on.%adn 3%gc-next_to.%adn=or place-%dat.%di always-%loc.%di put-%inf avoid-3%gc}
  ◊glfree{Never put magnets on or near the machine.}
}

Predicative relational phrases, taking the form of verbs, are coordinated ◊xref["verbs.html" "converbs" "Section"]{similarly to verbs}.

◊section[#:id "reltour"]{A tour of relationals}

This section gives an overview of the relationals of Ŋarâþ Crîþ.

◊subsection[#:id "reltour-spatial"]{Spatial relationals}

◊table/x[#:options (table-options #:caption "Spatial relationals in Ŋarâþ Crîþ." #:first-col-header? #f #:placement 'please-here) #:id "spatial-relationals"]{
  Relational & Gloss
  ◊nc{ar} & ◊trans{toward}
  ◊nc{jas} & ◊trans{away from}
  ◊nc{nîs} & ◊trans{through}
  ◊nc{âŋa} & ◊trans{bending toward}
  ◊nc{es} & ◊trans{inside}
  ◊nc{car} & ◊trans{outside of}
  ◊nc{il} & ◊trans{on top of}
  ◊nc{sêna} & ◊trans{above}
  ◊nc{čil} & ◊trans{on (a vertical surface)}
  ◊nc{desa} & ◊trans{below}
  ◊nc{etor} & ◊trans{in front of}
  ◊nc{þon} & ◊trans{in the midst of, in the middle of}
  ◊nc{cþar} & ◊trans{around, surrounding}
  ◊nc{cþarnîs} & ◊trans{revolving around}
  ◊nc{meþos} & ◊trans{taking a winding path around}
  ◊nc{fan} & ◊trans{next to, beside}
  ◊nc{dełir} & ◊trans{next to a body of water}
  ◊nc{nerła} & ◊trans{between}
  ◊nc{tfel} (3val) & ◊trans{across}
  ◊nc{lef} & ◊trans{perpendicular to}
  ◊nc{fansêna} & ◊trans{above by an offset}
}

Some words denoting such relations are verbs instead of relationals:

◊items{
  ◊item{◊l1{ecljat} = ◊trans{◊S is far from ◊I}}
  ◊item{◊l1{cþîšat} = ◊trans{◊S is near ◊O}}
}

◊subsection[#:id "reltour-temporal"]{Temporal relationals}

◊table/x[#:options (table-options #:caption "Temporal relationals in Ŋarâþ Crîþ." #:first-col-header? #f #:placement 'please-here) #:id "temporal-relationals"]{
  Relational & Gloss
  ◊nc{tecto} & ◊trans{before}
  ◊nc{mîr} & ◊trans{after}
  ◊nc{nîs} & ◊trans{during, while}
  ◊nc{els} & ◊trans{by, no later than [◊S is completed on or before the time ◊|O|]}
}

Some words denoting such relations are verbs instead of relationals:

◊items{
  ◊item{◊l1{cjašit} = ◊trans{◊S begins at the time of ◊I}}
}

◊subsection[#:id "reltour-syntactic"]{Syntactic relationals}

These relationals are used solely for syntactic support.

◊items{
  ◊item{◊l1{ro} marks the former indirect object when the causative voice is applied on a ditransitive verb.}
  ◊item{◊l1{peŋan} is used to mark the compared object in an equal comparison.}
  ◊item{◊l1{îþ} forms the superlative. It is defined as ◊trans{◊S performs an action to the greatest extent in or among ◊|O|}.}
}

◊subsection[#:id "reltour-mathematical"]{Mathematical relationals}

These relationals denote mathematical relations.

◊items{
  ◊item{◊l1{ema} = ◊trans{other than, not equal to}. Its antonym is the verb ◊l1{censit}.}
  ◊item{◊l1{cor} = ◊trans{not one of}. Its antonym is the verb ◊l1{varit}.}
  ◊item{◊l1{celna} = ◊trans{about the same as, about equal to}.}
}

Some words denoting such relations are verbs instead of relationals:

◊items{
  ◊item{◊l1{censit} = ◊trans{◊S is equal to ◊I}}
  ◊item{◊l1{varit} = ◊trans{◊S is one of ◊O}}
  ◊item{◊l1{mirit} = ◊trans{◊S is greater than ◊O by a margin of ◊I}}
  ◊item{◊l1{łavrit} = ◊trans{◊S is less than ◊O by a margin of ◊I}}
}

◊subsection{Other relationals}

◊items{
  ◊item{◊l1{dês}: ◊trans{instead of, rather than}}
  ◊item{◊l1{caf}: ◊trans{instead of, in place of}}
  ◊item{◊l1{ton}: ornative = ◊trans{◊S has ◊|O| attached to it as a feature or accessory}}
  ◊item{◊l1{vôr}: ◊trans{◊O is abundant within ◊|S|}}
  ◊item{◊l1{roc} (becoming ◊l1{rille} after the clitic ◊l1{=’moc}) = ◊trans{on behalf of}}
  ◊item{◊l1{nedo} = ◊trans{◊S happens in spite of ◊|O|}}
  ◊item{◊l1{uc} = ◊trans{◊S resembles ◊O in appearance, visually or otherwise}}
  ◊item{◊l1{mesa} and ◊l1{rjas} both translate to ◊trans{between}…}
  ◊item{◊l1{cire}: ◊trans{◊S has a price of ◊|O|}}
  ◊item{◊l1{csel}: ◊trans{◊S is done in response to a fear of ◊|O|}}
  ◊item{◊l1{creł}: ◊trans{against}}
  ◊item{◊l1{soþra}: ◊trans{in anticipation for}}
  ◊item{◊l1{peþôr}: ◊trans{formally defined as}}
  ◊item{◊l1{lef}, in addition to its spatial meaning, is used to mean ◊trans{unrelated to}.}
  ◊item{◊l1{es}, in addition to its spatial meaning, is used for the progressive aspect.}
  ◊item{◊l1{desa}, in addition to its spatial meaning, is used to mean ◊trans{regarding} or ◊trans{related to}.}
}
