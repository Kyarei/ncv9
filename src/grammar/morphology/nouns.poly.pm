#lang pollen

◊define-meta[title]{Nouns}

Nouns are also declined for nominal number and case. These declensions are divided into six ◊term{declension classes}, with a few irregular nouns failing to fall into any class. There are three broad categories of declension classes:

◊items{
  ◊item{Celestial (I and II): contain mainly celestial nouns and have suffixed instrumental and abessive forms}
  ◊item{Terrestrial (III): contain mainly terrestrial nouns and have circumfixed instrumental and abessive forms. Lack any thematic vowel.}
  ◊item{Stochastic (IV and V): the lemma has no suffix in the sense that I – III do; these classes employ rolls for some of the forms.}
}

The sixth declension is considered a hybrid between the celestial and terrestrial categories. Additionally, the second declension has penultimate and ultimate variants.

Any declension class can contain words of the human class, since names can in theory be derived from any content noun.

In all regular declension classes, genitive dual, plural, and singulative forms are eclipsed. Indeclinable parts of compound nouns do not have this behavior. Most irregular nouns do, although there are exceptions.

Most nouns have at least ◊term{N}, ◊term{L}, and ◊term{S} stems. The N stem is used for the nominative, accusative, genitive, and dative cases; the L stem is used for the locative, instrumental, and abessive; and the S stem is used for the semblative. The second penultimate declension additionally has a ◊term{G} stem, while third-declension ◊l0{-el} nouns have an ◊term{A} stem on top. The sixth declension adds ◊term{I} and ◊term{I◊prime} stems. In contrast, the fifth declension lacks an L stem. In declension classes that have one, the L stem is almost always distinct from the N stem because some forms differ only in the use of an N or an L stem.

Themes in declension classes can be classified into ◊term{thematic vowels} and ◊term{thematic consonants}. The ◊term{primary thematic vowel} (where it exists) is denoted by ◊l0{◊ṫ[0]}, and the ◊term{locative thematic vowel} is denoted by ◊l0{◊ṫ[1]}. In declension classes that have one, the thematic consonant is denoted by ◊l0{◊ṫ[2]}.

In addition, all declension classes use one or two phi consonants: ◊nc{◊ṫg{L}} and, for some classes, ◊nc{◊ṫg{N}}.

The first, second, and third declensions admit words of different ending types, which have different rules for certain forms. Each declension class is first given for the most representative ending type, followed by deviations for other endings.

◊(define (ncv . text)
  `(cls "varcell"
    (,@(apply nc text)
     ,(hatnote "varcell-note" "Variable entry" "Δ"))))

◊section[#:id "decl1"]{The first declension}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[0]} can be ◊l0{a}, ◊l0{e}, or ◊l0{o}.}
  ◊item{◊nc{◊ṫ[1]} can be ◊l0{a}, ◊l0{e}, or ◊l0{i}.}
  ◊item{◊nc{◊ṡ{L}} must be different from ◊nc{◊ṡ{N}}, unless ◊nc{◊ṫ[0]} is ◊l0{o} and ◊nc{◊ṫ[1]} is ◊l0{e}. The most common difference is to change the final vowel of ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
  ◊item{◊nc{◊ṡ{N}} cannot end in ◊l0{-nn} if ◊nc{◊ṫ[0]} is ◊l0{e}; otherwise, ◊nc{◊ṡ{N}◊ẋ{n}} is the same as ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ḋ[0]} = ◊l0{◊ṫ[0] ◊ṫ[0] e ◊ṫ[0] e e} « ◊nc{N}}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for first-declension -V nouns.
  ◊sup{1} ◊l0{i} in the suffix becomes ◊l0{e} after the onsets ◊l0{t-}, ◊l0{d-}, ◊l0{s-}, ◊l0{þ-}, ◊l0{ð-}, ◊l0{tf-}, or ◊l0{dv-}, as well as any onsets that end with ◊l0{l}.
} #:placement 'forcehere) #:id "declensions-1-v"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊ncv{◊ṡ{N}◊ṫ[0]} & ◊nc{◊ṡ{N}◊ṫ[0]c} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋpl}} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}l} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}◊ṫg{N}}
  Accusative & ◊ncv{◊ṡ{N}◊ṫ[0]n} & ◊nc{◊ṡ{N}ôr} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊ncv{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}e◊ṫg{N}en}
  Dative & ◊ncv{◊ṡ{N}◊ṫ[0]s} & ◊nc{◊ṡ{N}◊ẋ{t}◊ṫ[0]s} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}i} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}þ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}◊ṫg{N}es}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}n} & ◊nc{◊ṡ{N}◊ẋ{t}◊ṫ[0]◊ẋ{◊ẋgen}n} & ◊nc{◊ṡ{N}in} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}n} & ◊nc{◊ṡ{N}◊ẋ{n}e◊ṫg{N}}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]s} & ◊nc{◊ṡ{L}◊ṫ[1]c} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋpl}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{L}e◊ṫg{L}}
  Instrumental & ◊nc{◊ṡ{L}eca} & ◊nc{◊ṡ{L}ecca} & ◊nc{◊ṡ{L}ica}◊sup{1} & ◊nc{◊ṡ{L}inca}◊sup{1} & ◊nc{◊ṡ{L}e◊ṫg{L}ca}
  Abessive & ◊nc{◊ṡ{L}eþa} & ◊nc{◊ṡ{L}ecþa} & ◊nc{◊ṡ{L}iþa}◊sup{1} & ◊nc{◊ṡ{L}inþa}◊sup{1} & ◊nc{◊ṡ{L}e◊ṫg{L}þa}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for first-declension nouns.} #:placement 'forcehere) #:id "declensions-1-variable"]{
  Form & ◊l0{-◊ṫ[0]} & ◊l0{-◊ṫ[0]s} & ◊l0{-◊ṫ[0]◊ẋ{◊ẋht}þ} & ◊l0{-◊ṫ[0]n}
  Allowed ◊nc{◊ṫ[0]} & ◊nc{a}, ◊nc{e}, ◊nc{o} & ◊nc{a}, ◊nc{e} & ◊nc{a}, ◊nc{e} & ◊nc{a}, ◊nc{e}
  Nominative direct & ◊nc{◊ṡ{N}◊ṫ[0]} & ◊nc{◊ṡ{N}◊ṫ[0]s} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}þ} & ◊nc{◊ṡ{N}◊ṫ[0]n}
  Nominative plural & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋpl}} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋpl · ◊ẋht}þ} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋpl}}
  Nominative singulative & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}l} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen · ◊ẋht}þ} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}l}
  Accusative direct & ◊nc{◊ṡ{N}◊ṫ[0]n} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}ns} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}ns} & ◊nc{◊ṡ{N}◊ṫ[0]nen}
  Accusative singulative & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}s} & ◊nc{◊ṡ{N}◊ẋ{n}je} & ◊nc{◊ṡ{N}◊ẋ{n}je} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgen}s}
  Dative direct & ◊nc{◊ṡ{N}◊ṫ[0]s} & ◊nc{◊ṡ{N}o} & ◊nc{◊ṡ{N}◊ṫ[0]s} & ◊nc{◊ṡ{N}◊ṫ[0]s}
  Dative plural & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}i} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}ri} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}si} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr}ri}
  Consonantal conjunct (if ◊nc{◊ṡ{N}} is not vowel-final) & ◊nc{◊ṡ{N}◊ḋ[0]-} & ◊nc{◊ṡ{N}◊ḋ[0]-} & ◊nc{◊ṡ{N}◊ḋ[0]◊ẋ{◊ẋht}-} & ◊nc{◊ṡ{N}◊ḋ[0]n-}
  Consonantal conjunct (if ◊nc{◊ṡ{N}} is vowel-final) & ◊nc{◊ṡ{N}◊ḋ[0]-} & ◊nc{◊ṡ{N}◊ḋ[0]s-} & ◊nc{◊ṡ{N}◊ḋ[0]◊ẋ{◊ẋht}þ-} & ◊nc{◊ṡ{N}◊ḋ[0]n-}
  Vocalic conjunct & ◊nc{◊ṡ{N}-} & ◊nc{◊ṡ{N}◊ṫ[0]s-} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}þ-} & ◊nc{◊ṡ{N}◊ẋ{n}-}
}

Only a handful of ◊l0{-◊ṫ[0]n} nouns exist, and most such nouns are functional.

◊section[#:id "decl2p"]{The second declension (penultimate)}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[1]} can be either ◊l0{e} or ◊l0{i}.}
  ◊item{Has a separate ◊nc{◊ṡ{G}} stem, which can be same as or different from ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{L}} can potentially be the same as ◊nc{◊ṡ{N}} but is usually different from it. The difference is usually more substantial than a change in the final vowel of the stem.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
  ◊item{◊nc{◊ḋ[0]} = ◊l0{e i e} « ◊nc{N}}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for ◊l0{-in} and ◊l0{-is} nouns.
} #:placement 'forcehere) #:id "declensions-2p-ic"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊ncv{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}jor} & ◊ncv{◊ṡ{N}a◊ṫ[2]} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgenp}n} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊ncv{◊ṡ{N}◊ẋ{n}e} & ◊nc{◊ṡ{N}◊ẋ{n}ec} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}eħin} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}ns}
  Dative & ◊nc{◊ṡ{N}◊ẋ{t}ês} & ◊nc{◊ṡ{N}ecþ} & ◊nc{◊ṡ{N}erþ} & ◊nc{◊ṡ{N}erin} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}þ}
  Genitive & ◊ncv{◊ṡ{G}en} & ◊ncv{◊ṡ{G}jôr} & ◊ncv{◊ṡ{G}eþ} & ◊ncv{◊ṡ{G}◊ẋ{n}es} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}st}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]lt} & ◊nc{◊ṡ{L}◊ṫ[1]lt◊ṫ[0]c} & ◊nc{◊ṡ{G}◊ṫ[1]◊ẋ{e×i}lt} & ◊nc{◊ṡ{G}◊ṫ[1]lten} & ◊nc{◊ṡ{L}◊ṫ[1]◊ṫg{L}}
  Instrumental & ◊nc{◊ṡ{L}◊ṫ[1]lca} & ◊nc{◊ṡ{L}◊ṫ[1]lhac} & ◊nc{◊ṡ{L}◊ṫ[1]lco} & ◊nc{◊ṡ{L}◊ṫ[1]lcen} & ◊nc{◊ṡ{L}◊ṫ[1]lca◊ṫg{L}}
  Abessive & ◊nc{◊ṡ{L}◊ṫ[1]lþa} & ◊nc{◊ṡ{L}◊ṫ[1]lþac} & ◊nc{◊ṡ{L}◊ṫ[1]lþo} & ◊nc{◊ṡ{L}◊ṫ[1]lþen} & ◊nc{◊ṡ{L}◊ṫ[1]lþa◊ṫg{L}}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for second-declension penultimate nouns.} #:placement 'forcehere) #:id "declensions-2p-variable"]{
  Form & ◊l0{-in}, ◊l0{-is} & ◊l0{-◊ṫ[0]◊ẋ{◊ẋht}r}
  Allowed ◊nc{◊ṫ[0]} & ◊nc{i} & ◊nc{e}, ◊nc{i}
  Nominative direct & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}◊ṫ[2]}
  Nominative plural & ◊nc{◊ṡ{N}a◊ṫ[2]} & ◊nc{◊ṡ{N}î◊ṫ[2]}
  Accusative direct & ◊nc{◊ṡ{N}◊ẋ{n}e} & ◊nc{◊ṡ{N}◊ẋ{n}el}
  Genitive direct & ◊nc{◊ṡ{G}en} & ◊nc{◊ṡ{G}il}
  Genitive dual & ◊nc{◊ṡ{G}jôr} & ◊nc{◊ṡ{G}◊ẋ{t}il}
  Genitive plural & ◊nc{◊ṡ{G}eþ} & ◊nc{◊ṡ{G}evi}
  Genitive singulative & ◊nc{◊ṡ{G}◊ẋ{n}es} & ◊nc{◊ṡ{G}◊ẋ{n}il}
  Consonantal conjunct & ◊nc{◊ṡ{N}◊ḋ[0]◊ṫ[2]-} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}◊ṫ[2]-}
  Vocalic conjunct & ◊nc{◊ṡ{N}◊ḋ[0]◊ṫ[2]-} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋht}◊ṫ[2]-}
}

◊section[#:id "decl2u"]{The second declension (ultimate)}

Guidelines:

◊items{
  ◊item{As in IIp, ◊nc{◊ṫ[1]} can be either ◊l0{e} or ◊l0{i}.}
  ◊item{Does not have a separate ◊nc{◊ṡ{G}} stem.}
  ◊item{◊nc{◊ṡ{L}} can potentially be the same as ◊nc{◊ṡ{N}} but is usually different from it. The difference is usually more substantial than a change in the final vowel of the stem.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for vowel + ◊l0{r} nouns.
} #:placement 'forcehere) #:id "declensions-2u-vr"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}◊ṫ[0]c} & ◊ncv{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]◊ẋ{◊ẋgenp}n} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}s}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]rin} & ◊nc{◊ṡ{N}j◊ṫ[0]◊ẋ{◊ẋfr2 · ◊ẋht}r} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]rþ} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}ns}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]ls} & ◊nc{◊ṡ{N}◊ẋ{t}el} & ◊nc{◊ṡ{N}ari} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]ls} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}þ}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}i} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}ci} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}vi} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋfr3}ħin} & ◊nc{◊ṡ{N}◊ẋ{þ}◊ṫ[0]◊ẋ{◊ẋfr3}st}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]lt} & ◊nc{◊ṡ{L}◊ṫ[1]lt◊ṫ[0]c} & ◊nc{◊ṡ{N}◊ṫ[1]◊ẋ{e×i}lt} & ◊nc{◊ṡ{N}◊ṫ[1]lten} & ◊nc{◊ṡ{L}◊ṫ[1]◊ṫg{L}}
  Instrumental & ◊nc{◊ṡ{L}◊ṫ[1]lca} & ◊nc{◊ṡ{L}◊ṫ[1]lhac} & ◊nc{◊ṡ{L}◊ṫ[1]lco} & ◊nc{◊ṡ{L}◊ṫ[1]lcen} & ◊nc{◊ṡ{L}◊ṫ[1]lca◊ṫg{L}}
  Abessive & ◊nc{◊ṡ{L}◊ṫ[1]lþa} & ◊nc{◊ṡ{L}◊ṫ[1]lþac} & ◊nc{◊ṡ{L}◊ṫ[1]lþo} & ◊nc{◊ṡ{L}◊ṫ[1]lþen} & ◊nc{◊ṡ{L}◊ṫ[1]lþa◊ṫg{L}}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
  Consonantal conjunct & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]-}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for second-declension ultimate nouns.} #:placement 'forcehere) #:id "declensions-2u-variable"]{
  Form & ◊l0{-◊ṫ[0]r} & ◊l0{-◊ṫ[0]l} & ◊l0{-◊ṫ[0]þ} & ◊l0{-◊ṫ[0]rþ}
  Allowed ◊nc{◊ṫ[0]} & ◊nc{a}, ◊nc{e}, ◊nc{i} & ◊nc{a}, ◊nc{i}, ◊nc{o} & ◊nc{a}, ◊nc{e} & ◊nc{a}, ◊nc{e}
  Nominative plural & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}r} & ◊nc{◊ṡ{N}o◊ṫ[2]} & ◊nc{◊ṡ{N}o◊ṫ[2]}
  Vocalic conjunct & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{i/e}rl-} & ◊nc{◊ṡ{N}◊ṫ[0]ł-} & ◊nc{◊ṡ{N}◊ṫ[0]þ-} & ◊nc{◊ṡ{N}◊ṫ[0]rþ-}
}

◊section[#:id "decl3"]{The third declension}

Guidelines:

◊items{
  ◊item{There is no thematic vowel.}
  ◊item{◊nc{◊ṡ{L}} must be different from ◊nc{◊ṡ{N}}. The most common difference is to change the final vowel of ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}). In any case, it must be distinct from ◊nc{◊ṡ{L}}.}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for third-declension -os nouns.
  ◊sup{1} See below.
} #:placement 'forcehere) #:id "declensions-3-os"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊ncv{◊ṡ{N}os} & ◊nc{◊ṡ{N}oc} & ◊ncv{◊ṡ{N}or} & ◊ncv{◊ṡ{N}oren} & ◊nc{◊ṡ{N}u}
  Accusative & ◊ncv{◊ṡ{N}on} & ◊ncv{◊ṡ{N}◊ẋ{t}on} & ◊ncv{◊ṡ{N}◊ẋ{þ}on} & ◊nc{◊ṡ{N}elt} & ◊nc{◊ṡ{N}an}
  Dative & ◊nc{◊ṡ{N}oþ} & ◊nc{◊ṡ{N}◊ẋ{t}oþ} & ◊nc{◊ṡ{N}asor} & ◊nc{◊ṡ{N}◊ẋ{n}es} & ◊nc{◊ṡ{N}as}
  Genitive & ◊nc{◊ṡ{N}el}◊sup{1} & ◊nc{◊ṡ{N}◊ẋ{t}el} & ◊nc{◊ṡ{N}jel} & ◊nc{◊ṡ{N}◊ẋ{n}el} & ◊nc{◊ṡ{N}◊ẋ{n}e}
  Locative & ◊nc{◊ṡ{L}os} & ◊nc{◊ṡ{L}ocþ} & ◊nc{◊ṡ{L}or} & ◊nc{◊ṡ{L}oren} & ◊nc{◊ṡ{L}e◊ṫg{L}}
  Instrumental & ◊ncv{cja◊ṡ{L}os} & ◊ncv{cja◊ṡ{L}ocþ} & ◊nc{cja◊ṡ{L}or} & ◊nc{cja◊ṡ{L}olt} & ◊nc{cja◊ṡ{L}e◊ṫg{L}}
  Abessive & ◊ncv{þja○◊ṡ{L}os} & ◊ncv{þja○◊ṡ{L}ocþ} & ◊nc{þja○◊ṡ{L}or} & ◊nc{þja○◊ṡ{L}olt} & ◊nc{þja○◊ṡ{L}e◊ṫg{L}}
  Semblative & ◊nc{◊ṡ{S}ot} & ◊nc{◊ṡ{S}octos} & ◊nc{◊ṡ{S}et} & ◊nc{◊ṡ{S}ełi} & ◊nc{◊ṡ{S}ocþ}
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for third-declension nouns.} #:placement 'forcehere) #:id "declensions-3-variable"]{
  Form & ◊l0{-os} & ◊l0{-on} & ◊l0{-or}
  Nominative default & ◊nc{◊ṡ{N}os} & ◊nc{◊ṡ{N}on} & ◊nc{◊ṡ{N}or}
  Nominative plural & ◊nc{◊ṡ{N}or} & ◊nc{◊ṡ{N}or} & ◊nc{◊ṡ{N}osôr}
  Nominative singulative & ◊nc{◊ṡ{N}oren} & ◊nc{◊ṡ{N}oren} & ◊nc{◊ṡ{N}ons}
  Accusative default & ◊nc{◊ṡ{N}on} & ◊nc{◊ṡ{N}anon} & ◊nc{◊ṡ{N}on}
  Accusative dual & ◊nc{◊ṡ{N}◊ẋ{t}on} & ◊nc{◊ṡ{N}anor} & ◊nc{◊ṡ{N}◊ẋ{t}on}
  Accusative plural & ◊nc{◊ṡ{N}◊ẋ{þ}on} & ◊nc{◊ṡ{N}anor} & ◊nc{◊ṡ{N}◊ẋ{þ}on}
  Instrumental default & ◊nc{cja◊ṡ{L}os} & ◊nc{cja◊ṡ{L}on} & ◊nc{cja◊ṡ{L}or}
  Instrumental dual & ◊nc{cja◊ṡ{L}ocþ} & ◊nc{cja◊ṡ{L}oc} & ◊nc{cja◊ṡ{L}oc}
  Abessive default & ◊nc{þja○◊ṡ{L}os} & ◊nc{þja○◊ṡ{L}on} & ◊nc{þja○◊ṡ{L}or}
  Abessive dual & ◊nc{þja○◊ṡ{L}ocþ} & ◊nc{þja○◊ṡ{L}oc} & ◊nc{þja○◊ṡ{L}oc}
  Consonantal conjunct & ◊nc{◊ṡ{N}os-} & ◊nc{◊ṡ{N}on-} & ◊nc{◊ṡ{N}or-}
  Vocalic conjunct & ◊nc{◊ṡ{N}or-} & ◊nc{◊ṡ{N}on-} & ◊nc{◊ṡ{N}or-}
}

◊l0{-el} nouns have additional A and G stems and thus have their own declension:

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for third-declension -el nouns.
  ◊sup{1} See below.
} #:placement 'forcehere) #:id "declensions-3-el"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}el} & ◊nc{◊ṡ{N}oc} & ◊nc{◊ṡ{N}or} & ◊nc{◊ṡ{N}ons} & ◊nc{◊ṡ{A}ul}
  Accusative & ◊nc{◊ṡ{A}en} & ◊nc{◊ṡ{A}◊ẋ{t}en} & ◊nc{◊ṡ{A}on} & ◊nc{◊ṡ{N}elt} & ◊nc{◊ṡ{A}an}
  Dative & ◊nc{◊ṡ{A}oþ} & ◊nc{◊ṡ{N}◊ẋ{t}oþ} & ◊nc{◊ṡ{A}asor} & ◊nc{◊ṡ{N}◊ẋ{n}es} & ◊nc{◊ṡ{A}as}
  Genitive & ◊nc{◊ṡ{G}el}◊sup{1} & ◊nc{◊ṡ{G}◊ẋ{t}el} & ◊nc{◊ṡ{G}ol} & ◊nc{◊ṡ{G}◊ẋ{n}el} & ◊nc{◊ṡ{A}◊ẋ{n}e}
  Locative & ◊nc{◊ṡ{L}os} & ◊nc{◊ṡ{L}ocþ} & ◊nc{◊ṡ{L}or} & ◊nc{◊ṡ{L}oren} & ◊nc{◊ṡ{L}e◊ṫg{L}}
  Instrumental & ◊nc{cja◊ṡ{L}el} & ◊nc{cja◊ṡ{L}els} & ◊nc{cja◊ṡ{L}or} & ◊nc{cja◊ṡ{L}olt} & ◊nc{cja◊ṡ{L}e◊ṫg{L}}
  Abessive & ◊nc{þja○◊ṡ{L}el} & ◊nc{þja○◊ṡ{L}els} & ◊nc{þja○◊ṡ{L}or} & ◊nc{þja○◊ṡ{L}olt} & ◊nc{þja○◊ṡ{L}e◊ṫg{L}}
  Semblative & ◊nc{◊ṡ{S}ot} & ◊nc{◊ṡ{S}octos} & ◊nc{◊ṡ{S}et} & ◊nc{◊ṡ{S}ełi} & ◊nc{◊ṡ{S}ocþ}
  Consonantal conjunct & ◊nc{◊ṡ{N}el-}
  Vocalic conjunct & ◊nc{◊ṡ{N}el-}
}

In this case, the N and G stems must be distinct.

In the genitive singular, if the last bridge of ◊nc{◊ṡ{N}} or ◊nc{◊ṡ{G}} is ◊l0{l}, then the inflected form is ◊nc{◊ṡ{N}lu} or ◊nc{◊ṡ{G}lu}, where the ◊l0{l-} onset becomes the preceding coda. If the stem otherwise ends with ◊l0{l}, then the inflected form is ◊nc{◊ṡ{N}ô} or ◊nc{◊ṡ{G}ô}.

If the N stem ends in ◊l0{i}, then some forms are declined differently. Let ◊nc{◊ṡ{N′}} be the start-to-onset assemblage resulting from removing the final ◊l0{i} from ◊nc{◊ṡ{N}}. Then the declensions are as follows:

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for third-declension -ios nouns. The other cases are inflected as usual.
} #:placement 'forcehere) #:id "declensions-3-ios"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊ncv{◊ṡ{N′}ios} & ◊nc{◊ṡ{N′}ice} & ◊ncv{◊ṡ{N′}ia} & ◊ncv{◊ṡ{N′}ien} & ◊nc{◊ṡ{N′}iva}
  Accusative & ◊ncv{◊ṡ{N′}ion} & ◊ncv{◊ṡ{N′}eton} & ◊ncv{◊ṡ{N′}eþon} & ◊nc{◊ṡ{N′}ila} & (◊nc{◊ṡ{N′}ian})
  Dative & ◊nc{◊ṡ{N′}isa} & ◊nc{◊ṡ{N′}ista} & ◊nc{◊ṡ{N′}esor} & ◊ncv{◊ṡ{N′}ines} & (◊nc{◊ṡ{N′}ias})
  Genitive & ◊nc{◊ṡ{N′}ina} & ◊nc{◊ṡ{N′}inta} & ◊nc{◊ṡ{N′}ide} & (◊nc{◊ṡ{N′}inel}) & (◊nc{◊ṡ{N′}ine})
}

◊table/x[#:options (table-options #:caption ◊@{Variable declensions for third-declension nouns with N stems ending in ◊l0{i}.} #:placement 'forcehere) #:id "declensions-3i-variable"]{
  Form & ◊l0{-ios} & ◊l0{-ion} & ◊l0{-ior}
  Nominative default & (◊nc{◊ṡ{N′}ios}) & (◊nc{◊ṡ{N′}ion}) & (◊nc{◊ṡ{N′}ior})
  Nominative plural & ◊nc{◊ṡ{N′}ia} & ◊nc{◊ṡ{N′}ia} & ◊nc{◊ṡ{N′}esôr}
  Nominative singulative & ◊nc{◊ṡ{N′}ien} & ◊nc{◊ṡ{N′}ien} & ◊nc{◊ṡ{N′}ines}
  Accusative default & ◊nc{◊ṡ{N′}ion} & ◊nc{◊ṡ{N′}enon} & ◊nc{◊ṡ{N′}ion}
  Accusative dual & ◊nc{◊ṡ{N′}eton} & ◊nc{◊ṡ{N′}iaþ} & ◊nc{◊ṡ{N′}eton}
  Accusative plural & ◊nc{◊ṡ{N′}eþon} & ◊nc{◊ṡ{N′}iaþ} & ◊nc{◊ṡ{N′}eþon}
  Dative singulative & ◊nc{◊ṡ{N′}ines} & ◊nc{◊ṡ{N′}ines} & ◊nc{◊ṡ{N′}ineþ}
  Consonantal conjunct & ◊nc{◊ṡ{N′}es-} & ◊nc{◊ṡ{N′}en-} & ◊nc{◊ṡ{N′}ur-}
  Vocalic conjunct & ◊nc{◊ṡ{N′}er-} & ◊nc{◊ṡ{N′}en-} & ◊nc{◊ṡ{N′}ur-}
}

In ◊l0{-iel} nouns, only the nominative and dative forms are affected:

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for third-declension -iel nouns.
} #:placement 'forcehere) #:id "declensions-3-iel"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N′}iel} & ◊nc{◊ṡ{N′}ice} & ◊nc{◊ṡ{N′}ia} & ◊nc{◊ṡ{N′}ines} & ◊nc{◊ṡ{A}ul}
  Dative & ◊nc{◊ṡ{A}oþ} & ◊nc{◊ṡ{N′}◊ẋ{t}eþ} & ◊nc{◊ṡ{A}asor} & ◊nc{◊ṡ{N′}ineþ} & ◊nc{◊ṡ{A}as}
  Consonantal conjunct & ◊nc{◊ṡ{N′}il-}
  Vocalic conjunct & ◊nc{◊ṡ{N′}il-}
}

◊section[#:id "decl4"]{The fourth declension}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[0]} is ◊l0{o} for terrestrial nouns and ◊l0{a} for celestial nouns. Naturally, it can be either for human nouns.}
  ◊item{◊nc{◊ṫ[1]} can be either ◊l0{a} or ◊l0{e}.}
  ◊item{Some vowels used by this declension class depend on the letter sum of a certain form modulo some integer: ◊;
    ◊items{
      ◊item{◊nc{◊ḋ[0]} = ◊l0{e a i a i e} « ◊sc{nom.di} if ◊nc{◊ṫ[0]} = ◊l0{a}; otherwise ◊l0{o}}
      ◊item{◊nc{◊ḋ[1]} = ◊l0{i i a i e i} « ◊sc{nom.di} if ◊nc{◊ṫ[0]} = ◊l0{a}; otherwise ◊l0{e}}
    }
  }
  ◊item{◊nc{◊ṡ{L}} ◊strong{must be different} from ◊nc{◊ṡ{N}}. This is a departure from declension classes 5 and 11 on the old system, as the final coda of the nominative direct is no longer mutated in certain forms. The easiest way to fix these two stems being the same is to change the final bridge of ◊nc{◊ṡ{N}}.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for fourth-declension nouns.
  ◊sup{1} ◊nc{◊ṡ{N}es} if ◊nc{◊ṡ{N}} ends in ◊l0{j}; otherwise ◊nc{◊ṡ{N}e} if the last vowel of ◊nc{◊ṡ{N}} is ◊l0{i} or ◊l0{î}.
  ◊sup{2} ◊l0{i} in the suffix becomes ◊l0{e} after the onsets ◊l0{t-}, ◊l0{d-}, ◊l0{s-}, ◊l0{þ-}, ◊l0{ð-}, ◊l0{tf-}, or ◊l0{dv-}, as well as any onsets that end with ◊l0{l}.
} #:placement 'forcehere) #:id "declensions-4"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ẋ{ε}} & ◊nc{◊ṡ{N}ec} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ḋ[1]n} & ◊nc{◊ṡ{N}a◊ṫg{N}}
  Accusative & ◊nc{◊ṡ{N}◊ḋ[0]n} & ◊nc{◊ṡ{N}◊ẋ{t}◊ḋ[0]n} & ◊nc{◊ṡ{N}as} & ◊nc{◊ṡ{N}◊ḋ[0]nþ} & ◊nc{◊ṡ{N}a◊ṫg{N}en}
  Dative & ◊nc{◊ṡ{N}i}◊sup{1} & ◊nc{◊ṡ{N}ic} & ◊nc{◊ṡ{N}ir} & ◊nc{◊ṡ{N}ên} & ◊nc{◊ṡ{N}a◊ṫg{N}es}
  Genitive & ◊nc{◊ṡ{N}a} & ◊nc{◊ṡ{N}ac} & ◊nc{◊ṡ{N}o} & ◊nc{◊ṡ{N}ân} & ◊nc{◊ṡ{N}◊ẋ{n}e◊ṫg{N}}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]s} & ◊nc{◊ṡ{L}◊ṫ[1]c} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋpl}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{L}e◊ṫg{L}}
  Instrumental & ◊nc{◊ṡ{L}eca} & ◊nc{◊ṡ{L}ecca} & ◊nc{◊ṡ{L}ica}◊sup{2} & ◊nc{◊ṡ{L}inca}◊sup{2} & ◊nc{◊ṡ{L}e◊ṫg{L}ca}
  Abessive & ◊nc{◊ṡ{L}eþa} & ◊nc{◊ṡ{L}ecþa} & ◊nc{◊ṡ{L}iþa}◊sup{2} & ◊nc{◊ṡ{L}inþa}◊sup{2} & ◊nc{◊ṡ{L}e◊ṫg{L}þa}
  Semblative & ◊colspan*[5 'merged]{Same as I (III) for ◊nc{◊ṫ[0]} = ◊nc{a} (◊nc{o})}
  Consonantal conjucnt & ?
  Vocalic conjunct & ◊nc{◊ṡ{N}-}
}

◊section[#:id "decl5"]{The fifth declension}

Guidelines:

◊items{
  ◊item{This declension class has only N and S stems, with no L stem.}
  ◊item{◊nc{◊ṫ[0]} is any ◊em{nucleus}.}
  ◊item{◊nc{◊ṫ[2]} is a (possibly empty) simple coda other than ◊l0{-c} or ◊l0{-t}, with a transformation ◊nc{◊ẋ{◊ẋ*mod}} (‘zhe’).}
  ◊item{Some variables in this declension class depend on the letter sum of a certain form modulo some integer: ◊;
    ◊items{
      ◊item{◊nc{◊ḋ[0]} = ◊l0{e a i ô u o î ê â} « ◊sc{nom.di}}
      ◊item{◊nc{◊ḋ[1]} = ◊l0{o ô ô o} « ◊sc{nom.gc}}
      ◊item{◊nc{◊ḋ[2]} = ◊l0{i i e e} « ◊sc{nom.du}}
      ◊item{◊nc{◊ḋ[3]} = ◊l0{g d v} « ◊sc{acc.di}, increment until ◊nc{◊ḋ[3]} ≠ the last onset of ◊nc{◊ṡ{N}}}
      ◊item{◊nc{◊ḋ[4]} = ◊l0{e a i u a e o i jâ jê jô jê jâ o} « ◊sc{acc.pl}, increment until ◊nc{◊ḋ[4]} ≠ ◊nc{◊ṫ[0]}}
    }
  }
  ◊item{◊nc{◊ṡ{N}} is not necessraily a stem, as it is not required to have at least one full syllable.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
  ◊item{In this case, ◊nc{◊ṫg{L}} is based on ◊l0{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}}.}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for fifth-declension nouns.
  ◊sup{1} ◊nc{◊ṡ{N}◊ḋ[1]t} if ◊nc{◊ṡ{N}} does not contain at least one full syllable
} #:placement 'forcehere) #:id "declensions-5"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]} & ◊nc{◊ṡ{N}◊ẋ{t}◊ḋ[1]c}◊sup{1} & ◊nc{◊ṡ{N}◊ḋ[0]◊ṫ[2]} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}e} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}u}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}◊ḋ[2]n} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}jor} & ◊nc{◊ṡ{N}◊ḋ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}◊ḋ[2]◊ẋ{e×i}n} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}en} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}an}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}er} & ◊nc{◊ṡ{N}◊ṫ[0]◊ḋ[3]a◊ṫ[2]} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}ir} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}es} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}as}
  Genitive & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}es} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}ec} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}eris} & ◊nc{(◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod})◊ẋ{t}el} & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}e}
  Locative & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}a} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}ac} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}o} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}en} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}a◊ṫg{L}}
  Instrumental & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eca} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}ehac} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}ego} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}egen} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eca◊ṫg{L}}
  Abessive & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eþa} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eþac} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eðo} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eðen} & ◊nc{◊ṡ{N}◊ḋ[4]◊ṫ[2]◊ẋ{◊ẋ*mod}eþa◊ṫg{L}}
  Semblative & ◊nc{◊ṡ{S}ot} & ◊nc{◊ṡ{S}octos} & ◊nc{◊ṡ{S}et} & ◊nc{◊ṡ{S}ełi} & ◊nc{◊ṡ{S}ocþ}
  Consonantal conjucnt & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]-}
  Vocalic conjunct & ◊nc{◊ṡ{N}◊ṫ[0]◊ṫ[2]◊ẋ{◊ẋ*mod}-}
}

◊table/x[#:options (table-options #:caption ◊@{The ◊nc{◊ẋ{◊ẋ*mod}} transformation for the fifth declension.} #:placement 'forcehere #:first-col-header? #f) #:id "declensions-5-zhe"]{
  ◊nc{◊ṫ[2]} & ◊nc{◊ṫ[2]◊ẋ{◊ẋ*mod}}
  ∅ & ◊nc{ħ}
  ◊nc{s} & ◊nc{r}
  ◊nc{r} & ◊nc{r}
  ◊nc{n} & ◊nc{nþ}
  ◊nc{þ} & ◊nc{s}
  ◊nc{rþ} & ◊nc{rs}
  ◊nc{cþ} & ◊nc{cs}
  ◊nc{l} & ◊nc{ł}
  ◊nc{f} & ◊nc{m}
}

◊section[#:id "decl6"]{The sixth declension}

Guidelines:

◊items{
  ◊item{◊nc{◊ṫ[0]} can be ◊l0{a}, ◊l0{e}, or ◊l0{i}.}
  ◊item{◊nc{◊ṫ[1]} can be ◊l0{a} or ◊l0{e}.}
  ◊item{◊nc{◊ṡ{N}} cannot end with ◊l0{-nn-}.}
  ◊item{◊nc{◊ṡ{L}} is usually different from ◊nc{◊ṡ{N}}, but it does not need to be.}
  ◊item{This declension class additionally has I and I′ stems, which are usually similar to the L stem.}
  ◊item{◊nc{◊ṡ{S}} may be the same as ◊nc{◊ṡ{N}}, but the most common difference is to change the final consonants of ◊nc{◊ṡ{N}} (especially changing voiceless coronals to voiced coronals and ◊l0{r} to ◊l0{l}).}
}

◊table/x[#:options (table-options #:caption ◊@{
  Declensions for sixth-declension nouns.
  ◊sup{1} ◊nc{◊ṡ{N}ien}, ◊nc{◊ṡ{N}◊ẋ{n}ien} for ◊l0{-in} nouns
} #:placement 'forcehere) #:id "declensions-6"]{
  Case \ Number & Direct & Dual & Plural & Singulative & Generic
  Nominative & ◊nc{◊ṡ{N}◊ṫ[0]n} & ◊nc{◊ṡ{N}jor} & ◊nc{◊ṡ{N}in}◊sup{1} & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{◊ẋgen}l} & ◊nc{◊ṡ{N}u}
  Accusative & ◊nc{◊ṡ{N}◊ṫ[0]n◊ṫ[0]◊ẋ{ei/ae}} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]r} & ◊nc{◊ṡ{N}◊ṫ[0]n◊ṫ[0]◊ẋ{ei/ae}n} & ◊nc{◊ṡ{N}an}
  Dative & ◊nc{◊ṡ{N}◊ṫ[0]ns} & ◊nc{◊ṡ{N}◊ṫ[0]ŋa} & ◊nc{◊ṡ{N}eri} & ◊nc{◊ṡ{N}◊ẋ{n}◊ṫ[0]s} & ◊nc{◊ṡ{N}as}
  Genitive & ◊nc{◊ṡ{N}il} & ◊nc{◊ṡ{N}◊ẋ{t}il} & ◊nc{◊ṡ{N}evi} & ◊nc{◊ṡ{N}◊ẋ{n}in}◊sup{1} & ◊nc{◊ṡ{N}◊ẋ{n}e}
  Locative & ◊nc{◊ṡ{L}◊ṫ[1]s} & ◊nc{◊ṡ{L}◊ṫ[1]c} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋpl}s} & ◊nc{◊ṡ{L}◊ṫ[1]◊ẋ{◊ẋgen}ns} & ◊nc{◊ṡ{L}e◊ṫg{L}}
  Instrumental & ◊nc{cja◊ṡ{I}es} & ◊nc{cja◊ṡ{I}ecþ} & ◊nc{cja◊ṡ{I′}o} & ◊nc{cja◊ṡ{I′}ans} & ◊nc{cja◊ṡ{I}e◊ṫg{L}}
  Abessive & ◊nc{þja○◊ṡ{I}es} & ◊nc{þja○◊ṡ{I}ecþ} & ◊nc{þja○◊ṡ{I′}o} & ◊nc{þja○◊ṡ{I′}ans} & ◊nc{þja○◊ṡ{I}e◊ṫg{L}}
  Semblative & ◊nc{◊ṡ{S}it} & ◊colspan*[2 'merged]{◊nc{◊ṡ{S}et}} & ◊nc{◊ṡ{S}ict◊ṫ[0]} & ◊nc{◊ṡ{S}icþ}
  Consonantal conjunct & ◊nc{◊ṡ{N}◊ṫ[0]n-}
  Vocalic conjunct & ◊nc{◊ṡ{N}◊ṫ[0]◊ẋ{ei/ae}n-}
}

◊section[#:id "l-avoidance"]{L-avoidance}

All declensions except for the fifth require an L stem that is frequently or even obligatorily distinct from the N stem. In addition, the L stem of a noun is fairly unpredictable from the corresponding N stem. The use of L stems thus adds a significant burden in acquiring and using the language. As a result, several periphrastic constructions to replace the use of the locative, instrumental, and abessive cases have come into use.

◊table/x[#:options (table-options #:caption ◊@{
  L-avoidance strategies in Ŋarâþ Crîþ.
} #:placement 'forcehere #:colgroup '(c c c d)) #:id "l-avoidance-list"]{
  Case & Replacement & Signal & Comment
  Locative & Genitive & ◊nc{fjones} ◊trans{location-◊sc{loc}.◊sc{di}} &
  & Dative & ◊nc{es} ◊trans{inside} & Especially used for nouns describing areas
  Instrumental &
  Abessive & Genitive & ◊nc{intaras} ◊trans{absence-◊sc{loc}.◊sc{di}}
  & Genitive & ◊nc{linselþa} ◊trans{help-◊sc{abess}.◊sc{di}}
}

L-avoidance is more common with rarer nouns, as well as with names. However, using the cases that it replaces is considered more elegant and preferred in formal language.

◊section[#:id "pronouns"]{Pronouns and determiners}

◊subsection[#:id "pronouns-personal"]{Personal pronouns}

◊subsubsection[#:id "pronouns-personal-basic"]{Basic personal pronouns}

The ◊term{basic personal pronouns} (Tables ◊xref/l["pronouns-1" #f] – ◊xref/l["pronouns-3h" #f]) are defective: they lack any forms for the core cases.

◊table/x[#:options (table-options #:caption "Declensions for first-person pronouns.") #:id "pronouns-1"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{pelas} & ◊nc{pelsac} & ◊nc{pelir} & ◊nc{peris}
  Instrumental & ◊nc{pelca} & ◊nc{pelcac} & ◊nc{pelcar} & ◊nc{pelcef}
  Abessive & ◊nc{pilþa} & ◊nc{pilþac} & ◊nc{pilþar} & ◊nc{pilþef}
  Semblative & ◊nc{pjot} & ◊nc{pjocte} & ◊nc{pjet} & ◊nc{perþ}
}

◊table/x[#:options (table-options #:caption "Declensions for second-person pronouns.") #:id "pronouns-2"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{olas} & ◊nc{olsac} & ◊nc{oler} & ◊nc{ores}
  Instrumental & ◊nc{olca} & ◊nc{olcac} & ◊nc{olcar} & ◊nc{olcef}
  Abessive & ◊nc{eþa} & ◊nc{eþac} & ◊nc{eþar} & ◊nc{aþef}
  Semblative & ◊nc{cet} & ◊nc{cete} & ◊nc{cet} & ◊nc{cefte}
}

◊table/x[#:options (table-options #:caption "Declensions for third-person celestial pronouns.") #:id "pronouns-3c"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{eri} & ◊nc{erjor} & ◊nc{eren} & ◊nc{eref}
  Instrumental & ◊nc{cjas} & ◊nc{cjac} & ◊nc{cjar} & ◊nc{cjaf}
  Abessive & ◊nc{irþa} & ◊nc{irþac} & ◊nc{irþar} & ◊nc{irþef}
  Semblative & ◊nc{atir} & ◊nc{irce} & ◊nc{adit} & ◊nc{aden}
}

◊table/x[#:options (table-options #:caption "Declensions for third-person terrestrial pronouns.") #:id "pronouns-3t"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{ose} & ◊nc{osec} & ◊nc{oros} & ◊nc{oref}
  Instrumental & ◊nc{cjos} & ◊nc{cjoc} & ◊nc{cjor} & ◊nc{cjof}
  Abessive & ◊nc{irþos} & ◊nc{irþoc} & ◊nc{irþor} & ◊nc{irþof}
  Semblative & ◊nc{ator} & ◊nc{ircon} & ◊nc{adit} & ◊nc{aden}
}

◊table/x[#:options (table-options #:caption "Declensions for third-person human pronouns.") #:id "pronouns-3h"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Locative & ◊nc{lase} & ◊nc{lasec} & ◊nc{laser} & ◊nc{lasef}
  Instrumental & ◊nc{lasce} & ◊nc{lascel} & ◊nc{lasci} & ◊nc{lascef}
  Abessive & ◊nc{laþes} & ◊nc{laþecþ} & ◊nc{laþer} & ◊nc{laþef}
  Semblative & ◊nc{lefen} & ◊nc{lefedi} & ◊nc{adit} & ◊nc{aden}
}

The first- and second-person pronouns are treated as if they were in the celestial gender, even though they will often refer to humans.

Basic personal pronouns are in the category ◊var{p} and avoid manifesting as free morphemes. They manifest in the following ways, ordered from most to least preferred:

◊items{
  ◊item{Fused with a coordinating conjunction if found as the left element}
  ◊item{In a relational with an object prefix}
  ◊item{As an object affix on the verb if in the accusative or dative case}
  ◊item{As a ◊xref["nouns.html" "pronouns-personal-possessive" "Sub-subsection"]{possessive postclitic} if in the genitive case, or in the nominative case of a nominalized verb phrase}
  ◊item{Omitted if inferable from the subject affix on a finite verb form}
  ◊item{As an independent form when in a non-core case}
  ◊item{Homophonic with the ◊xref["nouns.html" "pronouns-personal-emphatic" "Sub-subsection"]{emphatic pronouns}}
}

◊subsubsection[#:id "pronouns-personal-possessive"]{Possessive clitics}

Ŋarâþ Crîþ uses clitics to mark a pronominal possessor, listed in ◊xref/l["poss-clitics" "Table"].

◊table/x[#:options (table-options #:caption "Pronominal clitics in Ŋarâþ Crîþ.") #:id "poss-clitics"]{
  Person ◊|'amp| gender & Form
  1st & ◊nc{=’pe}
  2nd & ◊nc{=’ve}
  3rd celestial & ◊nc{=’(a)c}
  3rd terrestrial & ◊nc{=’oc}
  3rd human & ◊nc{=’(o)r}
  Reflexive & ◊nc{=’(ê)cþ}
}

The vowels of the third-person celestial and human possessive clitics are omitted after an open syllable.

If the reflexive clitic immediately follows a vowel other than ◊l0{u}, then it changes that vowel to its hatted counterpart. If the vowel in question was not already hatted, then the ◊i{ŋos} is moved immmediately before it.

The clitic ◊l1{=’ħe} is used to indicate that the possessor is the referent of a prior ◊xref["nouns.html" "quantification" "Subsection"]{◊l1{šino} or ◊l1{nema}}.

The third-person possessive suffixes are also used in the double-marked ◊term{possessive construction}. In such a construction, the possessor takes the same case as the possessee and the clitic ◊l1{=’þ} after a vowel or ◊l1{=’eþ} after a consonant. The possessee takes the appropriate possessive clitic depending on the gender of the possessor. The possessor and possessee are not required to be adjacent to each other or even in a particular order.

In the general case, the possessive construction is used strictly for possession. That is, it does not have other functions of the genitive case such as apposition or composition.

The possessive construction is also used with the noun ◊l1{aliþ} ◊trans{something other than} as the possessee to ‘negate’ the possessor. This usage cannot be substituted with the genitive (although using non-third-person pronominal clitics on ◊l1{aliþ} is permitted).

This construction is used with the pronouns ◊l1{šino} and ◊l1{nema} as the possessor in order to avoid ambiguity with the determiners, which are the genitive singular forms of these pronouns.

The use of the possessive construction is otherwise quite rare.

◊subsubsection[#:id "pronouns-personal-reflexive"]{Reflexive and reciprocal pronouns}

There is only one reflexive pronoun, ◊l1{cenþ}, whose declensions are shown in ◊xref/l["reflexive-declensions"]{Table}. This is also used as a reciprocal pronoun.

◊table/x[#:options (table-options #:caption ◊@{Declensions for the reflexive pronoun ◊l1{cenþ}.} #:placement 'forcehere) #:id "reflexive-declensions"]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{cenþ} & ◊nc{cenþ} & ◊nc{cemar} & ◊nc{cemu}
  Accusative & ◊nc{cemen} & ◊nc{cemas} & ◊nc{cemas} & ◊nc{ceman}
  Dative & ◊nc{cemi} & ◊nc{cemic} & ◊nc{cemir} & ◊nc{cemase}
  Genitive & ◊nc{cema} & ◊nc{cemac} & ◊nc{cemo} & ◊nc{ceme}
  Locative & ◊nc{ces} & ◊nc{cesor} & ◊nc{cis} & ◊nc{cesef}
  Instrumental & ◊nc{ceŋa} & ◊nc{ceŋac} & ◊nc{ciŋa} & ◊nc{ceŋaf}
  Abessive & ◊nc{cinþa} & ◊nc{cinþac} & ◊nc{cinþa} & ◊nc{cinþaf}
  Semblative & ◊nc{cemit} & ◊nc{cjorto} & ◊nc{cit} & ◊nc{cemicþ}
}

The referent of the independent reflexive pronoun ◊l1{cenþ} or of the reflexive possessive clitic ◊l1{=’êcþ} is determined as follows:

◊items{
  ◊item{If there is a quantifier in the clause, then the pronoun corefers with the innermost quantifier.}
  ◊item{In a relative clause, the pronoun corefers with the head of the clause. This rule sometimes applies to a genitive-case nominalized verb phrase.}
  ◊item{In other clauses or in a quotative, the pronoun corefers with an argument mentioned earlier in that clause, most often the nominative argument but sometimes the accusative or dative. Referring to adjuncts is unattested. Occasionally, the argument may be taken from the earlier ◊em{subclause} instead:
  
  ◊gloss/x{
    ◊glfree{◊nc{navałes minels o pelhat ceme vjornełen; navałes dara o pelhat ceme’pe vjornełen.}}
    ◊gla{nav-ałes minels o pelh-at ceme vjorn-ełen; nav-ałes dara o pelh-at ceme=’pe vjorn-ełen.}
    ◊glb{person-%dat.%gc one-%dat.%hu %inf.%nom trouble-%inf %pr.%refl.%gen.%gc problem-%acc.%gc person-%dat.%gc 16⁴ %inf.%nom trouble-%inf %pr.%refl.%gen.%gc=%poss.1 problem-%acc.%gc}
    ◊glfree{When one person suffers, it’s their own problem; when thousands of people suffer, it’s all our problem.}
  }}
  ◊item{If no suitable argument exists or the clause is an imperative, then the pronoun corefers to the nominative argument of the clause.}
  ◊item{The referent of a reflexive possessive clitic on the any coordinand but the first of a coordinated noun phrase is the same as the first coordinand:

  ◊gloss/x{
    ◊glfree{◊nc{galac’êcþ’cil cfârnei trešils nelsanta.}}
    ◊gla{gal-ac=’êcþ=’cil cfârn-ei treš-ils nels-an-ta.}
    ◊glb{friend-%nom.%du=%poss.%refl=and.3 south-%gen.%di garden-%dat.%sg go-3%du-%pst}
    ◊glfree{He◊sub{◊var{i}} and his◊sub{◊var{i}} friends went to the garden in the south.}
  }

  However, this rule does not apply to an independent reflexive pronoun:
  
  ◊gloss/x{
    ◊glfree{◊nc{#môra #saþos mels’ôcþ’ce dteþral.}}
    ◊gla{#môr-a #saþ-os mels-o=’êcþ=’ce d\teþr-al.}
    ◊glb{(name)-%nom.%sg (name)-%dat.%sg osSb-%dat.%sg=%poss.%refl=and %pfv\call-3%sg.%inv}
    ◊glfree{#môra◊sub{◊var{i}} called #saþo◊sub{◊var{j}} and their◊sub{◊var{j}} sibling.}
  }
  
  ◊gloss/x{
    ◊glfree{◊nc{#môra #saþos cema melso’ce dteþral.}}
    ◊gla{#môr-a #saþ-os cema mels-o=’ce d\teþr-al.}
    ◊glb{(name)-%nom.%sg (name)-%dat.%sg %pr.%refl.%gen.%sg osSb-%dat.%sg=and %pfv\call-3%sg.%inv}
    ◊glfree{#môra◊sub{◊var{i}} called #saþo◊sub{◊var{j}} and their◊sub{◊var{i}} sibling.}
  }}
}

The referent of a reflexive affix on a verb, in contrast, can only be the head of a participle (or sometimes of a genitive-case nominalized verb) or the nominative argument of any other verb form. The reflexive affix on a relational corefers with the head of the relational phrase.

◊subsubsection[#:id "pronouns-personal-emphatic"]{Emphatic pronouns}

Combining a reflexive pronoun with a possessive clitic creates an ◊term{emphatic pronoun}, which acts roughly like a personal pronoun with an independent form but places focus on the referent.

◊subsubsection[#:id "pronouns-personal-clusive"]{Clusive pronouns}

◊subsection[#:id "pronouns-interrogative"]{Interrogative pronouns and determiners}

The interrogative determiners and pronouns in Ŋarâþ Crîþ are shown in ◊xref/l["interrogative-determiners"]{Table }.

◊table/x[#:options (table-options #:caption "Interrogative determiners and pronouns in Ŋarâþ Crîþ." #:colgroup '(c d)) #:id "interrogative-determiners"]{
  Type & Interrogative
  Determiner & ◊nc{mê◊|sei2|}
  Pronoun & ◊nc{pen, …}
  Pronoun (human) & ◊nc{penna, peþas, mpadit} (Ih)
  Pronoun (elective) & ◊nc{meel, maen, mełel, mirłos, meħot} (IIIt)
  Pronoun (place) & ◊nc{parja, perþas, pjalit} (Ic)
  Pronoun (time) & ◊nc{penelva, pełevas, pełevit} (Ic.m)
  Pronoun (event) & ?
  Pronoun (idea or speech) & ◊nc{peler, …}
  Pronoun (kind) & ◊nc{pełoþ, pełoðen, pełaðes, pełiðit} (IVc)
  Pro-verb & ◊nc{nepit, nea, nepelta, nelpa, nelpeta, nolpaþos, nolpeve, …}
}

Note that ◊l1{penna} has an intrinsically mutated S stem.

The pronouns ◊l1{pen} and ◊l1{peler} are irregular.

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{pen} ◊trans{what}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{pen} & ◊nc{pen} & ◊nc{penar} & ◊nc{penaf}
  Accusative & ◊nc{penen} & ◊nc{penas} & ◊nc{penas} & ◊nc{penas}
  Dative & ◊nc{peni} & ◊nc{penic} & ◊nc{penir} & ◊nc{penef}
  Genitive & ◊nc{pena} & ◊nc{vpenac} & ◊nc{vpeno} & ◊nc{penaf}
  Locative & ◊nc{pes} & ◊nc{pesor} & ◊nc{pis} & ◊nc{pesac}
  Instrumental & ◊nc{peŋa} & ◊nc{peŋac} & ◊nc{piŋa} & ◊nc{peŋaf}
  Abessive & ◊nc{pineþ} & ◊nc{pinþac} & ◊nc{pinþa} & ◊nc{pinþaf}
  Semblative & ◊nc{pedit} & ◊nc{pjorto} & ◊nc{pit} & ◊nc{pedecþ}
}

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{peler} ◊trans{what (idea, speech)}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{peler} & ◊nc{pelec} & ◊nc{penare} & ◊nc{penafel}
  Accusative & ◊nc{penreþ} & ◊nc{penareþ} & ◊nc{penareþ} & ◊nc{penres}
  Dative & ◊nc{penres} & ◊nc{penrecþ} & ◊nc{penares} & ◊nc{peneves}
  Genitive & ◊nc{penril} & ◊nc{vpenric} & ◊nc{vpenal} & ◊nc{penavil}
  Locative & ◊nc{penraþ} & ◊nc{penraþ} & ◊nc{penarþ} & ◊nc{penavaþ}
  Instrumental & ◊nc{penracþa} & ◊nc{penracþa} & ◊nc{penarcþa} & ◊nc{penavacþ}
  Abessive & ◊nc{penraþa} & ◊nc{penraþa} & ◊nc{penarþa} & ◊nc{penavaþa}
  Semblative & ◊nc{pelet} & ◊nc{pelecþ} & ◊nc{pelat} & ◊nc{pelfet}
}

Informally, ◊l1{pen} can be used instead of ◊l1{penna} to refer to persons.

Note the difference between using an interrogative pronoun modified by a relative clause and using a similar noun in its place:

◊gloss/x{
  ◊glfree{◊nc{feljan moren navan ame.}}
  ◊gla{felj-an mor-en nav-an am-e.}
  ◊glb{this_speech-%acc.%sg say-%rel.%nom,%acc.%cel person-%acc.%sg indifferent-1%sg}
  ◊glfree{The person who said this is not important (i.e. if ◊var{A} said this, then I don’t care about ◊var{A}).}
}

◊gloss/x{
  ◊glfree{◊nc{feljan moren pennan ame.}}
  ◊gla{felj-an mor-en penn-an am-e.}
  ◊glb{this_speech-%acc.%sg say-%rel.%nom,%acc.%cel who-%acc.%sg indifferent-1%sg}
  ◊glfree{Who said this is not important (i.e. whether ◊var{A} said it or someone else did is not important).}
}

◊subsection[#:id "pronouns-demonstrative"]{Demonstrative pronouns and determiners}

The demonstrative determiners and pronouns in Ŋarâþ Crîþ are shown in ◊xref/l["demonstrative-determiners"]{Table }.

The determiners ◊l1{lê} and ◊l1{tê} trigger eclipsis only if they lie directly before the head of what they modify.

◊table/x[#:options (table-options #:caption "Demonstrative determiners and pronouns in Ŋarâþ Crîþ." #:colgroup '(c d d)) #:id "demonstrative-determiners"]{
  Type & Proximal & Distal
  Determiner (celestial or human) & ◊nc{lê◊|uru|} & ◊nc{tê◊|uru|}
  Determiner (terrestrial) & ◊nc{el} & ◊nc{om}
  Pronoun (celestial) & ◊nc{ela, elras, elit} (Ic) & ◊nc{enta, ontas, ensit} (Ic)
  Pronoun (terrestrial) & ◊nc{elos, elros, elot} (IIIt) & ◊nc{entos, ontos, ensot} (IIIt)
  Pronoun (human) & ◊nc{eltan, elnas, enlit} (Ih) & ◊nc{eften, iftes, cjariftes, cjarefto, evrit} (VIh)
  Pronoun (place) & ◊nc{elgren, …} & ◊nc{engren, …}
  Pronoun (time) & ◊nc{endîr, endil, ondelt, endit} (IIc.m), ◊nc{ina, jonas, insit} (Ic.m) &
  Pronoun (event) & ? & ?
  Pronoun (idea or speech) & ◊nc{felja, foljas, felit} (Ic) & ◊nc{fetja, fotas, fedit} (Ic)
  Pro-verb & ◊nc{ħelit} &
}

The pronouns ◊nc{elgren} and ◊nc{engren} are irregular.

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{elgren} ◊trans{here}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{elgren} & ◊nc{elgjor} & ◊nc{elgrin} & ◊nc{elgref}
  Accusative & ◊nc{elgranen} & ◊nc{elgranor} & ◊nc{elgrenin} & ◊nc{elgrenef}
  Dative & ◊nc{elgres} & ◊nc{elgrecþ} & ◊nc{elgras} & ◊nc{elgresef}
  Genitive & ◊nc{elgrer} & ◊nc{elgreric} & ◊nc{elgrir} & ◊nc{elgrerif}
  Locative & ◊nc{eši} & ◊nc{ešic} & ◊nc{ešin} & ◊nc{ešif}
  Instrumental & ◊nc{esar} & ◊nc{esac} & ◊nc{esor} & ◊nc{esaf}
  Abessive & ◊nc{eþa} & ◊nc{eþac} & ◊nc{eþar} & ◊nc{ecþaf}
  Semblative & ◊nc{elgrit} & ◊nc{elgricte} & ◊nc{elgret} & ◊nc{elgricþ}
}

◊table/x[#:options (table-options #:caption ◊@{The declension of the irregular pronoun ◊l1{engren} ◊trans{there}.} #:placement 'forcehere)]{
  Case \ Number & Singular & Dual & Plural & Generic
  Nominative & ◊nc{engren} & ◊nc{engjor} & ◊nc{engrin} & ◊nc{engref}
  Accusative & ◊nc{engranen} & ◊nc{engranor} & ◊nc{engrenin} & ◊nc{engrenef}
  Dative & ◊nc{engres} & ◊nc{engrecþ} & ◊nc{engras} & ◊nc{engresef}
  Genitive & ◊nc{engrer} & ◊nc{engreric} & ◊nc{engrir} & ◊nc{engrerif}
  Locative & ◊nc{eči} & ◊nc{ečic} & ◊nc{ečin} & ◊nc{ečif}
  Instrumental & ◊nc{etar} & ◊nc{etac} & ◊nc{etor} & ◊nc{etaf}
  Abessive & ◊nc{eða} & ◊nc{eðac} & ◊nc{eðar} & ◊nc{egðaf}
  Semblative & ◊nc{engrit} & ◊nc{engricte} & ◊nc{engret} & ◊nc{engricþ}
}

◊subsection[#:id "quantification"]{Quantification}

The pronouns ◊l1{šino, šjonas, šedit} (Ic) means ◊trans{all}, and ◊l1{nema, nomes, nemit} (Ic) means ◊trans{some} or ◊trans{any}. When qualified with a modifying phrase, their scopes are restricted:

◊gloss/x{
  ◊glfree{◊nc{naven šinaf ndranlos.}}
  ◊gla{nav-en šin-af n\dranl-os.}
  ◊glb{human-%gen.%sg all-%nom.%gc %pfv\die-3%gc.%past.%pfv}
  ◊glfree{All humans [will] die.}
}

However, both of these quantifiers can also be used in the genitive singular as determiners, provided that the head of the noun phrase being modified is partially lenited. Furthermore, forms of ◊l1{ðên} are not mutated. That is, the above example may have used ◊l1{šinen navaf} instead.

When a noun phrase containing ◊l1{šino} or ◊l1{nema} is in the generic number, it is considered to cover all or some of the relevant individuals in general. When such a noun phrase is in any other number, it is considered to have a partitive meaning, with the number reflecting the quantity of the whole:

◊gloss/x{
  ◊glfree{◊nc{naven šinor sâna mênčeþ.}}
  ◊gla{nav-en šin-or sân-a mênč-e-þ.}
  ◊glb{human-%gen.%sg all-%acc.%pl bear-%nom.%sg eat-3%sg.%pfv-%past}
  ◊glfree{All of the humans were eaten by a bear.}
}

Again, this example could have used ◊l1{šinen navar} instead.

Scope ordering is covered in ◊xref["../syntax/overview.html" "scope" "Section" #:type 'inline]{the relevant section}.

The semantically related noun ◊l1{ruf} ◊trans{each} modifies a noun somewhere before it in the same clause with the same case. If ◊l1{ruf} does not immediately follow the noun that it affects, that noun undergoes a partial lenition if it does not already have a mutation.

The determiner ◊l1{mel} means ◊trans{much} or ◊trans{many}. It is not inflected, but the corresponding pronoun ◊l1{denfo, danfes, denfit} (Ic) is. From the latter is derived ◊l1{&denfo, &danfes, &denfit} (Ic) ◊trans{majority}. Similarly, the determiner ◊l1{dân} ◊trans{few, little} corresponds to the pronoun ◊l1{dane, dones, denit} (Ic), but the word for ◊trans{minority} is ◊l1{resa, risas, redit} (Ic). Additionally, ◊l1{dân} triggers eclipsis in the head noun.

For numerals, see ◊xref/c["../lexicon/numerals.html" #:type 'inline]{Numerals}.

◊section[#:id "coordination"]{Coordination}

Noun phrases are coordinated by attaching a clitic to all except the first coordinand. A noun phrase may be coordinated only with others of the same case.

◊table/x[#:options (table-options #:caption "Coordinating clitics in Ŋarâþ Crîþ.") #:id "coordination-table"]{
  Operation & ◊var{X} = NP & ◊var{X} = 1 & ◊var{X} = 2 & ◊var{X} = 3 & ◊@{Inherits number & gender from}
  ◊var{X} and ◊var{Y} & ◊nc{=’ce} & ◊nc{=’cjo} & ◊nc{=’gjo} & ◊nc{=’cil} & ◊var{X} plus ◊var{Y}
  ◊var{X} or ◊var{Y} & ◊nc{=’te} & ◊nc{=’čo} & ◊nc{=’djo} & ◊nc{=’čil} & ◊var{Y}
  ◊var{X} xor ◊var{Y} & ◊nc{=’re} & ◊nc{=’pre} & ◊nc{=’vre} & ◊nc{=’ril} & ◊var{Y}
  ◊var{X} but not ◊var{Y} & ◊nc{=’ne} & ◊nc{=’njo} & ◊nc{=’mjo} & ◊nc{=’nil} & ◊var{X}
}

When the first coordinand is pronominal, then it is fused into the coordinating clitic, leaving the other coordinands behind.

The gender of a coordinated noun phrase involving the ◊trans{and} operation is the strongest of those of the coordinands. For this purpose, the human gender is stronger than the celestial, which is stronger than the terrestrial gender.

All coordinated noun phrases inherit the person in the same way: the first person takes precedence over the second, which takes precedence over the third.

When there are more than two coordinands, then the respective clitics occur on each element after the first. ◊trans{◊var{X}◊sub{1} xor … xor ◊var{X}◊sub{◊var{n}}} means ◊trans{exactly one of ◊var{X}◊sub{◊var{i}}}; ◊trans{◊var{X}◊sub{1} but not … but not ◊var{X}◊sub{◊var{n}}} means ◊trans{◊var{X}◊sub{1} but not any later ◊var{X}◊sub{◊var{i}}}. All pronominal clitics occur at the end of the coordinated noun phrase. In ‘but not’-coordinated phrases, there can be only one pronominal clitic (namely, the one representing the first item).

If the coordinands are quotatives, then the clitics are placed after the quoted items themselves, immediately after the ◊i{þos}, and only one quotative particle is used.

◊subsection[#:id "arithmetic-conjunctions"]{Arithmetic conjunctions}

Arithmetic operations are expressed using independent particles in infix position.

◊table/x[#:options (table-options #:caption "Arithmetic conjunctions in Ŋarâþ Crîþ.") #:id "arithmetic-conjunctions"]{
  Precedence & Operation & Conjunction
  2 (R) & Exponentiation &
  1 & Multiplication &
    & Division &
  0 & Addition & ◊nc{tes}
    & Subtraction & ◊nc{gjan}
}

While these occur between nouns, a nominalized short numeral occurring anywhere other than the last word of an arithmetic expression can occur as a bare numeral, not requiring compounding with ◊l1{mener}.

◊section[#:id "quotatives"]{Quotatives}

Quotatives are formed by wrapping the quoted material in quotation marks, followed by a particle depending on case and directness, forming a noun phrase.

Direct quotatives are used for verbatim speech. Indirect quotatives indicate some kind of paraphrasing and do not necessarily represent what someone has said. There exists a separate set of switch-reference indirect quotatives, which are used when (1) both the outer and inner clauses have a third-person subject and (2) the subjects do not corefer.

◊table/x[#:options (table-options #:caption "Quotative particles in Ŋarâþ Crîþ.") #:id "quotative-table"]{
  Case & Direct & Indirect & Indirect-SR
  Nominative & ◊nc{ner} & ◊nc{ler} & n/a
  Accusative & ◊nc{ne} & ◊nc{reþ} & ◊nc{rast}
  Dative & ◊nc{nes} & ◊nc{res} & ◊nc{rens}
  Genitive & ◊nc{nel} & ◊nc{ril} & ◊nc{rels}
  Locative & ◊nc{nos} & ◊nc{raþ} & ◊nc{reþþe}
  Instrumental & ◊nc{noca} & ◊nc{racþa} & ◊nc{racþaf}
  Abessive & ◊nc{noþa} & ◊nc{raþa} & ◊nc{raþas}
  Semblative & ◊nc{nit} & ◊nc{ret} & ◊nc{ret}
}

Direct quotatives necessarily inherit the personal and temporal deixis of the one who said its contents. The personal deixis does not shift inside an indirect quotative, but the temporal deixis shifts to that of the outer event:

◊gloss/x{
  ◊glfree{◊nc{#flirora «eltan cþasce» reþ maraþ.}}
  ◊gla{#fliror-a «elt-an cþasc-e» reþ mar-a-þ.}
  ◊glb{(name)-%nom.%sg salmon-%acc.%co cook-3%sg %quot.%acc.%ind say-3%sg-%past}
  ◊glfree{#flirora₁ said that they₁ ◊i{(sg)} were cooking salmon.}
}

◊gloss/x{
  ◊glfree{◊nc{#flirora «eltan cþasce» rast maraþ.}}
  ◊gla{#fliror-a «elt-an cþasc-e» rast mar-a-þ.}
  ◊glb{(name)-%nom.%sg salmon-%acc.%co cook-3%sg %quot.%acc.%ind.%sr say-3%sg-%past}
  ◊glfree{#flirora₁ said that they₂ ◊i{(sg)} were cooking salmon.}
}

A qutation can be continued later using the particle ◊l1{e} followed by the continuation.

◊section[#:id "names"]{Names}

The most salient types of names – namely, personal and place names – have markers, although other types of names, such as titles of works, do not.

Names can manifest in two ways: as ◊term{unqualified names} or as ◊term{qualified names}. Unqualified names refer to names that stand alone as full noun phrases.

A qualified name, on the other hand, is a name accompanied by the type of entity it refers to as done in Toki Pona. In such a name, the common noun comes first and is suffixed with a marker or punctuation correlated to the name:

◊items{
  ◊item{◊l1{#} for a given name}
  ◊item{◊l1{+} for a surname}
  ◊item{◊l1{+#} for a surname and a given name}
  ◊item{◊l1{@} for a place name}
  ◊item{◊l1{«»} for a work title}
}

In particular, the common noun is never suffixed with a ◊i{nef}, and these postfixes do not affect the letter sum of the noun.

Qualified names are used in the following situations:

◊items{
  ◊item{As part of the conventional name for geographic features, such as ◊l1{vlêcadir@ @elþana} ◊trans{the Elþana archipelago}.}
  ◊item{To ascribe a title to a ◊xref["nouns.html" "names-personal" "Subsection"]{personal name}.}
  ◊item{To emphasize or disambiguate the type of entity that is being referred to.}
  ◊item{To refer to an entity by a name that is not phonotactically or morphologically adapted to Ŋarâþ Crîþ.}
}

◊subsection[#:id "names-personal"]{Personal names}

Ŋarâþ Crîþ recognizes two parts of personal names: the surname and the given name, in that order. Surnames are marked with either a ◊i{tor}, ◊l1{+} or a ◊i{njor}, ◊l1{+*}. The ◊i{tor} is used for surnames passed by native conventions (i.e. from parent to child within the same gender), while the ◊i{njor} marks a surname passed using non-native conventions. The presence of a ◊i{njor} is correlated but does not always coincide with that of a ◊i{nef} (◊l1{*}): ‘foreign’ surnames can be passed by ‘native’ conventions; in the opposite direction, a ◊i{njor} might be present without a ◊i{nef} in a calqued surname, as well as in a few native surnames that are traditionally passed by a non-native convention.

A given name is marked with a ◊i{carþ}, ◊l1{#}. If a person is known by a single name only, then the name is treated as a given name.

Both the surname and the given name are declined nouns.

A person is addressed or mentioned using the surname, given name, or both, with the surname being more formal than the given name. Nonetheless, the choice of whether to address someone by their surname or by their given name can also depend on other factors such as convenience of pronunciation or distinguishing between multiple people with the same given name or surname.

The use of titles is limited: there is no direct equivalent to ◊i{Mr.} or ◊i{Ms.}. Instead, titles are used merely to describe the role of the person. Notably, (1) they are always nouns, (2) they are never used in the vocative, (3) they are used with the person’s name when the person in question is introduced, and (4) the use of the title alone in later mentions over the name carries no social connotation otherwise. The closest equivalent to ◊i{sir} or ◊i{madam} is ◊l1{cercerin}, meaning ◊trans{stranger}, which is used to address someone whose name is not known.

A title can be used as a part of a qualified name:

◊gloss/x{
  ◊glfree{◊nc{dosareþ+# +astova #ageþne «tfaren inora sarałêns arnenden cenventês ndogenħal» reþ maraþ.}}
  ◊gla{dosareð-∅+# +astov-a #ageþn-e «tfar-en inor-a sarał-êns arnend-en cenvent-ês n\dogenħ-al» reþ mar-a-þ.}
  ◊glb{teacher-%nom.%sg (surname)-%nom.%sg (given)-%nom.%sg money-%gen.%sg void-%nom.%sg school-%acc.%sg music-%gen.%co course-%dat.%co %pfv\%caus-twist-3%sg.%inv %quot.%acc.%ind say-3%sg-%past}
  ◊glfree{+astova #ageþne, a teacher, stated that the lack of money has caused the school to reduce funding for music classes.}
}

Because titles are used with names only to introduce a person, ◊l1{+#} is the most common choice for the postmarker.

◊subsection[#:id "names-place"]{Place names}

The name of any kind of place is marked with an ◊i{es}, ◊l1{@}.

◊subsection[#:id "names-language"]{Language names}

Languaeg names are zero compounds in which the first word is ◊l1{ŋarâþ} ◊trans{language}. The second word is frequently the name of a place associated with the language, as in ◊l1{ŋarâþ @asoren}, or an uninflected word. The major exception is ◊l1{ŋarâþ crîþ}, which uses the common noun ◊l1{crîþ} ◊trans{forest}.

◊subsection[#:id "names-work"]{Titles of works}
