#lang pollen

◊define-meta[title]{Morphological paradigms}

Each ◊term{lexical entry} belongs to a ◊term{lexical category} (nouns, verbs, &c.). Each lexical category is associated with one or more ◊term{paradigms}, each of which has

◊items{
    ◊item{a name,}
    ◊item{a ◊term{schema}, which is a named tuple of ◊term{grammatical categories},}
    ◊item{and a ◊term{mapping} that computes a set of strings (usually one) from an instance of the schema and the lexical entry.}
}

For instance, the lexical category of nouns contains one paradigm named ◊var{default} with a schema of ◊${(\text{case}: \text{Case}, \text{number}: \text{Nnumber})}.

An ◊term{instance} of a schema is a named tuple, each of whose elements has a name identical to that of the corresponding element in the schema and a value in the set of values of the respective grammatical category. For example, ◊${(\text{case}: \text{nominative}, \text{number}: \text{dual})} is an instance of the schema ◊${(\text{case}: \text{Case}, \text{number}: \text{Nnumber})}.

Given a lexical category ◊${C} consisting of ◊${k} paradigms ◊${P_0 = (n_0, \sigma_0, f_0), \ldots, P_{k - 1}}, a lexical entry ◊${L} with a lexical category of ◊${C} consists of ◊${k} ◊term{paradigm applications} ◊${A_0 = (n_0, s_0), \ldots, A_{k - 1}}, where ◊${s_i = f_i(\cdot, L)} is a function mapping instances of ◊${\sigma_i} to a set of strings. For the noun ◊l1{ŋarâþ}, the ◊var{default} paradigm application maps ◊${(\text{case}: \text{nominative}, \text{number}: \text{dual})} to ◊${\{\text{ŋarac}\}}.

How exactly the mappings of paradigms are defined in Ŋarâþ Crîþ is described in ◊xref/c["overview.html" #:type 'inline]{Morphophonology}.

◊section{Grammatical categories}

A ◊term{grammatical category} consists of a set of two or more values. Grammatical categories are usually used as inputs to paradigm mappings but occasionally also appear as intrinsic properties of lexical entries.

◊subsection{Number}

Ŋarâþ Crîþ has separate ideas of ◊term{nominal} and ◊term{verbal numbers}, which are often abbreviated as ◊term{nnumbers} and as ◊term{vnumbers}. Nominal numbers are used in the declension of nouns, while verbal numbers are used for agreement affixes in verbs and relationals. There are four verbal numbers: ◊term{singular}, ◊term{dual}, ◊term{plural}, and ◊term{generic}.

There are five nominal numbers: ◊term{direct}, ◊term{dual}, ◊term{plural}, ◊term{singulative}, and ◊term{generic}. No noun can decline for all five nnumbers; instead, each noun is limited to a subset of these according to its ◊term{clareþ}, which governs how nominal numbers map to verbal numbers:

◊items{
  ◊item{◊term{Singular} nouns allow the direct (as a singular), the dual, the plural, and the generic.}
  ◊item{◊term{Collective} nouns allow the direct (as a collective, usually corresponding to the plural vnumber, but sometimes to the singular vnumber when treated as a singular mass instead of a collection of individuals), the singulative (corresponding to singular vnumber), and the generic. The following nouns tend to be collective: ◊;
  ◊items{
    ◊item{Objects that tend to be found in groups}
    ◊item{Some plants, including all trees and flowers}
    ◊item{Small animals}
    ◊item{Diminuitive nouns}
  }}
  ◊item{◊term{Mass} nouns allow only the direct (corresponding to singular vnumber) and the generic.}
}

Generic number is used to mean “X in general” or “X as a concept”. It is used on noun phrases that do not refer to a specific referent or referents:

◊gloss/x{
  ◊glfree{◊nc{mjoþelca glefteþ @asares tferamotras menu.}}
  ◊gla{mjoþ-elca glev-þeþ @asar-es tfera-motr-as men-u.}
  ◊glb{gender-%inst.%sg discrimination-%dat.%gc Asoren-%loc.%sg often-%ddt-%loc.%sg see-3%sg}
  ◊glfree{Discrimination based on gender is being seen increasingly often in Asoren.}
  ◊glfree{(metions discrimination in general)}
}

◊gloss/x{
  ◊glfree{◊nc{«ranaren gleverþ plence» reþ teno gcarþeþ.}}
  ◊gla{«ran-ar-en glev-erþ plenc-e» reþ ten-o g\carþ-e-þ.}
  ◊glb{3%pl.%hum-toward-%adn discrimination-%nom.%di illegal-%sg %quot.%acc.%ind court-%nom.%sg %pfv\declare-3%sg-%pst}
  ◊glfree{The court ruled that the discimination against them violated the law.}
  ◊glfree{(metions a specific case of discrimination)}
}

◊gloss/x{
  ◊glfree{◊nc{telu tovrełen mênču.}}
  ◊gla{tel-u tovr-ełen mênč-u.}
  ◊glb{fish-%nom.%gc flower-%acc.%gc eat-3%gc}
  ◊glfree{Fish eat flowers.}
  ◊glfree{(general truth, so “fish” would be in the generic and “flowers” would be in the generic)}
}

◊gloss/x{
  ◊glfree{◊nc{telos tovrełen mênči.}}
  ◊gla{tel-os tovr-ełen mênč-i.}
  ◊glb{fish-%nom.%co flower-%acc.%gc eat-3%pl}
  ◊glfree{The fish eat flowers.}
  ◊glfree{(referring to a particular group of fish, but no particular group of flowers, perhaps indicating a habitual action)}
}

◊gloss/x{
  ◊glfree{◊nc{telos tovran mênči.}}
  ◊gla{tel-os tovr-an mênč-i.}
  ◊glb{fish-%nom.%co flower-%acc.%co eat-3%pl}
  ◊glfree{The fish eat (some/the) flowers.}
  ◊glfree{(referring to particular groups of fish and of flowers)}
}

◊gloss/x{
  ◊glfree{◊nc{telu vônos respos tovran vilhenrotomin mênču.}}
  ◊gla{tel-u vôn-os resp-os tovr-an vil-henroto-min mênč-u.}
  ◊glb{fish-%nom.%gc norm-%loc.%di lifetime-%loc.%sg flower-%acc.%sg one-16².%approx-%ctr.small_plant eat-3%gc}
  ◊glfree{A fish will usually eat about 256 flowers during its lifespan.}
  ◊glfree{(“Fish” is generic, as this sentence is not referring to a particular fish but rather an idealized individual reflecting the average. “Flowers” is plural and “lifespan” is singular because they are definite relative to the subject, even though the subject is generic.)}
}

◊gloss/x{
  ◊glfree{◊nc{elêþ šileħe.}}
  ◊gla{el-êþ šile-ħe.}
  ◊glb{sun-%nom.%sg shine-3%sg}
  ◊glfree{The sun shines.}
  ◊glfree[#:prefix "Or: "]{The sun is shining.}
  ◊glfree{(The former interpretation states a general fact, but there is only one sun, so ◊l1{elêþ} still takes the singular number.)}
}

◊; TODO: examples actually in ŊCv9
◊; ◊items{
◊;   ◊item{◊em{Charlie fishes for trout every week.} – “Charlie” is singular; “trout” and “week” are generic}
◊;   ◊item{◊em{Charlie is fishing in that river.} – uses the singular}
◊;   ◊item{◊em{All ravens are black.} – also a general truth, so “raven” would be in the generic number. A speaker could reasonably say this, even if they have not seen every raven in existence.}
◊;   ◊item{◊em{All of the ravens landed on the fence.} – “all” is used with a partitive meaning, referring to every individual in some contextually relevant set of ravens, and the statement is made from observation, so this sentence would use the singular (as customary with a noun modified by “all” or “any”)}
◊;   ◊item{◊em{I abhor strawberries.} – “strawberries” would be in the generic, as the speaker is claiming to dislike strawberries in general}
◊;   ◊item{◊em{I abhor these strawberries because they’re too sour.} – “strawberries” would be in the plural here, referring to some strawberries in particular}
◊;   ◊item{◊em{I’m looking for the manager, whoever that may be.} – although the speaker does not know who the manager is, they expect that there exists someone who is the manager, so “manager” would be in the singular}
◊;   ◊item{◊em{Think of a word, any word.} – again, the speaker does not know which word the listener chose, but they expect that a particular word will be chosen, so “word” is in the singular}
◊; }

The generic number is also used for noun phrases that do not necessarily have a referent:

◊gloss/x{
  ◊glfree{◊nc{nemir nirþeftês es veła.}}
  ◊gla{nem-ir nirþev-tês es veła.}
  ◊glb{apple-%nom.%sg basket-%dat.%sg inside exist-3%sg}
  ◊glfree{(A/The) apple is in the basket.}
}

◊gloss/x{
  ◊glfree{◊nc{nemir nirþeftês es ceła.}}
  ◊gla{nem-ir nirþev-tês es ceła.}
  ◊glb{apple-%nom.%sg basket-%dat.%sg inside not_exist-3%sg}
  ◊glfree{The apple is not in the basket.}
}

◊gloss/x{
  ◊glfree{◊nc{nefþes nirþeftês es cir.}}
  ◊gla{nem-þes nirþev-tês es cir.}
  ◊glb{apple-%nom.%gc basket-%dat.%sg inside not_exist-3%gc}
  ◊glfree{There is no apple in the basket.}
}

◊gloss/x{
  ◊glfree{◊nc{ša nefþes nirþeftês es ver?}}
  ◊gla{ša nem-þes nirþev-tês es ver?}
  ◊glb{%int apple-%nom.%gc basket-%dat.%sg inside exist-3%gc}
  ◊glfree{Is there an apple in the basket?}
}

When a noun in the genitive case is used for description, it usually takes the direct number, not the generic.

A third-person pronoun in the generic number refers to a class of objects or people in general and can be translated as the English impersonal pronoun ◊i{one}. First-person and second-person generic pronouns act similarly, except that they include first-person or second-person referents. The use of generic-number pronouns is most notable in imperatives:

◊gloss/x{
  ◊glfree{◊nc{le celcols es coclat garu.}}
  ◊gla{le celc-ols es cocl-at gar-u.}
  ◊glb{%imp building-%dat.%sg inside run-%inf refrain_from-3%gc}
  ◊glfree{No running in the building. (least direct)}
}

◊gloss/x{
  ◊glfree{◊nc{le celcols es coclat garaf.}}
  ◊gla{le celc-ols es cocl-at gar-af.}
  ◊glb{%imp building-%dat.%sg inside run-%inf refrain_from-2%gc}
  ◊glfree{Don’t run in the building.}
}

◊gloss/x{
  ◊glfree{◊nc{le celcols es coclat garas.}}
  ◊gla{le celc-ols es cocl-at gar-as.}
  ◊glb{%imp building-%dat.%sg inside run-%inf refrain_from-2%sg}
  ◊glfree{Don’t run in the building. (aimed specifically at one person; most direct)}
}

◊subsection{Case}

Ŋarâþ Crîþ has eight cases (◊xref/l["cases" "Table"]). The nominative, accusative, dative, and genitive cases are considered ◊term{core cases}. In general, the first three of these are used for arguments to verbs, the genitive case for adnominal adjuncts, and the other four cases for adnominal or adverbial adjuncts.

◊table/x[#:options (table-options
    #:caption "The cases of Ŋarâþ Crîþ."
    #:first-col-header? #f
    #:colgroup '(c d)
  )
  #:id "cases"]{
  Name & Use
  Nominative & ◊;
      The subject of the clause. The citation form of a noun is the nominative singular.
  Accusative & ◊;
      The “direct object” of the clause.
  Dative & ◊;
      The “indirect object” of the clause. Also used as a vocative.
  Genitive & ◊;
      Shows such things as possession, composition, description, or apposition.
  Locative & ◊;
      Indicates the location or time of an object or an action: ◊trans{at X}, ◊trans{on X}, ◊trans{in X}. On a nominalized verb, this case can be translated as ◊trans{when}, ◊trans{where}, or ◊trans{as long as}.
  Instrumental & ◊;
      Indicates the comitative or the instrumental: ◊trans{with X}.
  Abessive & ◊;
      The negation of the instrumental: ◊trans{without X}. In the dual number, ◊trans{with only one X}.
  Semblative & ◊;
      ◊trans{like X in behavior}. On a nominalized verb, ◊trans{such that}, ◊trans{as though}, or ◊trans{to the point that} (although ◊l1{dôm} is used more often for the last sense). Not used for semblance in appearance.
}

◊; TODO: examples for each case

◊subsection{Gender}

Ŋarâþ Crîþ has three genders or noun classes: ◊term{celestial}, ◊term{terrestrial}, and ◊term{human}. Gender is an intrinsic part of each noun; that is, the gender of each noun is fixed. The unmarked gender is celestial, in that noun phrases are assumed to be celestial unless otherwise specified, and the celestial gender is used for an object whose gender is otherwise unknown (such as in participles of headless relative clauses).

Many words agree with noun phrases in gender when they are inflected, including:

◊items{
    ◊item{third-person possessive clitics}
    ◊item{demonstrative determiners and pronouns}
    ◊item{finite verb forms in the gender of a third-person accusative or dative argument}
    ◊item{verb participles in genera I and III}
    ◊item{relationals in the gender of a third-person object}
    ◊item{long numerals from 1 to 6}
}

◊subsection{Tense and aspect}

Ŋarâþ Crîþ has two tenses: ◊term{present} (more precisely, ◊term{nonpast}) and ◊term{past}. In addition to the present, the present tense covers the future as well as the immediate past:

◊gloss/x{
  ◊glfree{◊nc{enven sodas mêvan dtêmos.}}
  ◊gla{env-en sod-as mêv-an d\têm-os.}
  ◊glb{day-%gen.%sg next-%loc.%sg rain-%acc.%co %pfv\precipitate-3%gc.%inv}
  ◊glfree{It will rain tomorrow.}
}

◊gloss/x{
  ◊glfree{◊nc{ondelt on cerels fîðit gatregie.}}
  ◊gla{ond-elt on cer-els fîð-it g\atreg-ie.}
  ◊glb{now-%loc.%sg %inf.acc house-%dat.%sg sweep-%inf %pfv\finish-1%sg.%pfv}
  ◊glfree{I just finished sweeping my house.}
}

The present tense is also used as a narrative present: in stories, the past tense is used only for events that had happened before the current point.

Ŋarâþ Crîþ has two aspects: ◊term{imperfective} and ◊term{perfective}. The imperfective aspect is used for ongoing (such as progressive or habitual) actions.

◊subsection{Mood}

Ŋarâþ Crîþ inflects for mood only on ◊xref["verbs.html" "nominalized" "Section"]{nominalized forms of a verb} in certain cases. The mood distinction encodes a difference in modality in the dative and semblative cases only. In the locative, instrumental, and abessive, it encodes a distinction between adverbial and adnominal forms.

◊subsection{Attachment}

◊term{Attachment} refers to whether an adjunct is attached to a noun phrase or to a verb phrase. This is marked only on ◊xref["relationals.html" "attachment" "Section"]{relationals}, as well as nominalized forms of a verb in cases that use mood distinctions to signal attachment.

◊section{Conjunct forms}

Each lexical element that can appear as the first element of a ◊xref["../lexicon/derivations.html" "compounding" "Section"]{compound word} has two ◊term{conjunct forms}. The ◊term{consonantal conjunct form} is a sequence of nonterminal syllables and is used before a non-vowel-initial word; the ◊term{vocalic conjunct form} is a stem that is used before a vowel-initial word.

◊; For most nouns, this is identical to the lemma, but some nouns, such as those whose lemma forms end in complex codas, have distinct conjunct forms.

◊; A verb in a closed compound has its ◊l0{-at} or ◊l0{-it} ending replaced with ◊l0{-i}.
