#lang pollen

◊(require pollen/setup)

◊define-meta[title]{Morphophonology}

Inflected forms of a word are built from two or more ◊term{components}, which include ◊term{constants} and ◊term{variables}. Constants stay the same within a given form of a given paradigm, regardless of the noun within that paradigm to be declined, and are notated using usual Ŋarâþ Crîþ orthography. Variables depend on the noun being declined and can be divided into ◊term{stems}, ◊term{themes}, and ◊xref["overview.html" "rolls" "Section "]{◊term{rolls}}.

A ◊term{stem}, in a way, is one of the essences of a word. Each inflected form of a word contains exactly one instance of a stem. In most cases, a stem consists of one or more syllables followed by an onset that does not contain a lenited consonant, but it can otherwise be arbitrary. In our notation, stems are denoted using capital Latin letters.

A ◊term{theme} is a variable that is short (almost always one letter long). Unlike stems, themes are limited to a predefined number of options. Themes in noun paradigms can be classified into ◊term{thematic consonants} and ◊term{thematic vowels}. In our notation, themes are denoted using capital Greek letters.

Each regular lexical entry has a set of ◊term{principal components}, which are the set of components needed to determine all of its inflected forms. The ◊term{principal parts} are a set of inflected forms of the entry that collectively give all of its principal components. The principal components of an entry include all of its stems and themes but no rolls, but in practice, some principal parts might be included solely for containing rolls that are otherwise inconvenient to derive.

A theme or a roll may receive a ◊term{transformation}, which is a function from themes to themes or from rolls to rolls. In our notation, transformations are shown in superscript to the right of the variable to be transformed. The result of a transformation is sometimes called a ◊term{derivative}.

The following basic transformations are defined:

◊items{
  ◊item{◊nc{◊ẋ{◊var{λ}/◊var{μ}}}: Replace the vowels in ◊var{λ} with the corresponding ones in ◊var{μ}.}
  ◊item{◊nc{◊ẋ{◊var{λ}×◊var{μ}}}: Apply an ◊term{exchange transformation} on a vowel: ◊var{λ} → ◊var{μ}; ◊var{μ} → ◊var{λ}.}
  ◊item{◊nc{◊ẋ{‹◊var{λ}/◊var{μ}}}: Replace this vowel with ◊var{μ} when the previous vowel (if any) is ◊var{λ}.}
  ◊item{◊nc{◊ẋ{~◊var{σ}}}: Apply the transformation ◊var{σ} over both normal and hatted vowels.}
  ◊item{◊nc{◊ẋ{◊var{σ} · ◊var{τ}}}: Apply the transformations ◊var{σ} and ◊var{τ}, in that order.}
}

Other transformations can be expressed in terms of these:

◊items{
  ◊item{◊nc{◊ẋ{◊ẋpl}} = ◊nc{◊ẋ{~aoe/oei}}}
  ◊item{◊nc{◊ẋ{◊ẋgen}} = ◊nc{◊ẋ{~aoei/eeie}}}
  ◊item{◊nc{◊ẋ{◊ẋgenp}} = ◊nc{◊ẋ{~oi/ae}}}
  ◊item{◊nc{◊ẋ{◊ẋfr}} = ◊nc{◊ẋ{~o/a}}}
  ◊item{◊nc{◊ẋ{◊ẋfr2}} = ◊nc{◊ẋ{~o/e}}}
  ◊item{◊nc{◊ẋ{◊ẋfr3}} = ◊nc{◊ẋ{~oi/ee}}}
  ◊item{◊nc{◊ẋ{◊ẋv2}} = ◊nc{◊ẋ{~ai/ea}}}
  ◊item{◊nc{◊ẋ{◊ẋv3}} = ◊nc{◊ẋ{~ai/io}}}
  ◊item{◊nc{◊ẋ{◊ẋht}} = ◊nc{◊ẋ{aeio/âêîô}}}
◊;   ◊item{◊nc{◊ẋ{◊ẋdh}} = ◊nc{◊ẋ{âêîô/aeio}}}
◊;   ◊item{◊nc{◊ẋ{◊ẋng}} = ◊nc{◊ẋ{~aeio/o[jo][jo]o}}}
}

Additionally, inflection often uses ◊xref["../phonology/layer0.html" "stem-fusion" "Section"]{stem fusion}, which is notated by a superscript of either the fusion consonant or ◊nc{ε}.

When the inflected form is given, the concatenation operator is understood to be inserted between each variable and its neighboring components. The type of each component will usually be clear from the context.

◊section[#:id "rolls"]{Rolls}

Like themes, a ◊term{roll} is a short variable, but a roll is dependent on the letter sum of one of the word’s inflected forms. In our notation, rolls are denoted using die faces: ◊nc{◊ḋ[0]} (‘ace’), ◊nc{◊ḋ[1]} (‘deuce’), ◊nc{◊ḋ[2]} (‘trey’), ◊nc{◊ḋ[3]} (‘cater’), and ◊nc{◊ḋ[4]} (‘cinque’). (Fortunately, we’ve yet to find a need for a ◊i{sice}.)

A shorthand is used to specify the value of a roll. The notation ◊i{◊l0{◊var{x}◊sub{0} ◊var{x}◊sub{1} … ◊var{x}◊sub{◊var{n}−1}} « ◊var{y}} is used to mean that the letter sum of ◊var{y} should be taken modulo ◊var{n} and used as an index into the list. Sometimes, this will be followed by ‘increment until’ or ‘decrement until’ followed by a condition; in this case, the index should be incremented or decremented (wrapping around if necessary) until the condition holds.

For instance, ◊i{◊nc{◊ḋ[0]} = ◊l0{e a i a i e} « ◊sc{nom.di}} means that the letter sum of the nominative direct form of a noun should be calculated. If this is 0, 6, 12, 18, or so on, then ◊nc{◊ḋ[0]} is ◊l0{e}; if it is 1, 7, 13, 19, or so on, then ◊nc{◊ḋ[0]} is ◊l0{a}; and so forth.

◊section[#:id "phi"]{Phi consonants}

The ◊term{phi consonant} of a stem ◊nc{◊ṡ{X}}, denoted by ◊nc{◊ṫg{X}}, is a consonant used in some generic forms. It can be either ◊l0{f} and ◊l0{ł} according to the following rules:

◊enum{
  ◊item{If the final onset of ◊nc{◊ṡ{X}} is not preceded by ◊l0{-l} or ◊l0{ł}, and that onset contains any consonants whose base letter is any of ◊l0{p f v m g d ð ħ}, then ◊nc{◊ṫg{X}} is ◊l0{ł}.}
  ◊item{If any onset or coda in ◊nc{◊ṫg{X}} other than the final onset contains any consonants whose base letter is any of ◊l0{p f v m}, then ◊nc{◊ṫg{X}} is ◊l0{ł}.}
  ◊item{Otherwise, ◊nc{◊ṫg{X}} is ◊l0{f}.}
}

◊section[#:id "mutations"]{Mutations}

Ŋarâþ Crîþ has two kinds of initial mutations: ◊term{lenition} and ◊term{eclipsis}. Neither kind of mutation has any effect on plosive–fricative onsets or any of ◊l0{r l n ŋ ħ}.

Lenition tends to turn plosives into fricatives and is indicated with a middle dot ◊l0{·} after the consonant affected. In particular, it affects ◊l0{p t d č c g m f v ð}. (See ◊xref["../phonology/layer2s.html" "layer-2" "Section" #:type 'inline]{Layer 2s} for pronunciation details.) Partial lenition does not affect any of ◊l0{f v ð}; that is, it does not lenite consonants that would become silent. Unless otherwise qualified, lenition refers to total lenition, which affects ◊l0{f v ð}.

In a word containing ◊l0{&}, both instances of the reduplicated prefix are lenited. For example, ◊l1{&d·enfo} can be pronounced as ◊l3{ðeðenfo} but not as *◊l3{ðedenfo}.

Lenition occurs in the following environments:

◊items{
  ◊item{On the stem in abessive forms of third- or sixth-declension nouns}
  ◊item{On a noun modified by ◊l1{šinen} or ◊l1{nemen} when used as determiners, if that noun is not a form of ◊l1{ðên}}
  ◊item{Partially, on a noun modified by ◊l1{ruf} not immediately following it}
  ◊item{Partially, on a noun modified by ◊l1{mê} immediately preceding it}
  ◊item{On the first-person dual or plural present perfective forms of a resinous verb}
  ◊item{Partially, on the first- and second-person generic past imperfective forms of a resinous verb}
  ◊item{On a terrestrial noun modified by a participle-form verb belonging to a Type I genus}
  ◊item{To a dative-case nominalized verb phrase as explained in ◊xref["verbs.html" "nominalized" "Section" #:type 'inline]{Nominalized forms}}
  ◊item{Partially, on a verb when receiving the comparative prefixes ◊l0{mir-} or ◊l0{ła-}}
  ◊item{On a classifier attached to the numeral ◊l1{ces} or any numeral ending in ◊l1{ħas} or ◊l1{sreþas}}
  ◊item{On the second item of a compound noun, if it is neither terrestrial nor a form of ◊l1{vês}}
  ◊item{On a verb with the cessative prefix ◊l0{car-} or the terminative prefix ◊l0{er-}}
}

Eclipsis tends to add voice to voiceless consonants and change voiced stops into nasals. It is indicated by prefixing a consonant: ◊l0{t d c g f þ ł} become ◊l0{dt nd gc ŋg vf ðþ lł}, respectively. ◊l0{p} becomes ◊l0{vp} before any of ◊l0{i e u î ê} and ◊l0{mp} elsewhere. If a word starts with a vowel, then it is eclipsed by prefixing ◊l0{g}.

In a word containing ◊l0{&}, only the first instance of the reduplicated prefix is eclipsed. For example, ◊l1{n&denfin} can be pronounced as ◊l3{nedenfin} but not as *◊l3{nenenfin}.

Eclipsis occurs in the following environments:

◊items{
  ◊item{On the genitive dual, plural, and singulative forms of nouns}
  ◊item{On a noun modified by ◊l1{lê} or ◊l1{tê} immediately preceding it}
  ◊item{On a noun modified by ◊l1{dân}}
  ◊item{On a finite form of a vitreous verb or relational with perfective aspect}
  ◊item{On a finite form of a resinous verb with perfective aspect, unless it is a present-tense form and either a first-person dual or plural form or a first- or second-person generic form}
  ◊item{To a locative, instrumental, or abessive-case nominalized verb phrase that is not an object of a modifying relational, as explained in ◊xref["verbs.html" "nominalized" "Section" #:type 'inline]{Nominalized forms}}
  ◊item{On a short numeral modified by ◊l1{ceþe}}
}

Lenition can happen on any syllabic onset of a word, but eclipsis is limited to word-initial positions.

In this documentation, lenition is sometimes marked with an empty circle ◊|sei|, and eclipsis with an filled circle ◊|uru|. Partial lenition is marked with an empty triangle ◊|sei2|.
