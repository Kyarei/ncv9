#lang pollen

◊define-meta[title]{Welcome}

◊(require srfi/19)

◊list['blockquote '((class "title-quote"))]{
    ◊cenvos{ŋarâns crîþon sicarvlermiþe envał ŋarâþ crîþ sidranleþo envał.}

    ◊nc{ŋarâns crîþon sicarvlermiþe envał ŋarâþ crîþ sidranleþo envał.}

    ◊i{The day that Ŋarâþ Crîþ stops changing is the day Ŋarâþ Crîþ dies.}
}

Welcome to the website for version 9 of Ŋarâþ Crîþ, a constructed language created by +merlan #flirora.

You can start by ◊link["grammar/index.html"]{reading the grammar}. Once the language becomes more stable, I might write a friendlier tutorial.

A ◊link["https://discord.gg/jkkaMNCeyu"]{Discord server} and a ◊link["https://www.reddit.com/r/NecarassoCryssesa/"]{subreddit} for Ŋarâþ Crîþ are now available.

Last updated on: ◊render-date*[(current-date)]
