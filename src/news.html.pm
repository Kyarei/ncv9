#lang pollen

◊define-meta[title]{News}

◊(require srfi/19)

This page lists major changes to the Ŋarâþ Crîþ v9 website since ◊render-date{2023-02-15}. For more detailed changes, or for changes earlier than then, consult the ◊link["https://gitlab.com/ncv9/ncv9/-/commits/master"]{commit log for the website’s repository}.

This version of the website was built on ◊render-date*[(current-date)].

◊section[#:id "2023-02-15"]{◊render-date{2023-02-15}}

Added this page.

I’ve recently started work on a new book ◊link["iit/index.html"]{◊cite{On invertible inflection theory}}, which uses a lot of math notation. I’ve wanted to switch to outputting MathML for a long time, which is feasible now that ◊link["https://mathml.igalia.com/news/2023/01/10/mathml-in-chromium-project-is-completed/"]{Chromium has added support for it}. Unfortunately, ◊link["https://katex.org/"]{KaTeX} has had a lot of problems with its MathML output. For that reason, I’ve decided to switch to ◊link["https://temml.org/index.html"]{Temml} for math rendering.
