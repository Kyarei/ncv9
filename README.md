## ncv9

Source for the [Ŋarâþ Crîþ v9 website].

### Credits

* Credit to [Pollen] (Matthew Butterick) for making this website possible.
* Also uses [pollen-count] (Malcolm Still).
* [katex]
* [sql-js]

#### Fonts

* [Libertinus font family] (OFL 1.1)

[Ŋarâþ Crîþ v9 website]: https://ncv9.flirora.xyz/
[Pollen]: https://github.com/mbutterick/pollen
[pollen-count]: https://github.com/malcolmstill/pollen-count
[Libertinus font family]: https://github.com/alerque/libertinus
[katex]: https://github.com/KaTeX/KaTeX
[sql-js]: https://github.com/sql-js/sql.js
